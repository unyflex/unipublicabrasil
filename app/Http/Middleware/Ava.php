<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;

class Ava
{
    
   
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()){
           
            if(Auth::user()->power==3){
        return $next($request);
    }
        }
        return $request->wantsJson()

            ? new JsonResponse([], 204)

            : redirect('ava/login');
    }
 
}
