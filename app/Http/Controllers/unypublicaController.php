<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Category;
use App\Models\Classes;
use App\Models\Panel;
use App\Models\Depositions;
use App\Models\Hotel;
use App\Models\Banners;
use App\Models\Package;
use App\Models\certificate;
use App\Models\Photos;
use App\Models\Galery;
use App\Models\CategoryCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class unypublicaController extends Controller
{
    public function index()
    { 
        $hoje=  date('Y-m-d');
        $presenciais = Classes::where('start_date','>', $hoje)
        ->where('unyflex',0)->with('panels')->get();
        $banners= Banners::where('status', 'able')->get();
        $banner_count = Banners::where('status', 'able')->count();
        $online = Classes::where('unyflex',1)->with('panels')->get();
        $depositions = Depositions::where('status', 'able')->get();
      //  dd($depositions);
                            
      return view('index', [
            
            'presenciais' => $presenciais,
            'onlines' => $online,
            'banners' => $banners,
            'banner_count' => $banner_count,
            'depositions' => $depositions
        ]);
    }

    public function curso($slug)
    { 

        $classes=Classes::where('slug', $slug)->first();
        $class = Classes::where('slug', $slug)->with('panels')->first();
        $classes_id=$classes->id;
        $panels=Panel::where('classes_id', $classes_id)->with('teacher')->get();
        $teachers=Panel::where('classes_id', $classes_id)->with('teacher')->get();
       
        $hoteis=Hotel::where('cidade', 'Curitiba - PR')->get();
        $hoteisp=Hotel::where('cidade', 'Curitiba - PR')->with('packages')->first();
       
    // dd($hoteisp->packages);             
      return view('curso', [
            
            'class' => $classes,
            'panels'=> $panels,
            'teachers'=> $teachers,
            'hoteis'=> $hoteis,
            'packages'=> $hoteisp->packages
            
        ]);
    }



    public function agendados()
    { 
        $hoje=  date('Y-m-d');
        $agendados = Classes::where('start_date','>', $hoje)
        ->where('unyflex',0)
        ->with('panels')->get();
        $banners= Banners::where('status', 'able')->get();
        $banner_count = Banners::where('status', 'able')->count();
        $depositions = Depositions::where('status', 'able')->get();
       // dd($online);
                            
      return view('agendados', [
            
            'agendados' => $agendados,
            'banners' => $banners,
            'banner_count' => $banner_count,
            'depositions' => $depositions,
            'filters'=>'',
            
        ]);
    }


    
    public function certidoes()
    { 
        $orientacao = certificate::where('categoria','=', 'Orientações Técnicas')->get();
        $desempenho = certificate::where('categoria','=', 'Desempenhos Técnicos Unipública')->get();
        $jurisprudencia = certificate::where('categoria','=', 'Jurisprudência')->get();
        $certidoes = certificate::where('categoria','=', 'Certidões da Unipública')->get();
        //dd($certidoes);
                            
      return view('certidoes', [
            
            'orientacoes' => $orientacao,
            'desempenhos' => $desempenho,
            'jurisprudencias' => $jurisprudencia,
            'certidoes' => $certidoes
            
        ]);
    }


    public function quemsomos()
    { 
        $depositions = Depositions::where('status', 'able')->get();
        //dd($certidoes);
                            
      return view('quemsomos', [
            
        'depositions' => $depositions
            
        ]);
    }

    public function realizados()
    { 
        $hoje=  date('Y-m-d');
        $realizados = Classes::where('end_date','<', $hoje)
        ->where('unyflex',0)->with('panels')->get();
        $banners= Banners::where('status', 'able')->get();
        $banner_count = Banners::where('status', 'able')->count();
        $depositions = Depositions::where('status', 'able')->get();
        //dd($presenciais);
                            
      return view('realizados', [

        'realizados' => $realizados,
        'banners' => $banners,
        'banner_count' => $banner_count,
        'depositions' => $depositions
            
        ]);
    }


    public function galeria()
    { 
        $hoje=  date('Y-m-d');
        $realizados = Classes::where('end_date','<', $hoje)
        ->where('unyflex',0)->with('panels')->get();
        $banners= Banners::where('status', 'able')->get();
        $banner_count = Banners::where('status', 'able')->count();
        $depositions = Depositions::where('status', 'able')->get();
        $photos = Galery::where('status', 'able')->with('photos')->get();
        $galerys= Galery::where('status', 'able')->with('classes')->with('photos')->get();

       //dd($galerys[0]->classes);
                            
      return view('galeria', [

        'realizados' => $realizados,
        'banners' => $banners,
        'banner_count' => $banner_count,
        'depositions' => $depositions,
        'photos'=>$photos,
        'galerys'=>$galerys
            
        ]);
    }


    
    public function pesquisa(Request $request)
    {
        $filters = $request->search;
        $hoje=  date('Y-m-d');
        $agendados =Classes::where('end_date','>', $hoje)
        ->where('unyflex',0)->where('title', 'LIKE',  "%{$request->search}%")
            ->orWhere('subtitle', 'LIKE', "{$request->search}")->with('panels')->orderBy('id', 'DESC')->paginate();
            $banners= Banners::where('status', 'able')->get();
            $banner_count = Banners::where('status', 'able')->count();
            $depositions = Depositions::where('status', 'able')->get();
            //dd($filters);
        return view('agendados', [
            'agendados' => $agendados,
            'banners' => $banners,
            'banner_count' => $banner_count,
            'depositions' => $depositions,
            'filters'=> $filters
        ]);
    }

    public function setores($slug)
    {
        $id=Category::where('slug',$slug)->first();
        $category= CategoryCourse::where('category_id',$id->id)->with('classes')->get();
       //dd($category[0]->classes[0]->title);
        $filters = $id->title;
        $hoje=  date('Y-m-d');
        $agendados =Classes::where('end_date','>', $hoje)
        ->where('unyflex',0)->where('title', 'LIKE',  "%{$slug}%")
            ->orWhere('subtitle', 'LIKE', "{$slug}")->with('panels')->orderBy('id', 'DESC')->paginate();
            $banners= Banners::where('status', 'able')->get();
            $banner_count = Banners::where('status', 'able')->count();
            $depositions = Depositions::where('status', 'able')->get();
            //dd($filters);
        return view('setores', [
            'agendadoss' => $category,
            'banners' => $banners,
            'banner_count' => $banner_count,
            'depositions' => $depositions,
            'filters'=> $filters
        ]);
    }




}