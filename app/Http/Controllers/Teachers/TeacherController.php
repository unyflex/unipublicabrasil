<?php



namespace App\Http\Controllers\Teachers;



use App\Http\Controllers\Controller;

use App\Models\Teacher;

use App\Models\Resposta;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Models\Banners;

use App\Models\Panel;

use App\Models\VideoLesson;

use App\Models\Finance;

use App\Models\Duvida;

use App\Models\Hotel;

use App\Models\Course;

use App\Models\Classes;

use App\Models\Material;

use App\Models\TeacherPanels;

use App\Models\MaterialPanels;

use App\Models\Horario;

use Auth;


class TeacherController extends Controller

{





           /**

     * Create a new controller instance.

     *

     * @return void

     */


    

    public function home()

    {
        $hoje=  date('Y-m-d');

        $teacher_id=Auth::user()->teacher_id;

        $teacher = Teacher::where('id', $teacher_id)->first();

        $panels= Panel::where('teacher_id', $teacher_id)->where('start_time','>' ,$hoje)->with('classes')->get();

        $panels_aplicados= Panel::where('teacher_id', $teacher_id)->where('start_time','<' ,$hoje)->with('classes')->get();
       // dd($panels[0]);

        return view('professores.index', [

            'page_name' => 'Painel Unyflex - Lista de Banners cadastrados',
            'teacher'=> $teacher,
            'panels'=>$panels,
            'panels_aplicados'=> $panels_aplicados

        ]);

    }

    
   


    public function classes()
    {
        $hoje=  date('Y-m-d');

        $teacher_id=Auth::user()->teacher_id;

        $panels= Panel::where('teacher_id', $teacher_id)->where('start_time','>' ,$hoje)->with('classes')->groupBy('classes_id')->get();

        $panels_aplicados= Panel::where('teacher_id', $teacher_id)->where('start_time','<' ,$hoje)->with('classes')->groupBy('classes_id')->get();

        return view('professores.classes', [

            'page_name' => 'Painel Unyflex - Lista de Banners cadastrados',

            'panels_aplicados'=> $panels_aplicados,

            'panels'=> $panels

        ]);

    }

    public function class(Classes $course)
    {
        $hoje=  date('Y-m-d');

        $teacher_id=Auth::user()->teacher_id;

        $panels= Panel::where('teacher_id', $teacher_id)->where('classes_id',$course->id)->with('classes')->get();

        $courses= Panel::where('teacher_id', $teacher_id)->where('start_time','>' ,$hoje)->with('classes')->groupBy('classes_id')->get();
    
     

        return view('professores.class', [

            'page_name' => 'Painel Unyflex - Lista de Banners cadastrados',

            'course'=> $course,
            'panels'=> $panels,
            'courses'=>$courses,

           
        ]);

    }
    public function addmaterial($class)

    {
    
        
        $teacher_id=Auth::user()->teacher_id;
        $curso=Classes::where('id',$class)->first();
        $panels=Panel::where('classes_id', $class)->where('teacher_id', $teacher_id)->get();

        return view('professores.add-material', [

            'page_name' => 'Painel Unyflex - Adicionar Professor',
            'curso'=>$curso,
            'panels'=>$panels

        ]);

    }

    public function cadMaterial(Request $request)

    {

        $material = new Material();

        $material->name = $request->nome;

        $material->file_name = $request->file->getClientOriginalName();

        $material->type = $request->tipo;

        $material->status = $request->status;

        $name=$request->file->getClientOriginalName();

        if ($material->save()) {

            $materialPanel = new MaterialPanels();

            $materialPanel->material_id = $material->id;
    
            $materialPanel->panel_id = $request->panel;

            $materialPanel->save();
    

           $request->file('file')->storeAs('public/materials',$name);

            return redirect()->route('professores-cursos')->with('message', 'success');

        } else {

            return redirect()->route('adicionar-material')->with('message', 'erro_cadastro');

        }

    }

    public function formProfessor()

    {

        return view('painel.form-teacher', [

            'page_name' => 'Painel Unyflex - Adicionar Professor'

        ]);

    }


    public function duvidas()

    {

        $duvidas = Duvida::where('status', 'able')->orderBy('id', 'DESC')->with('student')->paginate();

        
        return view('painel.teacher-duvida', [

            'duvidas' => $duvidas,

            'page_name' => 'Painel Unyflex - Adicionar Professor'

        ]);

    }

    public function responder(Duvida $duvida)

    {

        $duvidas = Duvida::where('id', $duvida->id)->with('class')->with('student')->first();

     
        return view('painel.teacher-responde', [

            'page_name' => 'Painel Unyflex - Informações do Professor',

            'duvida' => $duvidas

        ]);


    }



    public function infoProfessor(Teacher $teacher)

    {

        return view('painel.teacher-info', [

            'page_name' => 'Painel Unyflex - Informações do Professor',

            'teacher' => $teacher

        ]);

    }

    
    public function insereresposta(Request $request)

    {


        $resposta = new Resposta();

        $resposta->id_questao = $request->questao;

        $resposta->id_teacher = $request->professor;

        $resposta->id_curso = $request->curso;

        $resposta->resposta = $request->resposta;

       
        

        if ($resposta->save()) {

            return redirect()->route('professores');

        } 

    }


    public function cadProfessor(Request $request)

    {

        



        $teacher = new Teacher();

        $teacher->name = $request->nome;

        $teacher->cpf = $request->cpf;

        $teacher->email = $request->email;

        $teacher->phone = $request->telefone;

        $teacher->short_resume = $request->short_resume;

        $teacher->full_resume = $request->full_resume;

        $teacher->status = $request->status;

        $teacher->photo = $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();

        

        if ($teacher->save()) {

           $request->file('file')->storeAs('public/docentes', $name);

            return redirect()->route('informacao-professor', ['teacher' => $teacher->id])->with('message', 'success');

        } else {

            return redirect()->route('informacao-professor', ['teacher' => $teacher->id])->with('message', 'erro');

        }

    }



    public function updProfessor(Teacher $teacher, Request $request)

    {



        $teacher->name = $request->nome;

        $teacher->cpf = $request->cpf;

        $teacher->email = $request->email;

        $teacher->phone = $request->telefone;

        $teacher->short_resume = $request->short_resume;

        $teacher->full_resume = $request->full_resume;

        $teacher->status = $request->status;



        $teacher->photo = $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();



        if ($teacher->save()) {

            $request->file('file')->storeAs('public/docentes', $name);

            return redirect()->route('informacao-professor', ['teacher' => $teacher->id])->with('message', 'teacher_updated');

        } else {

            return redirect()->route('informacao-professor', ['teacher' => $teacher->id])->with('message', 'teacher_update_error');

        }

    }



    public function destroyProfessor(Teacher $teacher)

    {

        if ($teacher->delete()) {

            return redirect()->route('painel-professores')->with('message', 'teacher_deleted');

        } else {

            return redirect()->route('informacao-professor', ['teacher' => $teacher->id])->with('message', 'teacher_delete_error');

        }

    }

}

