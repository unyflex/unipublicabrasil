<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\RequestLogin;
use App\Http\AvaModel;

class AvaController extends Controller
{
    
    /* Página Inicial Ava - Com verificação de status (logado sim ou não) */
    public function index(request $request){
        
        echo "pagina index";
    }
    
    /* Exibir form de login */
    public function login(request $request){       

        return view('ava.pages.form_login');

    }

    public function loginVerify(RequestLogin $request){
        print_r($_POST);
    }

}
