<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Auth;

class HomeController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */

    public function index()

    {

        $session=session();

        
        if (Auth::user()->power==1) {
            return redirect()->route('professores');
        }
        if(Auth::user()->power==2){
            return redirect()->route('unyflex-home');
    } if(Auth::user()->power==0){
        return redirect()->route('painel-alunos');
    }
    if(Auth::user()->power==3){
        return redirect()->route('ava-home');
    }
    }

}

