<?php

namespace App\Http\Controllers\Unyflex;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {

        $nextCourses = DB::table('tbCursos')
            ->select('*')
            ->where('cur_live', '=', '1')
            ->where('cur_status', '=', '0')
            ->where('cur_ava', '=', '0')
            ->where('cur_live_inicio', '>', '2021-03-31')
            ->orderBy('cur_live_inicio', 'ASC')
            ->limit(4)
            ->get();


        $searchCoursesExtesions = DB::table('tbCursoExtensao')
            ->select('*')
            ->where('ext_status', '=', '1')
            ->where('ext_destaque', '=', '1')
            ->orderBy('ext_nome', 'ASC')
            ->get();

        $newCourses = DB::table('tbCursos')
            ->select('*')
            ->where('cur_status', '=', '0')
            ->where('cur_ava', '=', '0')
            ->where('cur_ano', '>', '2018')
            ->where('cur_vagas', '=', '0')
            ->limit(5)
            ->orderBy('id_cursos', 'ASC')
            ->get();

        $faceToFaceCourses = DB::table('cursospresenciais')
            ->select('*')
            ->where('status', '=', 'agendado')
            ->where('ano', '=', '2022')
            ->limit(5)
            ->orderBy('id', 'DESC')
            ->get();

        return view('unyflex.home-institutional', [
            'nextCourses' => $nextCourses,
            'searchCoursesExtesions' => $searchCoursesExtesions,
            'newCourses' => $newCourses,
            'faceToFaceCourses' => $faceToFaceCourses
        ]);
    }
}
