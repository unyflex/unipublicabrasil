<?php

namespace App\Http\Controllers\Unyflex;



use App\Http\Controllers\Controller;

use App\Models\Course;

use App\Models\Category;

use App\Models\Classes;

use App\Models\Panel;

use App\Models\Degree;

use App\Models\Alternative;

use App\Models\Material;

use App\Models\MaterialPanels;

use App\Models\Student;

use App\Models\Depositions;

use App\Models\Hotel;

use App\Models\Banners;

use App\Models\Package;

use App\Models\certificate;

use App\Models\Photos;

use App\Models\Enrollment;

use App\Models\Question;

use App\Models\CategoryCourse;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Auth;



class UnyflexController extends Controller



{

    public function show()

    {



        $hoje=  date('Y-m-d');

        $presenciais = Classes::where('unyflex',0)->with('panels')->get();

        $banners= Banners::where('status', 'able')->get();

        $banner_count = Banners::where('status', 'able')->count();

        $online = Classes::where('unyflex',1)->with('panels')->get();

        $depositions = Depositions::where('status', 'able')->get();

        $student_id=Auth::user()->student_id;

        $student= Student::where('id', $student_id)->first();

        $certificados = Degree::where('id_aluno', $student_id)->with('students')->with('classes')->get();
        //dd($certificados);
 
        $matriculas_presenciais=Enrollment::where('student_id', $student_id)->where('modality', 'in_person')->with('classes')->get();

        $matriculas_online=Enrollment::where('student_id', $student_id)->where('modality', 'distance_learning')->with('classes')->get();
        //dd($enrrolement);

        return view('unyflex.home', [



            'presenciais' => $presenciais,

            'onlines' => $online,

            'banners' => $banners,

            'banner_count' => $banner_count,

            'depositions' => $depositions,

            'student' => $student,

            'matriculas_presenciais' => $matriculas_presenciais,

            'matriculas_online'=> $matriculas_online,

            'certificados'=>$certificados,


        ]);

    }



    public function assistir($slug)

    {

        $curso=Classes::where('unyflex',1)->where('slug', $slug)->with('panels')->first();
       
        $i=0;

        foreach($curso->panels as $painel){
            $materiais[$i]=MaterialPanels::where('panel_id',$painel->id)->with('materials')->first();
            $i++;
        }
    
     

        return view('unyflex.assistir', [



            'curso'=>$curso,
            'materiais'=>$materiais,

            

        ]);

    }


    public function prova($slug)

    {

        $curso=Classes::where('unyflex',1)->where('slug', $slug)->with('panels')->first();
      
        $cursoid=$curso->id;
        $questions=Question::where('class_id',$cursoid)->with('alternatives')->get();

       //dd($question);

      

        return view('unyflex.prova', [



            'curso'=>$curso,
            'questions'=>$questions

            

        ]);

    }


    
    public function resultprova(Request $request)

    {
         $idcurso=$request->idcurso;
         $curso=Classes::where('id',$idcurso)->first();
         
         $corretas=0;
         
         $questao0=$request->questao0;
         $vquestao0=Alternative::where('id', $questao0)->first();
         if($vquestao0->correct==1){
             $corretas=$corretas+1;
         }
         $questao1=$request->questao0;
         $vquestao1=Alternative::where('id', $questao1)->first();
         if($vquestao1->correct==1){
             $corretas=$corretas+1;
         }
         
        $porcentagem=$corretas*50;

        if($porcentagem>70){
            $degree = new Degree();
            $degree->id_aluno=Auth::user()->student_id;
            $degree->id_curso=$idcurso;
            $degree->save();
        }
    
        $student_id=Auth::user()->student_id;

        $student= Student::where('id', $student_id)->first();
      

        return view('unyflex.prova-resultado', [


            'student'=>$student,
            'porcentagem'=>$porcentagem,
            'curso'=>$curso,

        ]);

    }

    public function certificado(Request $request)

    {

        $student_id=Auth::user()->student_id;

        $student= Student::where('id', $student_id)->first();
        $idcurso=$request->idcurso;
        $curso=Classes::where('id',$idcurso)->first();

        return view('unyflex.certificado', [
            
            'student'=>$student,
            'curso'=>$curso,
            

        ]);

    }

    public function presencial($slug)

    {

        $curso=Classes::where('unyflex',0)->where('slug', $slug)->with('panels')->first();

         $paineis=$curso->panels;

         $i=0;

         foreach($curso->panels as $painel){
             $materiais[$i]=MaterialPanels::where('panel_id',$painel->id)->with('materials')->first();
             $i++;
         }

            //var_dump($paineis);

           // die();

        return view('unyflex.presencial', [



            'curso'=>$curso,

            'paineis'=>$paineis,

            'materiais'=>$materiais,

            

        ]);

    }

    public function certificadohome($slug)

    {

        $curso=Classes::where('slug', $slug)->with('panels')->first();
        //dd($curso);
        
        $student_id=Auth::user()->student_id;

        $student= Student::where('id', $student_id)->first();
       

        return view('unyflex.certificado', [



            'curso'=>$curso,
            'student'=>$student,
           

            

        ]);

    }




    

}

