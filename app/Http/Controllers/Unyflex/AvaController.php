<?php

namespace App\Http\Controllers\Unyflex;



use App\Http\Controllers\Controller;

use App\Models\Course;

use App\Models\Teacher;


use App\Models\Duvida;

use App\Models\Feedback;

use App\Models\users;

use App\Models\Token;

use App\Models\Category;

use App\Models\Classes;

use App\Models\Panel;

use App\Models\Degree;

use App\Models\Alternative;

use App\Models\Material;

use App\Models\MaterialPanels;

use App\Models\Student;

use App\Models\Depositions;

use App\Models\Hotel;

use App\Models\Banners;

use App\Models\Package;

use App\Models\certificate;

use App\Models\Photos;

use App\Models\Enrollment;

use App\Models\Question;

use App\Models\Podcasts;

use App\Models\CategoryCourse;

use App\Models\VideoLesson;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Hash;

use Auth;



class AvaController extends Controller



{

    public function show()

    {



        $hoje=  date('Y-m-d');

        
        $banner_count = Banners::where('status', 'able')->count();

        $banners= Banners::where('status', 'able')->get();

        $online = Classes::where('unyflex',1)->with('panels')->get();

        $depositions = Depositions::where('status', 'able')->get();

        $categories = Category::where('status', 'able')->get();

        $aovivo = Classes::where('live' , 1)->where('start_date', '<=', $hoje)->get();

        $recentes = Classes::where('unyflex' , 1)->skip(0)->take(25)->get();

        $juridico = CategoryCourse::where('category_id' , 4)->with('classes')->get();
        $licitacoes = CategoryCourse::where('category_id' , 5)->with('classes')->get();
        $controleinterno = CategoryCourse::where('category_id' , 6)->with('classes')->get();
        $tributacao = CategoryCourse::where('category_id' , 7)->with('classes')->get();
        $assessoria = CategoryCourse::where('category_id' , 8)->with('classes')->get();
        $secretariasmunicipais = CategoryCourse::where('category_id' , 9)->with('classes')->get();
        $legislativo = CategoryCourse::where('category_id' , 10)->with('classes')->get();
        $financasmunicipais = CategoryCourse::where('category_id' , 11)->with('classes')->get();
        $contadoresmunicipais = CategoryCourse::where('category_id' , 12)->with('classes')->get();
        $profissionaissaude = CategoryCourse::where('category_id' , 13)->with('classes')->get();
        $gestaoambiental = CategoryCourse::where('category_id' , 14)->with('classes')->get();
        $patrimonio = CategoryCourse::where('category_id' , 15)->with('classes')->get();
        $consorcio = CategoryCourse::where('category_id' , 16)->with('classes')->get();
        $autarquia = CategoryCourse::where('category_id' , 17)->with('classes')->get();
        $ambienteurbanismo = CategoryCourse::where('category_id' , 18)->with('classes')->get();
        $compraspublicas = CategoryCourse::where('category_id' , 19)->with('classes')->get();
        $novaprevidencia = CategoryCourse::where('category_id' , 24)->with('classes')->get();
        $sistemasdeinformacao = CategoryCourse::where('category_id' , 25)->with('classes')->get();
        $pregoeiros = CategoryCourse::where('category_id' , 26)->with('classes')->get();
        $outros = CategoryCourse::where('category_id' , 27)->with('classes')->get();
        $recursoshumanos = CategoryCourse::where('category_id' , 28)->with('classes')->get();
        $anoeleitoral = CategoryCourse::where('category_id' , 29)->with('classes')->get();
        $livesrealizadas = CategoryCourse::where('category_id' , 30)->with('classes')->get();
       
            

        return view('unyflex.ava.home', [



          

            'onlines' => $online,

            'banners' => $banners,

            'banner_count' => $banner_count,

            'categories' => $categories,

            'aovivos'=> $aovivo,

            'recentes'=> $recentes,

            'juridico'=>$juridico,
            'licitacoes'=>$licitacoes,
            'controleinterno'=>$controleinterno,
            'tributacao'=>$tributacao,
            'legislativo'=>$legislativo,
            'assessoria'=>$assessoria,
            'secretariasmunicipais'=>$secretariasmunicipais,
            'financasmunicipais'=>$financasmunicipais,
            'contadoresmunicipais'=>$contadoresmunicipais,
            'profissionaissaude'=>$profissionaissaude,
            'gestaoambiental'=>$gestaoambiental,
            'patrimonio'=>$patrimonio,
            'consorcio'=>$consorcio,
            'autarquia'=>$autarquia,
            'ambienteurbanismo'=>$ambienteurbanismo,
            'compraspublicas'=>$compraspublicas,
            'novaprevidencia'=>$novaprevidencia,
            'sistemasdeinformacao'=>$sistemasdeinformacao,
            'pregoeiros'=>$pregoeiros,
            'outros'=>$outros,
            'recursoshumanos'=> $recursoshumanos,
            'anoeleitoral'=>$anoeleitoral,
            'livesrealizadas'=>$livesrealizadas,

            


       

        ]);

    }


    
    public function busca(Request $request)

    {
        $categories = Category::where('status', 'able')->get();
        $termo = $request->termo;
        $ordem = '';
       
        return view('unyflex.ava.resultado', [



            'categories'=>$categories,
            'ordem'=>$ordem,
            'termo'=>$termo,
            

            

        ]);

    }



    public function podcasts()

    {

        $categories = Category::where('status', 'able')->get();
        $podcast = Podcasts::where('status', 'able')->skip(0)->take(120)->orderBy('id', 'DESC')->get();

        return view('unyflex.ava.podcasts', [



            'categories'=>$categories,
            'podcasts'=>$podcast
            

            

        ]);

    }

    public function materiais()

    {

        $categories = Category::where('status', 'able')->get();
        $materiais = Material::where('status', 'able')->get();
        
        return view('unyflex.ava.materiais', [



            'categories'=>$categories,
            'materiais'=>$materiais
            

            

        ]);

    }

    public function prova($slug)

    {

        $curso=Classes::where('slug', $slug)->with('panels')->first();
        //dd($curso);
        $cursoid=$curso->id;
        
        $questions=Question::where('class_id',$cursoid)->with('alternatives')->get();
        
  

        return view('unyflex.ava.prova', [



            'curso'=>$curso,
            'questions'=>$questions

            

        ]);

    }


    
    public function infocurso($slug)

    {

        $categories = Category::where('status', 'able')->get();
        $class = Classes::where('slug', $slug)->with('panels')->first();
        $cursoid=$class->id;
        $questions=Question::where('class_id',$cursoid)->with('alternatives')->count();
        $idaluno=Auth::user()->student_id;
        $certificado=Token::where('class_id', $cursoid)->where('id_aluno', $idaluno)->count();
        
        $start_date=$class->start_date;
        $anoo=explode("-",$start_date);
        $ano=$anoo[0];
        $materials=$class->panels;
        $videoaulas=$class->panels;
    //   dd($videoaulas[0]->video_lesson[0]);
        
        return view('unyflex.ava.infocurso', [


            'slug'=>$slug,
            'categories'=>$categories,
            'class'=>$class,
            'ano'=>$ano,
            'materials'=>$materials,
            'questions'=>$questions,
            'videoaulas'=>$videoaulas,
            'certificado'=>$certificado,
            

            

        ]);

    }

    
    public function professor($professor)

    {
        

        $professors = Teacher::where('name', $professor)->first();
        $teacher_id = $professors->id;
        $classes= Panel::where('teacher_id', $teacher_id)->with('classes')->groupBy('classes_id')->get();
       
        $categories = Category::where('status', 'able')->get();
    
       
        
        return view('unyflex.ava.professor', [


            'professor' => $professor, 
            'categories'=>$categories,
            'classes'=>$classes,
           

            

        ]);

    }

    public function assistir($curso , $idvideo)

    {

        
        $video = VideoLesson::where('id', $idvideo)->first();
        $categories = Category::where('status', 'able')->get();
       
        
        return view('unyflex.ava.assistir', [



            'video'=>$video,
            'categories'=>$categories,
           

            

        ]);

    }

    public function assistirbusca($video, $tempo, $termo)

    {

        
        
        $categories = Category::where('status', 'able')->get();
       
        
        return view('unyflex.ava.assistir-busca', [



              'categories'=>$categories,
              'idVideo'=>$video,
              'tempoVideo'=>$tempo,
              'termoPesquisa'=>$termo
           

            

        ]);

    }


    public function beneficios()

    {

        
        
        $categories = Category::where('status', 'able')->get();
       
        
        return view('unyflex.ava.beneficios', [



              'categories'=>$categories,
           

            

        ]);

    }


    public function teste()

    {
        $senha = Hash::make(1234);
        dd($senha);

        ini_set('max_execution_time', 1800); 
        
        $conecta = mysqli_connect("localhost", "root", "", "teste1");
        $conecta->set_charset("utf8");
        $buscausu = mysqli_query($conecta, "SELECT * FROM users WHERE id > 14 ORDER BY id DESC ");
        while ($usu = mysqli_fetch_array($buscausu)) {
            $idusu=$usu['id'];
           
            $senha = Hash::make($usu['cpf']);
            echo $senha;
            echo '<br>';

           $upd = mysqli_query($conecta, "UPDATE users SET password= '$senha' WHERE id='$idusu' ");
          
        }
        
        $categories = Category::where('status', 'able')->get();

        $users= users::where( 'id' ,'>', 14)->get();
        
       
        
        return view('unyflex.ava.teste', [



              'categories'=>$categories,
              'users'=>$users,
           

            

        ]);

    }


        public function pontuacao()

    {

        
        
        $categories = Category::where('status', 'able')->get();
       
        
        return view('unyflex.ava.pontuacao', [



              'categories'=>$categories,
           

            

        ]);

    }


    public function duvida($slug)

    {

       // dd($slug);
        
        $categories = Category::where('status', 'able')->get();
        $class = Classes::where('slug', $slug)->with('panels')->first();
        $duvidas = Duvida::where('id_classes', $class->id )->with('student')->get();
        
        return view('unyflex.ava.duvida', [



              'categories'=>$categories,
              'class'=>$class,
              'slug'=>$slug,
              'duvidas'=>$duvidas,  

            

        ]);

    }

    public function tiraduvida($slug)

    {

       // dd($slug);
        
        $categories = Category::where('status', 'able')->get();
        $class = Classes::where('slug', $slug)->with('panels')->first();
       
        
        return view('unyflex.ava.tiraduvida', [



              'categories'=>$categories,
              'class'=>$class,
              'slug'=>$slug,    

            

        ]);

    }

    public function feedback()

    {

        
        
        $categories = Category::where('status', 'able')->get();
       
        
        return view('unyflex.ava.feedback', [



              'categories'=>$categories,
           

            

        ]);

    }



    public function enviaduvida(Request $request)

    {

        
        
        $duvida = new Duvida();

        $duvida->id_student = $request->id_student;
        $duvida->id_category = $request->setor;
        $duvida->id_classes = $request->class;
        $duvida->assunto = $request->assunto;
        $duvida->duvida = $request->mensagem;
        $duvida->status='able'; 


       
        if ($duvida->save()) {

        return redirect()->route('ava-home');

        }



    }
    
    public function inserefeed(Request $request)

    {

        
        
        $feedback = new Feedback();

        $feedback->id_studente = $request->id_studente;
        $feedback->acho = $request->acho;
        $feedback->utilizacao = $request->utilizacao;
        $feedback->qualidade = $request->qualidade;
        $feedback->conhecia = $request->conhecia;
        $feedback->servidor = $request->servidor;
        $feedback->indicaria = $request->indicaria;
        $feedback->status='able'; 


       
        if ($feedback->save()) {

        return redirect()->route('ava-home');

        }



    }



    

    
    public function resultprova(Request $request)

    {

       // dd($request->loop);


       if($request->loop == 1){
        $categories = Category::where('status', 'able')->get();

        $idcurso=$request->idcurso;
        $curso=Classes::where('id',$idcurso)->first();
        
        $corretas=0;
        
        $questao0=$request->questao0;
        $vquestao0=Alternative::where('id', $questao0)->first();
        if($vquestao0->correct==1){
            $corretas=$corretas+1;
        }
        $questao1=$request->questao1;
        $vquestao1=Alternative::where('id', $questao1)->first();
        if($vquestao1->correct==1){
            $corretas=$corretas+1;
        }
      
        
       $porcentagem=$corretas*50;

    }









        if($request->loop == 2){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*33;

        }

        if($request->loop == 3){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao3=$request->questao3;
            $vquestao3=Alternative::where('id', $questao1)->first();
            if($vquestao3->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*25;

        }

        
        if($request->loop == 4){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao4=$request->questao4;
            $vquestao4=Alternative::where('id', $questao1)->first();
            if($vquestao4->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*20;

        }


        if($request->loop == 5){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao4=$request->questao4;
            $vquestao4=Alternative::where('id', $questao1)->first();
            if($vquestao4->correct==1){
                $corretas=$corretas+1;
            }

            $questao5=$request->questao5;
            $vquestao5=Alternative::where('id', $questao1)->first();
            if($vquestao5->correct==1){
                $corretas=$corretas+1;
            }
            
            
           $porcentagem=$corretas*16.66;

        }



        
        if($request->loop == 6){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao4=$request->questao4;
            $vquestao4=Alternative::where('id', $questao1)->first();
            if($vquestao4->correct==1){
                $corretas=$corretas+1;
            }

            $questao5=$request->questao5;
            $vquestao5=Alternative::where('id', $questao1)->first();
            if($vquestao5->correct==1){
                $corretas=$corretas+1;
            }
            
            $questao6=$request->questao6;
            $vquestao6=Alternative::where('id', $questao1)->first();
            if($vquestao6->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*14.28;

        }


          
        if($request->loop == 7){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao4=$request->questao4;
            $vquestao4=Alternative::where('id', $questao1)->first();
            if($vquestao4->correct==1){
                $corretas=$corretas+1;
            }

            $questao5=$request->questao5;
            $vquestao5=Alternative::where('id', $questao1)->first();
            if($vquestao5->correct==1){
                $corretas=$corretas+1;
            }
            
            $questao6=$request->questao6;
            $vquestao6=Alternative::where('id', $questao1)->first();
            if($vquestao6->correct==1){
                $corretas=$corretas+1;
            }

            $questao7=$request->questao7;
            $vquestao7=Alternative::where('id', $questao1)->first();
            if($vquestao7->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*12.5;

        }


        if($request->loop == 8){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao4=$request->questao4;
            $vquestao4=Alternative::where('id', $questao1)->first();
            if($vquestao4->correct==1){
                $corretas=$corretas+1;
            }

            $questao5=$request->questao5;
            $vquestao5=Alternative::where('id', $questao1)->first();
            if($vquestao5->correct==1){
                $corretas=$corretas+1;
            }
            
            $questao6=$request->questao6;
            $vquestao6=Alternative::where('id', $questao1)->first();
            if($vquestao6->correct==1){
                $corretas=$corretas+1;
            }

            $questao7=$request->questao7;
            $vquestao7=Alternative::where('id', $questao1)->first();
            if($vquestao7->correct==1){
                $corretas=$corretas+1;
            }

            $questao8=$request->questao8;
            $vquestao8=Alternative::where('id', $questao1)->first();
            if($vquestao8->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*11.1;

        }
       


        if($request->loop == 9){
            $categories = Category::where('status', 'able')->get();

            $idcurso=$request->idcurso;
            $curso=Classes::where('id',$idcurso)->first();
            
            $corretas=0;
            
            $questao0=$request->questao0;
            $vquestao0=Alternative::where('id', $questao0)->first();
            if($vquestao0->correct==1){
                $corretas=$corretas+1;
            }
            $questao1=$request->questao1;
            $vquestao1=Alternative::where('id', $questao1)->first();
            if($vquestao1->correct==1){
                $corretas=$corretas+1;
            }
            $questao2=$request->questao2;
            $vquestao2=Alternative::where('id', $questao1)->first();
            if($vquestao2->correct==1){
                $corretas=$corretas+1;
            }

            $questao4=$request->questao4;
            $vquestao4=Alternative::where('id', $questao1)->first();
            if($vquestao4->correct==1){
                $corretas=$corretas+1;
            }

            $questao5=$request->questao5;
            $vquestao5=Alternative::where('id', $questao1)->first();
            if($vquestao5->correct==1){
                $corretas=$corretas+1;
            }
            
            $questao6=$request->questao6;
            $vquestao6=Alternative::where('id', $questao1)->first();
            if($vquestao6->correct==1){
                $corretas=$corretas+1;
            }

            $questao7=$request->questao7;
            $vquestao7=Alternative::where('id', $questao1)->first();
            if($vquestao7->correct==1){
                $corretas=$corretas+1;
            }

            $questao8=$request->questao8;
            $vquestao8=Alternative::where('id', $questao1)->first();
            if($vquestao8->correct==1){
                $corretas=$corretas+1;
            }


            $questao9=$request->questao9;
            $vquestao9=Alternative::where('id', $questao1)->first();
            if($vquestao9->correct==1){
                $corretas=$corretas+1;
            }
            
           $porcentagem=$corretas*10;

        }
       


        return view('unyflex.ava.prova-resultado', [


           // 'student'=>$student,
            'porcentagem'=>$porcentagem,
            'curso'=>$curso,
            'categories'=>$categories,

        ]);

    }

    public function certificado(Request $request)

    {

       $student_id=Auth::user()->student_id;

      $student= Student::where('id', $student_id)->first();
     
        $idcurso=$request->idcurso;
        $curso=Classes::where('id',$idcurso)->first();
        

        $token = new Token();

        $token->id_aluno = $student_id;
        $token->class_id = $curso->id;
        
        $tokenid=$student_id.$idcurso;
        $tokeen = Hash::make($tokenid);
       
        $token->token = $tokeen;
        $token->save();

        return view('unyflex.ava.certificado', [
            
           // 'student'=>$student,
            'curso'=>$curso,
            'tokeen'=>$tokeen,
            

        ]);

    }

    public function presencial($slug)

    {

        $curso=Classes::where('unyflex',0)->where('slug', $slug)->with('panels')->first();

         $paineis=$curso->panels;

         $i=0;

         foreach($curso->panels as $painel){
             $materiais[$i]=MaterialPanels::where('panel_id',$painel->id)->with('materials')->first();
             $i++;
         }

            //var_dump($paineis);

           // die();

        return view('unyflex.presencial', [



            'curso'=>$curso,

            'paineis'=>$paineis,

            'materiais'=>$materiais,

            

        ]);

    }

    public function certificadohome($slug)

    {

        $curso=Classes::where('slug', $slug)->with('panels')->first();
        //dd($curso);
        
        $student_id=Auth::user()->student_id;

        $student= Student::where('id', $student_id)->first();
       

        return view('unyflex.certificado', [



            'curso'=>$curso,
            'student'=>$student,
           

            

        ]);

    }


   



    public function setores($slug)

    {

        $categories = Category::where('status', 'able')->get();
        $category = Category::where('slug', $slug)->first();
        $id=$category->id;
        $classes = CategoryCourse::where('category_id', $id)->with('classes')->get();
      
        return view('unyflex.ava.setores', [



          'categories'=>$categories,
          'classes'=>$classes,

            

        ]);

    }

    

}

