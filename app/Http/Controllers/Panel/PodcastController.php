<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Material;

use App\Models\MaterialPanels;

use App\Models\Classes;
use App\Models\Teacher;
use App\Models\Podcasts;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;



class PodcastCOntroller extends Controller

{



           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    public function show()

    {

        $podcasts = Podcasts::where('status', 'able')->orderBy('id', 'DESC')->get();

       // dd($podcasts);

        return view('painel.podcast', [

            'page_name' => 'Painel Unyflex - Lista de podcasts cadastrados',

            'podcasts' => $podcasts

        ]);

    }



    public function formPodcast()

    {
        $teachers= Teacher::where('status', 'able')->orderBy('id', 'DESC')->get();
       
        return view('painel.form-podcast', [
          
            'page_name' => 'Painel Unyflex - Adicionar Podcast',
            'teachers'=> $teachers

        ]);

    }



    public function infoPodcast(Podcasts $podcast)

    {

     

        $teachers= Teacher::where('status', 'able')->orderBy('id', 'DESC')->get();



        return view('painel.podcast-info', [

            'page_name' => 'Painel Unyflex - Informação do material Material',

            'podcast' => $podcast,

            'teachers' => $teachers

        ]);

    }



    public function cadPodcast(Request $request)

    {

        $podcast = new Podcasts();

        $podcast->titulo = $request->titulo;

        $podcast->arquivo = $request->file->getClientOriginalName();

        $podcast->descricao = $request->descricao;

        $podcast->professor = $request->professor;

        $podcast->status = $request->status;

        $name=$request->file->getClientOriginalName();

        if ($podcast->save()) {

           

           $request->file('file')->storeAs('public/podcasts',$name);

            return redirect()->route('painel-podcasts')->with('message', 'success');

        } else {

            return redirect()->route('painel-podcasts')->with('message', 'erro_cadastro');

        }

    }



    public function updPodcast(Podcasts $podcast, Request $request)

    {


        $podcast->titulo = $request->titulo;



        $podcast->descricao = $request->descricao;

        $podcast->professor = $request->professor;

        $podcast->status = $request->status;


        if ($podcast->save()) {

           

          
 
             return redirect()->route('painel-podcasts')->with('message', 'success');
 
         } else {
 
             return redirect()->route('painel-podcasts')->with('message', 'erro_cadastro');
 
         }

    }



    public function inserirMaterial(Request $request)

    {



        $materialPanel = new MaterialPanels();

        $materialPanel->material_id = $request->material;

        $materialPanel->panel_id = $request->panel;



        if ($materialPanel->save()) {

            return redirect()->route('informacao-material', ['material' => $request->material])->with('message', 'material_added');

        } else {

            return redirect()->route('informacao-material', ['material' => $request->material])->with('message', 'material_add_error');

        }

    }





    public function search(Request $request)

    {

        $filters = $request->except('_token');



        $materials = Material::where('name', 'LIKE',  "%{$request->search}%")

            ->orWhere('file_name', 'LIKE', "{$request->search}")->orderBy('id', 'DESC')->paginate();



        return view('painel.material', [

            'page_name' => 'Painel Unyflex - Lista de materiais cadastrados',

            'materials' => $materials,

            'filters' => $filters

        ]);

    }



    public function destroyMaterial(Material $material)

    {

        if ($material->delete()) {

            return redirect()->route('painel-materiais')->with('message', 'material_deleted');

        } else {

            return redirect()->route('painel-materiais')->with('message', 'material_delete_error');

        }

    }

}

