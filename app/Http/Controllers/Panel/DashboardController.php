<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
           /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show()
    {
        return view('painel.dashboard', [
            'page_name' => 'Painel Unyflex - Lista de alunos cadastrados',
           
        ]);
    }
}
