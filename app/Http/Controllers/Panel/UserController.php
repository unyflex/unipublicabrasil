<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Student;

use App\Models\User;

use App\Models\Subscription;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;





class UserController extends Controller

{

       /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }


    public function pesquisa(Request $request)

    {

        $campo = $request->campo;
        $operador = $request->operador;
        $valor = $request->valor;

        $users = User::where('power', '!=', '1')->where($campo , $operador , $valor)->where('power', '!=', '2')->where('power', '!=', '3')->get();

        //dd($users);

        return view('painel.user', [

            'page_name' => 'Painel Unyflex - Lista de Usuarios cadastrados',

            'users' => $users

        ]);

    }


    
    public function show()

    {



        $users = User::where('power', '!=', '1')->where('power', '!=', '2')->where('power', '!=', '3')->get();

        //dd($users);

        return view('painel.user', [

            'page_name' => 'Painel Unyflex - Lista de Usuarios cadastrados',

            'users' => $users

        ]);

    }



    public function infoUser($id)

    {

        $user = User::where('id', $id)->first();


        return view('painel.user-info', [

            'page_name' => 'Painel Unyflex - Informações do Aluno',

            'user' => $user,

          

        ]);

    }



    public function formUser()

    {

        return view('painel.form-user', [

            'page_name' => 'Painel Unyflex - Adicionar Aluno'

        ]);

    }



    public function cadUser(Request $request)

    {

        $user = new User();

        $user->name = $request->nome;

        $user->cpf = $request->cpf;

        $user->password = Hash::make($request->cpf);

        $user->email = $request->email;

        $user->teacher_id = 1;

        $user->student_id = 1;

        $user->telefone = $request->telefone;

        $user->setor = $request->setor;

        $user->power = 0;

        $user->funcao = $request->funcao;

        $user->photo = $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();



        if ($user->save()) {

            $request->file('file')->storeAs('public/usuarios/perfil', $name);

            return redirect()->route('informacao-usuario', ['id' => $user->id])->with('message', 'success');

        } else {

            return redirect()->route('informacao-aluno', ['id' => $student->id])->with('message', 'erro');

        }

    }



    public function updUser(Request $request, User $user)

    {



        $user->name = $request->nome;

        $user->cpf = $request->cpf;

        $user->password = Hash::make($request->cpf);

        $user->email = $request->email;

        $user->telefone = $request->telefone;

        $user->setor = $request->setor;

        $user->funcao = $request->funcao;




        if($request->file('file')!=''){

            $user->photo = $request->file->getClientOriginalName();

            $name=$request->file->getClientOriginalName();

            $request->file('file')->storeAs('public/usuarios/perfil', $name);

        }

      



        if ($user->save()) {

            return redirect()->route('informacao-usuario', ['id' => $user->id])->with('message', 'atualizado');

        } else {

            return redirect()->route('informacao-aluno', ['id' => $student->id])->with('message', 'erro');

        }

    }



    public function destroyAluno(Student $student)

    {

        if ($student->delete()) {

            return 'sucesso';

        }

    }

}

