<?php




namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Hotel;

use App\Models\Package;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;





class HotelController extends Controller

{



               /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    

    public function show()

    {

        $hoteis = Hotel::all();

        //dd($hoteis);



        return view('painel.hotel', [

            'page_name' => 'Painel Unyflex - Lista de hoteis cadastrados',

            'hoteis' => $hoteis

        ]);

    }



    public function formHotel()

    {

        return view('painel.form-hotel', [

            'page_name' => 'Painel Unyflex - Adicionar Hotel'

        ]);

    }



    public function infoHotel(Hotel $hotel)

    {

        $hotel->id;

        $packages= Package::where('id_hotel',$hotel->id)->get();

       // dd($packages);

        return view('painel.hotel-info', [

            'page_name' => 'Painel Unyflex - Informações do Hotel',

            'hotel' => $hotel,

            'packages' => $packages

        ]);

    }



    public function cadHotel(Request $request)

    {

        



        $hotel = new Hotel();

        $hotel->nome = $request->nome;

        $hotel->rua = $request->rua;

        $hotel->distancia = $request->distancia;

        $hotel->cidade = $request->cidade;

        $hotel->numero = $request->numero;

        $hotel->descricao = $request->descricao;

        $hotel->status = $request->status;

        $hotel->estrelas = $request->estrelas;

        $hotel->site = $request->site;



    

        if ($hotel->save()) {



            $pacotes=$request->pacote;

            $valores=$request->valor;

            $i=0;

            foreach($pacotes as $pacote){

               

                $package = new Package();

                $package->id_hotel=$hotel->id;

                $package->pacote=$pacote;

                $package->valor=$request->valor[$i];

                $package->save();

              

                $i++;

        

            }

          

            return redirect()->route('informacao-hotel', ['hotel' => $hotel->id])->with('message', 'success');

        } else {

            return redirect()->route('informacao-hotel', ['hotel' => $hotel->id])->with('message', 'erro');

        }

    }



    public function updHotel(Hotel $hotel, Request $request, Package $package)

    {



        $hotel->nome = $request->nome;

        $hotel->rua = $request->rua;

        $hotel->distancia = $request->distancia;

        $hotel->cidade = $request->cidade;

        $hotel->numero = $request->numero;

        $hotel->descricao = $request->descricao;

        $hotel->status = $request->status;

        $hotel->estrelas = $request->estrelas;

        $hotel->site = $request->site;



        $pacotes=$request->pacote;

        $valores=$request->valor;

      



      

        if ($hotel->save()) {

           

            return redirect()->route('informacao-hotel', ['hotel' => $hotel->id])->with('message', 'teacher_updated');

        } else {

            return redirect()->route('informacao-hotel', ['hotel' => $hotel->id])->with('message', 'teacher_update_error');

        }

    }



    public function destroyHotel(Hotel $hotel)

    {

        if ($hotel->delete()) {

            return redirect()->route('painel-hoteis')->with('message', 'teacher_deleted');

        } 

    }

}

