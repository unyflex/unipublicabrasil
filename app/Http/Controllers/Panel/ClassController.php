<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Classes;


use App\Models\Enrollment;

use App\Models\Polo;

use App\Models\Course;

use App\Models\Category;

use App\Models\Teacher;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;



class ClassController extends Controller





{



           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    

    public function formTurma($course)

    {

        $courses = Course::where('id', $course)->first();

        $polos= Polo::where('status', 'able')->get();



        return view('painel.form-class', [

            'page_name' => 'Painel Unyflex - Adicionar Curso',

            'course' => $courses,

            'polos'=> $polos

        ]);

    }





    public function cadTurma(Request $request)

    {

       if($request->confirmado=="on"){

          $confirmado="1";

       }else{

           $confirmado="0";

       }



       if($request->aovivo=="on"){

           $aovivo="1";

       }else{

           $aovivo="0";

       }



       if($request->unyflex=="on"){

        $unyflex="1";

    }else{

        $unyflex="0";

    }



  

        $class = new Classes();

        $class->title = $request->titulo;

        $class->subtitle = $request->subtitulo;

        $class->status = $request->status;

        $class->workload = $request->cargaHoraria;

        $class->slug=$request->slug;

        $dataInicio = $request->dataInicio;

        $dataInicio2= date('Y-m-d', strtotime($dataInicio));

        $dataTermino = $request->dataTermino;

        $dataTermino2= date('Y-m-d', strtotime($dataTermino));

        

       

        $class->start_date = $dataInicio2;

        $class->end_date = $dataTermino2;

        $class->type = $request->tipo;

        $class->course_id = $request->curso;

        $class->confirmed = $confirmado;

        $class->live = $aovivo;

        $class->id_polo=2;

        $class->polo = $request->polo;

        $class->unyflex = $unyflex;

        $class->photo = $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();

       

        if ($class->save()) {

            $request->file('file')->storeAs('public/cursos/banner', $name);

            return redirect()->route('informacao-turma', ['classes' => $class->id])->with('message', 'course_created');

        } else {

            return redirect()->route('informacao-turma', ['classes' => $class->id])->with('message', 'course_create_error');

        }

    }



    public function infoTurma(Classes $classes)

    {   



        $classes = Classes::where('id', $classes->id)->with('panels')->first();

        $allteachers = Teacher::all();

        $panels = $classes->panels;

        

        $polos= Polo::where('status', 'able')->get();

        return view('painel.class-info', [

            'page_name' => 'Painel Unyflex - Informações do Curso',

            'class' => $classes,

            'panels' => $panels,

            'allteachers' => $allteachers,

            'polos' => $polos

        ]);

    }

    public function inscritos($class)
 
    {
        
        $matricula = Enrollment::where('classes_id' , $class)->with('student')->get();
        //dd($matricula);
        
        return view('painel.inscritos', [

            'page_name' => 'Painel Unyflex - Lista de alunos cadastrados',

            'matricula' => $matricula

        ]);

    }


    public function updTurma(Classes $classes, Request $request)

    {



        $validator = Validator::make($request->all(), [

            'titulo' => 'required|max:40',

            'subtitulo' => 'required|max:40',

            'status' => 'required',

            'dataInicio' => 'required',

            'dataTermino' => 'required',

            'tipo' => 'required',

            'cargaHoraria' => 'required'

        ]);



        if ($validator->fails()) {

            return redirect()->route('informacao-turma', ['classes' => $classes->id])->withErrors($validator);

        }

        if($request->unyflex=="on"){

            $unyflex="1";

        }else{

            $unyflex="0";

        }



        



        $classes->title = $request->titulo;

        $classes->subtitle = $request->subtitulo;

        $classes->status = $request->status;

        $classes->start_date = inverteDataHora($request->dataInicio);

        $classes->end_date = inverteDataHora($request->dataTermino);

        $classes->type = $request->tipo;

        $classes->workload = $request->cargaHoraria;

        $classes->unyflex = $unyflex;

        $classes->polo = $request->polo;

        $classes->slug=$request->slug;

        $classes->id_polo=2;

        $classes->confirmed = $request->confirmado == 'on' ? '1' : '0';

        $classes->live = $request->aovivo == 'on' ? '1' : '0';

        if($request->file('file')!=''){

            $classes->photo = $request->file->getClientOriginalName();

            $name=$request->file->getClientOriginalName();

            $request->file('file')->storeAs('public/cursos/banner', $name);

        }

      



        if ($classes->save()) {

            return redirect()->route('informacao-turma', ['classes' => $classes->id])->with('message', 'class_updated');

        } else {

            return redirect()->route('informacao-turma', ['classes' => $classes->id])->with('message', 'class_update_error');

        }

    }

}

