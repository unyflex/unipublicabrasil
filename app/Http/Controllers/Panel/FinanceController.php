<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Course;

use App\Models\Category;

use App\Models\Finance;

use App\Models\Panel;

use App\Models\Flow;

use App\Models\Enrollment;

use App\Models\CategoryCourse;

use App\Models\Classes;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;



class FinanceController extends Controller

{



        /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }





    public function show()

    {

        $expenses = Finance::orderBy('id', 'DESC')->paginate();
        



        return view('painel.finances', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'expenses' => $expenses

        ]);

    }

    public function despesasgerais()

    {

        $expenses = Finance::orderBy('id', 'DESC')->paginate();
        $pessoal = Finance::where('anoDespesa', '2022')->where('categoria', 'pessoal')->sum('valorDespesa');
        $marketing = Finance::where('anoDespesa', '2022')->where('categoria', 'marketing')->sum('valorDespesa');
        $professores = Finance::where('anoDespesa', '2022')->where('categoria', 'professores')->sum('valorDespesa');
        $financeiras = Finance::where('anoDespesa', '2022')->where('categoria', 'financeiras')->sum('valorDespesa');
        $diretoria = Finance::where('anoDespesa', '2022')->where('categoria', 'diretoria')->sum('valorDespesa');
        $investimentos = Finance::where('anoDespesa', '2022')->where('categoria', 'investimentos')->sum('valorDespesa');
        $administrativas = Finance::where('anoDespesa', '2022')->where('categoria', 'administrativas')->sum('valorDespesa');
        $outros = Finance::where('anoDespesa', '2022')->where('categoria', 'outros')->sum('valorDespesa');
        //dd($pessoal);


        return view('painel.despesas', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'expenses' => $expenses,
            'pessoal' => $pessoal,
            'marketing' => $marketing,
            'professores' => $professores,
            'financeiras' => $financeiras,
            'diretoria' => $diretoria,
            'investimentos' => $investimentos,
            'administrativas' => $administrativas,
            'outros' => $outros,

        ]);

    }

    public function despesasano($ano)

    {
       

        $expenses = Finance::orderBy('id', 'DESC')->paginate();
        $janeiro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'janeiro')->sum('valorDespesa');
        $fevereiro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'fevereiro')->sum('valorDespesa');
        $marco = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'março')->sum('valorDespesa');
        $abril = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'abril')->sum('valorDespesa');
        $maio = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'maio')->sum('valorDespesa');
        $junho = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'junho')->sum('valorDespesa');
        $julho = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'julho')->sum('valorDespesa');
        $agosto = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'agosto')->sum('valorDespesa');
        $setembro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'setembro')->sum('valorDespesa');
        $outubro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'outubro')->sum('valorDespesa');
        $novembro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'novembro')->sum('valorDespesa');
        $dezembro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'dezembro')->sum('valorDespesa');
        
       
        


        return view('painel.despesas-ano', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'expenses' => $expenses,
            'janeiro' => $janeiro,
            'fevereiro' => $fevereiro,
            'marco' => $marco,
            'abril' => $abril,
            'maio' => $maio,
            'junho' => $junho,
            'julho' => $julho,
            'agosto' => $agosto,
            'setembro' => $setembro,
            'outubro' => $outubro,
            'novembro' => $novembro,
            'dezembro' => $dezembro,
         

        ]);

    }

    public function comparacaoano($ano)

    {
       

        $expenses = Finance::orderBy('id', 'DESC')->paginate();
        $janeiro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'janeiro')->sum('valorDespesa');
        $fevereiro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'fevereiro')->sum('valorDespesa');
        $marco = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'março')->sum('valorDespesa');
        $abril = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'abril')->sum('valorDespesa');
        $maio = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'maio')->sum('valorDespesa');
        $junho = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'junho')->sum('valorDespesa');
        $julho = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'julho')->sum('valorDespesa');
        $agosto = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'agosto')->sum('valorDespesa');
        $setembro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'setembro')->sum('valorDespesa');
        $outubro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'outubro')->sum('valorDespesa');
        $novembro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'novembro')->sum('valorDespesa');
        $dezembro = Finance::where('anoDespesa', $ano)->where('mesDespesa', 'dezembro')->sum('valorDespesa');

        $faturamentojaneiro = Enrollment::whereBetween('payday', ['2022-01-01', '2022-01-30'])->sum('final_value');
        $faturamentofevereiro = Enrollment::whereBetween('payday', ['2022-02-01', '2022-02-30'])->sum('final_value');
        $faturamentomarco = Enrollment::whereBetween('payday', ['2022-03-01', '2022-03-30'])->sum('final_value');
        $faturamentoabril = Enrollment::whereBetween('payday', ['2022-04-01', '2022-04-30'])->sum('final_value');
        $faturamentomaio = Enrollment::whereBetween('payday', ['2022-05-01', '2022-05-30'])->sum('final_value');
        $faturamentojunho = Enrollment::whereBetween('payday', ['2022-06-01', '2022-06-30'])->sum('final_value');
        $faturamentojulho = Enrollment::whereBetween('payday', ['2022-07-01', '2022-07-30'])->sum('final_value');
        $faturamentoagosto = Enrollment::whereBetween('payday', ['2022-08-01', '2022-08-30'])->sum('final_value');
        $faturamentosetembro = Enrollment::whereBetween('payday', ['2022-09-01', '2022-09-30'])->sum('final_value');
        $faturamentooutubro = Enrollment::whereBetween('payday', ['2022-10-01', '2022-10-30'])->sum('final_value');
        $faturamentonovembro = Enrollment::whereBetween('payday', ['2022-11-01', '2022-11-30'])->sum('final_value');
        $faturamentodezembro = Enrollment::whereBetween('payday', ['2022-12-01', '2022-12-30'])->sum('final_value');
      
        
       
        


        return view('painel.comparacao-ano', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'expenses' => $expenses,
            'janeiro' => $janeiro,
            'fevereiro' => $fevereiro,
            'marco' => $marco,
            'abril' => $abril,
            'maio' => $maio,
            'junho' => $junho,
            'julho' => $julho,
            'agosto' => $agosto,
            'setembro' => $setembro,
            'outubro' => $outubro,
            'novembro' => $novembro,
            'dezembro' => $dezembro,
            'ano' => $ano,
            
            'faturamentojaneiro'=>  $faturamentojaneiro,
            'faturamentofevereiro'=>  $faturamentofevereiro,
            'faturamentomarco'=>  $faturamentomarco,
            'faturamentoabril'=>  $faturamentoabril,
            'faturamentomaio'=>  $faturamentomaio,
            'faturamentojunho'=>  $faturamentojunho,
            'faturamentojulho'=>  $faturamentojulho,
            'faturamentoagosto'=>  $faturamentoagosto,
            'faturamentosetembro'=>  $faturamentosetembro,
            'faturamentooutubro'=>  $faturamentooutubro,
            'faturamentonovembro'=>  $faturamentonovembro,
            'faturamentodezembro'=>  $faturamentodezembro,
         

        ]);

    }

    public function faturamento($ano)

    {
       

        $expenses = Finance::orderBy('id', 'DESC')->paginate();
 
        $faturamentojaneiro = Enrollment::whereBetween('payday', ['2022-01-01', '2022-01-30'])->sum('final_value');
        $faturamentofevereiro = Enrollment::whereBetween('payday', ['2022-02-01', '2022-02-30'])->sum('final_value');
        $faturamentomarco = Enrollment::whereBetween('payday', ['2022-03-01', '2022-03-30'])->sum('final_value');
        $faturamentoabril = Enrollment::whereBetween('payday', ['2022-04-01', '2022-04-30'])->sum('final_value');
        $faturamentomaio = Enrollment::whereBetween('payday', ['2022-05-01', '2022-05-30'])->sum('final_value');
        $faturamentojunho = Enrollment::whereBetween('payday', ['2022-06-01', '2022-06-30'])->sum('final_value');
        $faturamentojulho = Enrollment::whereBetween('payday', ['2022-07-01', '2022-07-30'])->sum('final_value');
        $faturamentoagosto = Enrollment::whereBetween('payday', ['2022-08-01', '2022-08-30'])->sum('final_value');
        $faturamentosetembro = Enrollment::whereBetween('payday', ['2022-09-01', '2022-09-30'])->sum('final_value');
        $faturamentooutubro = Enrollment::whereBetween('payday', ['2022-10-01', '2022-10-30'])->sum('final_value');
        $faturamentonovembro = Enrollment::whereBetween('payday', ['2022-11-01', '2022-11-30'])->sum('final_value');
        $faturamentodezembro = Enrollment::whereBetween('payday', ['2022-12-01', '2022-12-30'])->sum('final_value');


        return view('painel.faturamento', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'expenses' => $expenses,
            'janeiro' => $faturamentojaneiro,
            'fevereiro' => $faturamentomarco,
            'marco' =>  $faturamentomarco,
            'abril' => $faturamentoabril,
            'maio' => $faturamentomaio,
            'junho' => $faturamentojunho,
            'julho' => $faturamentojulho,
            'agosto' =>  $faturamentoagosto,
            'setembro' => $faturamentosetembro,
            'outubro' => $faturamentooutubro,
            'novembro' =>  $faturamentonovembro,
            'dezembro' => $faturamentodezembro,
            'ano' => $ano,
           

        ]);

    }



    public function formDespesas()

    {

        $allCategories = Category::where('status', 'able')->orderBy('title')->get();



        return view('painel.formDespesas', [

            'page_name' => 'Painel Unyflex - Adicionar Despesa',

            'allcategories' => $allCategories

        ]);

    }



    public function cadDespesa()



    {



        $expense = new Finance();



        $expense->id=NULL;

        $expense->tipoDespesa= request('nome');

        $expense->valorDespesa=request('valor');

        $expense->mesDespesa= request('mes');

        $expense->anoDespesa= request('ano');

        $expense->produtoDespesa= request('produto');

        $expense->categoria= request('categoria');



        $expense->save();

        return redirect('/painel/financeiro');

    }



    public function destroyDespesa(Finance $despesa)

    {

        if ($despesa->delete()) {

            return redirect()->route('painel-financeiro')->with('message', 'expense_deleted');

        } else {

            return redirect()->route('painel-financeiro')->with('message', 'expense_delete_error');

        }

    }



    public function formFluxo(){



        $flows= Flow::orderBy('id','DESC')->paginate();



        return view('painel.formFluxo', [

            'page_name' => 'Painel Unyflex - Adicionar Fluxo De caixa',

            'flows'=>$flows





        ]);

    }

    public function fluxo(){



        $mytime = date('Y-m-d');

        $allCategories = Category::where('status', 'able')->orderBy('title')->get();

        $course = Classes::where('start_date', '>', $mytime)->get();

        //dd($course[1]->title);



        return view('painel.fluxo', [

            'page_name' => 'Painel Unyflex - Informações do Fluxo de caixa',

            'course' => $course,

            'allcategories' => $allCategories,

        ]);

    }

    public function cadFluxo()



    {



        $a=request('alunos0')*request('0');

        $b=request('alunos1')*request('1');

        $c=request('alunos2')*request('2');

        $d=request('alunos3')*request('3');

        $e=request('alunos4')*request('4');

        $f=request('alunos5')*request('5');

        $g=request('alunos6')*request('6');

        $h=request('alunos7')*request('7');

        $i=request('alunos8')*request('8');

        $o=$a+$b+$c+$d+$e+$f+$g+$h+$i;

        $discount=request('desconto');

        $finalvalue=$o-(($o/100)*$discount);

        $flow = new Flow();

        $flow->id=NULL;

        $flow->mes=date('m');;

        $flow->ano=date('Y');;

        $flow->numeroAlunos=request('alunos0')+request('alunos1')+request('alunos2')+request('alunos3')+request('alunos4')+request('alunos5')+request('alunos6')+request('alunos7')+request('alunos8');

        $flow->valorCurso=$o;

        $flow->desconto=$discount;

        $flow->valorFinal=$finalvalue;

        $flow->save();

        return redirect('/painel/financeiro');

    }



    public function destroyFluxo(Flow $fluxo)

    {

        if ($fluxo->delete()) {

            return redirect()->route('painel-financeiro')->with('message', 'expense_deleted');

        } else {

            return redirect()->route('painel-financeiro')->with('message', 'expense_delete_error');

        }

    }


    public function pagamentoprofessores()

    {

        $paineis_apagar = Panel::where('pagamento_status', '0')->with('teacher')->paginate();
       


        return view('painel.pagamento-professores', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'paineis_apagar' => $paineis_apagar,

        ]);

    }

    public function editpagamento(Panel $panel)

    {
   
       $panel =Panel::where('id', $panel->id)->with('teacher')->first();


        return view('painel.painel-pagamento', [

            'page_name' => 'Painel Unyflex - Financeiro',

            'panel' => $panel,

        ]);

    }

    public function updpagamento(Panel $panel, Request $request )

    {
   
       $panel->pagamento_status=$request->status;
       $panel->save();

      return redirect()->route('pagamento-professores');

    }


}





