<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Hotel;

use App\Models\Banners;

use App\Models\Package;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;





class BannerController extends Controller

{

           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    public function show()

    {

        $banner = Banners::where('status', 'able')->get();

        $desabilitados = Banners::where('status', 'disabled')->get();

       // dd($banner);



        return view('painel.banner', [

            'page_name' => 'Painel Unyflex - Lista de Banners cadastrados',

            'banners' => $banner,

            'desabilitados'=> $desabilitados

        ]);

    }



    public function formBanner()

    {

        return view('painel.form-banner', [

            'page_name' => 'Painel Unyflex - Adicionar Banner'

        ]);

    }



    public function infoBanner(Banners $banners)

    {



        //dd($banners->nome);

        return view('painel.banner-info', [

            'page_name' => 'Painel Unyflex - Informações do Banner',

            'banner' => $banners

        ]);

    }



    public function cadBanner(Request $request)

    {

        

       // dd($request->file->getClientOriginalName());



        $banner = new Banners();

        $banner->nome = $request->nome;

        $banner->link = $request->link;

        $banner->status = $request->status;

        $banner->imagem =  $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();

    

        if ($banner->save()) {

            $request->file('file')->storeAs('public/banners/home', $name);

            return redirect()->route('painel-banners')->with('message', 'success');

        } else {

            return redirect()->route('painel-banners')->with('message', 'erro');

        }

    }



    public function updBanner(Banners $banners, Request $request)

    {



        $banners->nome = $request->nome;

        $banners->link = $request->link;

        $banners->status = $request->status;

        $banners->imagem =  $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();



        if ($banners->save()) {

            $request->file('file')->storeAs('public/banners/home', $name);

            return redirect()->route('painel-banners')->with('message', 'success');

        } else {

            return redirect()->route('painel-banners')->with('message', 'erro');

           }

    }



    public function destroyHotel(Hotel $hotel)

    {

        if ($hotel->delete()) {

            return redirect()->route('painel-hoteis')->with('message', 'teacher_deleted');

        } 

    }

}

