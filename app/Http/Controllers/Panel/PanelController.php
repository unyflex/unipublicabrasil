<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Panel;

use App\Models\VideoLesson;

use App\Models\VideoLessonn;

use App\Models\Course;

use App\Models\Classes;

use App\Models\Teacher;

use App\Models\TeacherPanels;

use App\Models\Horario;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;





class PanelController extends Controller

{

           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    

    public function formPainel($class)

    {

        $classes = Classes::where('id', $class)->first();

        $course_id=$classes->course_id;

        $courses= Course::where('id', $course_id)->get();

        $teachers = Teacher::all();

        $horarios= Horario::all();

       // dd($horarios);



        return view('painel.form-panel', [

            'page_name' => 'Painel Unyflex - Adicionar Painel',

            'class' => $classes,

            'teachers'=>$teachers,

            'horarios'=>$horarios,

            

        ]);

    }



    public function infoPainel(Panel $panel)

    {

        $video_lessons=VideoLesson::where('panel_id', $panel->id)->get();
        $video_lessonsid=VideoLesson::where('panel_id', $panel->id)->first();
        $video_lessonsid2=VideoLesson::where('panel_id', $panel->id)->skip(1)->first();

        $id1 = $video_lessonsid->id;
       // dd($video_lessonsid2);
        if($video_lessonsid2 != null){
        $id2 = $video_lessonsid2->id;
    }else{
        $id2=1;
    }
        
        $teachers = Teacher::all();

        $horarios= Horario::all();

       // dd($video_lessons);

        return view('painel.painel-info', [

            'page_name' => 'Painel Unyflex - Informações do Painel',

            'class' => $panel,

            'panel' => $panel,

            'teachers'=>$teachers,

            'horarios'=>$horarios,

            'video_lessons'=>$video_lessons, 

            'id1' => $id1,
            'id2' => $id2,

            

        ]);

    }



    public function cadPainel(Request $request)

    {



       

        $panel = new Panel();

        $panel->title = $request->titulo;

        $panel->subtitle = $request->subtitulo;

        $panel->subtitle = $request->subtitulo;

        $panel->status = $request->status;

        $panel->content=$request->conteudo;

        $panel->valor=$request->valor;

        $panel->classes_id=$request->class_id;

        $dataInicio = $request->dataInicio;

        $dataInicio2= date('Y-m-d', strtotime($dataInicio));

        $dataTermino = $request->dataTermino;

        $panel->start_time=$dataInicio2;

        $panel->horario=$request->horario;

        $panel->teacher_id=$request->docente;

      

       // dd($panel->id)

        if ($panel->save()) {

        

            if($request->unyflex==1){

               $video= new VideoLesson();

               $video->panel_id=$panel->id;

               $video->link=$request->link;

               $video->tasting_link=$request->degustacao;

               $video->source=$request->source;

               $video->status=$request->status;

               $video->save();

               if (isset($request->link2)) {

                $video= new VideoLesson();

                $video->panel_id=$panel->id;
 
                $video->link=$request->link2;
 
                $video->tasting_link=$request->degustacao2;
 
                $video->source=$request->source;
 
                $video->status=$request->status;
 
                $video->save();
                   
               }

                 

            }



            return redirect()->route('informacao-painel', ['panel' => $panel->id])->with('message', 'course_created');

        } else {

            return redirect()->route('informacao-painel')->with('message', 'course_create_error');

        }

    }



    public function updPainel(Panel $panel, VideoLesson $video, Request $request)

    {

            

        

        $idTurma = $request->class_id;

/*

        $validator = Validator::make($request->all(), [

            'titulo' => 'required|max:40',

            'subtitulo' => 'required|max:40',

            'status' => 'required',

            'dataInicio' => 'required',

            'dataTermino' => 'required'

        ]);



        if ($validator->fails()) {

            return redirect()->route('informacao-turma', ['classes' => $idTurma])->withErrors($validator);

        }



        //exclui todas as categorias do curso atual

        $teachersPanel = TeacherPanels::where('panel_id', $panel->id)->get();

        foreach ($teachersPanel as $teacherPanel) {

            $teacherPanel->delete();

        }



        //adiciona novas categorias selecionadas

        foreach ($request->docentes as $docente) {

            $teacherPanel = new TeacherPanels();

            $teacherPanel->panel_id = $panel->id;

            $teacherPanel->teacher_id = $docente;

            $teacherPanel->save();

        }*/

        $panel->title = $request->titulo;

        $panel->subtitle = $request->subtitulo;

        $panel->content = $request->conteudo;

        $panel->status = $request->status;

        $panel->teacher_id=$request->docente;

        $panel->start_time = inverteDataHora($request->dataInicio);

        $panel->horario =$request->horario;

        $panel->valor =$request->valor;



        

        if($request->unyflex==1){

                  

            $video->panel_id=$panel->id;

            $video->link=$request->link;

            $video->tasting_link=$request->degustacao;

            $video->source=$request->source;

            $video->status=$request->status;

            $video->save();

          

              

         }



        if ($panel->save()) {



        

                if($request->unyflex==1){

                  

                   $video->panel_id=$panel->id;

                   $video->link=$request->link;

                   $video->tasting_link=$request->degustacao;

                   $video->source=$request->source;

                   $video->status=$request->status;

                   $video->save();

                     

                }

            return redirect()->route('painel-cursos')->with('message', 'panel_updated');

        } else {

            return redirect()->route('painel-cursos')->with('message', 'panel_update_error');

        }

    }



    public function updPainelv(Panel $panel, VideoLessonn $video, VideoLesson $video2,  Request $request)

    {

            

        

        $idTurma = $request->class_id;


        $panel->title = $request->titulo;

        $panel->subtitle = $request->subtitulo;

        $panel->content = $request->conteudo;

        $panel->status = $request->status;

        $panel->teacher_id=$request->docente;

        $panel->start_time = inverteDataHora($request->dataInicio);

        $panel->horario =$request->horario;

        $panel->valor =$request->valor;


     //   dd($request->conteudo);
        

        if($request->unyflex==1){

          // dd($video);
         
          $video->panel_id=$panel->id;
 
          $video->link=$request->link1;

          $video->tasting_link=$request->degustacao1;

          $video->source=$request->source;

          $video->status=$request->status;

       
         
          $video->save();

            if ($video2->id !=1) {

              

                $video2->panel_id=$panel->id;
 
                $video2->link=$request->link2;
 
                $video2->tasting_link=$request->degustacao2;
 
                $video2->source=$request->source;
 
                $video2->status=$request->status;
 
                $video2->save();

                
                   
               }

             

         }



        if ($panel->save()) {



        

             

            return redirect()->route('painel-cursos')->with('message', 'panel_updated');

        } else {

            return redirect()->route('painel-cursos')->with('message', 'panel_update_error');

        }

    }









    public function destroyPainel(Panel $panel, Request $request)

    {

        $idTurma = $request->idTurma;



        if ($panel->delete()) {

            return redirect()->route('informacao-turma', ['classes' => $idTurma])->with('message', 'panel_deleted');

        } else {

            return redirect()->route('informacao-turma', ['classes' => $idTurma])->with('message', 'panel_delete_error');

        }

    }

}

