<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Course;

use App\Models\Horario;

use App\Models\Category;

use App\Models\Classes;
use App\Models\Question;
use App\Models\Alternative;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;



class TestController extends Controller

{

           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    

    public function show()

    {

        $questions = Question::orderBy('id', 'DESC')->get();
      

        return view('painel.test', [

            'page_name' => 'Painel Unyflex - Lista de cursos cadastrados',

            'questions' => $questions

        ]);

    }

    public function filtra(Request $request)

    {
        

        $filters = $request->search;

        $hoje=  date('Y-m-d');

        $questions = Question::where('text', 'LIKE',  "%{$request->search}%")->orderBy('id', 'DESC')->get();

           

            return view('painel.test', [

                'page_name' => 'Painel Unyflex - Lista de cursos cadastrados',
    
                'questions' => $questions
    
            ]);

    }



    public function formTest()

    {

        $classes = Classes::all();



        return view('painel.form-test', [

            'page_name' => 'Painel Unyflex - Adicionar Prova',

            'classes' => $classes

        ]);

    }



    public function infoTest(Question $question)

    {

        $alternatives = Alternative::where('idquestion', $question->id)->orderBy('id')->get();

        $classes = Classes::all();

       //dd($alternatives[1]);



        return view('painel.test-info', [

            'page_name' => 'Painel Unyflex - Informações do Curso',

            'alternatives' => $alternatives,

            'question' => $question,

            'classes' => $classes,

           

        ]);

    }



 
    public function cadTest(Request $request)
{
  
        $question = new Question();

        $question->text = $request->questao;

        $question->class_id = $request->idclass;

        
        
        if ($question->save()) {
        $alternative = new Alternative();

        $alternative->idquestion = $question->id;
        $alternative->text = $request->a;
        if($request->correta=='a'){
            $alternative->correct = 1;
        } 

        $alternative->save();

        
        $alternative = new Alternative();

        $alternative->idquestion = $question->id;
        $alternative->text = $request->b;
        if($request->correta=='b'){
            $alternative->correct = 1;
        } 

        $alternative->save();


        
        $alternative = new Alternative();

        $alternative->idquestion = $question->id;
        $alternative->text = $request->c;
        if($request->correta=='c'){
            $alternative->correct = 1;
        } 

        $alternative->save();


            return redirect()->route('informacao-prova', ['question' => $question->id])->with('message', 'course_created');

        } else {

            return redirect()->route('informacao-prova', ['question' => $question->id])->with('message', 'course_create_error');

        }

    }



    public function updTest(Question $question, Request $request)

    {

      

        //exclui todas as categorias do curso atual

        $alternatives = Alternative::where('idquestion', $question->id)->get();

        foreach ($alternatives as $alternative) {

            $alternative->delete();

        }



            $alternative = new Alternative();
    
            $alternative->idquestion = $question->id;
            $alternative->text = $request->a;
            if($request->correta=='a'){
                $alternative->correct = 1;
            } 
    
            $alternative->save();
    
            
            $alternative = new Alternative();
    
            $alternative->idquestion = $question->id;
            $alternative->text = $request->b;
            if($request->correta=='b'){
                $alternative->correct = 1;
            } 
    
            $alternative->save();
    
    
            
            $alternative = new Alternative();
    
            $alternative->idquestion = $question->id;
            $alternative->text = $request->c;
            if($request->correta=='c'){
                $alternative->correct = 1;
            } 
    
            $alternative->save();


        $question->text = $request->questao;

        $question->class_id = $request->idclass;

        if ($question->save()) {

            return redirect()->route('informacao-prova', ['question' => $question->id])->with('message', 'course_updated');

        } else {

            return redirect()->route('informacao-curso', ['question' => $question->id])->with('message', 'course_update_error');

        }

    }



    public function destroyCurso(Course $course)

    {

        if ($course->delete()) {

            return redirect()->route('painel-cursos')->with('message', 'course_deleted');

        } else {

            return redirect()->route('painel-cursos')->with('message', 'course_delete_error');

        }

    }



    public function horario()

    {

        return view('painel.form-horario', [

            'page_name' => 'Painel Unyflex - Adicionar Curso',

    

        ]);

    }



    public function cadHorario(Request $request)

    {

        //dd($request);

        $horario = new Horario();

        $horario->inicio = $request->inicio;

        $horario->fim = $request->fim;



        if ($horario->save()) {



          

        return redirect()->route('painel-cursos');

        } else {

            return redirect()->route('painel-cursos');

        }

    }

}

