<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\MaterialPanels;
use App\Models\Classes;
use App\Models\certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CertificateController extends Controller
{

           /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show()
    {
        $certificate = certificate::orderBy('id', 'DESC')->paginate();

        return view('painel.certificate', [
            'page_name' => 'Painel Unyflex - Lista de Certificados cadastrados',
            'certificates' => $certificate
        ]);
    }

    public function formCertificate()
    {
        return view('painel.form-certificate', [
            'page_name' => 'Painel Unyflex - Adicionar Certificado'
        ]);
    }

    public function infoCertificate(certificate $certidao)
    {
        $certidao = certificate::where('id', $certidao->id)->first();
       

        return view('painel.certificate-info', [
            'page_name' => 'Painel Unyflex - Informação do Certificado',
            'certificate' => $certidao,
          
        ]);
    }

    public function cadCertificate(Request $request)
    {


        

        $certificate = new certificate();
        $certificate->nome = $request->nome;
        $certificate->arquivo = $request->file->getClientOriginalName();
        $certificate->categoria = $request->categoria;
        $certificate->vencimento = $request->vencimento;
        $name=$request->file->getClientOriginalName();
        if ($certificate->save()) {
           
           $request->file('file')->storeAs('certificados',$name);
            return redirect()->route('painel-certidoes');
        } else {
            return redirect()->route('painel-certidoes');
        }
    }

    public function updCertificate(certificate $certificate, Request $request)
    {
        
        

        if ($request->arquivo == null) {

            $certificate->nome = $request->nome;
            $certificate->categoria = $request->categoria;
            $certificate->vencimento = $request->vencimento;

            if ($certificate->save()) {
                return redirect()->route('informacao-material', ['certidao' => $certificate->id])->with('message', 'material_updated');
            } else {
                return redirect()->route('adicionar-material', ['certidao' => $certificate->id])->with('message', 'material_updated_error');
            }
        }

        $certificate->nome = $request->nome;
        $certificate->arquivo = $request->arquivo->getClientOriginalName();
        $certificate->categoria = $request->categoria;
        $certificate->vencimento = $request->vencimento;
        $name=$request->arquivo->getClientOriginalName();

        if ($certificate->save()) {
            $request->file('arquivo')->storeAs('certificados',$name);
            return redirect()->route('informacao-certidao', ['certidao' => $certificate->id])->with('message', 'material_updated');
        } else {
            return redirect()->route('adicionar-certidao', ['certidao' => $certificate->id])->with('message', 'material_updated_error');
        }
    }

   
    public function destroyCertificate(certificate $certificate)
    {
        if ($certificate->delete()) {
            return redirect()->route('painel-certidoes')->with('message', 'material_deleted');
        } else {
            return redirect()->route('painel-certidoes')->with('message', 'material_delete_error');
        }
    }
}
