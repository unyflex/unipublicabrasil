<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Hotel;

use App\Models\Photos;

use App\Models\Classes;

use App\Models\Banners;

use App\Models\Package;

use App\Models\Galery;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;





class GaleryController extends Controller

{



           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    public function show()

    {

        $galerias = Galery::where('status', 'able')->with('classes')->get();

        $desabilitados = Banners::where('status', 'disabled')->get();

       

       



        return view('painel.galery', [

            'page_name' => 'Painel Unyflex - Lista de Banners cadastrados',

            'galerias' => $galerias,

            'desabilitados'=> $desabilitados

        ]);

    }



    public function formGalery()

    {

        $classes = Classes::where('status', 'able')->get();

        return view('painel.form-galery', [

            'page_name' => 'Painel Unyflex - Adicionar galeria',

            'classes'=> $classes

        ]);

    }



    public function infoGalery(Galery $galery)

    {

        $classes = Classes::where('status', 'able')->get();

        //dd($banners->nome);

        return view('painel.galery-info', [

            'page_name' => 'Painel Unyflex - Informações do Banner',

            'classes'=> $classes,

            'galery' => $galery

        ]);

    }





    public function fotosAdd(Galery $galery)

    {

        $photos = Photos::where('id_galeria', $galery->id)->get();

        $galerias = Galery::where('status', 'able')->with('classes')->get();

        //dd($banners->nome);

        return view('painel.add-photos', [

            'page_name' => 'Painel Unyflex - Informações do Banner',

            'photos'=> $photos,

            'galery' => $galery

        ]);

    }



    public function cadPhotos(Request $request)

    {

        

       

        $files=$request->files;

       //  dd($files);

      

         foreach($files as $file){

          $i=0;

            foreach($file as $fil){

             $image=$fil;

       

            

             $name=$image->getClientOriginalName();

           //  $uploadfiles=$image->storeAs('galeria/fotos', $name);

             $photos = new Photos();

             $photos->id_galeria=$request->galeria;

             $photos->foto=$name;

             $request->file('file')[$i]->storeAs('public/galeria/fotos', $name);

             $photos->save();

             $i++;

         }}



    

        

            

            return redirect()->route('painel-galerias')->with('message', 'success');

        

    }



    public function cadGalery(Request $request)

    {

        

       // dd($request->file->getClientOriginalName());



        $galery = new Galery();

        $galery->id_class = $request->curso;

        $galery->status = $request->status;

        $galery->capa =  $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();

    

        if ($galery->save()) {

            $request->file('file')->storeAs('public/galeria/capas', $name);

            return redirect()->route('painel-galerias')->with('message', 'success');

        } else {

            return redirect()->route('painel-galerias')->with('message', 'erro');

        }

    }



    public function updGalery(Galery $galery, Request $request)

    {



        $galery->id_class = $request->curso;

        $galery->status = $request->status;

        $galery->capa =  $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();


        

        if ($galery->save()) {

            $request->file('file')->storeAs('public/galeria/capas', $name);

            return redirect()->route('painel-galerias')->with('message', 'success');

        } else {

            return redirect()->route('painel-galerias')->with('message', 'erro');

           }

    }



    public function destroyHotel(Hotel $hotel)

    {

        if ($hotel->delete()) {

            return redirect()->route('painel-hoteis')->with('message', 'teacher_deleted');

        } 

    }

}

