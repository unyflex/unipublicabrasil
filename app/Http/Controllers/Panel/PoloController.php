<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Models\Package;
use App\Models\Polo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PoloController extends Controller
{

           /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show()
    {
        $polos = Polo::all();
        //dd($hoteis);

        return view('painel.polo', [
            'page_name' => 'Painel Unyflex - Lista de polos cadastrados',
            'polos' => $polos
        ]);
    }

    public function formPolo()
    {
        return view('painel.form-polo', [
            'page_name' => 'Painel Unyflex - Adicionar polo'
        ]);
    }

    public function infoPolo(Polo $polo)
    {
       
        return view('painel.polo-info', [
            'page_name' => 'Painel Unyflex - Informações do Polo',
            'polo' => $polo
           
        ]);
    }

    public function cadPolo(Request $request)
    {
        

        $polo = new Polo();
        $polo->cidade = $request->cidade;
        $polo->rua = $request->rua;
        $polo->bairro = $request->bairro;
        $polo->numero = $request->numero;
        $polo->status = $request->status;
        $polo->uf = $request->uf;
        

    
        if ($polo->save()) {

          
            return redirect()->route('informacao-polo', ['polo' => $polo->id])->with('message', 'success');
        } else {
            return redirect()->route('informacao-polo', ['polo' => $polo->id])->with('message', 'erro');
        }
    }

    public function updPolo(Polo $polo, Request $request)
    {

        $polo->cidade = $request->cidade;
        $polo->rua = $request->rua;
        $polo->bairro = $request->bairro;
        $polo->numero = $request->numero;
        $polo->status = $request->status;
        $polo->uf = $request->uf;

        if ($polo->save()) {
           
            return redirect()->route('informacao-polo', ['polo' => $polo->id])->with('message', 'teacher_updated');
        } else {
            return redirect()->route('informacao-polo', ['polo' => $polo->id])->with('message', 'teacher_update_error');
        }
    }

    public function destroyPolo(Polo $polo)
    {
        if ($polo->delete()) {
            return redirect()->route('painel-polos');
        } 
    }
}
