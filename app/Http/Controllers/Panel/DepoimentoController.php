<?php



namespace App\Http\Controllers\Panel;



use App\Http\Controllers\Controller;

use App\Models\Depositions;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;





class DepoimentoController extends Controller

{



           /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }

    

    public function show()

    {

        $depositions = Depositions::all();



        return view('painel.depoimentos', [

            'page_name' => 'Painel Unyflex - Lista de professores Depoimentos',

            'depositions' => $depositions

        ]);

    }



    public function formDepoimento()

    {

        return view('painel.form-depoimento', [

            'page_name' => 'Painel Unyflex - Adicionar Professor'

        ]);

    }



    public function infoDepoimento(Depositions $depositions)

    {

        return view('painel.depoimento-info', [

            'page_name' => 'Painel Unyflex - Informações do Depoimento',

            'deposition' => $depositions

        ]);

    }



    public function cadDepoimento(Request $request)

    {

        



        $deposition = new Depositions();

        $deposition->nome = $request->nome;

        $deposition->cidade = $request->cidade;

        $deposition->cargo = $request->cargo;

        $deposition->depoimento = $request->depoimento;

        $deposition->status = $request->status;

        $deposition->foto = $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();

        

        if ($deposition->save()) {

           $request->file('file')->storeAs('public/depoimentos', $name);

            return redirect()->route('painel-depoimentos');



        } else {

            return redirect()->route('painel-depoimentos');

              }

    }



    public function updDepoimento(Depositions $depositions, Request $request)

    {



        $depositions->nome = $request->nome;

        $depositions->cidade = $request->cidade;

        $depositions->cargo = $request->cargo;

        $depositions->depoimento = $request->depoimento;

        $depositions->status = $request->status;

       

        $depositions->foto = $request->file->getClientOriginalName();

        $name=$request->file->getClientOriginalName();



        if ($depositions->save()) {

            $request->file('file')->storeAs('public/depoimentos', $name);

            return redirect()->route('painel-depoimentos');



        } else {

            return redirect()->route('painel-depoimentos');

              }

    }



    public function destroyDepoimento(Depositions $depositions)

    {

        if ($depositions->delete()) {

            return redirect()->route('painel-depoimentos')->with('message', 'teacher_deleted');

        } 

    }

}

