<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Degree extends Model

{

    use HasFactory;



    protected $table = 'degree';



    protected $fillable = [

        'classes_id',

        'title',

        'subtitle',

        'content'

    ];





    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'created_at',

        'updated_at',

    ];





    public function students()

    {

        return $this->hasOne(Student::class, 'id', 'id_aluno', );

    }

    public function classes()

    {

        return $this->hasOne(Classes::class, 'id', 'id_curso', );

    }





}
