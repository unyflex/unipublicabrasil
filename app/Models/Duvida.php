<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Duvida extends Model

{

    

    

    use HasFactory;



    protected $table = 'duvida';



   

    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'created_at',

        'updated_at',

    ];



    
    public function student()

    {

        return $this->hasOne(Student::class, 'id', 'id_student', );

    }

    public function class()

    {

        return $this->hasOne(Classes::class, 'id', 'id_classes', );

    }

    

}

