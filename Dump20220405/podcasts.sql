-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 11-Abr-2022 às 16:05
-- Versão do servidor: 10.4.22-MariaDB
-- versão do PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `teste1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `podcasts`
--

CREATE TABLE `podcasts` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `status` enum('able','disabled') NOT NULL,
  `professor` bigint(20) UNSIGNED NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `podcasts`
--

INSERT INTO `podcasts` (`id`, `titulo`, `descricao`, `status`, `professor`, `arquivo`, `created_at`, `updated_at`) VALUES
(1, 'TESTE 01', 'Podcast de teste cadastrado diretamente do banco de dados', 'able', 1, 'teste.mp3', '2022-04-11 14:44:01', '2022-04-11 14:44:01'),
(2, 'Podcast 02 teste', 'Teste sem o arquivo correto ainda', 'able', 1, 'ANCHI_REL_PLANO_ENSINO_POR_CURSO_E_DISCIPLINA.pdf', '2022-04-11 10:11:47', '2022-04-11 10:11:47'),
(3, 'Teste 03 de podcast', 'Cadastrando corretamente', 'able', 1, 'regularidade_fgts.pdf', '2022-04-11 10:19:58', '2022-04-11 10:19:58');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `podcasts`
--
ALTER TABLE `podcasts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teachers_podcast` (`professor`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `podcasts`
--
ALTER TABLE `podcasts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `podcasts`
--
ALTER TABLE `podcasts`
  ADD CONSTRAINT `teachers_podcast` FOREIGN KEY (`professor`) REFERENCES `teachers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;