-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 198.136.59.231    Database: unipubli_novo_painel
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_06_02_102425_create_categories_table',1),(5,'2021_06_11_155757_create_courses_table',1),(6,'2021_06_28_164007_create_students_table',1),(7,'2021_06_28_164614_create_classes_table',1),(8,'2021_06_29_113058_create_category_courses_table',1),(9,'2021_07_01_171103_create_video_lessons_table',1),(10,'2021_07_07_103212_create_materials_table',1),(11,'2021_07_07_150849_create_enrollments_table',1),(12,'2021_07_09_171446_create_enrollment_observations_table',1),(13,'2021_07_13_143728_create_questions_table',1),(14,'2021_07_13_145458_create_question_categories_table',1),(15,'2021_07_13_153530_create_alternatives_table',1),(16,'2021_07_13_165337_create_teachers_table',1),(17,'2021_08_06_092748_add_modality',1),(18,'2021_08_06_151032_add_password',1),(19,'2021_08_13_091654_create_subscriptions_table',1),(20,'2021_08_18_155937_create_subscription_payments_table',1),(21,'2021_08_19_161120_add_on_delete_subscription_payment',1),(22,'2021_09_01_164643_create_panels_table',1),(23,'2021_09_06_095901_create_video_lessons_panels_table',1),(24,'2021_09_07_110112_create_material_panels_table',1),(25,'2021_09_13_144555_create_question_panels_table',1),(26,'2021_09_13_165923_create_teacher_panels_table',1),(27,'2021_10_26_142844_add_workload',1),(28,'2022_01_03_205936_create_finance_table',1),(29,'2022_01_03_212425_create_flow_table',1),(30,'2014_10_12_200000_add_two_factor_columns_to_users_table',2),(31,'2019_12_14_000001_create_personal_access_tokens_table',2),(32,'2020_05_21_100000_create_teams_table',2),(33,'2020_05_21_200000_create_team_user_table',2),(34,'2020_09_24_002747_create_sessions_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05  9:57:03
