-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 198.136.59.231    Database: unipubli_novo_painel
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Título da Categoria',
  `status` enum('able','disabled') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'disabled' COMMENT 'Status da Categoria',
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (2,'Servidores Federais','able','servidores-federais','2022-02-01 12:03:25','2022-02-01 12:03:25'),(3,'RH Municipal','able','rh-municipal','2022-02-01 12:03:44','2022-02-01 12:03:44'),(4,'Jurídico','able','juridico','2022-02-01 12:04:22','2022-02-01 12:04:22'),(5,'Licitações Públicas','able','licitacoes-publicas','2022-02-01 12:04:42','2022-02-01 12:04:42'),(6,'Controle Interno','able','controle-interno','2022-02-01 12:05:04','2022-02-01 12:05:04'),(7,'Tributação Municipal','able','tributacao-municipal','2022-02-01 12:05:35','2022-02-01 12:05:35'),(8,'Assessoria','able','assessoria','2022-02-01 12:05:49','2022-02-01 12:05:49'),(9,'Secretarias Municipais','able','secretarias-municipais','2022-02-01 12:06:16','2022-02-01 12:06:16'),(10,'Legislativo','able','legislativo','2022-02-01 12:06:28','2022-02-01 12:06:28'),(11,'Finanças Municipais','able','financas-municipais','2022-02-01 12:06:38','2022-02-01 12:06:38'),(12,'Contadores Municipais','able','contadores-municipais','2022-02-01 12:06:54','2022-02-01 12:06:54'),(13,'Profissionais de saúde','able','profissionais-de-saude','2022-02-01 12:07:10','2022-02-01 12:07:10'),(14,'Gestão Ambiental','able','gestao-ambiental','2022-02-01 12:07:20','2022-02-01 12:07:20'),(15,'Patrimônio','able','patrimonio','2022-02-01 12:07:34','2022-02-01 12:07:34'),(16,'Consórcio','able','consorcio','2022-02-01 12:07:50','2022-02-01 12:07:50'),(17,'Autarquia','able','autarquia','2022-02-01 12:08:04','2022-02-01 12:08:04');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05  9:55:47
