-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 198.136.59.231    Database: unipubli_novo_painel
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_galeria` int unsigned NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_galeria` (`id_galeria`),
  CONSTRAINT `id_galeria` FOREIGN KEY (`id_galeria`) REFERENCES `galery` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (31,1,'369.png','2022-03-11 14:56:18','2022-03-11 14:56:18'),(32,1,'370.png','2022-03-11 14:56:18','2022-03-11 14:56:18'),(33,1,'368.png','2022-03-11 14:56:18','2022-03-11 14:56:18'),(34,1,'controle interno 367.png','2022-03-11 14:56:18','2022-03-11 14:56:18'),(35,2,'Card Site Uniflex - adv municipal.png','2022-03-11 14:58:10','2022-03-11 14:58:10'),(36,2,'Finanças e tesouraria - card Unyflex.png','2022-03-11 14:58:11','2022-03-11 14:58:11'),(37,2,'granvile.png','2022-03-11 14:58:11','2022-03-11 14:58:11'),(38,2,'Card Site Uniflex - novo e social.png','2022-03-11 14:58:11','2022-03-11 14:58:11'),(39,3,'WhatsApp Image 2022-02-24 at 21.08.54 (2).jpeg','2022-03-11 15:05:51','2022-03-11 15:05:51'),(40,3,'WhatsApp Image 2022-02-24 at 21.08.53.jpeg','2022-03-11 15:05:51','2022-03-11 15:05:51'),(41,3,'WhatsApp Image 2022-02-24 at 21.08.54 (1).jpeg','2022-03-11 15:05:51','2022-03-11 15:05:51'),(42,3,'WhatsApp Image 2022-02-24 at 21.08.54.jpeg','2022-03-11 15:05:51','2022-03-11 15:05:51'),(43,2,'gran.png','2022-03-14 11:36:22','2022-03-14 11:36:22');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05  9:57:33
