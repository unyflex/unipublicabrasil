-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 198.136.59.231    Database: unipubli_novo_painel
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category_courses`
--

DROP TABLE IF EXISTS `category_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_courses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint unsigned NOT NULL,
  `course_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_courses_category_id_foreign` (`category_id`),
  KEY `category_courses_course_id_foreign` (`course_id`),
  CONSTRAINT `category_courses_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_courses_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_courses`
--

LOCK TABLES `category_courses` WRITE;
/*!40000 ALTER TABLE `category_courses` DISABLE KEYS */;
INSERT INTO `category_courses` VALUES (2,12,2,'2022-02-01 12:10:51','2022-02-01 12:10:51'),(3,3,3,'2022-02-03 12:23:39','2022-02-03 12:23:39'),(4,9,3,'2022-02-03 12:23:39','2022-02-03 12:23:39'),(5,7,3,'2022-02-03 12:23:39','2022-02-03 12:23:39'),(6,12,3,'2022-02-03 12:23:39','2022-02-03 12:23:39'),(7,11,3,'2022-02-03 12:23:39','2022-02-03 12:23:39'),(8,12,4,'2022-02-03 12:54:58','2022-02-03 12:54:58'),(9,15,4,'2022-02-03 12:54:58','2022-02-03 12:54:58'),(10,9,4,'2022-02-03 12:54:58','2022-02-03 12:54:58'),(11,6,5,'2022-02-03 13:30:16','2022-02-03 13:30:16'),(12,3,5,'2022-02-03 13:30:16','2022-02-03 13:30:16'),(13,11,6,'2022-02-03 16:54:20','2022-02-03 16:54:20'),(14,12,6,'2022-02-03 16:54:20','2022-02-03 16:54:20'),(15,4,7,'2022-03-07 14:00:32','2022-03-07 14:00:32'),(16,15,7,'2022-03-07 14:00:32','2022-03-07 14:00:32'),(17,5,8,'2022-03-15 11:41:31','2022-03-15 11:41:31'),(18,9,8,'2022-03-15 11:41:31','2022-03-15 11:41:31');
/*!40000 ALTER TABLE `category_courses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05  9:56:24
