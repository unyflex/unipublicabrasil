-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 198.136.59.231    Database: unipubli_novo_painel
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `course_id` bigint unsigned NOT NULL,
  `unyflex` int unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Título da Turma',
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Subtítulo da Turma',
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unypublica',
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL COMMENT 'Data de início',
  `end_date` date NOT NULL COMMENT 'Data de Término',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tipo da Turma',
  `workload` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Carga Horária da Turma',
  `live` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('able','disabled') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'disabled' COMMENT 'Status da Turma',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `id_polo` int NOT NULL,
  `polo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `classes_course_id_foreign` (`course_id`),
  KEY `id_polo` (`id_polo`),
  CONSTRAINT `classes_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`id_polo`) REFERENCES `poles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (2,2,0,'Portal, Ouvidoria e LGPD','Estudo Prático','portal-ouvidoria-lgpd','bef66d6730b491d4ec64fd213f98e4c413122021190124.png','2022-04-15','2022-04-12','Premium','18',0,'able',1,2,'Curitiba-PR','2022-02-01 12:12:26','2022-02-03 12:04:00'),(3,3,0,'Lei Orgânica Municipal','Curso Completo','lei-organica-municipal','NOVA LICITAÇÃO ENTENDER E IMPLANTAR A 14.13321.png','2022-05-03','2022-05-03','Premium','24',0,'able',1,2,'Curitiba-PR','2022-02-03 12:27:26','2022-03-18 17:51:13'),(4,4,0,'Dívida Ativa e Gestão Tributária','Preparando as Receitas para 2022','divida-ativa','Assessoria Governamental.png','2022-08-03','2022-12-03','Premium','25',0,'able',1,2,'Curitiba-PR','2022-02-03 12:56:07','2022-02-03 12:56:07'),(5,5,0,'Controle Interno','Recomendações para Encerrar o Exercício','controle-interno','Controladores Municipais.png','2022-12-01','2022-12-31','Premium','36',0,'able',1,2,'Curitiba-PR','2022-02-03 13:31:13','2022-02-03 13:46:04'),(6,6,0,'Finanças, Contábeis e Tesouraria','Apontamentos em Final de Exercício','financas-contabeis-tesouraria','fb5c81ed3a220004b71069645f11286718022022232325.png','2022-05-13','2022-02-26','Premium','88',0,'able',1,2,'Curitiba-PR','2022-02-03 16:55:42','2022-03-18 18:01:20'),(9,7,1,'Turma de Gestão Online','UNYFLEX','gestao-online','Card UNYFLEX (2)-depositphotos-bgremover.png','2022-03-03','2022-04-03','Premium','9',1,'able',1,2,'Curitiba-PR','2022-03-07 14:01:27','2022-03-07 14:01:27'),(10,8,1,'Licitação 2022','Municipal','licitacao-municipal','365.png','2022-12-01','2022-12-10','Premium','24',1,'able',1,2,'Curitiba-PR','2022-03-15 11:45:28','2022-03-15 11:51:19'),(11,8,1,'Nova Licitação 2023','Turma Especial','licitacao-turma-especial','328.png','2022-05-04','2022-08-04','Premium','16',1,'able',1,2,'Curitiba-PR','2022-03-16 16:09:08','2022-03-16 16:09:08'),(12,2,1,'Portal, Ouvidoria e LGPD','No município','portal-ouvidoria-municipal','4.png','2022-05-04','2022-09-04','Premium','20',1,'able',1,2,'Curitiba-PR','2022-03-16 16:20:36','2022-03-16 16:20:36');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05  9:56:10
