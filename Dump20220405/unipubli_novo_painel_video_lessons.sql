-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 198.136.59.231    Database: unipubli_novo_painel
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `video_lessons`
--

DROP TABLE IF EXISTS `video_lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `video_lessons` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Link da Videoaula',
  `tasting_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Link de Degustação',
  `source` enum('youtube','vimeo') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'youtube' COMMENT 'Servidor de Origem',
  `subtitle` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'Legenda da videoaula',
  `status` enum('able','disabled') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'disabled' COMMENT 'Status da Videoaula',
  `panel_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_id` (`panel_id`),
  CONSTRAINT `panel_id` FOREIGN KEY (`panel_id`) REFERENCES `panels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_lessons`
--

LOCK TABLES `video_lessons` WRITE;
/*!40000 ALTER TABLE `video_lessons` DISABLE KEYS */;
INSERT INTO `video_lessons` VALUES (1,'https://www.youtube.com/embed/-L2VcJT3pdc','https://www.youtube.com/watch?v=QcZA3qmpi18','youtube',NULL,'able',34,'2022-03-07 14:12:08','2022-03-07 14:48:13'),(2,'https://www.youtube.com/embed/-L2VcJT3pdc','https://www.youtube.com/watch?v=QcZA3qmpi18','youtube',NULL,'able',35,'2022-03-07 14:17:09','2022-03-07 14:17:09'),(7,'https://www.youtube.com/embed/9EZfyjHSTB8','https://www.youtube.com/embed/ZNuZvNrdjs0','youtube',NULL,'able',36,'2022-03-16 16:05:26','2022-03-16 16:05:26'),(8,'https://www.youtube.com/embed/NwZ3oZh1oUI','https://www.youtube.com/embed/-W0UxnGAnPQ','youtube',NULL,'able',37,'2022-03-16 16:06:09','2022-03-16 16:06:09'),(9,'https://www.youtube.com/embed/E19W4PMxyz0','https://www.youtube.com/embed/_PnbNPe125w','youtube',NULL,'able',38,'2022-03-16 16:06:48','2022-03-16 16:06:48'),(10,'https://www.youtube.com/embed/SfeZR8o1qfo','https://www.youtube.com/embed/gGjCY5CF1K4','youtube',NULL,'able',39,'2022-03-16 16:09:59','2022-03-16 16:09:59'),(11,'https://www.youtube.com/embed/7C-uV977vPY','https://www.youtube.com/embed/SEPQ-pLEHPY','youtube',NULL,'able',40,'2022-03-16 16:10:49','2022-03-16 16:10:49'),(12,'https://www.youtube.com/embed/c0lf6V-vKeU','https://www.youtube.com/embed/vHUCgGi8izU','youtube',NULL,'able',41,'2022-03-16 16:16:02','2022-03-16 16:16:02'),(13,'https://www.youtube.com/embed/3nVEQORugjM','https://www.youtube.com/embed/by6jNGjRSfQ','youtube',NULL,'able',42,'2022-03-16 16:21:53','2022-03-16 16:21:53'),(14,'https://www.youtube.com/embed/-L2VcJT3pdc','https://www.youtube.com/embed/TtiEEAku3uE','youtube',NULL,'able',43,'2022-03-16 16:23:19','2022-03-16 16:23:19'),(15,'https://www.youtube.com/embed/x5WyV8e1RjY','https://www.youtube.com/embed/SSlRiuqX-Qc','youtube',NULL,'able',44,'2022-03-16 16:25:48','2022-03-16 16:25:48'),(16,'https://www.youtube.com/embed/KGUc1arzwyE','https://www.youtube.com/embed/rmukBAWj5Hk','youtube',NULL,'able',45,'2022-03-16 16:26:57','2022-03-16 16:26:57');
/*!40000 ALTER TABLE `video_lessons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05  9:56:01
