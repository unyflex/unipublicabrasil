function validaCampos(){
    
    var nomeAluno = document.getElementById("inputnome").value;
    var nomeEmpresa = document.getElementById("inputempresa").value;
    var emailEmpresa = document.getElementById("inputemail").value;
    var telefoneEmpresa = document.getElementById("inputtelefone").value;

    var padrao = /[^a-zà-ú ]/gi;
    var padraonum = /[^0-9]/;

    var valida_nome = nomeAluno.match(padrao);

    var validado = 1;

    if( valida_nome || !nomeAluno ){
        alert("Nome possui caracteres inválidos ou é vazio");
        validado = 0;
    }

    
    if(!nomeEmpresa){
        alert("O campo Empresa possui caracteres inválidos ou é vazio");
        validado = 0;
    }
    
    if(!emailEmpresa){
        alert("E-mail possui caracteres inválidos ou é vazio");
        validado = 0;
    }

    if(!telefoneEmpresa){
        alert("Telefone possui caracteres inválidos ou é vazio");
        validado = 0;
    }

    if(validado == 1){
        $('#FormCorporativo').submit();

    }

}