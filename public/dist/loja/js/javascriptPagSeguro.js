var Root = "https://" + document.location.hostname + "/";
var Amount = document.getElementById("ValorTotal").value;
var JurosParcelas = document.getElementById("JurosParcelas").value;

//INICIANDO A SEÇÃO DO PAGSEGURO
function iniciarSessao() {
    $.ajax({
        url: Root + "class/ControlerPagSeguro.php",
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            PagSeguroDirectPayment.setSessionId(data.id);
        },
        error: function(response) {
            alert("deu bad!");
        console.log(response);
        },
    });
}

iniciarSessao();


//VALIDANDO OS CAMPOS COM JQUERY
$(document).ready(function () {
    $("input").blur(function () {
        if($(this).val() == "")
        {
            $(this).css({"border-color" : "#F00"});
        }
        if($(this).val() != "")
        {
            $(this).css({"border-color" : "#ced4da"});
        }    
    });
})

$("#PagSeguro").click(function(){

    var tipopag = document.getElementById('exampleFormControlSelect1').value;

    if(tipopag == 'cartao')
    {
        var cont = 0;
        $("#FormPagseguro input").each(function(){
            if($(this).val() == "")
            {
                $(this).css({"border" : "1px solid #F00",});
                cont++;
            }
        });
        if(cont != 0)
        {
            alert('É necessario preencher todos os campos em vermelho!'); 
        }
        if(cont == 0)
        {
            getTokenCard(); 
        }
    }

    if(tipopag == 'boleto')
    {
        var nomeAluno = document.getElementById("inputprimeironome").value;
        var sobrenomeAluno = document.getElementById("inputsobrenome").value;
        var cpfAluno = document.getElementById("inputcpf").value;
        var telefoneAluno = document.getElementById("inputtelefone").value;
        var emailAluno = document.getElementById("inputemail").value;
        var cidadeAluno = document.getElementById("inputcidade").value;
        var enderecoAluno =  document.getElementById("inputendereco").value;
        var numAluno =  document.getElementById("inputnumero").value;
        var bairroAluno =  document.getElementById("inputbairro").value;
        var ufAluno =  document.getElementById("inputuf").value;
        var cepAluno = document.getElementById("inputcep").value; 
        var termosAluno = document.getElementById("inputtermos");  

        var padrao = /[^a-zà-ú ]/gi;
        var padraonum = /[^0-9]/;

        var valida_nome = nomeAluno.match(padrao);
        var valida_sobrenome = sobrenomeAluno.match(padrao);
        var valida_cidade = cidadeAluno.match(padrao);
        var valida_num = numAluno.match(padraonum);
        
        var validado = 1;

        if( valida_nome || !nomeAluno ){
            alert("Nome possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if( valida_sobrenome || !sobrenomeAluno ){
            alert("Sobrenome possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!cpfAluno){
            alert("CPF possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!telefoneAluno){
            alert("Telefone possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!emailAluno){
            alert("E-mail possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!enderecoAluno ){
            alert("Endereço possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if( valida_num || !numAluno ){
            alert("Número possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if( valida_cidade || !cidadeAluno ){
            alert("Cidade possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!bairroAluno ){
            alert("Bairro possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!ufAluno ){
            alert("UF possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!cepAluno ){
            alert("CEP possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(termosAluno.checked == false ){
            alert("Você deve aceitar os termos de condições de matrícula.");
            validado = 0;
        }
        
        if(validado == 1){
            $( "#PagSeguro" ).prop( "disabled", true );
            $('#FormPagseguro').submit();
        }

    }
    if(tipopag == 'boletoMensal')
    {
        var nomeAluno = document.getElementById("inputprimeironome").value;
        var sobrenomeAluno = document.getElementById("inputsobrenome").value;
        var cpfAluno = document.getElementById("inputcpf").value;
        var telefoneAluno = document.getElementById("inputtelefone").value;
        var emailAluno = document.getElementById("inputemail").value;
        var cidadeAluno = document.getElementById("inputcidade").value;
        var enderecoAluno =  document.getElementById("inputendereco").value;
        var numAluno =  document.getElementById("inputnumero").value;
        var bairroAluno =  document.getElementById("inputbairro").value;
        var ufAluno =  document.getElementById("inputuf").value;
        var cepAluno = document.getElementById("inputcep").value; 
        var termosAluno = document.getElementById("inputtermos");  

        var padrao = /[^a-zà-ú ]/gi;
        var padraonum = /[^0-9]/;

        var valida_nome = nomeAluno.match(padrao);
        var valida_sobrenome = sobrenomeAluno.match(padrao);
        var valida_cidade = cidadeAluno.match(padrao);
        var valida_num = numAluno.match(padraonum);
        
        var validado = 1;

        if( valida_nome || !nomeAluno ){
            alert("Nome possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if( valida_sobrenome || !sobrenomeAluno ){
            alert("Sobrenome possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!cpfAluno){
            alert("CPF possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!telefoneAluno){
            alert("Telefone possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!emailAluno){
            alert("E-mail possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!enderecoAluno ){
            alert("Endereço possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if( valida_num || !numAluno ){
            alert("Número possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if( valida_cidade || !cidadeAluno ){
            alert("Cidade possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!bairroAluno ){
            alert("Bairro possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!ufAluno ){
            alert("UF possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(!cepAluno ){
            alert("CEP possui caracteres inválidos ou é vazio");
            validado = 0;
        }

        if(termosAluno.checked == false ){
            alert("Você deve aceitar os termos de condições de matrícula.");
            validado = 0;
        }
        
        if(validado == 1){
            $( "#PagSeguro" ).prop( "disabled", true );
            $('#FormPagseguro').submit();
        }

    }
    if(tipopag == '0')
    {
        
        alert('Por Favor escolha uma forma de pagamento.'); 
    }
});





//INICIANDO A FUNÇÃO TOKEN DO CARTAO
//    $('#PagSeguro').on('click',function(){
//       getTokenCard(); 
//    });


//PEGANDO O TOKEN DO CARTÃO

function getTokenCard(){

    var vencimentoData = document.getElementById('expirationdate').value;
    var arrayData = vencimentoData.split("/");
    var MesCartao = arrayData[0];
    var AnoCartaoInco = arrayData[arrayData.length - 1];
    var AnoCartao = "20"+AnoCartaoInco;
    var cardnumber = $('#cardnumber').val();
    while (cardnumber.indexOf(' ') != -1) cardnumber = cardnumber.replace(' ', '');
    document.getElementById('cardnumber').value = cardnumber;
    document.getElementById('MesCartao').value = MesCartao;
    document.getElementById('AnoCartao').value = AnoCartao;

    PagSeguroDirectPayment.createCardToken({
        cardNumber: $('#cardnumber').val(), // Número do cartão de crédito
        brand: $('#BandeiraCartao').val(), // Bandeira do cartão
        cvv: $('#securitycode').val(), // CVV do cartão
        expirationMonth: $('#MesCartao').val(), // Mês da expiração do cartão
        expirationYear: $('#AnoCartao').val(), // Ano da expiração do cartão, é necessário os 4 dígitos.
        success: function(response) {
            $('#TokenCard').val(response.card.token);
            document.getElementById("FormPagseguro").submit()
        },
        error: function(response) {
            alert("Dados do Cartão Incorretos e ou Cartão Inexistente!");
            console.log(response);
            alert($('#cardnumber').val().replace(""," "));
        },
    });
}

//PEGANDO AUTOMATICAMENTE A BANDEIRA DO CARTÃO
$('#cardnumber').on('blur',function(){
    var cardnumber=$(this).val();
    var QtdCaracteres=cardnumber.length;


    while (cardnumber.indexOf(' ') != -1) cardnumber = cardnumber.replace(' ', '');
    PagSeguroDirectPayment.getBrand({
        cardBin: cardnumber,
        success: function(response) {
            var bandeiranome=response.brand.name;
            $("#BandeiraCartao").val(bandeiranome);
            getParcelas(bandeiranome);
        },
        error: function(response) {
            alert('Cartão não reconhecido ou inexistente!');
            $('.bandeiranum').empty();
        }
    }); 



});

//EXIBE A QUANTIDADE E VALORES DAS PARCELAS DO PAGAMENTO
function getParcelas(Bandeira){
    PagSeguroDirectPayment.getInstallments({
        amount: Amount,
        maxInstallmentNoInterest: JurosParcelas,
        brand: Bandeira,
        success: function(response)
        {
            $.each(response.installments,function(i,obj){
                $.each(obj,function(i2,obj2){
                    var NumberValue=obj2.installmentAmount;
                    var Number= "R$ "+ NumberValue.toFixed(2).replace(".",",");
                    var NumberParcelas= obj2.quantity+NumberValue.toFixed(2);
                    $('#QtdParcelas').show().append("<option value='"+obj2.quantity+"' >"+obj2.quantity+" parcelas de "+Number+"</option>");
                    //$('#QtdParcelas').show().append("<option value='"+obj2.quantity+"' label='"+obj2.quantity+' parcelas de '+NumberParcelas+"' >"+obj2.quantity+" parcelas de "+Number+"</option>");
                    
                    
                });
            });
        },
        error: function(response) {
            console.log(response);
            alert('Deu bad' +Amount+ '--'+JurosParcelas);
        }
    });
}

//PEGA O VALOR DE CADA PARCELA PARA O BACKEND
$("#QtdParcelas").on('change',function(){
    var ValueSelected=document.getElementById('QtdParcelas');
    $("#ValorParcelas").val(ValueSelected.options[ValueSelected.selectedIndex].label);
});

//GERAR O SENDER HASH DO PAG SEGURO
$("#exampleFormControlSelect1").on('click',function(event){
    event.preventDefault();
    PagSeguroDirectPayment.onSenderHashReady(function(response){
        $("#HashCard").val(response.senderHash)
    });

});



