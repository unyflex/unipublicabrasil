@extends('painel.layouts.app')



@section('content')

@if(isset($video_lessons))

<form action="{{ route('atualizar-painelv', ['panel' => $panel->id, 'video'=> $id1, 'video2'=> $id2]) }}" enctype="multipart/form-data"

        data-single="true" method="post">

@else

<form action="{{ route('atualizar-painel', ['panel' => $panel->id]) }}" enctype="multipart/form-data"

        data-single="true" method="post">

@endif

    

        <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">

            <div class="col-span-12 lg:col-span-8">

                <!-- BEGIN: Personal Information -->

                <div class="intro-y box mt-5">

                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                        <h2 class="font-medium text-base mr-auto">

                            Informação do Painel {{$id2}}

                        </h2>

                    </div>

                    <div class="p-5">

                        @foreach ($errors->all() as $error)

                            <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert">

                                <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> {{ $error }}

                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">

                                    <i data-feather="x" class="w-4 h-4"></i>

                                </button>

                            </div>

                        @endforeach

                        <div class="grid grid-cols-12 gap-x-5">

                            @csrf

                            @method('PUT')

                            <input type="hidden" value="{{$panel->id}}" name="class_id">

                            <div class="col-span-12 xl:col-span-6">

                                <div class="mt-3">

                                    <label for="update-profile-form-7"

                                        class="form-label"><strong>Título</strong></label>

                                    <input id="update-profile-form-7" type="text" name="titulo" class="form-control"

                                        placeholder="Título da turma" value="{{ $panel->title }}">

                                </div>

                                <div class="col-span-12 xl:col-span-6">

                                <div class="mt-3">

                                    <label for="update-profile-form-7"

                                        class="form-label"><strong>Subtitulo</strong></label>

                                    <input id="update-profile-form-7" type="text" name="subtitulo" class="form-control"

                                        placeholder="Título da turma" value="{{ $panel->title }}">

                                </div>

                                <div class="mt-3">

                                    <label for="dataInicio" class="form-label"><strong>Data De Começo</strong></label>

                                    <div class="relative mx-auto">

                                        <div

                                            class="absolute rounded-l w-10 h-full flex items-center justify-center bg-gray-100 border text-gray-600 dark:bg-dark-1 dark:border-dark-4">

                                            <i data-feather="calendar" class="w-4 h-4"></i>

                                        </div>

                                        <input type="text" autocomplete="off" class="data form-control pl-12"

                                            name="dataInicio" value="{{ $panel->start_date  }}"

                                            data-single-mode="true">

                                    </div>

                                </div>

                                <div class="mt-3">

                                    <label for="status" class="form-label"><strong>Status</strong></label>

                                    <div class="mt-2">

                                        <select data-placeholder="Selecione o status do curso" name="status"

                                            class="tom-select w-full">

                                            <option @if ($panel->status == 'able')

                                                selected

                                                @endif value="able">Habilitado</option>

                                            <option @if ($panel->status == 'disabled')

                                                selected

                                                @endif value="disabled">Desabilitado</option>

                                        </select>

                                    </div>

                                </div>

                                 <div class="mt-3">

                                    <label for="docente" class="form-label"><strong>Professor</strong></label>

                                    <div class="mt-2">

                                        <select data-placeholder="Selecione o status do curso" name="docente"

                                            class="tom-select w-full">

                                            @foreach ($teachers as $teacher)

                                                <option value="{{$teacher->id}}">{{$teacher->name}}</option>

                                            @endforeach

                                            

                                        </select>

                                    </div>

                                </div>

                                <div class="mt-3">

                                    <label for="update-profile-form-7"

                                        class="form-label"><strong>Valor do Painel Que será Pago ao Professor</strong></label>

                                    <input id="update-profile-form-7" type="number" name="valor" class="form-control"

                                        placeholder="Valor do Painel Que será Pago ao Professor" value="{{ $panel->valor }}">

                                </div>

                                  <div class="mt-3">

                                    <label for="horario" class="form-label"><strong>Horário  </strong></label>

                                    <div class="mt-2">

                                        <select data-placeholder="Selecione o horario do painel" name="horario"

                                            class="tom-select w-full">

                                            @foreach ($horarios as $horario )

                                         <option value="{{$horario->inicio}} ás {{$horario->fim}}">{{$horario->inicio}} ás {{$horario->fim}}</option>    

                                            @endforeach

                                        </select>

                                    </div>

                                </div>

                                <div class="flex justify-end mt-12">

                                    <input type="submit" value="Atualizar painel" class="btn btn-primary mr-auto mb-2">

                                </div>

                            </div>

                            

                        

                            </div>

                        </div>

                    </div>

                </div>

                <!-- END: Personal Information -->

            </div>

            <!-- BEGIN: Post Info -->

            <div class="col-span-12 lg:col-span-4">

                <div class="intro-y box mt-5 p-5">

                     <div class="mt-6">

                            <label for="conteudo" class="form-label">Conteudo do Painel</label>

                            <div class="mt-2">

                                <textarea class="form-control editor" name="conteudo" id="full_resume" cols="30"

                                    rows="15">{{$panel->content}}</textarea>

                            </div>

                         

                    </div>

               @if(isset($video_lessons))

                        

                    
@foreach ($video_lessons as $video_lesson)
    
                      <div class="mt-3">

                                    <label for="update-profile-form-7"

                                        class="form-label"><strong>Link Da Aula {{$loop->iteration}}</strong></label>

                                    <input id="update-profile-form-7" type="text" name="link{{$loop->iteration}}" class="form-control"

                                        placeholder="Link Da Aula {{$loop->iteration}}" value="{{$video_lesson->link}}" >
                                    <input type="hidden" name="videoid{{$loop->iteration}}" value="{{$video_lesson->id}}">
                                </div>

                         <div class="mt-3">

                                    <label for="update-profile-form-7"

                                        class="form-label"><strong>Link de Degustação {{$loop->iteration}}</strong></label>

                                    <input id="update-profile-form-7" type="text" name="degustacao{{$loop->iteration}}" class="form-control"

                                        placeholder="Link de Degustação" value="{{$video_lesson->tasting_link}}" >

                                </div>

    @endforeach
                    


                         <div class="mt-3">

                                    <label for="source" class="form-label"><strong>Source</strong></label>

                                    <div class="mt-2">

                                        <select data-placeholder="Selecione o status do curso" name="source"

                                            class="tom-select w-full">

                                            <option value="youtube">Youtube</option>

                                            <option value="vimeo">Vimeo</option>

                                        </select>

                                    </div>

                                </div>

                                <input type="hidden" name="unyflex" value="1">

                                @endif

                </div>

            </div>





            <!-- END: Post Info -->

        </div>



    </form>



    </div>

    </div>

    </div>



    </div>

    </div> <!-- END: Users Layout -->



    <!-- END: Users Layout -->

    @if (session()->get('message') == 'panel_updated')

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="p-5 text-center"> <i data-feather="check-circle"

                            class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                        <div class="text-3xl mt-5">Bom trabalho!</div>

                        <div class="text-gray-600 mt-2">Os dados do painel foram atualizados com sucesso!</div>

                    </div>

                    <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                            class="btn btn-primary w-24">Ok</button> </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->

    @endif



    @if (session()->get('message') == 'panel_update_error')

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-body p-0">

                        <div class="p-5 text-center"> <i data-feather="check-circle"

                                class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                            <div class="text-3xl mt-5">Erro!</div>

                            <div class="text-gray-600 mt-2">Não foi possível atualizar os dados do painel!</div>

                        </div>

                        <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                                class="btn btn-primary w-24">Ok</button> </div>

                    </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->

    @endif



    <!-- END: Users Layout -->

    @if (session()->get('message') == 'class_updated')

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="p-5 text-center"> <i data-feather="check-circle"

                            class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                        <div class="text-3xl mt-5">Bom trabalho!</div>

                        <div class="text-gray-600 mt-2">Os dados da turma foram atualizados com sucesso!</div>

                    </div>

                    <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                            class="btn btn-primary w-24">Ok</button> </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->

    @endif



    @if (session()->get('message') == 'class_update_error')

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-body p-0">

                        <div class="p-5 text-center"> <i data-feather="check-circle"

                                class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                            <div class="text-3xl mt-5">Erro!</div>

                            <div class="text-gray-600 mt-2">Não foi possível atualizar os dados da turma!</div>

                        </div>

                        <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                                class="btn btn-primary w-24">Ok</button> </div>

                    </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->

    @endif

@endsection

@push('custom-scripts')

    @if (session()->get('message'))

        <script>

            cash(function() {

                cash('#modalInfo').modal('show');

            });

        </script>

    @endif

    <script>

        cash(".data").each(function() {

            let options = {

                autoApply: true,

                singleMode: false,

                lang: "pt-BR",

                numberOfColumns: 2,

                numberOfMonths: 2,

                resetButton: true,

                format: 'DD/MM/YYYY',

                dropdowns: {

                    minYear: 1990,

                    maxYear: null,

                    months: true,

                    years: true,

                },

            };

            if (cash(this).data("single-mode")) {

                options.singleMode = true;

                options.numberOfColumns = 1;

                options.numberOfMonths = 1;

            }



            const picker = new Litepicker({

                element: this,

                ...options

            });



        });

    </script>

@endpush

