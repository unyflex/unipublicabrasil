@extends('painel.layouts.app')



@section('content')

    <form action="{{ route('atualizar-prova', ['question' => $question->id]) }}" enctype="multipart/form-data" data-single="true" method="">

        <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">

            <div class="col-span-12 lg:col-span-8">

                <!-- BEGIN: Personal Information -->

                <div class="intro-y box mt-5">

                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                        <h2 class="font-medium text-base mr-auto">

                            Informação da Prova

                        </h2>

                    </div>

                    <div class="p-5">

                        @foreach ($errors->all() as $error)

                            <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert">

                                <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> {{ $error }}

                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">

                                    <i data-feather="x" class="w-4 h-4"></i>

                                </button>

                            </div>

                        @endforeach

                        <div class="grid grid-cols-12 gap-x-5">

                            @csrf

                            @method('PUT')

                            <div class="col-span-12 xl:col-span-6">

                                <div class="mt-3">

                                    <label for="update-profile-form-7" class="form-label"><strong>Questão</strong></label>

                                    <input id="update-profile-form-7" type="text" name="questao" class="form-control"

                                        placeholder="Questão da prova" value="{{$question->text}}">

                                </div>

                                           <div class="mt-3">

                                    <label for="update-profile-form-7" class="form-label"><strong>Alternativa A</strong></label>

                                    <input id="update-profile-form-7" type="text" name="a" class="form-control"

                                        placeholder="Alternativa A" value="{{$alternatives[0]->text}}">

                                </div>

                                           <div class="mt-3">

                                    <label for="update-profile-form-7" class="form-label"><strong>Alternativa B</strong></label>

                                    <input id="update-profile-form-7" type="text" name="b" class="form-control"

                                        placeholder="Alternativa B" value="{{$alternatives[1]->text}}"">

                                </div>


                                           <div class="mt-3">

                                    <label for="update-profile-form-7" class="form-label"><strong>Alternativa C</strong></label>

                                    <input id="update-profile-form-7" type="text" name="c" class="form-control"

                                        placeholder="Alternativa C" value="{{$alternatives[2]->text}}">

                                </div>


                                           <div class="mt-3">

                                    <label for="status" class="form-label"><strong>Alternativa Correta</strong></label>

                                    <div class="mt-2">

                                        <select data-placeholder="Selecione a alternativa correta da Prova" name="correta"

                                            class="tom-select w-full">

                                            <option value="a">Alternativa A</option>

                                            <option value="b">Alternativa B</option>

                                            <option value="c">Alternativa C</option>

                                        </select>

                                    </div>

                                </div>



                                <div class="mt-3">

                                    <label for="post-form-3" class="form-label"><strong>Curso</strong></label>

                                    <select data-placeholder="Selecione as categorias" name="idclass"

                                        class="tom-select w-full" id="post-form-3" >

                                        @foreach ($classes as $class)

                                            <option value="{{ $class->id }}">{{$class->id}} -{{ $class->title }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>



                        </div>

                        <div class="flex justify-end mt-4">

                            <button type="submit" class="btn btn-primary w-40 mr-auto">Atualizar prova curso</button>

                        </div>

                    </div>

                </div>

                <!-- END: Personal Information -->

            </div>

        </div>

    </form>

@endsection

@push('custom-scripts')

    @if (session()->get('message'))

        <script>

            cash(function() {

                cash('#modalInfo').modal('show');

            });

        </script>

    @endif



@endpush

