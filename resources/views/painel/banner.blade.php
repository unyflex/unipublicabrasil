@extends('painel.layouts.app')



@section('content')



    <h2 class="intro-y text-lg font-medium mt-10">

        Lista de Banners cadastrados

    </h2>

    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

            <a href="{{ route('adicionar-banner') }}" class="btn btn-primary shadow-md mr-2">

                <i data-feather="plus" class="w-4 h-4 mr-2"></i> Adicionar Banner

            </a>

            <div class="hidden md:block mx-auto text-gray-600"></div>

            {{-- <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">

                <div class="w-56 relative text-gray-700 dark:text-gray-300">

                    <input type="text" class="form-control w-56 box pr-10 placeholder-theme-13" placeholder="Pesquisar...">

                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>

                </div>

            </div> --}}

        </div></div>

        <!-- BEGIN: Users Layout -->

        <div class="grid grid-cols-12 gap-5 mt-5 pt-5 border-t">

       

                @foreach ($banners as $banner)

                    



                            <a href="/painel/banners/{{$banner->id}}" data-tw-toggle="modal" data-tw-target="#add-item-modal" class="intro-y block col-span-12 sm:col-span-4 2xl:col-span-3">

                                <div class="box rounded-md p-3 relative zoom-in">

                                    <div class="flex-none relative block before:block before:w-full before:pt-[100%]">


                                    <center>

                                     <img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="{{url("storage/banners/home/$banner->imagem");}}">

                                    </center>

                                        <div class="absolute top-0 left-0 w-full h-full image-fit">

                                                 </div>

                                    </div>

                                    <div class="block font-medium text-center truncate mt-3">{{$banner->nome}}</div>

                                </div>

                            </a>

                   @endforeach

                        </div>

                            <h2 class="intro-y text-lg font-medium mt-10">

        Lista de Banners Desabilitados

    </h2>

                         <div class="grid grid-cols-12 gap-5 mt-5 pt-5 border-t">

     

                @foreach ($desabilitados as $desabilitado)

                    



                            <a href="/painel/banners/{{$desabilitado->id}}" data-tw-toggle="modal" data-tw-target="#add-item-modal" class="intro-y block col-span-12 sm:col-span-4 2xl:col-span-3">

                                <div class="box rounded-md p-3 relative zoom-in">

                                    <div class="flex-none relative block before:block before:w-full before:pt-[100%]">

                                    <center>

                                     <img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="{{url("storage/banners/home/$desabilitado->imagem");}}">

                                    </center>

                                        <div class="absolute top-0 left-0 w-full h-full image-fit">

                                                 </div>

                                    </div>

                                    <div class="block font-medium text-center truncate mt-3">{{$desabilitado->nome}}</div>

                                </div>

                            </a>

                   @endforeach

                        </div>

                    </div>

                    </div>

        







        <!-- END: Users Layout -->

    </div></div></div>

@endsection

@push('custom-scripts')





@endpush

