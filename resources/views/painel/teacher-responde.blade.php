@extends('professores.layouts.header')
@section('content')



    <!-- BEGIN: Personal Information -->

    <div class="intro-y box mt-5">

        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

            <h2 class="font-medium text-base mr-auto">

                Responde Duvida

            </h2>

        </div>



        <form action="{{ route('insere-resposta')}}" enctype="multipart/form-data" data-single="true" method="post">

            @csrf
            <input type="hidden" value="{{$duvida->id}}" name="questao">
            <input type="hidden" value="{{$duvida->id_classes}}" name="curso">
            <input type="hidden" value="{{Auth::user()->teacher_id}}" name="professor">
          
     

            <div class="p-5">

                <div class="grid grid-cols-12 gap-x-5">

                    @csrf

                    <div class="col-span-12 xl:col-span-6">

                        <div class="mt-3">

                            <label for="update-profile-form-7" class="form-label">Assunto </label>

                            <input id="update-profile-form-7" type="text" name="assunto" class="form-control"
                                    disabled
                                placeholder="Nome do professor" value="{{ $duvida->assunto }}">

                        </div>

                        <div class="mt-3">

                            <label for="update-profile-form-6" class="form-label">Aluno</label>

                            <input id="update-profile-form-6" type="text" name="aluno" class="form-control"
                                    disabled
                                placeholder="E-mail do professor" value="{{ $duvida->student->name }}">

                        </div>

                        <div class="mt-6">

                            <label for="short-resume" class="form-label">Pergunta</label>

                            <div class="mt-2">

                                <input disabled type="text" class="form-control" name="pergunta" id="short_resume" cols="30"
                                value="{{ $duvida->duvida }}" 
                                    rows="10">

                            </div>

                        </div>




                    </div>

                    <div class="col-span-12 xl:col-span-6">

                      <div class="mt-3">

                            <label for="update-profile-form-7" class="form-label">Curso</label>

                            <input id="update-profile-form-7" type="text" name="curso" class="form-control"
                                disabled
                                placeholder="curso" value="{{ $duvida->class->title }}">

                        </div>

                       
                        

                        <div class="mt-6">

                            <label for="full-resume" class="form-label">Resposta</label>

                            <div class="mt-2">

                                <textarea class="form-control editor" name="resposta" id="full_resume" cols="30"

                                    rows="15"></textarea>

                            </div>

                        </div><br>

                     

                    </div>

                </div>

                <div class="flex justify-end mt-10">

                    <button type="submit" class="btn btn-primary mr-auto">Enviar Resposta</button>

                </div>

            </div>

        </form>

    </div>

    <!-- END: Personal Information -->

    <!-- END: Users Layout -->

    @if (session()->get('message') == 'teacher_updated')

        <!-- END: Modal Toggle -->

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="p-5 text-center"> <i data-feather="check-circle"

                            class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                        <div class="text-3xl mt-5">Bom trabalho!</div>

                        <div class="text-gray-600 mt-2">Os dados do professor foram atualizados com sucesso!</div>

                    </div>

                    <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                            class="btn btn-primary w-24">Ok</button> </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->



    @endif



    @if (session()->get('message') == 'teacher_update_error')

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-body p-0">

                        <div class="p-5 text-center"> <i data-feather="check-circle"

                                class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                            <div class="text-3xl mt-5">Erro!</div>

                            <div class="text-gray-600 mt-2">Não foi possível atualizar os dados do professor!</div>

                        </div>

                        <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                                class="btn btn-primary w-24">Ok</button> </div>

                    </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->

    @endif

@endsection

@push('custom-scripts')

    @if (session()->get('message'))

        <script>

            cash(function() {

                cash('#modalInfo').modal('show');

            });

        </script>

    @endif

@endpush

