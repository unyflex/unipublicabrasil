@extends('painel.layouts.app')



@section('content')



    <h2 class="intro-y text-lg font-medium mt-10">

        Lista de hoteis cadastrados

    </h2>

    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

            <a href="{{ route('adicionar-hotel') }}" class="btn btn-primary shadow-md mr-2">

                <i data-feather="plus" class="w-4 h-4 mr-2"></i> Adicionar Hotel

            </a>

            <div class="hidden md:block mx-auto text-gray-600"></div>

            {{-- <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">

                <div class="w-56 relative text-gray-700 dark:text-gray-300">

                    <input type="text" class="form-control w-56 box pr-10 placeholder-theme-13" placeholder="Pesquisar...">

                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>

                </div>

            </div> --}}

        </div>

        <!-- BEGIN: Users Layout -->

        @foreach ($hoteis as $hotel)

            <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4">

                <div class="box h-72">

                    <div class="flex items-start px-5 pt-5">

                        <div class="w-full flex flex-col lg:flex-row items-center">

                            <div class="w-16 h-16 image-fit">

                          
                                <img alt="Rubick Tailwind HTML Admin Template" class="rounded-full"

                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAAZlBMVEXJ09xkd4XL1d5gdIJkd4RhdYPL1d3J091kdoVdcX9jd4Rgc4LO2OFleIVecn/I1Ny8ydJrfYl+j5vBzda3w8yLmqV4iZShrriaqLGls72Qnqmtu8Wotb9pfIeElaCzvsdyg42Rn6yZnqDuAAAHxElEQVR4nO2da3ejOAyGQWCDwdyvCWlC//+fXJskM2mbTkmQg+j6+bQzZ/acvEeyJMu2cByLxWKxWCwWi8VisVgsFovFYrFYLBaLxWKxWCwWi8VisVgsFovF8r8nyzInc+DM5c+/CQAJMqiqvFbkVRCoP05CfwXKZtWpa9oiYd5E9Da2TXdy4Bdo5I6Udd/6IvaYewPzhHDLPpfA1/6JS1CeWfWFiFkYuncIYzF21YZ9FWR99GJludBP7glURMJtlBnX/qXPkDkyP6beN8JuzOilTSW5szmRIHsv/lHehBd22zOirAsvcu8uvc/4vpu2uVz7F88HnIDzLmY/S/sLc3fKTTeT/IE34hF5ijDdb8ZLA+60j+pz/Ugct6IQnPbn4HkHr1T/6wa8FJzxKX2uG5ebyPlQqvD5HKKR5Cu3TD4cX24V9uQFym6BPlW5DZJ2TQP5k+vvqjCpSNuQQ/FQfv+KV1IuaQLZL3HQCbEjbELIF9pPk1R0V6EsZ+4f/oW3Jxtn4CSi73a284niPFhbyTfIlkX+Un2hHx9pxpmAnxZHmDNpTtNFZYkQYjReQ9KEPE9x9LkJqyh2E+UeIYRqfFd0QC+QBlmBo08zEuyy8UHM6zHNwavpuag8IoWYSeCeXJiBAE+e67KCnEA+LNsnfYJeKkSLoWd0HCVFIEfEJah8lNq2kAdIZdqViJgFdZJA1SeILULZY9VpF4FpRysTLmiG3odawR0UaFXMGdaSuqIAFa6HKhJS1SjUKa6HqkxYETqHAdjhCzwREuhAjy4w7SgJ5EfUQk3j9ZTWoGxRC7VJIKneGh+x9ak8QehqSRYs7/d+EThmhARW2CFG8ba2qhsytI7hDX5Fx4JQI2+WFCHL6ZTbgLxZmgSmOZ1ECAcTAmtCAjsDa1AMhFzUCnxG4IGSQNSm6FUgoTXY/3aBZixIyUXx16BPKsjs8POg2tLTEcgPJqIopUR/MiCQVKlmYDcRRoTaaryKkPu+SmBB6L4TYF5AuMBGOttB3XRCtyAj9cpANsxdfEvtI/E7nSwBUymD3HeiVKlNPQtsHxVVRijKOE6I3PllRQCUBEKJbMGY2AEoX34b/SPpgVIQ1akeW+Daij7BkY9faB29aDjujikdaHmoIsPMg2zklELohOwx3hRcEDtSOcLRr+adrFj+puBCWJBzUAe1byEGSndkrgS8RTqo9xqC8pSL8gpHX1gQOtv9gBw8hKc9JG+kX1j2vvVCSnIBXlAKlxbd4kBYn77T5YVR9PSVhDDyT9RqtI+AnkKywHwjsYu+d+BZ86Sbhkz0FB9lfUYZsU3ZQ43SqcTz0nIrQ2WAD2X84PXD1CvrLZjvTMYh7wvhzdwislgUegLZpqblgfLUvkyEUnmWObls5Efn/7zOIWNxnL6VfU7qgvZcQIJT7/ZlkURp6qV6Jh5TTMPx1F+IlCVFud/VAVDbG81DmwQ45xKqvB4OXd/v983Eft/33WGo80pK9Q/0JMdNKvwDXJEX/vzF2r/MYrFYLBaLZT6/sX7J9DCyqUxTwipNEARymrOdnceLO5stRDN9/wIkz/Khez+241vi6/1ElLwV7XHfDbnecOhqe6tow+W7ffvmCaGbGJrLxBK1aRJqJ9juD3mwTYnqZ8tqdyxEHF83t8m1iaH7itPGNwyZEEVzCCSHLXnq5Jh5N6YzplArlH3brpIbMiQo27WeCOe3nZjwy0NGf7ionm2vjde8Cd2ImS3QnzQW75UkPgM3cNTKO5XiuXPCyBPHmrgVMxha8ex9kihxWVqeqEqcxoef2pTpyefPCYy0xLispe6yUYup6jdBdUy95VfWPK/JOL1yTuX0XuC8f4k8v6NWsGYZnEaB9FJZlQGiraVDKaByZ5+i3lWL4x4IJX6Zj+hve9KWyLzmLOOyE8/HzvsoN2XhQCNjQNCkaJe4PiDeKXgpr1oP/93LmbhcfywQrxPm4o2l/KywWHtagMQdafhVYVLLNWsaaeJl5C1J5A8rjl2Zwqc5B51g8aBsuI5GlKtpPxKJw0o2VPoMjJH5KjDy1ri+FgR8hz6o6htC77SCCaWBESvfwVYYGAs18rTGf6F2wi8eD5RBtfTTII/BxtduniDAuoA+lxe/9ZGN2QLmDqJ/mULQD5XwB6n9qHDgr/JSyF8XX254WaDJOP4kwznE5YsEyncDkytmEKWdVAWGcX1Qv6RC+0rC2CvmdwCMZhoUPxKFrJTmFfL315VoXxA748uQL/x620LMR1KUj2M9TWT4SxuZA6cX1tj3iM3uK0CObF2B+vN95jIF6DGU6wp0RW0wFWYwGm0wzcHopzakifFiD+KL2twqVDl+bX2uz0yVpKAn3a4vUAVSUyYEWWLPNXoKU9/ayKBeM8f/JYwrIwIdjvtppecRhub8Z2sLu8LMjLrgBmZNP4ehTCHLVfcRN/isMXAcA5XZY7IHiFiBLk9PUyESYvQ1zXjAvp4A2kMpZPkzBlIhVAmJLH8BfSJLpmLo8yMq8EnR4+gKpxH/Av+bPvK1x2U/wVrkRZhhj/ZbiufghlE6ZcyFFPnYnu9JLUE3xF6EsM6B0vcw5BNfIJQiJtSOAtOEJkahLyRFvd4FBxLdmL+ELu6WCXoylfYVscM8spf43xdcSvyOF2WAXhBVAmeG0f8Ax8B+AWS6/5gAAAAASUVORK5CYII=">

                            </div>

                            <div class="lg:ml-4 text-center lg:text-left mt-3 lg:mt-0">

                                <a href="{{ route('informacao-hotel', ['hotel' => $hotel->id]) }}"

                                    class="font-medium">{{ $hotel->nome }}</a>

                                <div class="text-gray-600 text-xs mt-0.5">{{ $hotel->cidade }}</div>

                            </div>

                        </div>

                        <div class="absolute right-0 top-0 mr-5 mt-3 dropdown">

                            <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false"> <i

                                    data-feather="more-horizontal" class="w-5 h-5 text-gray-600 dark:text-gray-300"></i>

                            </a>

                            <div class="dropdown-menu w-40">

                                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">

                                    <a href="{{ route('informacao-hotel', ['hotel' => $hotel->id]) }}"

                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">

                                        <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> Editar </a>

                                    <a href="javascript:;" data-toggle="modal"

                                        data-target="#excluirProfessor{{ $hotel->id }}"

                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">

                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i> Excluir </a>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="text-center lg:text-left p-5">

                        <div>Hotel {!! $hotel->estrelas!!} estrelas</div>

                        <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5"> <i

                                data-feather="mail" class="w-3 h-3 mr-2"></i> {{ $hotel->distancia }} </div>

                        <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1"> <i

                                data-feather="phone" class="w-3 h-3 mr-2"></i> {{ $hotel->site }} </div>

                    </div>

                </div>

            </div>

            <!-- BEGIN: Modal Content -->

            <div id="excluirProfessor{{ $hotel->id }}" class="modal" tabindex="-1" aria-hidden="true">

                <div class="modal-dialog">

                    <div class="modal-content">

                        <form action="{{ route('excluir-hotel', ['hotel' => $hotel->id]) }}" method="POST">

                            <div class="modal-body p-0">

                                <div class="p-5 text-center"> <i data-feather="x-circle"

                                        class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>

                                    <div class="text-3xl mt-5">Você realmente quer excluir este hotel?</div>

                                    <div class="text-gray-600 mt-2">

                                        Esse processo não poderá ser desfeito.

                                    </div>

                                </div>

                                <div class="px-5 pb-8 text-center">

                                    <button type="button" data-dismiss="modal"

                                        class="btn btn-outline-secondary w-24 dark:border-dark-5 dark:text-gray-300 mr-1">Cancelar</button>

                                    @csrf

                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger w-24">Excluir</button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        @endforeach

        <!-- END: Users Layout -->

    </div>

@endsection

@push('custom-scripts')





@endpush

