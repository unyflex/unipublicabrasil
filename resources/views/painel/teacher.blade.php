@extends('painel.layouts.app')



@section('content')



    <h2 class="intro-y text-lg font-medium mt-10">

        Lista de professores cadastrados

    </h2>

    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

            <a href="{{ route('adicionar-professor') }}" class="btn btn-primary shadow-md mr-2">

                <i data-feather="plus" class="w-4 h-4 mr-2"></i> Adicionar professor

            </a>

            <div class="intro-y box p-5 mt-5">

        <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">

            <form action="{{ route('pesquisa-professores')}}" id="tabulator-html-filter-form" class="xl:flex sm:mr-auto" enctype="multipart/form-data" data-single="true"  method="post">
                @csrf
                <div class="sm:flex items-center sm:mr-4">

                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Campo</label>

                    <select id="tabulator-html-filter-field"
                        name="campo"
                        class="form-select w-full sm:w-32 xxl:w-full mt-2 sm:mt-0 sm:w-auto">

                        <option value="name">Aluno</option>

                        <option value="email">E-mail</option>

                        <option value="cpf">CPF</option>

                        <option value="phone">Telefone</option>

                        <option value="cep">CEP</option>
                        
                        <option value="city">Cidade</option>

                        <option value="state">Estado</option>

                        

                    </select>

                </div>
 
                <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">

                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Filtro</label>

                    <select name="operador" id="tabulator-html-filter-type" class="form-select w-full mt-2 sm:mt-0 sm:w-auto">
                        
                        <option value="like" selected>Contêm</option>

                        <option value="=">=</option>

                        <option value="<">&lt;</option>

                        <option value="<=">&lt;=</option>

                        <option value=">">></option>

                        <option value=">=">>=</option>

                        <option value="!=">!=</option>

                    </select>

                </div>

                <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">

                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Valor</label>

                    <input name="valor" id="tabulator-html-filter-value" type="text" class="form-control sm:w-40 xxl:w-full mt-2 sm:mt-0"

                        placeholder="...">

                </div>

                <div class="mt-2 xl:mt-0">

                    <button id="tabulator-html-filter-go" type="submit"

                        class="btn btn-primary w-full sm:w-16">Filtrar</button>

                   

                </div>

            </form>

            
            </div>

        </div>

        </div>

        <!-- BEGIN: Users Layout -->

        @foreach ($teachers as $teacher)

            <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4">

                <div class="box h-72">

                    <div class="flex items-start px-5 pt-5">

                        <div class="w-full flex flex-col lg:flex-row items-center">

                            <div class="w-16 h-16 image-fit">
                                     @if(isset($teacher->photo))
                                <img alt="" class="rounded-full"

                                    src="{{url("storage/docentes/$teacher->photo");}}">

                                    @else
                                     <img  class="rounded-full" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtMMQfcoo0WJoDcZowrVhrmhuL9Kguv_hkVNaPYmq-kDpHiW1P9lpvESWKjfGjNpCdkMM&usqp=CAU">
                                     @endif
                            </div>

                            <div class="lg:ml-4 text-center lg:text-left mt-3 lg:mt-0">

                                <a href="{{ route('informacao-professor', ['teacher' => $teacher->id]) }}"

                                    class="font-medium">{{ $teacher->name }}</a>

                                <div class="text-gray-600 text-xs mt-0.5">{{ $teacher->cpf }}</div>

                            </div>

                        </div>

                        <div class="absolute right-0 top-0 mr-5 mt-3 dropdown">

                            <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false"> <i

                                    data-feather="more-horizontal" class="w-5 h-5 text-gray-600 dark:text-gray-300"></i>

                            </a>

                            <div class="dropdown-menu w-40">

                                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">

                                    <a href="{{ route('informacao-professor', ['teacher' => $teacher->id]) }}"

                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">

                                        <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> Editar </a>

                                    <a href="javascript:;" data-toggle="modal"

                                        data-target="#excluirProfessor{{ $teacher->id }}"

                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">

                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i> Excluir </a>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="text-center lg:text-left p-5">

                        <div>{!! $teacher->short_resume !!}</div>

                        <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5"> <i

                                data-feather="mail" class="w-3 h-3 mr-2"></i> {{ $teacher->email }} </div>

                        <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1"> <i

                                data-feather="phone" class="w-3 h-3 mr-2"></i> {{ $teacher->phone }} </div>

                    </div>

                </div>

            </div>

            <!-- BEGIN: Modal Content -->

            <div id="excluirProfessor{{ $teacher->id }}" class="modal" tabindex="-1" aria-hidden="true">

                <div class="modal-dialog">

                    <div class="modal-content">

                        <form action="{{ route('excluir-professor', ['teacher' => $teacher->id]) }}" method="POST">

                            <div class="modal-body p-0">

                                <div class="p-5 text-center"> <i data-feather="x-circle"

                                        class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>

                                    <div class="text-3xl mt-5">Você realmente quer excluir este professor?</div>

                                    <div class="text-gray-600 mt-2">

                                        Esse processo não poderá ser desfeito.

                                    </div>

                                </div>

                                <div class="px-5 pb-8 text-center">

                                    <button type="button" data-dismiss="modal"

                                        class="btn btn-outline-secondary w-24 dark:border-dark-5 dark:text-gray-300 mr-1">Cancelar</button>

                                    @csrf

                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger w-24">Excluir</button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        @endforeach

        <!-- END: Users Layout -->

    </div>

@endsection

@push('custom-scripts')



@endpush

