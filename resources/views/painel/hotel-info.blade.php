@extends('painel.layouts.app')

@section('content')

    <!-- BEGIN: Personal Information -->
    <div class="intro-y box mt-5">
        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
            <h2 class="font-medium text-base mr-auto">
                Cadastro de Hoteis
            </h2>
        </div>

        <form action="{{ route('atualiza-hotel', ['hotel' => $hotel->id]) }}" enctype="multipart/form-data" data-single="true" method="post">
            @csrf
            @method('PUT')
            <div class="p-5">
                <div class="grid grid-cols-12 gap-x-5">
                    @csrf
                    <div class="col-span-12 xl:col-span-6">
                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Nome</label>
                            <input id="update-profile-form-7" type="text" name="nome" class="form-control"
                                placeholder="Nome do Hotel" value="{{$hotel->nome}}">
                        </div>
                        <div class="mt-3">
                            <label for="update-profile-form-6" class="form-label">Cidade</label>
                            <input id="update-profile-form-6" type="text" name="cidade" class="form-control"
                                placeholder="Cidade do Hotel" value="{{$hotel->cidade}}">
                        </div>
                        <div class="mt-6">
                            <label for="short-resume" class="form-label">Descrição</label>
                            <div class="mt-2">
                                <textarea class="form-control editor" name="descricao" id="descricao" cols="30"
                                    rows="10">{{$hotel->descricao}}</textarea>
                            </div>
                        </div>
                        <div class="mt-3">
                            <label for="status" class="form-label"><strong>Status</strong></label>
                            <div class="mt-2">
                                <select data-placeholder="Selecione o status do professor" name="status"
                                    class="tom-select w-full">
                                    <option selected disabled>Selecione</option>
                                    <option value="able">Habilitado</option>
                                    <option value="disabled">Desabilitado</option>
                                </select>
                            </div>
                        </div>

                        

                    </div>
                    <div class="col-span-12 xl:col-span-6">
                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Rua</label>
                            <input id="update-profile-form-7" type="text" name="rua" class="form-control"
                                placeholder="Rua do hotel" value="{{$hotel->rua}}">
                        </div>
                         <div class="col-span-12 xl:col-span-6">
                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Distância</label>
                            <input id="update-profile-form-7" type="number" name="distancia" class="form-control"
                                placeholder="Distância do hotel" value="{{$hotel->distancia}}">
                        </div>
                        <div class="mt-3">
                            <label for="update-profile-form-9" class="form-label">Site</label>
                            <input id="update-profile-form-9" type="text" name="site" class="form-control"
                                placeholder="Site do Hotel " value="{{$hotel->site}}">
                        </div>
                          <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Número</label>
                            <input id="update-profile-form-7" type="number" name="numero" class="form-control"
                                placeholder="Número do hotel" value="{{$hotel->numero}}">
                        </div>
                         <div class="mt-3">
                            <label for="estrelas" class="form-label"><strong>Estrelas</strong></label>
                            <div class="mt-2">
                                <select data-placeholder="Selecione as estrelas do hotel" name="estrelas"
                                    class="tom-select w-full">
                                    <option selected disabled>Selecione as estrelas</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                         <div class="mt-3">
                         @foreach ($packages as $package)
                             
                         
                            <label for="update-profile-form-7" class="form-label">Pacote {{$loop->iteration}} </label>

                            <input id="update-profile-form-7" type="text" name="pacote[]" class="form-control"
                                placeholder="Nome do pacote" disabled value="{{$package->pacote}}">
                                <br><br>
                            <input id="update-profile-form-7" type="number" name="valor[]" class="form-control"
                                placeholder="Valor do pacote" disabled value="{{$package->valor}}">
                        </div>
                        <br>
                        @endforeach
                        



                        </div>
                       <br>
                         
                    </div>
                    </div>
                </div>
                <div class="flex justify-end mt-10">
                    <button type="submit" class="btn btn-primary mr-auto">Atualizar informações do Hotel</button>
                </div>
            </div>
        </form>
    </div>
    <!-- END: Personal Information -->
    
    <!-- END: Users Layout -->
    @if (session()->get('message') == 'teacher_updated')
        <!-- END: Modal Toggle -->
        <!-- BEGIN: Modal Content -->
        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="p-5 text-center"> <i data-feather="check-circle"
                            class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Bom trabalho!</div>
                        <div class="text-gray-600 mt-2">Os dados do professor foram atualizados com sucesso!</div>
                    </div>
                    <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"
                            class="btn btn-primary w-24">Ok</button> </div>
                </div>
            </div>
        </div> <!-- END: Modal Content -->

    @endif

    @if (session()->get('message') == 'teacher_update_error')
        <!-- BEGIN: Modal Content -->
        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="p-5 text-center"> <i data-feather="check-circle"
                                class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>
                            <div class="text-3xl mt-5">Erro!</div>
                            <div class="text-gray-600 mt-2">Não foi possível atualizar os dados do professor!</div>
                        </div>
                        <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"
                                class="btn btn-primary w-24">Ok</button> </div>
                    </div>
                </div>
            </div>
        </div> <!-- END: Modal Content -->
    @endif
@endsection
@push('custom-scripts')
    @if (session()->get('message'))
        <script>
            cash(function() {
                cash('#modalInfo').modal('show');
            });
        </script>
    @endif
@endpush
