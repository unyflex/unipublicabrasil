@extends('painel.layouts.app')



@section('content')



    <h2 class="intro-y text-lg font-medium mt-10">

        Lista de Podcasts cadastrados

    </h2>

    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

            <a href="{{ route('adicionar-podcast') }}" class="btn btn-primary shadow-md mr-2">

                <i data-feather="plus" class="w-4 h-4 mr-2"></i> Adicionar Podcast

            </a>

            <div class="hidden md:block mx-auto text-gray-600"></div>

            {{-- <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">

                <div class="w-56 relative text-gray-700 dark:text-gray-300">

                    <input type="text" class="form-control w-56 box pr-10 placeholder-theme-13" placeholder="Pesquisar...">

                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>

                </div>

            </div> --}}

        </div>

        <!-- BEGIN: Users Layout -->

        

        @foreach ($podcasts as $podcast)

             <div class="intro-y col-span-12 md:col-span-6">

                        <div class="box">

                            <div class="flex flex-col lg:flex-row items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">

                                <div class="w-24 h-24 lg:w-12 lg:h-12 image-fit lg:mr-1">

                                    <img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="https://cdn-icons-png.flaticon.com/512/937/937363.png">

                                </div>

                                <div class="lg:ml-2 lg:mr-auto text-center lg:text-left mt-3 lg:mt-0">

                                    <a href="" class="font-medium">{{$podcast->titulo}}</a> 

                                    <div class="text-slate-500 text-xs mt-0.5">{{$podcast->descricao}} </div>

                                </div>

                                

                            </div>

                            <div class="flex flex-wrap lg:flex-nowrap items-center justify-center p-5">

                               <audio id="player" controls="controls">
                                <source src="{{url("storage/podcasts/$podcast->arquivo")}}" type="audio/mp3" />
                             
                                </audio>

                             <a href="/painel/podcasts/{{$podcast->id}}"><button style="margin:17px" class="btn btn-primary py-1 px-2 mr-2">Editar</button></a>

                             

                            </div>

                        </div>

                    </div>

            <!-- BEGIN: Modal Content -->

            <div id="excluirProfessor{{ $podcast->id }}" class="modal" tabindex="-1" aria-hidden="true">

                <div class="modal-dialog">

                    <div class="modal-content">

                        <form action="{{ route('excluir-polo', ['polo' => $podcast->id]) }}" enctype="multipart/form-data" data-single="true" method="post">

                            <div class="modal-body p-0">

                                <div class="p-5 text-center"> <i data-feather="x-circle"

                                        class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>

                                    <div class="text-3xl mt-5">Você realmente quer excluir este polo?</div>

                                    <div class="text-gray-600 mt-2">

                                        Esse processo não poderá ser desfeito.

                                    </div>

                                </div>

                                <div class="px-5 pb-8 text-center">

                                    <button type="button" data-dismiss="modal"

                                        class="btn btn-outline-secondary w-24 dark:border-dark-5 dark:text-gray-300 mr-1">Cancelar</button>

                                    @csrf

                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger w-24">Excluir</button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        @endforeach

        <!-- END: Users Layout -->

    </div>

@endsection

@push('custom-scripts')





@endpush

