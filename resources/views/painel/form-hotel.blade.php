@extends('painel.layouts.app')

@section('content')

    <!-- BEGIN: Personal Information -->
    <div class="intro-y box mt-5">
        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
            <h2 class="font-medium text-base mr-auto">
                Cadastro de Hotéis
            </h2>
        </div>

        <form action="{{ route('cadastrar-hotel') }}" enctype="multipart/form-data" data-single="true" method="post">
            <div class="p-5">
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert">
                        <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> {{ $error }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <i data-feather="x" class="w-4 h-4"></i>
                        </button>
                    </div>
                @endforeach

                <div class="grid grid-cols-12 gap-x-5">

                    @csrf
                    <div class="col-span-12 xl:col-span-6">
                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Nome</label>
                            <input id="update-profile-form-7" type="text" name="nome" class="form-control"
                                placeholder="Nome do Hotel" value="">
                        </div>
                        <div class="mt-3">
                            <label for="update-profile-form-6" class="form-label">Cidade</label>
                            <input id="update-profile-form-6" type="text" name="cidade" class="form-control"
                                placeholder="Cidade do Hotel" value="">
                        </div>
                        <div class="mt-6">
                            <label for="short-resume" class="form-label">Descrição</label>
                            <div class="mt-2">
                                <textarea class="form-control editor" name="descricao" id="descricao" cols="30"
                                    rows="10"></textarea>
                            </div>
                        </div>
                        <div class="mt-3">
                            <label for="status" class="form-label"><strong>Status</strong></label>
                            <div class="mt-2">
                                <select data-placeholder="Selecione o status do professor" name="status"
                                    class="tom-select w-full">
                                    <option selected disabled>Selecione</option>
                                    <option value="able">Habilitado</option>
                                    <option value="disabled">Desabilitado</option>
                                </select>
                            </div>
                        </div>

                        

                    </div>
                    <div class="col-span-12 xl:col-span-6">
                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Rua</label>
                            <input id="update-profile-form-7" type="text" name="rua" class="form-control"
                                placeholder="Rua do hotel" value="">
                        </div>
                         <div class="col-span-12 xl:col-span-6">
                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Distância</label>
                            <input id="update-profile-form-7" type="number" name="distancia" class="form-control"
                                placeholder="Distância do hotel" value="">
                        </div>
                        <div class="mt-3">
                            <label for="update-profile-form-9" class="form-label">Site</label>
                            <input id="update-profile-form-9" type="text" name="site" class="form-control"
                                placeholder="Site do Hotel " value="">
                        </div>
                          <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Número</label>
                            <input id="update-profile-form-7" type="number" name="numero" class="form-control"
                                placeholder="Número do hotel" value="">
                        </div>
                      
                         <div class="mt-3">
                            <label for="estrelas" class="form-label"><strong>Estrelas</strong></label>
                            <div class="mt-2">
                                <select data-placeholder="Selecione as estrelas do hotel" name="estrelas"
                                    class="tom-select w-full">
                                    <option selected disabled>Selecione as estrelas</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Pacote </label>

                            <input id="update-profile-form-7" type="text" name="pacote[]" class="form-control"
                                placeholder="Nome do pacote" value="">
                                <br><br>
                            <input id="update-profile-form-7" type="number" name="valor[]" class="form-control"
                                placeholder="Valor do pacote" value="">
                        </div>

                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Pacote 02</label>

                            <input id="update-profile-form-7" type="text" name="pacote[]" class="form-control"
                                placeholder="Nome do pacote" value="">
                                <br><br>
                            <input id="update-profile-form-7" type="number" name="valor[]" class="form-control"
                                placeholder="Valor do pacote" value="">
                        </div>

                        <div class="mt-3">
                            <label for="update-profile-form-7" class="form-label">Pacote 03</label>

                            <input id="update-profile-form-7" type="text" name="pacote[]" class="form-control"
                                placeholder="Nome do pacote" value="">
                                <br><br>
                            <input id="update-profile-form-7" type="number" name="valor[]" class="form-control"
                                placeholder="Valor do pacote" value="">
                        </div>
                        </div>
                       <br>
                         
                    </div>
                    </div>
                </div>
                <div class="flex justify-end mt-4">
                    <button type="submit" class="btn btn-primary w-40 mr-auto">Cadastrar Hotel</button>
                </div>
            </div>
        </form>
    </div>
    <!-- END: Personal Information -->
    <!-- END: Users Layout -->
    </div>
@endsection
@push('custom-scripts')
      <script>
        (function(cash) {
            document.getElementById('file').onchange = function() {
                var arquivo = document.getElementById('file').value;
                var nomearquivo = arquivo.substring(12);
                var modeloArquivo =
                    '<div class="file box rounded-md px-5 sm:px-5 relative zoom-in">' +
                    '<p class="w-1/5 file__icon file__icon--file mx-auto">' +
                    '</p>' +
                    '<p class="block font-medium mt-4 text-center truncate">' + nomearquivo + '</p>' +
                    '</div>';
                cash('#areaArquivo').html(modeloArquivo);
            }
        })(cash);
    </script>
@endpush
