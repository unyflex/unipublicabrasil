@extends('painel.layouts.app')



@section('content')



    <!-- BEGIN: Personal Information -->

    <div class="intro-y box mt-5">

        <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

            <h2 class="font-medium text-base mr-auto">

                Cadastro de Galeria

            </h2>

        </div>



        <form action="{{ route('cadastrar-fotos') }}" enctype="multipart/form-data" data-single="true" method="post">

            <div class="p-5">

                @foreach ($errors->all() as $error)

                    <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert">

                        <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> {{ $error }}

                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">

                            <i data-feather="x" class="w-4 h-4"></i>

                        </button>

                    </div>

                @endforeach



                <div class="grid grid-cols-6 gap-x-5">



                    @csrf

                    

                      @foreach ($photos as $photo )

                            <a href="/painel/banners/{{$photo->id}}" data-tw-toggle="modal" data-tw-target="#add-item-modal" class="intro-y block col-span-12 sm:col-span-4 2xl:col-span-3">

                                <div class="box rounded-md p-3 relative zoom-in">

                                    <div class="flex-none relative block before:block before:w-full before:pt-[100%]">

                                    <center>

                                     <img alt="Rubick Tailwind HTML Admin Template" class="rounded-md" src="{{url("storage/galeria/fotos/$photo->foto");}}">

                                    </center>

                                        <div class="absolute top-0 left-0 w-full h-full image-fit">

                                                 </div>

                                    </div>

                                    <div class="block font-medium text-center truncate mt-3">{{$photo->id}}</div>

                                </div>

                            </a>

                      @endforeach

                    </div>

                    <br><br>

                    <input type="hidden" name="galeria" value="{{$galery->id}}">

                    <div class="col-span-12 xl:col-span-6">

                        <label class="form-label"><strong>Upload Fotos dos Album</strong></label>

                        <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">

                            <div class="px-4 pt-24 pb-24 flex items-center justify-center cursor-pointer relative">

                                <div id="areaArquivo">

                                    <i data-feather="image" class="w-4 h-4 mr-2"></i>

                                    <span class="mr-1 font-bold">Adicionar arquivo</span>

                                </div>

                                <input type="file" id="file" name="file[]"   multiple="multiple"

                                    class="w-full h-full top-0 left-0 absolute opacity-0">

                            </div>

                        </div>

                    </div>

                </div>

                <div class="flex justify-end mt-4">

                    <button type="submit" class="btn btn-primary w-40 mr-auto">Cadastrar Galeria</button>

                </div>

            </div>

        </form>

    </div>



    <!-- END: Personal Information -->

    <!-- END: Users Layout -->

    </div>

@endsection

@push('custom-scripts')

    <script>

        (function(cash) {

            document.getElementById('file').onchange = function() {

                var arquivo = document.getElementById('file').value;

                var nomearquivo = arquivo.substring(12);

                var modeloArquivo =

                    '<div class="file box rounded-md px-5 sm:px-5 relative zoom-in">' +

                    '<p class="w-1/5 file__icon file__icon--file mx-auto">' +

                    '</p>' +

                    '<p class="block font-medium mt-4 text-center truncate">' + nomearquivo + '</p>' +

                    '</div>';

                cash('#areaArquivo').html(modeloArquivo);

            }

        })(cash);

    </script>

@endpush

