@extends('painel.layouts.app')



@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">



    <h1 class="intro-y text-lg font-medium mt-10">

        Lista de Usuarios cadastrados

    </h1>

    <a href="{{ route('adicionar-usuario') }}" class="btn btn-primary w-49 mr-2 mt-5 mb-2"> <i data-feather="plus"

            class="w-4 h-4 mr-2"></i> Adicionar Usuario </a>
<div class="intro-y box p-5 mt-5">

        <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">

            <form action="{{ route('pesquisa-usuarios')}}" id="tabulator-html-filter-form" class="xl:flex sm:mr-auto" enctype="multipart/form-data" data-single="true"  method="post">
                @csrf
                <div class="sm:flex items-center sm:mr-4">

                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Campo</label>

                    <select id="tabulator-html-filter-field"
                        name="campo"
                        class="form-select w-full sm:w-32 xxl:w-full mt-2 sm:mt-0 sm:w-auto">

                        <option value="name">Aluno</option>

                        <option value="email">E-mail</option>

                        <option value="cpf">CPF</option>

                        <option value="phone">Telefone</option>

                        <option value="cep">CEP</option>
                        
                        <option value="city">Cidade</option>

                        <option value="state">Estado</option>

                        

                    </select>

                </div>
 
                <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">

                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Filtro</label>

                    <select name="operador" id="tabulator-html-filter-type" class="form-select w-full mt-2 sm:mt-0 sm:w-auto">
                        
                        <option value="like" selected>Contêm</option>

                        <option value="=">=</option>

                        <option value="<">&lt;</option>

                        <option value="<=">&lt;=</option>

                        <option value=">">></option>

                        <option value=">=">>=</option>

                        <option value="!=">!=</option>

                    </select>

                </div>

                <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">

                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Valor</label>

                    <input name="valor" id="tabulator-html-filter-value" type="text" class="form-control sm:w-40 xxl:w-full mt-2 sm:mt-0"

                        placeholder="...">

                </div>

                <div class="mt-2 xl:mt-0">

                    <button id="tabulator-html-filter-go" type="submit"

                        class="btn btn-primary w-full sm:w-16">Filtrar</button>

                   

                </div>

            </form>

            
            </div>

        </div>

 <!-- END: Top Bar -->

                

                <div class="grid grid-cols-12 gap-6 mt-5">

                    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

                        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">

                        </div>

                    </div>

                    <!-- BEGIN: Users Layout -->

                     @foreach ($users as $user)

                    <div class="intro-y col-span-12 md:col-span-6">

                        <div class="box">

                            <div class="flex flex-col lg:flex-row items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">

                                <div class="w-24 h-24 lg:w-12 lg:h-12 image-fit lg:mr-1">

                                    <img  class="rounded-full" src="{{ url("storage/usuarios/perfil/$user->photo") }}">

                                </div>

                                <div class="lg:ml-2 lg:mr-auto text-center lg:text-left mt-3 lg:mt-0">

                                    <a href="" class="font-medium">{{$user->name}} </a> 

                                    <div class="text-slate-500 text-xs mt-0.5">{{$user->email}}</div>

                                     <div class="text-slate-500 text-xs mt-0.5">{{$user->cpf}}-</div>

                                </div>

                                <div class="flex -ml-2 lg:ml-0 lg:justify-end mt-3 lg:mt-0">

                                

                                <a href="/painel/usuarios/{{$user->id}}"> <button class="btn btn-primary py-1 px-2 mr-2">Editar</button></a>

                                

                                 </div>

                            </div>

                           

                        </div>

                    </div>

                  @endforeach

                    </div>

                    </div>

    </div>

 

@endsection

@push('custom-scripts')

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>

        var url = "http://localhost/unipublica-site/public/";



        function excluirAluno(id) {

            urlFinal = url + 'painel/alunos/excluir/' + id;

            axios.post(urlFinal, {

                    _method: 'DELETE',

                    data: {

                        'id': id

                    }

                })

                .then(function(response) {

                    if (response.data == 'sucesso') {

                        cash('.modal').modal('hide');

                        setTimeout(

                            function() {

                                cash('#deleteSucesso').modal('show');

                            }, 1000);

                    } else {

                        cash('.modal').modal('hide');

                        setTimeout(

                            function() {

                                cash('.mensagemErro').html(response.data);

                                cash('#deleteErro').modal('show');

                            }, 1000);

                    }

                })

                .catch(function(error) {

                    cash('.modal').modal('hide');

                    setTimeout(

                        function() {

                            cash('.mensagemErro').html(error);

                            cash('#deleteErro').modal('show');

                        }, 1000);

                });

        }

    </script>

@endpush

