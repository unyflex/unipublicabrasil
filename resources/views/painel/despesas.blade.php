@extends('painel.layouts.app')



@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">



    <h1 class="intro-y text-lg font-medium mt-10">

       Painel Financeiro

    </h1>

    <a href="{{ route('adicionar-despesas') }}" class="btn btn-primary w-49 mr-2 mt-5 mb-2"> <i data-feather="plus"

            class="w-4 h-4 mr-2"></i> Adicionar Despesas

    </a>

    <a href="{{ route('adicionar-fluxo') }}" class="btn btn-primary w-49 mr-2 mt-5 mb-2"> <i data-feather="plus"

        class="w-4 h-4 mr-2"></i> Ver Fluxos de Caixa

</a>


    <a href="{{ route('pagamento-professores') }}" class="btn btn-primary w-49 mr-2 mt-5 mb-2"> <i data-feather="plus"

        class="w-4 h-4 mr-2"></i> Ver Pagamento de Professores

</a>

    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

            <div class="w-full mr-0 sm:w-auto mt-3 sm:mt-0 sm:ml-auto">

                <div class="w-56 relative text-gray-700 dark:text-gray-300">

                    <form action="{{ route('filtra-cursos') }}" method="post">

                        @csrf

                        <input type="text" name="search" class="form-control w-56 box pr-10 placeholder-theme-13"

                            placeholder="Pesquisar...">

                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>

                    </form>

                </div>

            </div>

        </div>



        <!-- BEGIN: Data List -->

        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">

        <!-- GRÁFICO DE DESPESAS GERAIS -->
<script>
window.onload = function () {

var options02 = {
	animationEnabled: true,
    exportEnabled: true,
	title: {
		text: "Despesas Gerais 2022"
	},
	axisY: {
		title: "Valor da Despesa",
		prefix: "R$"
	},
	axisX: {
		title: "Categorias"
	},
	data: [{
		type: "column",
		yValueFormatString: "#,##0.0#"%"",
		dataPoints: [
			{ label: "Professores", y: {{$professores}} },	
			{ label: "Despesa com pessoal", y: {{$pessoal}} },	
			{ label: "Despesas Financeiras", y: {{$financeiras}} },
			{ label: "Verba de marketing", y: {{$marketing}} },
			{ label: "Diretoria", y: {{$diretoria}} },
			{ label: "Investimentos", y: {{$investimentos}} },
			{ label: "Despesas administrativas", y: {{$administrativas}} },
            { label: "Outros", y: {{$outros}} },	
			
		]
	}]
};
$("#chartContainer01").CanvasJSChart(options02);

}
</script>
<div id="chartContainer01" style="height: 300px; width: 100%;"></div>

 <!-- FIM GRÁFICO DE DESPESAS GERAIS -->


    @if (session()->get('message') == 'expense_delete_error')

        <!-- BEGIN: Modal Content -->

        <div id="modalInfo" class="modal" tabindex="-1" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-body p-0">

                        <div class="p-5 text-center"> <i data-feather="check-circle"

                                class="w-16 h-16 text-theme-9 mx-auto mt-3"></i>

                            <div class="text-3xl mt-5">Erro!</div>

                            <div class="text-gray-600 mt-2">Não foi possível excluir a despesa!</div>

                        </div>

                        <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"

                                class="btn btn-primary w-24">Ok</button> </div>

                    </div>

                </div>

            </div>

        </div> <!-- END: Modal Content -->

    @endif

@endsection

@push('custom-scripts')

    @if (session()->get('message'))

        <script>

            cash(function() {

                cash('#modalInfo').modal('show');

            });

        </script>

    @endif

@endpush

<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
