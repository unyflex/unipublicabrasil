@extends('painel.layouts.app')



@section('content')

    <form action="{{ route('atualizar-pagamento', ['panel' => $panel->id])}}" enctype="multipart/form-data" data-single="true" method="post">

        <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">

            <div class="col-span-12 lg:col-span-8">

                <!-- BEGIN: Personal Information -->

                <div class="intro-y box mt-5">

                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                        <h2 class="font-medium text-base mr-auto">

                            Informação de Pagamento de Professor

                        </h2>

                    </div>

                    <div class="p-5">

                        @foreach ($errors->all() as $error)

                            <div class="alert alert-danger alert-dismissible show flex items-center mb-2" role="alert">

                                <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> {{ $error }}

                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">

                                    <i data-feather="x" class="w-4 h-4"></i>

                                </button>

                            </div>

                        @endforeach

                        <div class="grid grid-cols-7 gap-x-5">

                            @csrf
  @method('PUT')
                            <div class="col-span-12 xl:col-span-6">

                                <div class="mt-3">

                                    <label for="update-profile-form-7" class="form-label"><strong>Nome do Professor</strong></label>

                                    <input id="update-profile-form-7" type="text" name="nome" class="form-control"

                                      disabled  placeholder="Nome do Professor" value="{{$panel->teacher->name}}">

                                </div>

                                <div class="grid grid-cols-7 gap-x-5">

                                    @csrf
  @method('PUT')
                                    <div class="col-span-12 xl:col-span-6">

                                        <div class="mt-3">

                                            <label for="update-profile-form-7" class="form-label"><strong>Valor do Painel</strong></label>

                                            <input id="update-profile-form-7" type="text" name="valor" class="form-control"

                                             disabled   placeholder="R$" value="{{$panel->valor}}">

                                        </div>

                                <div class="mt-3">

                                    <label for="mes" class="form-label"><strong>Mês da Despesa</strong></label>

                                    <div class="mt-2">

                                        <select data-placeholder="Selecione o status do curso" name="status"

                                            class="tom-select w-full">

                                            <option value="1">Pago</option>

                                            <option value="0">Ainda Não efetuado</option>


                                        </select>

                                    </div>

                                </div>

                            </div> </div>

                        <div class="flex justify-end mt-4">

                            <button type="submit" class="btn btn-primary w-40 mr-auto">Atualizar Status</button>

                        </div>

                    </div>

                </div>

                <!-- END: Personal Information -->

            </div>

        </div>

    </form>

@endsection

@push('custom-scripts')

    @if (session()->get('message'))

        <script>

            cash(function() {

                cash('#modalInfo').modal('show');

            });

        </script>

    @endif



@endpush



