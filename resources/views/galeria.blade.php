@include('headunypublica')

@include('headerunypublica')

 <section id="titleLine">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <h1>Galeria de Fotos</h1>

                </div>

            </div>

        </div>

    </section>



    <section id="galeria">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <h4>Filtrar Galeria por Ano:</h4>



                    <div class="dropdown">

                        <button class="dropbtn">Seleciona o Ano <i class="fas fa-chevron-down"></i></button>

                        <div class="dropdown-content">

                            <a href="fotos.php?ano=2021">Fotos de 2021</a>

                            <a href="fotos.php?ano=2020">Fotos de 2020</a>

                            <a href="fotos.php?ano=2019">Fotos de 2019</a>

                        </div>

                    </div>



                    <hr>

                </div>

            </div>



         @foreach ($galerys as $galery)

 <div class="row">

        <div class="">

          <div class="box-galeria">

            <img src="{{url("storage/galeria/capas/$galery->capa");}}" class="img-fluid">

            <p><strong>

            @foreach ($galery->classes as $galeriaa )

                                    {{$galeriaa->title}}

                                     

                                     </strong></p>

            <h6>{{$galeriaa->subtitle}}</h6>

            

            <p><i class="far fa-calendar"></i>{{$galeriaa->start_date}}</p>

            @endforeach

            <button  data-toggle="modal" data-target="#exampleModal{{$loop->index}}" class="btn-unyflex-solid my-3"> VER FOTOS </button>

          </div>

        </div>



        



       

<!-- Modal -->

<div class="modal fade" id="exampleModal{{$loop->index}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title" id="exampleModalLabel">  @foreach ($galery->classes as $galeriaa )

                                    {{$galeriaa->title}}

                                    @endforeach</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">&times;</span>

        </button>

      </div>

      <div class="modal-body">

          

    <section id="testemunhos">

         <div class="container">

        <div class="row">

            <div class="col-lg-12">

                <div id="carouselTestemunho{{$loop->index}}" class="carousel slide" data-ride="carousel" data-interval="false">

                    <ol class="carousel-indicators">

                        <li data-target="#carouselTestemunho{{$loop->index}}" data-slide-to="0" class="active"></li>

                        <li data-target="#carouselTestemunho{{$loop->index}}" data-slide-to="1" ></li>

                        <li data-target="#carouselTestemunho{{$loop->index}}" data-slide-to="2"></li>

                    </ol>



                    <div class="carousel-inner">

                     

                           @foreach ($galery->photos as $galeriaa )

                            

                      

                            <div class="carousel-item  <?php if ($loop->index == 0) { ?> active <?php } ?>">

                           

                              <center>

                             <img src="{{url("storage/galeria/fotos/$galeriaa->foto");}}" style="max-height:500px; max-width:500px;" >

                               </center>         

                            </div>

                          @endforeach

                    </div>

                    <div style="color:#000">

                    <a class="carousel-control-prev" style="color:#000" href="#carouselTestemunho{{$loop->index}}" role="button" data-slide="prev">

                        <span class="carousel-control-prev-icon" style="color:#000" aria-hidden="true"></span>

                        <span class="sr-only" style="color:#000">Previous</span>

                    </a>

                    <a class="carousel-control-next" style="color:#000" href="#carouselTestemunho{{$loop->index}}" role="button" data-slide="next">

                        <span class="carousel-control-next-icon"  style="color:#000" aria-hidden="true"></span>

                        <span class="sr-only" style="color:#000">Next</span>

                    </a>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>



      </div>

   

      

    </div>

  </div>

</div>



  @endforeach

    