<!-- SEPARAR HEADER-->





<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MW4GW4P" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->



<!--<div class="top-menu">-->

<!--  <div class="container-fluid">-->

<!--    <div class="row">-->

<!--      <div class="col-lg-12 text-center">-->

<!--        <p>Adquira cursos presenciais e onlines em <strong>promoção por tempo limitado.</strong> <a href="https://unyflex.com.br/setores/geral">Ver Promoção!</a></p>-->

<!--      </div>-->

<!--    </div>-->

<!--  </div>-->

<!--</div>-->



<header>

  <div class="container">

    <nav class="navbar navbar-expand-lg static-top p-0">

      <a class="navbar-brand" href="{{ route('home-uny') }}">

        <img src="https://unipublicabrasil.com.br/uny.png" href="{{ route('home-uny') }}">
        <!--<img src="{{url("storage/depoimentos/363.png");}}">-->
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuMobile" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

        <i class="fas fa-bars"></i>

      </button>



      <div class="w-full sm:w-auto relative mr-auto mt-3 sm:mt-0">

        <i class="w-4 h-4 absolute my-auto inset-y-0 ml-3 left-0 z-10 text-gray-700 dark:text-gray-300" data-feather="search"></i>

        <form action="{{ route('filtra-courses') }}" method="post">

          @csrf

          <input type="text" name="search" class="form-control w-full sm:w-64 box px-10 text-gray-700 dark:text-gray-300 placeholder-theme-13" placeholder="O que quer aprender?">

        </form>

      </div>

      <div class="collapse navbar-collapse" id="menuMobile">

        <ul class="navbar-nav ml-auto">

          <li class="nav-item dropdown">

            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

              Cursos Presenciais <i class="fas fa-chevron-down"></i>

            </a>

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

              <a class="dropdown-item" href="{{ route('agendados') }}">Cursos Agendados</a>

              <a class="dropdown-item" href="{{ route('realizados') }}">Cursos Realizados</a>

              <a class="dropdown-item" href="{{ route('realizados') }}">Cursos Incompany</a>

              <!--<a class="dropdown-item" href="#">Categorias</a>-->

            </div>

          </li>

          <li class="nav-item">

            <a class="nav-link" href="{{route('unyflex')}}">Unyflex</a>

          </li>



          <a class="nav-link" href="{{ route('certidoes') }}">Certidões</a>

          </li>

          <li class="nav-item dropdown">

            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

              Contato <i class="fas fa-chevron-down"></i>

            </a>

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

              <a class="dropdown-item" href="{{ route('galeria') }}">Galeria de Fotos</a>

              <a class="dropdown-item" href="https://unyflex.com.br/assinatura">Assinatura</a>

              <a class="dropdown-item" href="{{ route('quemsomos') }}">Sobre Nós</a>

            </div>

          </li>

          <li class="nav-item">

            <a class="nav-link btn-call" href="https://unyflex.com.br/area"><i class="far fa-user"></i> Já Sou Aluno</a>

          </li>

        </ul>

      </div>

    </nav>

  </div>

</header>