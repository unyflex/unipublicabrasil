@extends('professores.layouts.header')



@section('content')




    <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        {{$course->title}}
                    </h2>
                </div><br>
                <div class="w-full sm:w-auto flex">

                    <a href="{{ route('adicionar-material-professor' , ['class' => $course->id]) }}" class="btn btn-primary shadow-md mr-2">

                        Cadastrar Material

                    </a>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                  
                    <!-- BEGIN: Profile Menu -->
                    <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
                        <div class="intro-y box mt-5 lg:mt-0">
                            <div class="relative flex items-center p-5">
                                <div class="w-12 h-12 image-fit">
                                    <img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="{{url("storage/cursos/banner/$course->photo");}}">
                                </div>
                                <div class="ml-4 mr-auto">
                                    <div class="font-medium text-base">{{$course->title}}</div>
                                    <div class="text-slate-500">{{$course->subtitle}}</div>
                                </div>
                                <div class="dropdown">
                                    <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-feather="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                                   
                                </div>
                            </div>
                            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                              <img src="{{url("storage/cursos/banner/$course->photo");}}">
                            </div>
                            <div class="p-5 border-t border-slate-200/60 dark:border-darkmode-400">
                                <a class="flex items-center" href=""> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Data de Inicio: {{$course->start_date}} </a>
                                <a class="flex items-center mt-5" href=""> <i data-feather="box" class="w-4 h-4 mr-2"></i> Data de Termino: {{$course->end_date}} </a>
                                <a class="flex items-center mt-5" href=""> <i data-feather="lock" class="w-4 h-4 mr-2"></i> @if ($course->confirmed==1)Confirmado @else Agendado @endif</a>
                                <a class="flex items-center mt-5" href=""> <i data-feather="settings" class="w-4 h-4 mr-2"></i>{{$course->polo}} </a>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END: Profile Menu -->
                    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
                        <div class="grid grid-cols-12 gap-6">
                            <!-- BEGIN: Daily Sales -->
                            <div class="intro-y box col-span-12 2xl:col-span-6">
                                <div class="flex items-center px-5 py-5 sm:py-3 border-b border-slate-200/60 dark:border-darkmode-400">
                                    <h2 class="font-medium text-base mr-auto">
                                     Meus Paineis do Curso
                                    </h2>
                                    <div class="dropdown ml-auto sm:hidden">
                                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-feather="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                                        <div class="dropdown-menu w-40">
                                            <ul class="dropdown-content">
                                                <li>
                                                    <a href="javascript:;" class="dropdown-item"> <i data-feather="file" class="w-4 h-4 mr-2"></i> Download Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="p-5">
                                @foreach ($panels as $panel )
                                    <div class="relative flex items-center" style="margin:20px">

                                        <div class="ml-4 mr-auto">
                                            <a href="" class="font-medium">{{$panel->title}}</a> 
                                            <div class="text-slate-500 mr-5 sm:mr-5"><?php $data=$panel->start_time; 
                                                                                $data=date('d/m/Y', strtotime($data) );
                                                                                echo $data; ?></div>
                                        </div>
                                        @if($panel->pagamento_status==0)
                                        <div class="font-medium text-slate-600 dark:text-slate-500" style="color:#ff0000">R$ {{$panel->valor}},00</div>
                                        @else
                                        <div class="font-medium text-slate-600 dark:text-slate-500" style="color:#00ff00">R$ {{$panel->valor}},00</div>
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- END: Daily Sales -->
                            <!-- BEGIN: Announcement -->
                            <div class="intro-y box col-span-12 2xl:col-span-6">
                                <div class="flex items-center px-5 py-3 border-b border-slate-200/60 dark:border-darkmode-400">
                                    <h2 class="font-medium text-base mr-auto">
                                     Meus Próximos Cursos
                                    </h2>
                                    <button data-carousel="announcement" data-target="prev" class="tiny-slider-navigator btn btn-outline-secondary px-2 mr-2"> <i data-feather="chevron-left" class="w-4 h-4"></i> </button>
                                    <button data-carousel="announcement" data-target="next" class="tiny-slider-navigator btn btn-outline-secondary px-2"> <i data-feather="chevron-right" class="w-4 h-4"></i> </button>
                                </div>
                                <div class="tiny-slider py-5" id="announcement">
                                @foreach ($courses as $class)
                                    
                               
                                    <div class="px-5">
                                        <div class="font-medium text-lg">{{$class->classes->title}} - {{$class->classes->subtitle}}</div>
                                     
                                        <div class="font-medium">Data: {{$class->classes->start_date}}</div>
                                        <div class="text-slate-600 dark:text-slate-500 mt-2">
                                         <a href="/professores/cursos/{{$class->classes->id}}" data-tw-toggle="modal" data-tw-target="#add-item-modal" class="intro-y block col-span-12 sm:col-span-4 2xl:col-span-3">
                                        <?php $foto=$class->classes->photo; ?>
                                            <img src="{{url("storage/cursos/banner/$foto");}}">
                                        </div>
                                       </a>
                                    </div>
                                   @endforeach
                                </div>
                            </div>
                            <!-- END: Announcement -->
                            

@endsection

@push('custom-scripts')





@endpush

