@extends('professores.layouts.header')



@section('content')

 <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                       Meu Perfil
                    </h2>
                </div>
                <!-- BEGIN: Profile Info -->
                <div class="intro-y box px-5 pt-5 mt-5">
                    <div class="flex flex-col lg:flex-row border-b border-slate-200/60 dark:border-darkmode-400 pb-5 -mx-5">
                        <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                            <div class="w-20 h-20 sm:w-24 sm:h-24 flex-none lg:w-32 lg:h-32 image-fit relative">
                                <img alt="Rubick Tailwind HTML Admin Template" class="rounded-full" src="{{url("storage/docentes/$teacher->photo");}}">
                                <div class="absolute mb-1 mr-1 flex items-center justify-center bottom-0 right-0 bg-primary rounded-full p-2"> <i class="w-4 h-4 text-white" data-feather="camera"></i> </div>
                            </div>
                            <div class="ml-5">
                                <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">{{$teacher->name}}</div>
                               
                            </div>
                        </div>
                        <div class="mt-6 lg:mt-0 flex-1 px-5 border-l border-r border-slate-200/60 dark:border-darkmode-400 border-t lg:border-t-0 pt-5 lg:pt-0">
                            <div class="font-medium text-center lg:text-left lg:mt-3">Detalhes de Contato</div>
                            <div class="flex flex-col justify-center items-center lg:items-start mt-4">
                                <div class="truncate sm:whitespace-normal flex items-center"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> {{$teacher->email}} </div>
                                <div class="truncate sm:whitespace-normal flex items-center mt-3"> <i data-feather="instagram" class="w-4 h-4 mr-2"></i> {{$teacher->phone}}</div>
                                
                            </div>
                        </div>
                        <div class="mt-6 lg:mt-0 flex-1 px-5 border-t lg:border-0 border-slate-200/60 dark:border-darkmode-400 pt-5 lg:pt-0">
                            <div class="font-medium text-center lg:text-left lg:mt-5">Currículo Breve</div>
                            <div class="truncate sm:whitespace-normal flex items-center"> <?php echo $teacher->short_resume; ?> </div>
                        </div>
                    </div>
                    
                </div>
                <!-- END: Profile Info -->
                <div class="intro-y tab-content mt-5">
                    <div id="dashboard" class="tab-pane active" role="tabpanel" aria-labelledby="dashboard-tab">
                        <div class="grid grid-cols-12 gap-6">
                            <!-- BEGIN: Top Categories -->
                            <div class="intro-y box col-span-12 lg:col-span-6">
                                <div class="flex items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">
                                    <h2 class="font-medium text-base mr-auto">
                                        Meus Próximos Cursos
                                    </h2>
                                    <div class="dropdown ml-auto">
                                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-feather="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                                        <div class="dropdown-menu w-40">
                                            <ul class="dropdown-content">
                                                <li>
                                                    <a href="" class="dropdown-item"> <i data-feather="plus" class="w-4 h-4 mr-2"></i> Add Category </a>
                                                </li>
                                                <li>
                                                    <a href="" class="dropdown-item"> <i data-feather="settings" class="w-4 h-4 mr-2"></i> Settings </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-5">
                         @foreach ($panels as $panel )
                                    
                                    <div class="flex flex-col sm:flex-row" style="margin:20px">
                                        <div class="mr-auto">
                                            <a href="" class="font-medium">{{$panel->title}}</a> 
                                            <div class="text-slate-500 mt-1">{{$panel->classes->title}}</div>
                                        </div>
                                        <div class="flex">
                                          
                                            <div class="text-center">
                                                <div class="font-medium"><?php $data=$panel->start_time; 
                                                                                $data=date('d/m/Y', strtotime($data) );
                                                                                echo $data; ?></div>
                                                <div class="bg-success/20 text-success rounded px-2 mt-1.5"></div>
                                            </div>
                                        </div>
                                    </div>
                                 @endforeach
                                </div>
                            </div>
                            
                            <!-- END: Top Categories -->
                            <!-- BEGIN: Work In Progress -->
                                            <div class="intro-y box col-span-12 lg:col-span-6">
                                <div class="flex items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">
                                    <h2 class="font-medium text-base mr-auto">
                                        Meus Cursos Já Aplicados
                                    </h2>
                                    <div class="dropdown ml-auto">
                                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false" data-tw-toggle="dropdown"> <i data-feather="more-horizontal" class="w-5 h-5 text-slate-500"></i> </a>
                                        <div class="dropdown-menu w-40">
                                            <ul class="dropdown-content">
                                                <li>
                                                    <a href="" class="dropdown-item"> <i data-feather="plus" class="w-4 h-4 mr-2"></i> Add Category </a>
                                                </li>
                                                <li>
                                                    <a href="" class="dropdown-item"> <i data-feather="settings" class="w-4 h-4 mr-2"></i> Settings </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-5">
                         @foreach ($panels_aplicados as $panel )
                                    
                                    <div class="flex flex-col sm:flex-row" style="margin:20px">
                                        <div class="mr-auto">
                                            <a href="" class="font-medium">{{$panel->title}}</a> 
                                            <div class="text-slate-500 mt-1">{{$panel->classes->title}}</div>
                                        </div>
                                        <div class="flex">
                                          
                                            <div class="text-center">
                                                <div class="font-medium"><?php $data=$panel->start_time; 
                                                                                $data=date('d/m/Y', strtotime($data) );
                                                                                echo $data; ?></div>
                                                <div class="bg-success/20 text-success rounded px-2 mt-1.5"></div>
                                            </div>
                                        </div>
                                    </div>
                                 @endforeach
                                </div>
                            </div>
                            
                            <!-- END: Latest Tasks -->
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Content -->
        </div>
        <!-- BEGIN: Dark Mode Switcher-->
       
        <!-- END: Dark Mode Switcher-->
        
        <!-- BEGIN: JS Assets-->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=["your-google-map-api"]&libraries=places"></script>
        <script src="dist/js/app.js"></script>
        <!-- END: JS Assets-->
    </body>
</html>
@endsection

@push('custom-scripts')





@endpush

