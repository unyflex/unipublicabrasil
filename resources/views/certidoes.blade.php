@include('headunypublica')

@include('headerunypublica')





  <section id="titleLine">

    <div class="container">

      <div class="row">

        <div class="col-lg-12">

          <h1>Certidões</h1>

        </div>

      </div>

    </div>

  </section>





  <section id="quemSomos">

    <div class="container">

      <div class="row">

        <div class="col-lg-12">

          <h2 class="pb-2">A Unyflex é uma empresa do grupo Unypública, que é faculdade registrada no Mec, <a href="/certificacao/portaria_615.pdf">portaria 615</a>. Com especialidade em gestão pública Municipal reconhecido em todo Brasil</h2>

        </div>

      </div>

      <div class="row">

        <div class="col-lg-12">

          <p>Preparamos os agentes governamentais para a melhora dos serviços públicos, a eliminação das falhas, irregularidades, prejuízos, e responsabilizações. Promovendo a promoção da eficiência e moralização nos órgãos estatais, e contribuímos para a construção do Brasil que queremos!</p>

        </div>

      </div>

      <div class="row">

        <div class="col-lg-6">

          <div class="box-certidoes">

            <img src="https://unipublicabrasil.com.br/dev-paulo/resources/images/icon/icon-certidoes-01.png" alt="Orientações Técnicas" class="img-fluid">

            <h6>Orientações Técnicas</h6>

            <p>A Unipública fornece todo suporte jurídico que sustenta a fundamentação de nossos cursos, bem como o embasamento legal da obrigatoriedade na capacitação de Agentes e Servidores Públicos.</p>

            <div class="dropdown-certidoes">

              <span>BAIXE AQUI AS ORIENTAÇÕES TÉCNICAS</span>

              <div class="dropdown-content-certidoes">

              @foreach ($orientacoes as $orientacao )

                   <a href="{{url("storage/certificados/$orientacao->arquivo");}}" target="_blank" title="Abrir PDF">{{$orientacao->nome}}</a>

              @endforeach

               

              </div>

            </div>

          </div>

        </div>

        <div class="col-lg-6">

          <div class="box-certidoes">

            <img src="https://unipublicabrasil.com.br/dev-paulo/resources/images/icon/icon-certidoes-02.png" alt="Orientações Técnicas" class="img-fluid">

            <h6>Desempenhos Técnicos Unipública</h6>

            <p>Prova do bom desempenho da Unipública, aqui é possível verificar a capacidade técnica e boa referência desta séria escola.</p>

            <br>

            <div class="dropdown-certidoes">

              <span>BAIXE AQUI OS DESEMPENHOS TÉCNICOS</span>

              <div class="dropdown-content-certidoes">

                @foreach ($desempenhos as $desempenho )

                   <a href="{{url("storage/certificados/$desempenho->arquivo");}}" target="_blank" title="Abrir PDF">{{$desempenho->nome}}</a>

              @endforeach

              </div>

            </div>

          </div>

        </div>

        <div class="col-lg-6">

          <div class="box-certidoes">

            <img src="https://unipublicabrasil.com.br/dev-paulo/resources/images/icon/icon-certidoes-03.png" alt="Orientações Técnicas" class="img-fluid">

            <h6>Jurisprudência</h6>

            <p>Aqui você encontra o modelo dos procedimentos para tais contratações, usando como referencias os tribunais de contas.</p>

            <div class="dropdown-certidoes">

              <span>BAIXE AQUI AS JURISPRUDÊNCIA</span>

              <div class="dropdown-content-certidoes">

               @foreach ($jurisprudencias as $jurisprudencia )

        <a href="{{url("storage/certificados/$jurisprudencia->arquivo");}}" target="_blank" title="Abrir PDF">{{$jurisprudencia->nome}}</a>

              @endforeach

              </div>

            </div>

          </div>

        </div>

        <div class="col-lg-6">

          <div class="box-certidoes">

            <img src="https://unipublicabrasil.com.br/dev-paulo/resources/images/icon/icon-certidoes-04.png" alt="Orientações Técnicas" class="img-fluid">

            <h6>Certidões da Unipública</h6>

            <p>Ache aqui as certidões atualizadas da Unipublica para comprovações.</p>

            <div class="dropdown-certidoes">

              <span>BAIXE AQUI AS CERTIDÕES UNIPÚBLICA</span>

              <div class="dropdown-content-certidoes">

               @foreach ($certidoes as $certidao )

        <a href="{{url("storage/certificados/$certidao->arquivo");}}" target="_blank" title="Abrir PDF">{{$certidao->nome}}</a>

              @endforeach

                </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </section>







  <script src="./assets/vendor/js/highlight.js"></script>

  <script>

    $(document).ready(function() {

      $(".owl-carousel").owlCarousel({

        loop: true,

        margin: 10,

        dots: false,

        nav: true,

        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],

        responsive: {

          0: {

            items: 1,

            margin: 0,

            loop: false

          },

          600: {

            items: 3

          },

          1000: {

            items: 4

          }

        }

      });

    });

  </script>



  <script src="./assets/vendor/js/aos.js"></script>

  <script>

    AOS.init({

      easing: 'ease-in-out-sine'

    });



    $(function() {

      $('[data-toggle="tooltip"]').tooltip()

    })



    // Remove Items From Cart

    $('a.remove').click(function() {

      event.preventDefault();

      $(this).parent().parent().parent().hide(400);



    })



    // Just for testing, show all items

    $('a.btn.continue').click(function() {

      $('li.items').show(400);

    })

  </script>



  <script src="./assets/js/script.js"></script>



  <script>

    var acc = document.getElementsByClassName("accordion-perguntas");

    var i;



    for (i = 0; i < acc.length; i++) {

      acc[i].addEventListener("click", function() {

        this.classList.toggle("active-perguntas");

        var panel = this.nextElementSibling;

        if (panel.style.maxHeight) {

          panel.style.maxHeight = null;

        } else {

          panel.style.maxHeight = panel.scrollHeight + "px";

        }

      });

    }

  </script>

</body>



</html>