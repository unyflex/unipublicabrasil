@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')

<div class="videoWrapper" style="background-color: #000000;">
    <div id="conteudoVideo">
      
    <iframe id="player" frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player" width="100%" height="537" src="{{$video->link}}"></iframe></div>

   </div>

</div><div style="background-color: #2e2e2e;">
  <div class="row" >

            <center style="margin:0 auto;">
                
              
            
                <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold;"> UNY<span style="color:#ff6600">FLEX</span></p>
                <p style="color:#fff">CNPJ: 11227107/0001-93 &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#ff6600">www.unipublicabrasil.com.br</span>&nbsp;&nbsp; Copyright 2019 - Todos os direitos reservados.</p>
            </center>
        </div>

<script>

</script>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- <script src="https://unyflex.com.br/ava/views/assets/js/bootstrap.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unyflex.com.br/ava/views/assets/js/slick.js"></script>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script>
    function viewSession() {
        var a = eval('({"idAluno":"12129","tipo":null,"cpf":"41743114800"})');
        console.log(a)
    }
</script>

<script>
    $(document).ready(function() {
        $('.assistindo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.assistidos').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recomendados').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recentemente').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }

        $('#modal_erro').on('show.bs.modal', function(e) {
            $('#txt_url').val(window.location.href)
        })

        $('#enviar_erro').on('click', function() {
            envia_erro()
        })

    });

    function envia_erro() {

        var erro = $('#txt_erro').val()

        $.ajax({
            type: 'POST',
            url: "/ava/views/public/includes/functions.php",
            cache: false,
            data: {
                "func": "envia_erro",
                "url": window.location.href,
                "erro": erro
            },
            success: function(e) {
                if (e == 'sucesso') {
                    alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')
                    $('#modal_erro').modal('hide')
                }

            },
        })

    }

    function primeirasCategorias() {
        $('.financasmunicipais').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.controleinterno').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.juridico').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function segundasCategorias() {
        $('.rh').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.legislativo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.contadores').slick({
            dots: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }


    function terceirasCategorias() {
        $('.patrimonio').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.licitacoes').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.compras').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function quartasCategorias() {
        $('.ano').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.nova').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.sistemas').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }
</script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>
    $(window).on('mousemove', function(e) {
        var lSide = $(this).width() / 2;
        var cursorImg = e.pageX < lSide ? $('.popover__content_direita').attr('class', 'popover__content_esquerda') : $('.popover__content_esquerda').attr('class', 'popover__content_direita');

    })
</script>


<script>
    var videos = [];
    var aula = [];
    var atual;

    var altura = $(window).height();
    var alturadois = altura - 120;


    $(document).ready(function() {

        // 2. This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var idCurso = 36;
        $('#loader2').attr('style', 'display:block');
        $.ajax({
            type: 'POST',
            url: "https://unyflex.com.br/ava/views/public/includes/functions.php",
            cache: false,
            dataType: "json",
            data: {
                "func": "busca_videos_ava",
                "idCurso": idCurso,
            },
            success: function(e) {

                $.each(e, function(key, video) {
                    var idVideo = video['id_video'];
                    var vidLink = video['vid_link'];

                    aula.push(idVideo, vidLink);
                    videos.push(aula);
                    aula = [];

                });

                carregaPrimeiraAula();
                $('#loader2').attr('style', 'display:none');
            },
        })
            var Curso= '36';
            var Aluno= '12129';
        //AJAX QUE INSERE PONTUACAO
        setTimeout(function(){  

 $.ajax( 

        {
            type: 'POST',
            url: "https://unyflex.com.br/ava/views/public/includes/scriptpontos.php",
        
            cache: false,
            dataType: "json",
            data: {
                "func": "busca_videos_ava",
                "idCurso": Curso,
                "aluno" : Aluno,
            },
        });

         }, 300000);
     



    setTimeout(function(){  

$.ajax( 

       {
           type: 'POST',
           url: "https://unyflex.com.br/ava/views/public/includes/scriptpontosdez.php",
       
           cache: false,
           dataType: "json",
           data: {
               "func": "busca_videos_ava",
               "idCurso": Curso,
               "aluno" : Aluno,

              
               
               
           },
       });

        }, 600000);
    



   setTimeout(function(){  

$.ajax( 

       {
           type: 'POST',
           url: "https://unyflex.com.br/ava/views/public/includes/scriptpontosvinte.php",
       
           cache: false,
           dataType: "json",
           data: {
               "func": "busca_videos_ava",
               "idCurso": Curso,
               "aluno" : Aluno,

              
               
               
           },
       });

        }, 1200000);
    




   setTimeout(function(){  

$.ajax( 

       {
           type: 'POST',
           url: "https://unyflex.com.br/ava/views/public/includes/scriptpontosquarenta.php",
       
           cache: false,
           dataType: "json",
           data: {
               "func": "busca_videos_ava",
               "idCurso": Curso,
               "aluno" : Aluno,

              
               
               
           },
       });

        }, 2400000);
    

   });



    function carregaPrimeiraAula() {

        $('#conteudoVideo').append('<div id="player"></div>');
        $('#aulaAnterior').hide();
        $('#proximaAula').hide();
        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;

        var linkVideoAtual = videos[0][1];
        linkVideoAtual = linkVideoAtual.split('/');
        var codigoVideo = linkVideoAtual[4];

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: alturadois,
                width: '100%',
                videoId: codigoVideo,
                events: {
                    'onReady': onPlayerReady
                }
            });
            $('#proximaAula').show();
        }

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            event.target.playVideo();
        }

        atual = 0;
        onYouTubeIframeAPIReady();

    }

    $(document).on('click', '#proximaAula', function() {
        var numVideos = videos.length - 1;
        atual++;

        $('#aulaAnterior').show();

        if (atual >= numVideos) {
            $('#proximaAula').hide();
        }

        $('#conteudoVideo').html('');

        $('#conteudoVideo').append('<div id="player"></div>');

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;

        var linkVideoAtual = videos[atual][1];
        linkVideoAtual = linkVideoAtual.split('/');
        var codigoVideo = linkVideoAtual[4];

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: alturadois,
                width: '100%',
                videoId: codigoVideo,
                events: {
                    'onReady': onPlayerReady
                }
            });
        }

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            event.target.playVideo();
        }

        onYouTubeIframeAPIReady();
    });


    $(document).on('click', '#aulaAnterior', function() {

        var numVideos = videos.length - 1;
        atual--;

        if (atual == 0) {
            $('#aulaAnterior').hide();
        }

        if (atual < numVideos) {
            $('#proximaAula').show();
        }

        $('#conteudoVideo').html('');

        $('#conteudoVideo').append('<div id="player"></div>');

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;

        var linkVideoAtual = videos[atual][1];
        linkVideoAtual = linkVideoAtual.split('/');
        var codigoVideo = linkVideoAtual[4];

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: alturadois,
                width: '100%',
                videoId: codigoVideo,
                events: {
                    'onReady': onPlayerReady
                }
            });
        }

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            event.target.playVideo();
        }

        onYouTubeIframeAPIReady();

    });
</script></body></html>