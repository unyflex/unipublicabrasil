@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')
    <!-- Required meta tags -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://unyflex.com.br/ava/views/assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- SLICK CAROUSEL -->
    <link rel="stylesheet" href="https://unyflex.com.br/ava/views/assets/css/slick.css">
    <link rel="stylesheet" href="https://unyflex.com.br/ava/views/assets/css/slick-theme.css">
    <!-- STYLE.CSS -->
    <link rel="stylesheet" href="https://unyflex.com.br/ava/views/assets/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.es.gov.br/fonts/font-awesome/css/font-awesome.min.css">

    <title>Unyflex - Gestão Pública Online</title>


</head>
<style>
    body {
        background-image: url(https://www.unyflex.com.br/ava/views/public/diagmonds.png);
        background-color: #41403b;
    }
</style>

<?php 


    
        $conexao2 = mysqli_connect("localhost", "root", "", "teste1");
        $conexao2->set_charset("utf8");

//BUSCA VIDEO DO PAINEL SELECIONADO
$buscaVideo = mysqli_query($conexao2, "SELECT * FROM `tbVideos` WHERE id_video = $idVideo");
$resultVideo = mysqli_fetch_array($buscaVideo);

$idVideoAula = $resultVideo['id_video'];
$idCurso = $resultVideo['id_curso'];
$linkVideoAula = $resultVideo['vid_link'];
$codVideo = substr($linkVideoAula, 30);
$codVideo = trim($codVideo);

$tempoVideo = str_replace('-', ':', $tempoVideo);

?>
<div class="row">
    <div class="col-md-4 col-xs-12" style="">
        <div class="container">
            <div class="area-infovideo">
                <h2>Termo Pesquisado: <?php echo $termoPesquisa; ?></h2>
                <p><b>Momento Aproximado:</b> <?php echo $tempoVideo; ?></p>
                <br>
                <p><b>Como utilizar:</b></p>
                <p>- Arraste a linha do tempo do player ao lado, para o tempo desejado.</p>
                <p>- Lembrando que o momento aproximado onde o termo foi falado, pode variar em alguns segundos para mais ou para menos.</p>
                <p>- Aperte o botão play e assista sua aula.</p>
                <a href="https://unyflex.com.br/ava/info-curso/<?php echo $idCurso ?>" class="btn btn-success btn-block">Ir para página do curso</a>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        
           <iframe width="853" height="480" src="<?= $linkVideoAula ?>" title="YouTube video player" frameborder="0" style="margin-top:7%" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        
    </div>
</div>

