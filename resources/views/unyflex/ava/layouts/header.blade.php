<body><nav id="responsivo" class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

        @foreach ($categories as $category)
            
      
                  <a href="{{ route('setores', ['slug' => $category->slug]) }}">
                    <li class="nav-item">{{$category->title}}</li>
                </a>

              @endforeach
                            
                    </ul>
    </div>
    <a href="https://unyflex.com.br/ava/inicio/todos/ordem" class="logo">
        <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold; padding-right: 30px;">UNY<span style="color:#ff6600">FLEX</span></p>
    </a>
</nav>

<nav id="escondeMenu" class="navbar navbar-expand-lg navbar-light bg-light ">
    <div class="col-md-8">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">

            <a href="https://unyflex.com.br/ava/inicio/todos/ordem" class="logo">
                <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold; padding-right: 30px;"> UNY<span style="color:#ff6600">FLEX</span></p>
            </a>

            <ul class="navbar-nav">
                <!--
<li class="nav-item ">
<a class="nav-link opcao" href="https://unyflex.com.br/ava/aovivo">AO VIVO</a>
</li>
-->
                <li class="dropdown nav-item active">
                    <a class="nav-link opcao" data-toggle="dropdown" href="#">CURSOS ONLINE <i class="fas fa-angle-down" style="margin-left:5px;"></i></a>
                    <ul class="dropdown-menu">
                    
                  @foreach ($categories as $category)
            
      
                    <a href="{{ route('setores', ['slug' => $category->slug]) }}">
                    <li class="nav-item">{{$category->title}}</li>
                </a>

              @endforeach
                            
                                            </ul>
                </li>

                <!--<li class="nav-item ">
                    <a class="nav-link opcao" href="https://unyflex.com.br/ava/chat">CHAT</a>
                </li>-->

                <li class="nav-item ">
                    <a class="nav-link opcao" href="{{ route('podcasts') }}">PODCASTS</a>
                </li>

               
                
                                    <li class="nav-item ">
                        <a class="nav-link opcao" href="https://unyflex.com.br/ava/feedback">FEEDBACK</a>
                    </li>
                                <li class="nav-item ">
                    <a class="nav-link opcao" href="{{ route('materiais') }}">MATERIAIS</a>
                </li>

                <li class="dropdown nav-item active">
                    <a class="nav-link opcao" data-toggle="dropdown" href="#">
                        VOCÊ É NÍVEL 3<i class="fas fa-angle-down" style="margin-left:5px;"></i></a>
                    <ul class="dropdown-menu">

                        <a href="#">
                            <li class="nav-item">
                                <div class="row">
                                    <div class="col-xs" style="height: 80px;width: 80px;border-radius: 50%;background-color: transparent;border-color: #ff7300; border-style: solid;border-width:5px;margin-left: 2em;">
                                        <p class="text-center" style="margin-top: 0.9em; color: #ff7300; font-size: 22px; ">70%
                                        </p>
                                    </div>
                                    <div class="col-xs">
                                        <p style="color: #ff7300; font-size: 22px;margin-left: 2em;">Nível 3</p>
                                        <p style="color: #ff7300; font-size: 14px;margin-left: 2em;">6299/9000 unypoints</p>
                                    </div>
                                </div>
                            </li>
                            </a><a href="{{ route('pontuacao') }}">
                                <li class="nav-item">


                                    <p class="text-center" style="font-size: 22px; ">
                                        Pontuação</p>

                                </li>
                            </a>
                            <a href="{{ route('beneficios') }}">
                                <li class="nav-item">


                                    <p class="text-center" style="font-size: 22px; ">
                                        Benefícios </p>

                                </li>
                            </a>
                            <a href="https://unyflex.com.br/ava/ranking">
                                <li class="nav-item">


                                    <p class="text-center" style="font-size: 22px; ">
                                        Ranking </p>

                                </li>
                            </a>
                    </ul>
                </li>
                                <!-- 
<li class="nav-item ">
<a class="nav-link opcao" href="https://unyflex.com.br/ava/duvidas">DÚVIDAS</a>
</li> -->

            </ul>

        </div>
    </div>

    <div class="col-md-2">
        <div class="submit-line collapse navbar-collapse">



            <script language="javascript">
                function retirarAcento(objResp) {
                    var varString = new String(objResp.value);
                    var stringAcentos = new String('àâêôûãõáéíóúçüÀÂÊÔÛÃÕÁÉÍÓÚÇÜABCDEFGHIJKLMNOPQRSTUVWXYZ /\\');
                    var stringSemAcento = new String('aaeouaoaeioucuAAEOUAOAEIOUCUabcdefghijklmnopqrstuvwxyz-');

                    var i = new Number();
                    var j = new Number();
                    var cString = new String();
                    var varRes = '';

                    for (i = 0; i < varString.length; i++) {
                        cString = varString.substring(i, i + 1);
                        for (j = 0; j < stringAcentos.length; j++) {
                            if (stringAcentos.substring(j, j + 1) == cString) {
                                cString = stringSemAcento.substring(j, j + 1);
                            }
                        }
                        varRes += cString;
                    }
                    objResp.value = varRes;
                }
            </script>
            <script>
                var form = document.getElementById('pesquisaForm');
                if (form != null) {
                    var campo = document.getElementById('pesquisa');
                    var caminho = "https://unyflex.com.br/ava/cursos/todos/a-z/";

                    form.addEventListener('submit', function(e) {
                        // mostra o valor do campo                    
                        window.location.href = caminho + campo.value;

                        // impede o envio do form
                        e.preventDefault();
                    });
                }
            </script>

        </div>
    </div>
    <div class="col-md-2">

        <div class="collapse navbar-collapse" id="navbarNavDropdown" style="float: right;">
            <ul class="navbar-nav">
                <!--
     <p class="nav-item " style="width: 200PX; color: white; margin-top: 8px;  font-size: 12px;">
                 VOCÊ POSSUI: 6299 UNYPOINTS
                </p>&emsp;-->


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-right: 60px;font-size: 12px; margin-top: -13px;">
                                                    <img style="width: 50px; height: 50px;" src="https://unipublicabrasil.com.br/nivel3.png">&emsp; <span id="alunoUny">{{Auth::user()->name}}</span>
                        
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                        <a class="dropdown-item" href="https://unyflex.com.br/ava/financeiro">Financeiro</a>
                        <a class="dropdown-item" href="https://unyflex.com.br/ava/sair-sessao/sair/sair">Sair</a>
                    </div>
                </li>
            </ul>
            <!--<i class="fas fa-cogs" style="color: #fff;"></i>-->
        </div>
    </div>
</nav>
