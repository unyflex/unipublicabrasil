@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')

<div class="row conteudo">
    <div class="col-md-11 conteudo">
        <?php


  


        if ($ordem == '') {
            $pesquisaFinal = "SELECT * FROM `tbVideos` v INNER JOIN tbPainel p ON p.id_painel = v.id_painel INNER JOIN tbDocentes d ON p.nel_docente = d.id_docente WHERE legenda LIKE '%$termo%' ORDER BY id_cursos DESC  LIMIT 50";
        } else {
            if ($ordem == 'crescente') {
                $pesquisaFinal = "SELECT * FROM `tbVideos` v INNER JOIN tbPainel p ON p.id_painel = v.id_painel INNER JOIN tbDocentes d ON p.nel_docente = d.id_docente WHERE legenda LIKE '%$termo%' ORDER BY id_cursos ASC LIMIT 50";
            } else if ($ordem == 'decrescente') {
                $pesquisaFinal = "SELECT * FROM `tbVideos` v INNER JOIN tbPainel p ON p.id_painel = v.id_painel INNER JOIN tbDocentes d ON p.nel_docente = d.id_docente WHERE legenda LIKE '%$termo%' ORDER BY id_cursos DESC LIMIT 50";
            } else if ($ordem == 'a-b') {
                $pesquisaFinal = "SELECT * FROM `tbVideos` v INNER JOIN tbPainel p ON p.id_painel = v.id_painel INNER JOIN tbDocentes d ON p.nel_docente = d.id_docente WHERE legenda LIKE '%$termo%' ORDER BY p.nel_titulo ASC LIMIT 50";
            }
        }

        ?>

        <h2 style="color: #ffffff; padding-left: 30px; font-size: 24px; padding-top: 21px; margin-bottom: -5px;">Pesquisa: <?php echo $termo; ?></h2>

        <h3 style="color:#fff; padding-left: 30px; margin-top: 30px; font-size: 18px">Escolha a ordenação: </h3>
        <p style="color:#fff; padding-left: 30px; font-size: 15px;">
            <span style="text-decoration: underline; cursor: pointer;" onclick="filtro('crescente')">Ordenar data crescente</span>
            <span style="margin-left: 30px; text-decoration: underline; cursor: pointer;" onclick="filtro('decrescente')">Ordenar data decrescente</span>
            <span style="margin-left: 30px; text-decoration: underline; cursor: pointer;" onclick="filtro('a-b')">Ordem Alfabética</span>
        </p>

        <div class="results">
            <div class="col-md-12">
                <div class="row">

                    <?php
                        $conexao2 = mysqli_connect("localhost", "root", "", "teste1");
                        $conexao2->set_charset("utf8");
                    $buscaVideos = mysqli_query($conexao2, $pesquisaFinal);

                    if (mysqli_num_rows($buscaVideos) >= 1) {

                        $i = 0;
                        while ($resultVideos = mysqli_fetch_array($buscaVideos)) {

                            $idVideo = $resultVideos['id_video'];
                            $idCurso = $resultVideos['id_curso'];
                            $idPainel = $resultVideos['id_painel'];

                            $tituloPainel = $resultVideos['nel_titulo'];
                            $subtituloPainel = $resultVideos['nel_subtitulo'];

                            $nomePainel = $tituloPainel . ' ' . $subtituloPainel;

                            $max = 50;
                            $nomePainel =  substr_replace($nomePainel, (strlen($nomePainel) > $max ? '...' : ''), $max);


                            $nomeDocente = $resultVideos['doc_nome'];

                            $legenda = $resultVideos['legenda'];

                            //posição que foi falado o termo na string legenda completa
                            $pos = strpos($legenda, $termo);

                            $inicioAproximado = $pos - 100;
                            $fimAproximado = 200;
                            //limita a string para uma ocorrencia aproximada


                            $legendaAproximada = substr($legenda, $inicioAproximado, $fimAproximado);

                            $linhas = explode("<br>", $legendaAproximada);



                            $matches = array();
                            foreach ($linhas as $k => $v) {
                                if (preg_match("/\b$termo\b/i", $v)) {
                                    $matches[$k] = $v;
                                    $linhaTempo = $k - 1;
                                }
                            }


                            $tempoTermo = $linhas[$linhaTempo];
                            $tempoTermo = substr($tempoTermo, 0, 8);
                            $tempoTermo = str_replace(':', '-', $tempoTermo);

                            if (mb_strpos($tempoTermo, '-') !== false) {
                                $achatempo = true;
                            } else {
                                $achatempo = false;
                            }

                            $linkTermo = str_replace(' ', '-', $termo);


                            $linkTermo = preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(ç)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u c U n N"), $linkTermo);


                            if ($pos != false && $achatempo == true) {

                    ?>

                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="../../ava/assistir-curso-busca/<?php echo $idVideo ?>/<?php echo $tempoTermo ?>/<?php echo $linkTermo ?>/" target="_blank">
                                        <div class="boxcurso">
                                            <div class="icone-curso">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <i style="color:red; font-size:35px" class="fab fa-youtube"></i>
                                              
                                            </div>
                                            <div class="area-info-curso">
                                                <div class="titulo-curso">
                                                    <?php echo $nomePainel ?>
                                                </div>
                                                <div class="atributo-docente">
                                                    <?php echo $nomeDocente; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                    <?php
                                $i++;
                            }
                        }
                    } else {

                        echo '<br> <span style="color:#fff; margin-left:30px; margin-top:20px;">Não foram encontrados resultados para o termo digitado. Verifique a acentuação e tente novamente.</span>';
                    }

                    if ($i == 0 && mysqli_num_rows($buscaVideos) >= 1) {
                        echo '<br> <span style="color:#fff; margin-left:30px; margin-top:20px;">Não foram encontrados resultados para o termo digitado. Verifique a acentuação e tente novamente.</span>';
                    }


                    ?>
                    <!-- CLASS results CHAMA O AJAX DATA.PHP E MOSTRA OS CURSOS -->
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-1 d-none d-sm-none d-md-block" style="background-color: #32312f;">

        
        <img src="https://unyflex.com.br/ava/views/assets/images//bannerpesquisa.jpg" data-toggle="modal" data-target="#modalPesquisaAva" style="position: sticky; max-width:100%; cursor:pointer; top: 0px; margin-left: -6px; border-left: 1px black solid;">

    </div>
</div>
<form style="display: none;" id="formfiltro" method="POST" action="">
    <input id="ordem" type="hidden" name="ordem" value="">
    <input type="hidden" name="termo" value="<?= $termo ?>">
</form>
<script>
    function filtro(ordem) {
        $('#ordem').val(ordem);
        $('#formfiltro').submit();
    }
</script>
