@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')
    

<div class="modal fade" id="modalPesquisaAva" tabindex="-1" role="dialog" aria-labelledby="pesqavaModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content col conteudo">
      <div class="modal-header">
        <h4 class="modal-title" id="pesqavaModalLabel" style="font-weight: bold;text-align: center">Pesquisa Avançada</h4>
        <p style="color:#fff; padding-left:30px; padding-top:20px; ">Procurando por um termo falado pelo professor em sala de aula? Pesquise no campo abaixo.</p>
        <button type="button" class="close buttonclose" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row conteudopesq">
    
    <style>
        input::-webkit-input-placeholder {
            color: #b9bbc0 !important;
        }

        input:-moz-placeholder { /* Firefox 18- */
            color: #b9bbc0 !important;  
        }

        input::-moz-placeholder {  /* Firefox 19+ */
            color: #b9bbc0 !important;  
        }

        input:-ms-input-placeholder {  
            color: #b9bbc0 !important;  
        }
    </style>
    <div class="col conteudo">
        <form id="termo" action="https://unyflex.com.br/ava/resultadoTermo" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">
            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">                        
                        <input name="termo" type="text" placeholder="Ex: LRF, transparência, etc." class="form-control col-md-10" id="inputtermo" style="float:left; border-bottom-right-radius: 0px; border-top-right-radius: 0px;">
                        <input type="submit" name="pesquisar" value="Buscar" class="btn btn-success col-md-2" style="float:left; border-bottom-left-radius: 0px; border-top-left-radius: 0px;">
                    </div>                        

                </div> 
            </div>            

        </form>

        <div style="background-color:#929292; height:1px; width:70%; margin-left: 30px; margin-top: 40px;  "></div>


        <h2 style="color: #ffffff; padding-left: 30px; font-size: 18px; padding-top: 10px; margin-bottom: -5px; margin-top: 10px; font-weight:bold;text-align: center">Filtro Avançado</h2>
        <p style="color:#fff; padding-left:30px; padding-top:20px; text-align: center">Não encontrou os cursos que deseja? Gostaria de algo mais específico?</p>
        <p style="color:#fff; padding-left:30px; text-align: center">Preencha os campos abaixo com as informações que achar necessário para ajudar em sua pesquisa.</p>


        <form id="avancado" action="https://unyflex.com.br/ava/resultadoAvancado" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">

            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputcpf">Título</label>
                        <input name="titulo" type="text" class="form-control" id="inputtitulo">
                    </div>     
                    <div class="form-group col-md-12">
                        <label for="inputtelefone">Subtítulo</label>
                        <input name="subtitulo" type="text" class="form-control" id="inputsubtitulo">
                    </div>    
                    <div class="form-group col-md-4">
                        <label for="painel">Ano</label>
                        <select class="form-control" id="ano" name="ano">
                            <option value="" selected="">Selecione um ano</option>                  
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                        </select>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="painel">Departamento</label>
                        <select class="form-control" id="departamento" name="departamento">
                            <option value="" selected="">Selecione um departamento</option>                  
                            <option value="1">Ambiente e Urbanismo</option>
                            <option value="2">Compras Públicas</option>
                            <option value="3">Contadores Municipais</option>
                            <option value="4">Controle Interno</option>
                            <option value="5">Finanças Municipais</option>
                            <option value="6">Jurídico</option>
                            <option value="8">Legislativo</option>
                            <option value="11">Licitações Públicas</option>
                            <option value="10">Patrimônio</option>
                            <option value="7">Recursos Humanos</option>
                            <option value="9">Tributação Municipal</option>
                            <option value="12">Outros</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">                        
                        <input type="button" onclick="formSubmit()" value="Filtrar" name="pesquisar" class="btn btn-success btn-block">
                    </div>                    
                </div> 
            </div>            

        </form>

    </div>
</div>

<script>
    function formSubmit()
    {
        document.getElementById("avancado").submit();
    }
</script>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar mudanças</button>
      </div> -->
    </div>
  </div>
</div><link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400&amp;display=swap" rel="stylesheet">


 <style type="text/css">  body {
        
        font-family: poppins;
         background-color: #2e2e2e
    }


</style>

      

    <div class="col-md-1 d-none d-sm-none d-md-block" style="background-color:#2e2e2e; padding-left:8px; display: inline-table; position: right;float: right;">
        <img src="https://unyflex.com.br/ava/views/assets/images//bannerpesquisa.jpg" data-toggle="modal" data-target="#modalPesquisaAva" style="position: sticky; max-width:100%; cursor:pointer; top: 0px;  border-left: 1px white solid; display: inline-table;float: right;">
    </div> 

 <div class="container-fluid" style=" height: 300px;">
        <div class="row ">

             <div class="col-xs titulo-bn-pt">
                 <p style="font-size: 30px; color: #ffffff ">
                   Conheça os benefícios dos alunos Unyflex 
                </p>
                
            </div>
            <div class="col-xs">

                <div class="porcentagem">
                    <center>
                        <h2 style="margin-top: 0.9em; color: #ff7300; ">72%</h2>
                    </center>
                </div>
                
            </div>
            <div class="col-xs" style=" margin-top:3em; margin-left: 2.5em;color: #ff7300  ">
                <h6>Nível 3</h6>
                   <h3>Veterano</h3>
                        <h6>
                          6509/9000 unypoints</h6>
            </div>  
            
                  
        </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-3 box-beneficios" style="">
                <div class="carde">
                    <div class="face front" style=" background-color: #6C6C71;">
                         <div class="area-beneficio">
                             <center>
                                 <h2 class="porcentagem-titulo">10%</h2>
                             </center>
                              <p class="text-center star">
                            <i class="fas fa-star"></i>
                        </p>
                         <p class="text-center subt-beneficios">
                            <strong class="ststyle">
                                Calouro 
                            </strong>
                        </p>      
                         </div>
                       
                                          
                        
                  
                    </div>
                    <div class="face back" style=" background-color: #6C6C71;">
                                <p class="txt-back">
                                      1-Recebe até 10% de desconto nas próximas compras 
                                </p>
                    </div>
            </div>
        </div>    

                <div class="col-3 box-beneficios" style="">
                <div class="carde">
                    <div class="face front" style=" background-color: #4F4F5A;">
                         <div class="area-beneficio">
                             <center>
                                 <h2 class="porcentagem-titulo">15%</h2>
                             </center>
                              <p class="text-center star">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </p>
                           <p class="text-center subt-beneficios">
                    
                            <strong class="ststyle" style="margin-left: -25px;">
                                Universitário 
                            </strong>
                        </p>   
                         </div>
                       
                                           
                        
                  
                    </div>
                    <div class="face back" style=" background-color: #4F4F5A;">
                                <p class="txt-back">
                               1-Recebe até 15% de desconto nas próximas compras<br>
                               2-Um kit escolar unipublica completo
                                </p>
                    </div>
            </div>
        </div>   


                <div class="col-3 box-beneficios" style="">
                <div class="carde">
                    <div class="face front" style=" background-color: #353546;">
                         <div class="area-beneficio">
                             <center>
                                 <h2 class="porcentagem-titulo">20%</h2>
                             </center>
                              <p class="text-center star">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </p>
                        <p class="text-center subt-beneficios">
                            <strong class="ststyle">
                                 Veterano  
                            </strong>
                        </p>   
                         </div>
                       
                                              
                        
                  
                    </div>
                    <div class="face back" style=" background-color: #353546;">
                                <p class="txt-back">
                               1-recebe até 20% de desconto nas próximas compras<br>
                               2-Um kit escolar unipublica completo<br>
                               3-Mentoria exclusivas com nossos professores
                                </p>
                    </div>
            </div>
        </div>   
        
        
                <div class="col-3 box-beneficios" style="">
                <div class="carde">
                    <div class="face front" style=" background-color: #000023;">
                         <div class="area-beneficio">
                             <center>
                                 <h2 class="porcentagem-titulo">40%</h2>
                             </center>
                              <p class="text-center star">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </p>
                            <p class="text-center subt-beneficios">
                            <strong class="ststyle">
                                 Master Class  
                            </strong>
                        </p> 
                         </div>
                       
                                            
                        
                  
                    </div>
                    <div class="face back" style=" background-color: #000023;">
                                <p class="txt-back">
                              1-Recebe até 40% de desconto nas próximas compras<br>
                                 2-Um kit escolar unipublica completo<br> 
                                 3-Mentoria exclusivas com nossos professores <br>
                                 4-Certificados impressos<br>
                                 5-Uma bolsa de assinatura unyflex para presentear qualquer amigo <br>
                                 6-Um voucher para qualquer curso presencial
                                </p>
                    </div>
            </div>
        </div>   

      
                <div class="col-3 box-beneficios" style="">
                <div class="carde">
                    <div class="face front" style=" background-color: #02021C;">
                         <div class="area-beneficio">
                             <center>
                                 <h2 class="porcentagem-titulo">50%</h2>
                             </center>
                              <p class="text-center star">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </p>
                          <p class="text-center subt-beneficios">
                            <strong class="ststyle" style="margin-left: -25px;">
                                 Especialista  
                            </strong>
                        </p>                         
                        
                         </div>
                       
                      
                  
                    </div>
                    <div class="face back" style=" background-color: #02021C;">
                                <p class="txt-back">
                              1-Recebe até 50% de desconto nas próximas compras<br>
                                    2-Um kit escolar unipublica completo<br>
                                    3-Mentoria exclusivas com nossos professores<br>
                                    4-Certificados impressos<br>
                                    5-Uma bolsa de estudo para uma graduação de sua escolha<br>
                                    6-Uma bolsa de assinatura unyflex para presentear qualquer amigo<br>
                                    7-Dois vouchers para qualquer curso presencial<br>
                                </p>
                    </div>
                </div>
             </div> 
         </div>        
        </div>      
     


        <!--
             <div class="col-3 box-beneficios" style="background-color: #4F4F5A;">
                <div class="area-beneficio">
                    <center>
                        <h2 class="porcentagem-titulo">15%</h2>
                    </center>
                </div>
                  <p class="text-center star">
                   <i class="fas fa-star"></i>
                   <i class="fas fa-star"></i>
                </p>
                <p class="text-center subt-beneficios" >
                    <strong>
                        Universitário
                    </strong>
                </p>
                  
                <p class="text-center txt-descricao" >
                     recebe até 15% de desconto nas próximas compras + um kit escolar unipublica completo
                </p>
            </div>
    
            <div class="col-3 box-beneficios" style="background-color: #353546;">
                      <div class="area-beneficio">
                    <center>
                        <h2 class="porcentagem-titulo">20%</h2>
                    </center>
                </div>
                  <p class="text-center" style="font-size: 16px; margin-top: 5px;">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </p>
                <p class="text-center subt-beneficios" >
                    <strong>
                        Veterano
                    </strong>
                </p>
                <p class="text-center txt-descricao" >
                     recebe até 20% de desconto nas próximas compras + um kit escolar unipublica completo + mentoria exclusivas com nossos professores
                </p>
            </div>
         
             <div class="col-3 box-beneficios" style="background-color: #000023; ">
                            <div class="area-beneficio">
                    <center>
                        <h2 class="porcentagem-titulo">40%</h2>
                    </center>
                </div>
                      <p class="text-center" style="font-size: 16px; margin-top: 5px;">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                    </p>
                <p class="text-center subt-beneficios" >
                    <strong>
                       Master Class
                    </strong>
                </p>
                <p class="text-center txt-descricao" >
                     recebe até 40% de desconto nas próximas compras + um kit escolar unipublica completo + mentoria exclusivas com nossos professores + certificados impressos e 1 bolsa de assinatura unyflex para presentear qualquer amigo + 1 voucher para qualquer curso presencial
                </p>
            </div>
             <div class="col-3 box-beneficios" style="background-color: #02021C; ">
                            <div class="area-beneficio">
                    <center>
                        <h2 class="porcentagem-titulo">50%</h2>
                    </center>
                </div>
                  <p class="text-center" style="font-size: 16px; margin-top: 5px;">
                     <i class="fas fa-star"></i>
                     <i class="fas fa-star"></i>
                     <i class="fas fa-star"></i>
                     <i class="fas fa-star"></i>
                     <i class="fas fa-star"></i>
                </p>
                <p class="text-center subt-beneficios" >
                    <strong>
                        Especialista
                    </strong>
                </p>
                <p class="text-center txt-descricao" >
                     recebe até 50% de desconto nas próximas compras + um kit escolar unipublica completo + mentoria exclusivas com nossos professores + certificados impressos + 1 bolsa de estudo para 1 pós graduação de sua escolha + 1 bolsa de assinatura unyflex para presentear qualquer amigo + 2 voucher para qualquer curso presencial
                </p>-->
            </div>
          
            
      
       <div class="row" style="margin-top:800px;">

            <center style="margin:0 auto;">
                               <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold;"> UNY<span style="color:#ff6600">FLEX</span></p>
                <p style="color:#fff">CNPJ: 36.731.728/0001-30 &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#ff6600">www.unipublicabrasil.com.br</span>&nbsp;&nbsp; Copyright 2019 - Todos os direitos reservados.</p>
            </center>
        </div>
    



       
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- <script src="https://unyflex.com.br/ava/views/assets/js/bootstrap.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unyflex.com.br/ava/views/assets/js/slick.js"></script>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script>
    function viewSession() {
        var a = eval('({"idAluno":"12129","tipo":null,"cpf":"41743114800"})');
        console.log(a)
    }
</script>

<script>
    $(document).ready(function() {
        $('.assistindo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.assistidos').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recomendados').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recentemente').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }

        $('#modal_erro').on('show.bs.modal', function(e) {
            $('#txt_url').val(window.location.href)
        })

        $('#enviar_erro').on('click', function() {
            envia_erro()
        })

    });

    function envia_erro() {

        var erro = $('#txt_erro').val()

        $.ajax({
            type: 'POST',
            url: "/ava/views/public/includes/functions.php",
            cache: false,
            data: {
                "func": "envia_erro",
                "url": window.location.href,
                "erro": erro
            },
            success: function(e) {
                if (e == 'sucesso') {
                    alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')
                    $('#modal_erro').modal('hide')
                }

            },
        })

    }

    function primeirasCategorias() {
        $('.financasmunicipais').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.controleinterno').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.juridico').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function segundasCategorias() {
        $('.rh').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.legislativo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.contadores').slick({
            dots: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }


    function terceirasCategorias() {
        $('.patrimonio').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.licitacoes').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.compras').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function quartasCategorias() {
        $('.ano').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.nova').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.sistemas').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }
</script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>
    $(window).on('mousemove', function(e) {
        var lSide = $(this).width() / 2;
        var cursorImg = e.pageX < lSide ? $('.popover__content_direita').attr('class', 'popover__content_esquerda') : $('.popover__content_esquerda').attr('class', 'popover__content_direita');

    })
</script>



</body></html>