<!doctype html>

<html lang="en">

  <head>

  	<title>SEJA BEM-VINDO À UNYFLEX</title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="{{asset('app-assets/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('app-assets/css/style.css')}}">

<!-- style="background-image: linear-gradient(#333, black), url(https://unipublicabrasil.com.br/uploads/cursos/online/curso--id--data-14022019192959.jpg); background-repeat: no-repeat; background-size: cover; background-attachment: fixed ;background-blend-mode: multiply;"><header style="background: linear-gradient(90deg, rgba(255,102,0,1) 0%, rgba(255,131,49,1) 100%)" -->


	</head>                                                   

	<body class="img js-fullheight" style="background-image: url({{url('storage/banners/bg.jpg')}})">

	<section class="ftco-section">

		<div class="container">

			<div class="row justify-content-center">

				<div class="col-md-6 text-center mb-5">

					<h2 class="heading-section">SEJA BEM-VINDO À UNYFLEX</h2>

				</div>

			</div>

			<div class="row justify-content-center">

				<div class="col-md-6 col-lg-4">

					<div class="login-wrap p-0">
		      	
		      			<form action="#" class="signin-form">

		      				<div class="form-group">

		      					<input type="text" class="form-control" placeholder="Username" required>

		      				</div>

	            			<div class="form-group">

	                			<input id="password-field" type="password" class="form-control" placeholder="Password" required>

	                			<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>

	            			</div>           
                               
	            			<button style="width: 49%" type="submit" class="form-control btn btn-primary submit px-3">Acessar</button>               

	            			<button id="teste" type="submit" class="form-control btn btn-primary submit px-3">Voltar</button>                             
                               
	            			<div class="form-group d-md-flex">

	            				<div class="w-50">

		            				<label class="checkbox-wrap checkbox-primary">Lembre-Me

									<input type="checkbox" checked>

									<span class="checkmark"></span>

									</label>

								</div>
								<!--<div class="w-50 text-md-right">
									<a href="#" style="color: #fff">Forgot Password</a>
								</div>-->
	            			</div>

	            		</form>

					</div>	
						
				</div>

			</div>

	        <p class="w-100 text-center"> * Se ainda não cadastrou uma senha, utilize seu CPF no campo senha. </p>
	          
		</div>				
	
	</section>

	<script src="{{asset('app-assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('app-assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('app-assets/js/main.js')}}"></script>
    <script src="{{asset('app-assets/js/popper.js')}}"></script>
	</body>
</html>

