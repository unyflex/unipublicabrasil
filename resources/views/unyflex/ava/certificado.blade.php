<html lang="pt" class="no-js"><head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="https://unyflex.com.br/views/assets/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://unyflex.com.br/views/assets/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://unyflex.com.br/views/assets/fav/favicon-16x16.png">
    <link rel="manifest" href="https://unyflex.com.br/views/assets/fav/site.webmanifest">
    <link rel="mask-icon" href="https://unyflex.com.br/views/assets/fav/safari-pinned-tab.svg" color="#ff6600">
    <meta name="apple-mobile-web-app-title" content="Unyflex">
    <meta name="application-name" content="Unyflex">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- STYLE.CSS -->
    <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/style.css">
    <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/newstyle.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!--FONTES EXTERNAS-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700" rel="stylesheet">
    <!-- Player -->
    <link href="//vjs.zencdn.net/7.8.2/video-js.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- SLICK CAROUSEL -->
    <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/slick.css">
    <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/slick-theme.css">
    <!-- STYLE.CSS -->
    <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.es.gov.br/fonts/font-awesome/css/font-awesome.min.css">
    <script type="text/javascript" async="" src="https://www.googleadservices.com/pagead/conversion_async.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="https://www.googletagmanager.com/gtag/js?id=G-1ZZTP95N9C&amp;l=dataLayer&amp;cx=c"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MW4GW4P"></script><script src="https://connect.facebook.net/signals/config/766200407638558?v=2.9.57&amp;r=stable" async=""></script><script src="https://connect.facebook.net/signals/config/557906795071452?v=2.9.57&amp;r=stable" async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script src="https://unyflex.com.br/views/assets/css/profonts.js"></script><link href="https://kit-pro.fontawesome.com/releases/latest/css/pro-v4-shims.min.css" media="all" rel="stylesheet"><link href="https://kit-pro.fontawesome.com/releases/latest/css/pro-v4-font-face.min.css" media="all" rel="stylesheet"><link href="https://kit-pro.fontawesome.com/releases/latest/css/pro.min.css" media="all" rel="stylesheet">

    <title>Unyflex - Flexibilidade no Ensino Unipública</title>
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '557906795071452');
        fbq('track', 'PageView');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '766200407638558');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=766200407638558&ev=PageView
&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '766200407638558');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=766200407638558&ev=PageView
&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=557906795071452&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-166619218-1"></script>


    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-166619218-1');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MW4GW4P');
    </script>
    <!-- End Google Tag Manager -->

    <script type="text/javascript" async="" src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/19516d45-aa63-48d8-be9a-0f95ff676c29-loader.js"></script>

    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    
    fbq('init', '766200407638558');
    fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=766200407638558&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

<meta charset="iso-8859-1"><title>Certificado Unipública</title><meta name="description" content=""><meta name="viewport" content="width=device-width"><link rel="stylesheet" href="https://unipublicabrasil.com.br/aluno/css/bootstrap2.min.css"><style>

        td{
            width:600px;
            font-size:12px;
            font-weight:bold;
           
           
        }
        th{
            font-size:15px;
           
        }
        p{
            font-size:13px;
        }
		@media print {
			.botao_imprimir {
				display: none;
			}
		}
	</style><script src="https://www.googleadservices.com/pagead/conversion/429051028/?random=1650480984035&amp;cv=9&amp;fst=1650480984035&amp;num=1&amp;value=0&amp;label=rEabCOT-q4IDEJSZy8wB&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=6&amp;u_tz=-180&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wg4i1&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Funyflex.com.br%2FcertificadoKIEIT&amp;ref=https%3A%2F%2Funyflex.com.br%2FassistirPresencial%2F2612&amp;tiba=Unyflex%20-%20Flexibilidade%20no%20Ensino%20Unip%C3%BAblica&amp;auid=645882134.1649094877&amp;hn=www.googleadservices.com&amp;bttype=purchase&amp;async=1&amp;rfmt=3&amp;fmt=4"></script><script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/429051028/?random=1650480984038&amp;cv=9&amp;fst=1650480984038&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=6&amp;u_tz=-180&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wg4i1&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Funyflex.com.br%2FcertificadoKIEIT&amp;ref=https%3A%2F%2Funyflex.com.br%2FassistirPresencial%2F2612&amp;tiba=Unyflex%20-%20Flexibilidade%20no%20Ensino%20Unip%C3%BAblica&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script></head>



<body onload="window.print()">

	<center>
		<div class="botao_imprimir">
			<p><a class="btn btn-default" href="javascript:window.print();">Imprimir</a></p>
		</div>
	</center>
	<div class="container">
		<div style="position: absolute;">
        
			<!--<img src="http://www.unipublicabrasil.com.br/aluno/img/certificado_presencial.jpg" style="width: 595px; height: 842px;">-->
                        <img src="{{url("storage/galeria/certificadosemdados.png");}}" style="width:773px;">
		</div>
		<div class="info" style="position: absolute; margin-top:245px;margin-left: 260px; margin-top: 200px ">

			<p style="color:#fff; font-size:22px; font-family: Century Schoolbook "> {{Auth::user()->name}}</b></p>
			
			</div>

            	<div class="info" style="position: absolute; margin-top:245px;margin-left: 242px; margin-top: 243px ">

			<p style="color:#fff; font-size:22px; font-family: Century Schoolbook "> {{Auth::user()->cpf}}</b></p>
			
			</div>
		</div>
		<div class="resultado" style="position: absolute; margin-top:277px;margin-left: 465px ">
			<p style="color:#fff; font-size:16px;font-family: Century Schoolbook"><b> {{$curso->title}}</b></p>
					</div>


                    		<div class="resultado" style="position: absolute; margin-top:367px;margin-left: 165px ">
			<p style="color:#fff; font-size:12px;font-family: Century Schoolbook"><b> Token:{{$tokeen}}</b></p>
					</div>

	</b></div><b>

</b></div></body></html>