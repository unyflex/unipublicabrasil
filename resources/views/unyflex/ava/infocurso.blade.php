@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')
    



<div class="modal fade" id="modalPesquisaAva" tabindex="-1" role="dialog" aria-labelledby="pesqavaModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content col conteudo" style="opacity: 1;">
      <div class="modal-header">
        <h4 class="modal-title" id="pesqavaModalLabel" style="font-weight: bold;text-align: center">Pesquisa Avançada</h4>
        <p style="color:#fff; padding-left:30px; padding-top:20px; ">Procurando por um termo falado pelo professor em sala de aula? Pesquise no campo abaixo.</p>
        <button type="button" class="close buttonclose" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row conteudopesq">
    
    <style>
        input::-webkit-input-placeholder {
            color: #b9bbc0 !important;
        }

        input:-moz-placeholder { /* Firefox 18- */
            color: #b9bbc0 !important;  
        }

        input::-moz-placeholder {  /* Firefox 19+ */
            color: #b9bbc0 !important;  
        }

        input:-ms-input-placeholder {  
            color: #b9bbc0 !important;  
        }
    </style>
    <div class="col conteudo" style="opacity: 1;">
        <form id="termo" action="https://unyflex.com.br/ava/resultadoTermo" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">
            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">                        
                        <input name="termo" type="text" placeholder="Ex: LRF, transparência, etc." class="form-control col-md-10" id="inputtermo" style="float:left; border-bottom-right-radius: 0px; border-top-right-radius: 0px;">
                        <input type="submit" name="pesquisar" value="Buscar" class="btn btn-success col-md-2" style="float:left; border-bottom-left-radius: 0px; border-top-left-radius: 0px;">
                    </div>                        

                </div> 
            </div>            

        </form>

        <div style="background-color:#929292; height:1px; width:70%; margin-left: 30px; margin-top: 40px;  "></div>


        <h2 style="color: #ffffff; padding-left: 30px; font-size: 18px; padding-top: 10px; margin-bottom: -5px; margin-top: 10px; font-weight:bold;text-align: center">Filtro Avançado</h2>
        <p style="color:#fff; padding-left:30px; padding-top:20px; text-align: center">Não encontrou os cursos que deseja? Gostaria de algo mais específico?</p>
        <p style="color:#fff; padding-left:30px; text-align: center">Preencha os campos abaixo com as informações que achar necessário para ajudar em sua pesquisa.</p>


        <form id="avancado" action="https://unyflex.com.br/ava/resultadoAvancado" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">

            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputcpf">Título</label>
                        <input name="titulo" type="text" class="form-control" id="inputtitulo">
                    </div>     
                    <div class="form-group col-md-12">
                        <label for="inputtelefone">Subtítulo</label>
                        <input name="subtitulo" type="text" class="form-control" id="inputsubtitulo">
                    </div>    
                    <div class="form-group col-md-4">
                        <label for="painel">Ano</label>
                        <select class="form-control" id="ano" name="ano">
                            <option value="" selected="">Selecione um ano</option>                  
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                        </select>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="painel">Departamento</label>
                        <select class="form-control" id="departamento" name="departamento">
                            <option value="" selected="">Selecione um departamento</option>                  
                            <option value="1">Ambiente e Urbanismo</option>
                            <option value="2">Compras Públicas</option>
                            <option value="3">Contadores Municipais</option>
                            <option value="4">Controle Interno</option>
                            <option value="5">Finanças Municipais</option>
                            <option value="6">Jurídico</option>
                            <option value="8">Legislativo</option>
                            <option value="11">Licitações Públicas</option>
                            <option value="10">Patrimônio</option>
                            <option value="7">Recursos Humanos</option>
                            <option value="9">Tributação Municipal</option>
                            <option value="12">Outros</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">                        
                        <input type="button" onclick="formSubmit()" value="Filtrar" name="pesquisar" class="btn btn-success btn-block">
                    </div>                    
                </div> 
            </div>            

        </form>

    </div>
</div>

<script>
    function formSubmit()
    {
        document.getElementById("avancado").submit();
    }
</script>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar mudanças</button>
      </div> -->
    </div>
  </div>
</div>
<div id="loader2" style="margin: 0px auto; background-color: rgb(35, 35, 35); height: 1500px; display: none;">
    <center>
        <img style="margin-top:10%" src="https://unyflex.com.br/ava/views/assets/images/loader.gif">
    </center>
</div>




<div class="row conteudo">
        <div class="col-md-11 conteudo" style="padding-right: 0; padding-left: 0;">
        <div class="col-md-12" style="background-image: url('https://www.unipublicabrasil.com.br/uploads/cursos/1eb4a864dfd1997224265efda90f0bdd27072021015150.png'); background-repeat: no-repeat; background-position: center; background-attachment: fixed; background-size: cover; position: absolute; z-index: 0; -webkit-filter: brightness(0.30); filter: brightness(0.30) blur(14px); height: 97%; width: 100%;">

        </div>
        

        <div class="col-md-12">
            <a href="https://unyflex.com.br/ava/inicio/todos/ordem"><i class="fas fa-times fa-3x" style="color: #fff;"></i></a>
            <div class="titulo-info">
               {{$class->title}}           
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="info-curso">
                        <span style="color: #ff6600">{{$ano}}</span>&ensp;&ensp;
                        •&ensp;&ensp;Carga Horária: <span style="color: #ff6600">{{$class->workload}}h</span>&ensp;&ensp;
                        <!--•&ensp;&ensp;Tempo de Vídeo: <span style="color: #ff6600">
h</span>&ensp;&ensp;•&ensp;&ensp;<i class="fas fa-star" style="color: #ff6600;"></i> <i class="fas fa-star" style="color: #ff6600;"></i> <i class="fas fa-star" style="color: #ff6600;"></i> <i class="fas fa-star" style="color: #ff6600;"></i> <i class="fas fa-star" style="color: #ff6600;"></i>-->
                    </div>
                </div>
                <div class="col-md-2"></div>
            
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5" style="padding-top: 50px;">

        @foreach ($class->panels as $panel)
            
                         <div class="painel">
                            PAINEL {{$loop->iteration}}                        </div>
                        <div class="titulo-painel">
                            {{$panel->title}}                        </div>
                        <div class="programacao-painel">
                                                    </div>
                        <div class="programacao-painel" style="color:#adabab;">
                        <a href="/ava/professor/{{$panel->teachers->name}}" style="color:#adabab;">
                            <i class="fas fa-graduation-cap"></i>&nbsp;&nbsp;&nbsp;{{$panel->teachers->name}}  
                        </a> 
                        </div>
                        <div class="ver-programacao-painel">
                            <!-- Button trigger modal -->
                            <a href="" data-toggle="modal" data-target="#verProgramacao{{$loop->iteration}}  ">Ver programação</a>
                        </div>


  @endforeach
                                    </div>

                <div class="col-md-5" style="padding-top: 50px;">
                    <img src="{{url("storage/cursos/banner/$class->photo");}}" style="width: 452px; border: 1px solid #000;">
                                            <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <div class="favoritos">
                                    <!--
<i class="far fa-heart"></i> Adicionar aos favoritos
-->
                                </div>
                                <div class="assistir">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <form action="" method="post">
                                        <button type="button" class="btn btn-secondary assistir"  data-toggle="modal" data-target="#assistir">ASSISTIR</button></form>
                                        <div class="btn-group" role="group" style="display: none;">
                                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary assistir-curso dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-camera fa-2x" style="color: #fff;"></i>
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="background-color: #ff6600;">
                                                                                                    <a class="dropdown-item" href="https://unyflex.com.br/ava/assistir-curso/335/1">Painel 1</a>
                                                                                                    <a class="dropdown-item" href="https://unyflex.com.br/ava/assistir-curso/335/2">Painel 2</a>
                                                                                                    <a class="dropdown-item" href="https://unyflex.com.br/ava/assistir-curso/335/3">Painel 3</a>
                                                                                                    <a class="dropdown-item" href="https://unyflex.com.br/ava/assistir-curso/335/4">Painel 4</a>
                                                                                                    <a class="dropdown-item" href="https://unyflex.com.br/ava/assistir-curso/335/5">Painel 5</a>
                                                                                                    <a class="dropdown-item" href="https://unyflex.com.br/ava/assistir-curso/335/6">Painel 6</a>
                                                                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="materiais-curso">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-outline-primary materiais" data-toggle="modal" data-target="#exampleModal">Ver materiais do curso</button>
                                </div>
                                @if($questions!=0)
                                            <div class="materiais-curso">
                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-outline-primary materiais" data-toggle="modal" data-target="#provaModal">Ver prova</button>
                                            </div>
                                 @endif


                                    @if($certificado>0)
                                            <div class="materiais-curso">
                                                 <form action="{{ route('ava-certificado') }}" method="post" enctype="multipart/form-data" data-single="true" method="post">
                                                        <center>
                                                        @csrf
                                                        <input type="hidden" value="{{$class->id}}" name="idcurso">
                                                        <button type="submit" class="btn btn-outline-primary materiais">Ver Certificado</button>
                                                        </center>
                                                        </form>        
                                              
                                            </div>
                                 @endif
                                                                <div class="materiais-curso">
                                    <!-- Button trigger modal -->
                                    <a href="/ava/duvida/{{$slug}}"><button type="button" class="btn btn-outline-primary materiais">Tira Dúvidas</button></a>
                                </div>


                              
                                <!--
<div class="questionario-curso">
<button type="button" class="btn btn-outline-primary questionario">Responder questionário</button>
</div>
-->
                            </div>
                        </div>
                                    </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>
 

    <div class="modal fade" id="modal_erro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Reportar erro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" id="txt_url" disabled="">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="txt_erro" rows="3" placeholder="Descreva o erro em detalhes para que possamos corrigí-los o mais breve possível"></textarea>
                        </div>
                        <button type="button" class="btn mb-2" style="float: right; background-color: #ff6600; color:#fff" id="enviar_erro">Enviar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    @foreach ($class->panels as $panel)
    <!-- Modal -->
    <div class="modal fade" id="verProgramacao{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="verProgramacaoLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header programacao">
                    <h5 class="modal-title titulo-programacao" id="verProgramacaoLabel">{{$panel->title}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body programacao">
                    <p><span style="font-weight: 400;">
                   <?php echo $panel->content; ?>
                     </div>
                <div class="modal-footer programacao">
                    <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>
                </div>
            </div>
         </div>
            </div>
      
@endforeach

    <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header programacao">
                <h5 class="modal-title titulo-programacao" id="exampleModalLabel">Materiais </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body programacao">

                                    <table class="table table-hover table-dark">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Tipo</th>
                                
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($materials as $material)
                                 @foreach ($material->material as $materiais)
                                
                           
                                                            <tr>
                                    <th scope="row"><button style="background-color: transparent; border:none;" onclick="sample(this.value);"> 
                                        <a target="_blank" class="btn btn-primary" href="{{url("storage/materials/$materiais->file_name")}}" style="color: #fff; text-decoration: none; font-weight: bold; width: 145px; height: 45px; background-color: #ff6600; border-radius: 8px; border-color: #ff6600; padding: 0.5rem;">
                                                <i class="fas fa-download"></i> Download 
                                        </a></button>
                                    </th>
                                    <td style="vertical-align: middle;">{{$materiais->name}}</td>
                                    <td style="vertical-align: middle;">{{$materiais->type}}</td>
                                  
                                </tr>
                                    @endforeach 
                                @endforeach               
                            
                        </tbody>
                    </table>
                

            </div>
            <div class="modal-footer programacao">
                <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>  

<!-- Modal Assistir -->

 <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="assistir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header programacao">
                <h5 class="modal-title titulo-programacao" id="exampleModalLabel">Vídeo Aulas </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body programacao">

                                    <table class="table table-hover table-dark">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Painel</th>
                                <th scope="col">Professor</th>
                                
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($videoaulas as $videoaul)
                                @foreach ($videoaul->video_lesson as $videoaula)
                                
                                
                           
                                                            <tr>
                                    <th scope="row"><button style="background-color: transparent; border:none;" onclick="sample(this.value);"> 
                                        <a target="_blank" class="btn btn-primary" href="/ava/assistir/{{$slug}}/{{$videoaula->id}}" style="color: #fff; text-decoration: none; font-weight: bold; width: 145px; height: 45px; background-color: #ff6600; border-radius: 8px; border-color: #ff6600; padding: 0.5rem;">
                                                <i class="fas fa-download"></i> Assistir Aula 
                                        </a></button>
                                    </th>
                                    <td style="vertical-align: middle;">{{$videoaul->title}}</td>
                                    <td style="vertical-align: middle;">{{$videoaul->teachers->name}}</td>
                                  
                                </tr>
                                   
                                @endforeach   
                                @endforeach              
                            
                        </tbody>
                    </table>
                

            </div>
            <div class="modal-footer programacao">
                <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>  

  <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="provaModal" tabindex="-1" role="dialog" aria-labelledby="provaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header programacao">
                <h5 class="modal-title titulo-prova" id="provaModalLabel">Prova</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body prova">

                <p>
                    O questionário sobre o curso estará disponível assim que clicar no botão começar. Não será permitido fechar esta janela após o início da prova. Assim que o tempo expirar as respostas selecionadas serão enviadas automaticamente.
                </p>

                
                <p>
                    Tentatívas Restantes: 2                </p>

                <p>
                    Duração Máxima: 30 minutos.
                </p>
                                            <a href="/ava/prova/{{$class->slug}}">
                                <button class="btn btn-primary"  onclick="move()">Começar</button>                
                                </a>
            </div>
            <div class="modal-footer programacao">
                <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


        <div id="placeHolder" style="color: #fff"></div>



        <div class="row" style="margin-top:60px;">

            <center style="margin:0 auto;">
                <div id="loader" style="display:none">
                    <img src="https://unyflex.com.br/ava/views/assets/images/loader.gif">
                    <p style="color:#fff; font-size:12px;">procurando novos cursos</p>
                </div>
                <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold;"> UNY<span style="color:#ff6600">FLEX</span></p>
                <p style="color:#fff">CNPJ: 11227107/0001-93 &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#ff6600">www.unipublicabrasil.com.br</span>&nbsp;&nbsp; Copyright 2019 - Todos os direitos reservados.</p>
            </center>
        </div>


    </div>



    <div class="col-md-1 d-none d-sm-none d-md-block" style="    background-color:#2e2e2e; padding-left:8px;">

        <img src="https://unyflex.com.br/ava/views/assets/images//bannerpesquisa.jpg" data-toggle="modal" data-target="#modalPesquisaAva" style="position: sticky; max-width:100%; cursor:pointer; top: 0px; margin-left: -6px; border-left: 1px black solid;">

    </div>

</div>
<script>

    function move() {
        var elem = document.getElementById("myBar"); 
        var width = 1;
        var id = setInterval(frame, 18000);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
                //submit no form                
                alert("Seu tempo de prova expirou, suas respostas foram enviadas.");
                document.getElementById("resultadoProva").submit();

            } else {
                width++; 
                elem.style.width = width + '%';  
                elem.innerHTML = width * 1  + '%';

            }
        }
    }

</script>



    <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="certificado" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header programacao">
                <h5 class="modal-title titulo-programacao" id="exampleModalLabel">Certificação Unyflex</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body programacao">
                <form action="../certificacao" method="post">
                    <p>Para se certificar pela Unyflex você terá que fazer uma avaliação de conhecimento de acordo com o conteudo assistido. Essa avaliação será uma prova com quatro questões objetivas, em 3 dias a partir do dia da solicitação você receberá por email a avaliação.</p>
                    <p>O prazo para execução da prova e retorno da mesma é de 7 dias corridos, após a correção de sua avaliação, em um prazo de 15 dias você recebera seu certificado, isto se alcançar a nota de valor 7,0.</p>
                    <p>Os demais detalhes seram mandados junto com a prova.</p>
                    <p>Duvidas sobre a certificação.
                        <br><a class="btn btn-primary" href="http://unyflex.com.br/contato">Entre em Contato</a></p>
                    <div class="text-center">
                    <input type="hidden" value="335" name="idCurso">    
                    <input type="hidden" value="41743114800" name="usuario">    
                    <input class="btn btn-success btn-block" type="submit" value="Enviar Solicitação de Certificado">
                    </div>    
                </form>                
            </div>
            <div class="modal-footer programacao">
                <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

    <!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- <script src="https://unyflex.com.br/ava/views/assets/js/bootstrap.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unyflex.com.br/ava/views/assets/js/slick.js"></script>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script>
    function viewSession() {
        var a = eval('({"idAluno":"12129","tipo":null,"cpf":"41743114800"})');
        console.log(a)
    }
</script>

<script>
    $(document).ready(function() {
        $('.assistindo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.assistidos').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recomendados').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recentemente').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }

        $('#modal_erro').on('show.bs.modal', function(e) {
            $('#txt_url').val(window.location.href)
        })

        $('#enviar_erro').on('click', function() {
            envia_erro()
        })

    });

    function envia_erro() {

        var erro = $('#txt_erro').val()

        $.ajax({
            type: 'POST',
            url: "/ava/views/public/includes/functions.php",
            cache: false,
            data: {
                "func": "envia_erro",
                "url": window.location.href,
                "erro": erro
            },
            success: function(e) {
                if (e == 'sucesso') {
                    alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')
                    $('#modal_erro').modal('hide')
                }

            },
        })

    }

    function primeirasCategorias() {
        $('.financasmunicipais').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.controleinterno').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.juridico').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function segundasCategorias() {
        $('.rh').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.legislativo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.contadores').slick({
            dots: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }


    function terceirasCategorias() {
        $('.patrimonio').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.licitacoes').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.compras').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function quartasCategorias() {
        $('.ano').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.nova').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.sistemas').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }
</script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>
    $(window).on('mousemove', function(e) {
        var lSide = $(this).width() / 2;
        var cursorImg = e.pageX < lSide ? $('.popover__content_direita').attr('class', 'popover__content_esquerda') : $('.popover__content_esquerda').attr('class', 'popover__content_direita');

    })
</script>


</div></body></html>