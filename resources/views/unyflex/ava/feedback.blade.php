@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')
    



<div class="modal fade" id="modalPesquisaAva" tabindex="-1" role="dialog" aria-labelledby="pesqavaModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content col conteudo">
      <div class="modal-header">
        <h4 class="modal-title" id="pesqavaModalLabel" style="font-weight: bold;text-align: center">Pesquisa Avançada</h4>
        <p style="color:#fff; padding-left:30px; padding-top:20px; ">Procurando por um termo falado pelo professor em sala de aula? Pesquise no campo abaixo.</p>
        <button type="button" class="close buttonclose" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row conteudopesq">
    
    <style>
        input::-webkit-input-placeholder {
            color: #b9bbc0 !important;
        }

        input:-moz-placeholder { /* Firefox 18- */
            color: #b9bbc0 !important;  
        }

        input::-moz-placeholder {  /* Firefox 19+ */
            color: #b9bbc0 !important;  
        }

        input:-ms-input-placeholder {  
            color: #b9bbc0 !important;  
        }
    </style>
    <div class="col conteudo">
        <form id="termo" action="https://unyflex.com.br/ava/resultadoTermo" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">
            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">                        
                        <input name="termo" type="text" placeholder="Ex: LRF, transparência, etc." class="form-control col-md-10" id="inputtermo" style="float:left; border-bottom-right-radius: 0px; border-top-right-radius: 0px;">
                        <input type="submit" name="pesquisar" value="Buscar" class="btn btn-success col-md-2" style="float:left; border-bottom-left-radius: 0px; border-top-left-radius: 0px;">
                    </div>                        

                </div> 
            </div>            

        </form>

        <div style="background-color:#929292; height:1px; width:70%; margin-left: 30px; margin-top: 40px;  "></div>


        <h2 style="color: #ffffff; padding-left: 30px; font-size: 18px; padding-top: 10px; margin-bottom: -5px; margin-top: 10px; font-weight:bold;text-align: center">Filtro Avançado</h2>
        <p style="color:#fff; padding-left:30px; padding-top:20px; text-align: center">Não encontrou os cursos que deseja? Gostaria de algo mais específico?</p>
        <p style="color:#fff; padding-left:30px; text-align: center">Preencha os campos abaixo com as informações que achar necessário para ajudar em sua pesquisa.</p>


        <form action="https://unyflex.com.br/ava/resultadoAvancado" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">

            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputcpf">Título</label>
                        <input name="titulo" type="text" class="form-control" id="inputtitulo">
                    </div>     
                    <div class="form-group col-md-12">
                        <label for="inputtelefone">Subtítulo</label>
                        <input name="subtitulo" type="text" class="form-control" id="inputsubtitulo">
                    </div>    
                    <div class="form-group col-md-4">
                        <label for="painel">Ano</label>
                        <select class="form-control" id="ano" name="ano">
                            <option value="" selected="">Selecione um ano</option>                  
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                        </select>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="painel">Departamento</label>
                        <select class="form-control" id="departamento" name="departamento">
                            <option value="" selected="">Selecione um departamento</option>                  
                            <option value="1">Ambiente e Urbanismo</option>
                            <option value="2">Compras Públicas</option>
                            <option value="3">Contadores Municipais</option>
                            <option value="4">Controle Interno</option>
                            <option value="5">Finanças Municipais</option>
                            <option value="6">Jurídico</option>
                            <option value="8">Legislativo</option>
                            <option value="11">Licitações Públicas</option>
                            <option value="10">Patrimônio</option>
                            <option value="7">Recursos Humanos</option>
                            <option value="9">Tributação Municipal</option>
                            <option value="12">Outros</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">                        
                        <input type="button" onclick="formSubmit()" value="Filtrar" name="pesquisar" class="btn btn-success btn-block">
                    </div>                    
                </div> 
            </div>            

        </form>

    </div>
</div>

<script>
    function formSubmit()
    {
        document.getElementById("avancado").submit();
    }
</script>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar mudanças</button>
      </div> -->
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="m_sucesso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Feedback enviado com sucesso!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Agradecemos a sua opinião !
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="row conteudo">
    <div class="col-md-11 conteudo">

 <form action="{{ route('inserefeedback') }}" method="post" enctype="multipart/form-data" data-single="true" method="post">

                @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8" style="margin-left: auto; margin-right:auto">
                    <div class="card shadow p-3 mb-5 bg-white rounded">
                        <h2 class="card-header">A sua opinião é muito importante para nós!</h2>
                        <small class="card-header text-muted">Conte-nos um pouco da sua experiência em nossa plataforma!</small>
                        <div class="card-body">
                            <form method="POST" action="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="p1">O que achou da Unyflex?</label>
                                            <select class="form-control" id="p1" name="acho">
                                                <option selected="">Selecione a resposta</option>
                                                <option>Ruim</option>
                                                <option>Médio</option>
                                                <option>Bom</option>
                                                <option>Ótimo</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="p2">O que você mais utiliza em nossa plataforma?</label>
                                            <select class="form-control" id="p2" name="utilizacao">
                                                <option selected="">Selecione a resposta</option>
                                                <option>Videos</option>
                                                <option>Áudios</option>
                                                <option>Transmissões</option>
                                                <option>Materiais</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="p3">O que achou da qualidade dos nossos videos e áudios?</label>
                                            <select class="form-control" id="p3" name="qualidade">
                                                <option selected="">Selecione a resposta</option>
                                                <option>Ruim</option>
                                                <option>Médio</option>
                                                <option>Bom</option>
                                                <option>Ótimo</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="p4">Você já conhecia a Unipública?</label>
                                            <select class="form-control" id="p4" name="conhecia">
                                                <option selected="">Selecione a resposta</option>
                                                <option>Sim</option>
                                                <option>Não</option>
                                                <option>Sim, mas não era aluno</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="p5">Você é Servidor Público?</label>
                                            <select class="form-control" id="p5" name="servidor">
                                                <option selected="">Selecione a resposta</option>
                                                <option>Sim</option>
                                                <option>Não</option>
                                                <option>Não, mas pretendo ser</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="p6">Você indicaria a nossa plataforma a um amigo?</label>
                                            <select class="form-control" id="p6" name="indicaria">
                                                <option selected="">Selecione a resposta</option>
                                                <option value="Sim">Sim</option>
                                                <option value="Não">Não</option>
                                            </select>
                                        </div>
                                        <?php $student = Auth::user()->student_id; ?>
                                    <input type="hidden" value="{{$student}}" name="id_studente">
                                    </div>
                                    <div class="col-md-6" style="height: auto">
                                        <img src="https://unipublicabrasil.com.br/uploads/email/uny_sf.png" style="margin: auto;    
    display: block; padding-top: 35px">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-block" style="background-color:#ff6600; border: 0px">Enviar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
</form>
        </div>
    </div>
    <div class="col-md-1 d-none d-sm-none d-md-block" style="    background-color: #32312f;">

        <img src="https://unyflex.com.br/ava/views/assets/images//bannerpesquisa.jpg" data-toggle="modal" data-target="#modalPesquisaAva" style="position: sticky; max-width:100%; cursor:pointer; top: 0px; margin-left: -6px; border-left: 1px black solid;">

    </div>

    <!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- <script src="https://unyflex.com.br/ava/views/assets/js/bootstrap.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unyflex.com.br/ava/views/assets/js/slick.js"></script>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script>
    function viewSession() {
        var a = eval('({"idAluno":"12129","tipo":null,"cpf":"41743114800"})');
        console.log(a)
    }
</script>

<script>
    $(document).ready(function() {
        $('.assistindo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.assistidos').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recomendados').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recentemente').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }

        $('#modal_erro').on('show.bs.modal', function(e) {
            $('#txt_url').val(window.location.href)
        })

        $('#enviar_erro').on('click', function() {
            envia_erro()
        })

    });

    function envia_erro() {

        var erro = $('#txt_erro').val()

        $.ajax({
            type: 'POST',
            url: "/ava/views/public/includes/functions.php",
            cache: false,
            data: {
                "func": "envia_erro",
                "url": window.location.href,
                "erro": erro
            },
            success: function(e) {
                if (e == 'sucesso') {
                    alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')
                    $('#modal_erro').modal('hide')
                }

            },
        })

    }

    function primeirasCategorias() {
        $('.financasmunicipais').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.controleinterno').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.juridico').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function segundasCategorias() {
        $('.rh').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.legislativo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.contadores').slick({
            dots: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }


    function terceirasCategorias() {
        $('.patrimonio').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.licitacoes').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.compras').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function quartasCategorias() {
        $('.ano').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.nova').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.sistemas').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }
</script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>
    $(window).on('mousemove', function(e) {
        var lSide = $(this).width() / 2;
        var cursorImg = e.pageX < lSide ? $('.popover__content_direita').attr('class', 'popover__content_esquerda') : $('.popover__content_esquerda').attr('class', 'popover__content_direita');

    })
</script>



    
    <script>
        $(document).ready(function() {
            $("#p6").on("change", function(e) {
                var a = $('#p6').val();
                if (a == 'Não') {
                    $('#d_motivo').attr('style', 'display:block')
                } else {
                    $('#d_motivo').attr('style', 'display:none')
                }
            });

            $('#m_sucesso').on('hidden.bs.modal', function(e) {
                window.location.href = "https://unyflex.com.br/baixar";
            })
        });
    </script></div></body></html>