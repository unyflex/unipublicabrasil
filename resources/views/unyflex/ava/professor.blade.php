@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')

<div class="row conteudo">

    	 

    <div class="col-md-11 conteudo">

        <h2 style="color: #ffffff; padding-left: 30px; font-size: 24px; padding-top: 21px; margin-bottom: -5px;">Cursos do Professor(a): {{$professor}}</h2>

        <div class="row">

        @foreach ($classes as $classe )
            
       <?php $photo = $classe->classes->photo; ?>

                        <div class="popover__wrapper col-md-2 col-xs-12" style="max-height:180px; margin-top: 20px;"> 
                <a href="{{ route('infocurso', ['slug' => $classe->classes->slug]) }}"><img class="popover__title img-curso" src="{{url("storage/cursos/banner/$photo");}}"></a>
                <div id="vira" class="popover__content_direita" style="max-height:180px; min-width:280px;">                    
                    <a href="{{ route('infocurso', ['slug' => $classe->classes->slug]) }}"><h2 style="font-size: 13pt; padding-top: 22px; padding-left: 30px; color: #ff6600; font-weight: bold;">{{$classe->classes->title}}  </h2></a>
                    <span style="padding-left: 30px; font-size: 10pt; color: #908e8e;"> Carga Horária: {{$classe->classes->workload}} horas </span>
                    
                    <a href="{{ route('infocurso', ['slug' => $classe->classes->slug]) }}"><div style="margin-left:30px; width:80%; margin-top:10px; height: 60px;"><i class="fas fa-play-circle" style="font-size: 36pt;margin-top: 5px; color: #ff6600; float:left; margin-right:20px;"></i> 
                        <p style="font-size: 11pt; color: #fff; line-height: 17px; padding-top: 12px; padding-right: 15px;">Reproduzir o vídeo 1 da aula completa</p></div></a>
                </div>
            </div>                        
                
                  @endforeach                      
                    </div>
        
         <div class="row" style="margin-top:60px; margin: 0 auto;">
            <center style="margin:0 auto;">
                <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold;"> UNY<span style="color:#ff6600">FLEX</span></p>            
                <p style="color:#fff">CNPJ: 36.731.728/0001-30 &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#ff6600">www.unipublicabrasil.com.br</span>&nbsp;&nbsp; Copyright 2019 - Todos os direitos reservados.</p>
            </center>
        </div>

    </div>
    <div class="col-md-1 d-none d-sm-none d-md-block" style="background-color: #32312f;">

        <img src="https://unyflex.com.br/ava/views/assets/images//bannerpesquisa.jpg" data-toggle="modal" data-target="#modalPesquisaAva" style="position: sticky; max-width:100%; cursor:pointer; top: 0px; margin-left: -6px; border-left: 1px black solid;">

    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- <script src="https://unyflex.com.br/ava/views/assets/js/bootstrap.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unyflex.com.br/ava/views/assets/js/slick.js"></script>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script>
    function viewSession() {
        var a = eval('({"idAluno":"12129","tipo":null,"cpf":"41743114800"})');
        console.log(a)
    }
</script>

<script>
    $(document).ready(function() {
        $('.assistindo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.assistidos').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recomendados').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recentemente').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }

        $('#modal_erro').on('show.bs.modal', function(e) {
            $('#txt_url').val(window.location.href)
        })

        $('#enviar_erro').on('click', function() {
            envia_erro()
        })

    });

    function envia_erro() {

        var erro = $('#txt_erro').val()

        $.ajax({
            type: 'POST',
            url: "/ava/views/public/includes/functions.php",
            cache: false,
            data: {
                "func": "envia_erro",
                "url": window.location.href,
                "erro": erro
            },
            success: function(e) {
                if (e == 'sucesso') {
                    alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')
                    $('#modal_erro').modal('hide')
                }

            },
        })

    }

    function primeirasCategorias() {
        $('.financasmunicipais').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.controleinterno').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.juridico').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function segundasCategorias() {
        $('.rh').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.legislativo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.contadores').slick({
            dots: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }


    function terceirasCategorias() {
        $('.patrimonio').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.licitacoes').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.compras').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function quartasCategorias() {
        $('.ano').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.nova').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.sistemas').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }
</script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>
    $(window).on('mousemove', function(e) {
        var lSide = $(this).width() / 2;
        var cursorImg = e.pageX < lSide ? $('.popover__content_direita').attr('class', 'popover__content_esquerda') : $('.popover__content_esquerda').attr('class', 'popover__content_direita');

    })
</script>



</body></html>