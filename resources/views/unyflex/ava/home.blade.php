@include('unyflex.ava.layouts.head')
@include('unyflex.ava.layouts.header')
    



<div class="modal fade" id="modalPesquisaAva" tabindex="-1" role="dialog" aria-labelledby="pesqavaModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content col conteudo" style="opacity: 1;">
      <div class="modal-header">
        <h4 class="modal-title" id="pesqavaModalLabel" style="font-weight: bold;text-align: center">Pesquisa Avançada</h4>
        <p style="color:#fff; padding-left:30px; padding-top:20px; ">Procurando por um termo falado pelo professor em sala de aula? Pesquise no campo abaixo.</p>
        <button type="button" class="close buttonclose" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row conteudopesq">
    
    <style>
        input::-webkit-input-placeholder {
            color: #b9bbc0 !important;
        }

        input:-moz-placeholder { /* Firefox 18- */
            color: #b9bbc0 !important;  
        }

        input::-moz-placeholder {  /* Firefox 19+ */
            color: #b9bbc0 !important;  
        }

        input:-ms-input-placeholder {  
            color: #b9bbc0 !important;  
        }
    </style>
    <div class="col conteudo" style="opacity: 1;">
        <form id="termo" action="{{ route('ava-resultado-busca') }}" method="post" style="color:#fff; padding-left:15px; padding-top:20px;">
         @csrf
            <div class="col">
                <div class="form-row">
                    <div class="form-group col-md-12">                        
                        <input name="termo" type="text" placeholder="Ex: LRF, transparência, etc." class="form-control col-md-10" id="inputtermo" style="float:left; border-bottom-right-radius: 0px; border-top-right-radius: 0px;">
                        <button type="submit" name="pesquisar" value="Buscar" class="btn btn-success col-md-2" style="float:left; border-bottom-left-radius: 0px; border-top-left-radius: 0px;">Buscar</button>
                    </div>                        

                </div> 
            </div>            

        </form>

        <div style="background-color:#929292; height:1px; width:70%; margin-left: 30px; margin-top: 40px;  "></div>



    </div>
</div>

<script>
    function formSubmit()
    {
        document.getElementById("avancado").submit();
    }
</script>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar mudanças</button>
      </div> -->
    </div>
  </div>
</div>
<div id="loader2" style="margin: 0px auto; background-color: rgb(35, 35, 35); height: 1500px; display: none;">
    <center>
        <img style="margin-top:10%" src="https://unyflex.com.br/ava/views/assets/images/loader.gif">
    </center>
</div>


<div class="row conteudo" style="opacity: 1;">

        <div class="col-md-11 conteudo p-0" style="opacity: 1;">
                    <div id="carouselId" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                                            <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselId" data-slide-to="1" class=""></li>
                                    </ol>
                <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item active">
                            <a href="https://unipublicabrasil.com.br/agendados.php?status=Confirmado"><img src="https://unipublicabrasil.com.br/uploads/banners/Banners (10).gif" class="img-fluid" style="min-width:100%;" alt="cursos ao vivo e online"></a>
                        </div>
                                        
                                    </div>
                <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">

                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>  <!--<div id="painelpos" class="chart tab-pane" id="programacao-curso" style="background-color: white; height: 400px; width: 500px;
display: none;margin-left: 30%; position: fixed; position: relative;">
   
</div>-->
    
                   <div class="areacursos">
                <h3 class="titulocategoria">Próximos Cursos Ao Vivo</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>
        <!-- Cursos Ao vivo -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
            
            @foreach ($aovivos as $aovivo)
                
                
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($loop->index==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $aovivo->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$aovivo->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                    </a>
                </div>
                
                 @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>
       
                 
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Recentes</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example27" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
            
            @foreach ($recentes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($loop->index==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>

                 @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example27" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example27" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>   






            <div class="areacursos">
                <h3 class="titulocategoria">Cursos Juridicos</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example2" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
              <?php $i=0;?>
            @foreach ($juridico as $recentes)
           
            @foreach ($recentes->classes as $recente)
                
          
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
    <?php $i++; ?>
                 @endforeach
                 @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>   





            
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Licitações</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example25" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <?php $i=0;?>
            @foreach ($licitacoes as $licitacao )
            @foreach ($licitacao->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                    <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example25" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example25" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>   


        

            
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Controle Interno</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example24" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <?php $i=0;?>
            @foreach ($controleinterno as $controleinternos )
            @foreach ($controleinternos->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                    <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example24" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example24" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>   





            
            
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Tributação</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example23" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
            <?php $i=0;?>
            @foreach ($tributacao as $tributacaos )
            @foreach ($tributacaos->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                    <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example23" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example23" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>   



              
            
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Assessoria</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example22" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($assessoria as $assessorias )
            @foreach ($assessorias->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                    <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example22" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example22" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>   




        

              
            
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Secretarias Municipaiss</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example21" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($secretariasmunicipais as $secretariasmunicipaiss )
            @foreach ($secretariasmunicipaiss->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                    <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example21" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example21" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 


        
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Legislativo</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example29" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($legislativo as $legislativos )
            @foreach ($legislativos->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                     <?php $i++; ?>

                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example29" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example29" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 







        
        
                   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Finanças Municipais</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example30" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($financasmunicipais as $financasmunicipaiss )
            @foreach ($financasmunicipaiss->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                         <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example30" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example30" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 







             <div class="areacursos">
                <h3 class="titulocategoria">Cursos Contadores Municipais</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example31" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($contadoresmunicipais as $contadoresmunicipaiss )
            @foreach ($contadoresmunicipaiss->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example31" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example31" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 






         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Profissionais Saúde</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example32" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($profissionaissaude as $profissionaissaudes )
            @foreach ($profissionaissaudes->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example32" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example32" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 








              <div class="areacursos">
                <h3 class="titulocategoria">Cursos Gestão Ambiental</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example33" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($gestaoambiental as $gestaoambientals )
            @foreach ($gestaoambientals->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example33" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example33" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 







        
              <div class="areacursos">
                <h3 class="titulocategoria">Cursos Patrimonio</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example35" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($patrimonio as $patrimonios )
            @foreach ($patrimonios->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example35" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example35" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 



                <div class="areacursos">
                <h3 class="titulocategoria">Cursos Consorcio</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example34" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($consorcio as $consorcios )
            @foreach ($consorcios->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example34" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example34" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 




 <div class="areacursos">
                <h3 class="titulocategoria">Cursos Autarquia</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example36" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($autarquia as $autarquias )
            @foreach ($autarquias->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example36" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example36" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 





         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Ambiente & Urbanismo</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example37" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($ambienteurbanismo as $ambienteurbanismos )
            @foreach ($ambienteurbanismos->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>  
                 <?php $i++; ?>

                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example37" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example37" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 





            
         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Ambiente & Urbanismo</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example40" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($ambienteurbanismo as $ambienteurbanismos )
            @foreach ($ambienteurbanismos->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>

                 <?php $i++; ?>

                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example40" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example40" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 



                    
         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Compras Públicas</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example38" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($compraspublicas as $compraspublicass )
            @foreach ($compraspublicass->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example38" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example38" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 





        


                    
         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Sistemas de Informacao</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example41" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($sistemasdeinformacao as $sistemasdeinformacaos )
            @foreach ($sistemasdeinformacaos->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                     <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example41" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example41" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div>



                   
         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Pregoeiros</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example42" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($pregoeiros as $pregoeiross )
            @foreach ($pregoeiross->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                 <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example42" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example42" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 




                     
         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Outros</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example43" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($outros as $outross )
            @foreach ($outross->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                     <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example43" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example43" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 



            
                     
         <div class="areacursos">
                <h3 class="titulocategoria">Cursos Recursos Humanos</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example44" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($recursoshumanos as $recursoshumanoss )
            @foreach ($recursoshumanoss->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                     <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example44" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example44" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 


   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Ano Eleitoral</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example45" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($anoeleitoral as $anoeleitorals )
            @foreach ($anoeleitorals->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                     <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example45" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example45" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 


            
   <div class="areacursos">
                <h3 class="titulocategoria">Cursos Lives Realizadas</h3>
                <div style="margin-left: -30px;">
                    <div style="height:2px; width:30%; background-color:#ff6600;"></div>
                    <div style="height:1px; background-color:#ff6600; margin-bottom:10px;"></div>



        <!-- Cursos Recentes -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example46" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
             <?php $i=0;?>
            @foreach ($livesrealizadas as $livesrealizadass )
            @foreach ($livesrealizadass->classes as $recente)
                
           
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 @if($i==0) active @endif">
                <a href="{{ route('infocurso', ['slug' => $recente->slug]) }}">
                    <img src="{{url("storage/cursos/banner/$recente->photo");}}" class="img-fluid mx-auto d-block" alt="img1">
                      </a>
                </div>
                     <?php $i++; ?>
                 @endforeach
                  @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example46" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example46" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
                </div>
                   
            </div> 




        <div id="placeHolder" style="color: #fff"></div>



        <div class="row" style="margin-top:60px;">

            <center style="margin:0 auto;">
               
                <p style="color: #fff; font-size: 25pt; padding-top: 13px; font-weight: bold;"> UNY<span style="color:#ff6600">FLEX</span></p>
                <p style="color:#fff">CNPJ: 11227107/0001-93 &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#ff6600">www.unipublicabrasil.com.br</span>&nbsp;&nbsp; Copyright 2019 - Todos os direitos reservados.</p>
            </center>
        </div>


    </div>



    <div class="col-md-1 d-none d-sm-none d-md-block" style="    background-color:#2e2e2e; padding-left:8px;">

        <img src="https://unyflex.com.br/ava/views/assets/images//bannerpesquisa.jpg" data-toggle="modal" data-target="#modalPesquisaAva" style="position: sticky; max-width:100%; cursor:pointer; top: 0px; margin-left: -6px; border-left: 1px black solid;">

    </div>

</div>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- <script src="https://unyflex.com.br/ava/views/assets/js/bootstrap.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unyflex.com.br/ava/views/assets/js/slick.js"></script>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script>
    function viewSession() {
        var a = eval('({"idAluno":"12129","tipo":null,"cpf":"41743114800"})');
        console.log(a)
    }
</script>

<script>
    $(document).ready(function() {
        $('.assistindo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.assistidos').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recomendados').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.recentemente').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }

        $('#modal_erro').on('show.bs.modal', function(e) {
            $('#txt_url').val(window.location.href)
        })

        $('#enviar_erro').on('click', function() {
            envia_erro()
        })

    });

    function envia_erro() {

        var erro = $('#txt_erro').val()

        $.ajax({
            type: 'POST',
            url: "/ava/views/public/includes/functions.php",
            cache: false,
            data: {
                "func": "envia_erro",
                "url": window.location.href,
                "erro": erro
            },
            success: function(e) {
                if (e == 'sucesso') {
                    alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')
                    $('#modal_erro').modal('hide')
                }

            },
        })

    }

    function primeirasCategorias() {
        $('.financasmunicipais').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.controleinterno').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.juridico').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function segundasCategorias() {
        $('.rh').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.legislativo').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.contadores').slick({
            dots: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }


    function terceirasCategorias() {
        $('.patrimonio').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.licitacoes').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.compras').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }

    function quartasCategorias() {
        $('.ano').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.nova').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        $('.sistemas').slick({
            dots: false,
            infinite: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 6,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev d-none d-sm-block ">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next d-none d-sm-block ">Next</button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        infinite: true,
                        slidesToScroll: 2
                    }
                }
            ]
        });

        var larguratela = $(window).width();
        if (larguratela < 500) {
            $('.popover__content_direita').css("display", "none");
            $('.popover__content_esquerda').css("display", "none");
            $('#escondeMenu').css('display', 'none');

        }
    }
</script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>
    $(window).on('mousemove', function(e) {
        var lSide = $(this).width() / 2;
        var cursorImg = e.pageX < lSide ? $('.popover__content_direita').attr('class', 'popover__content_esquerda') : $('.popover__content_esquerda').attr('class', 'popover__content_direita');

    })
</script>


     <script>

    var num = 0;



    var larguratela = $(window).width();
    if (larguratela > 2000) {
        num++;
        AddMoreContent(num);
    }

    $(window).scroll(function() {
        console.log($(window).scrollTop());
        console.log($(document).height() - $(window).height());
        if ($(window).scrollTop() >= $(document).height() - $(window).height() && num < 5) {
            num++;
            AddMoreContent(num);
        }
    });

    function AddMoreContent(e) {
        $('#loader').attr('style', 'display:block');
        $.post('https://unyflex.com.br/ava/busca-categorias/todos/ordem/', 'lastId=' + e, function(data) {
            //Assuming the returned data is pure HTML
            //console.log(data)
            $('#loader').attr('style', 'display:none');
            $("#placeHolder").append(data);
            $('.fadein').fadeTo(1500, 1);
        });

    }

    $(window).on('load', function() {
        $('#loader2').fadeOut();
        $('#loader2').delay(350).fadeOut('slow');
        $('.conteudo').fadeTo(400, 1);
    })
</script></body></html>