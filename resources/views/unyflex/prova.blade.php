@include('unyflex.header-provas')

<body>
<div class="container-fluid">
    <div class="modal-dialog">
     <form action="{{ route('resultado-prova') }}" method="post" enctype="multipart/form-data" data-single="true" method="post">

                @csrf
    @foreach ($questions as $question)
        
    
        <div class="modal-content">
            <div class="modal-header">
                <h3>{{$question->text}}</h3>
            </div>
            <div class="modal-body">
                <div class="col-xs-3 5"> </div>
                <?php $i=$loop->index; ?>
                <div class="quiz" id="quiz" data-toggle="buttons">
                    @foreach ($question->alternatives as $alternative)
                 <label class="element-animation1 btn btn-lg btn-block" ><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" class="radio{{$question->id}} " name="questao{{$i}}" value="{{$alternative->id}}">{{$alternative->text}}</label>
                @endforeach
             </div>
            </div>
        </div>
    
<script type='text/javascript'>

$(document).ready(function() {                         
  
  $('.radio{{$question->id}}').on('click',function() {               
      $('.radio{{$question->id}} ').parent().css('color','black');  
      $(this).parent().css('color','green');
  });
});
</script>    <br>
 @endforeach
    </div>
        <input type="hidden" value="{{$curso->id}}" name="idcurso">
        <button type="submit" class="btn btn-primary w-full  mr-2 mb-2">Entregar Prova</button>
   </form>


</div>
</body>