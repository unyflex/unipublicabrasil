<html lang="pt">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Unyflex - Flexibilidade no Ensino Unipública</title>
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="https://unyflex.com.br/views/assets/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://unyflex.com.br/views/assets/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://unyflex.com.br/views/assets/fav/favicon-16x16.png">
    <link rel="manifest" href="https://unyflex.com.br/views/assets/fav/site.webmanifest">
    <link rel="mask-icon" href="https://unyflex.com.br/views/assets/fav/safari-pinned-tab.svg" color="#ff6600">
    <meta name="apple-mobile-web-app-title" content="Unyflex">
    <meta name="application-name" content="Unyflex">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap CSS -->

    <!-- <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- STYLE.CSS -->
    <link rel="stylesheet" href="{{asset('/dist/loja/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/dist/loja/css/newstyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!--FONTES EXTERNAS-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700" rel="stylesheet">
    <!-- Player -->

    <link href="//vjs.zencdn.net/7.8.2/video-js.min.css" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- SLICK CAROUSEL -->
    <!--<link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/slick.css">
    <link rel="stylesheet" href="https://unyflex.com.br/views/assets/css/slick-theme.css">-->
    <!-- STYLE.CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.es.gov.br/fonts/font-awesome/css/font-awesome.min.css">
    <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/releases/zLD1nfkNCJC1kEswSRdSyd-p/recaptcha__pt_br.js" crossorigin="anonymous" integrity="sha384-aD2Cw88n2FSD/axj1/zJSZSweL32W5FoxVFBihj6D9MQkWvs1iiqflI1JYmaNYVx"></script>
    <script type="text/javascript" async="" src="https://www.googleadservices.com/pagead/conversion_async.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MW4GW4P"></script>
    <script src="https://connect.facebook.net/signals/config/766200407638558?v=2.9.57&amp;r=stable" async=""></script>
    <script src="https://connect.facebook.net/signals/config/557906795071452?v=2.9.57&amp;r=stable" async=""></script>
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script>
    <script src="https://unyflex.com.br/views/assets/css/profonts.js"></script>
    <link href="https://kit-pro.fontawesome.com/releases/latest/css/pro-v4-shims.min.css" media="all" rel="stylesheet">
    <link href="https://kit-pro.fontawesome.com/releases/latest/css/pro-v4-font-face.min.css" media="all" rel="stylesheet">
    <link href="https://kit-pro.fontawesome.com/releases/latest/css/pro.min.css" media="all" rel="stylesheet">

    <!-- Meta Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1152074441555950');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1152074441555950&ev=PageView&noscript=1" /></noscript>
    <!-- End Meta Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-166619218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-166619218-1');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MW4GW4P');
    </script>
    <!-- End Google Tag Manager -->
    <script type="text/javascript" async="" src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/19516d45-aa63-48d8-be9a-0f95ff676c29-loader.js"></script>

    <!-- End Facebook Pixel Code -->
    <meta http-equiv="origin-trial" content="A+sitaPn3hlQ8QipTsncwHz+k1NvfPtFsQqIOiD8GK3M9v9XCeQqlF7x1P9AVJdoYTiJPZXZc5XZYpwc10fH4wEAAACfeyJvcmlnaW4iOiJodHRwczovL3d3dy5nb29nbGVhZHNlcnZpY2VzLmNvbTo0NDMiLCJmZWF0dXJlIjoiQ29udmVyc2lvbk1lYXN1cmVtZW50IiwiZXhwaXJ5IjoxNjQzMTU1MTk5LCJpc1N1YmRvbWFpbiI6dHJ1ZSwiaXNUaGlyZFBhcnR5Ijp0cnVlLCJ1c2FnZSI6InN1YnNldCJ9">
    <script src="https://www.googleadservices.com/pagead/conversion/429051028/?random=1647448350616&amp;cv=9&amp;fst=1647448350616&amp;num=1&amp;value=0&amp;label=rEabCOT-q4IDEJSZy8wB&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=5&amp;u_tz=-180&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wg3e0&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Funyflex.com.br%2Farea&amp;ref=https%3A%2F%2Funyflex.com.br%2Flogin&amp;tiba=Unyflex%20-%20Flexibilidade%20no%20Ensino%20Unip%C3%BAblica&amp;auid=705317506.1641236889&amp;hn=www.googleadservices.com&amp;bttype=purchase&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>
    <script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/429051028/?random=1647448350623&amp;cv=9&amp;fst=1647448350623&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=5&amp;u_tz=-180&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wg3e0&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Funyflex.com.br%2Farea&amp;ref=https%3A%2F%2Funyflex.com.br%2Flogin&amp;tiba=Unyflex%20-%20Flexibilidade%20no%20Ensino%20Unip%C3%BAblica&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50" style="overflow-x: hidden;" class="">
    <header style="background: linear-gradient(90deg, rgba(255,102,0,1) 0%, rgba(255,131,49,1) 100%)">
        <nav class="navbar navbar-expand-lg navbar-expand-md navbar-expand-sm navbar-light" style="background-color: #000; height: 40px" id="nav">
            <a href="https://unyflex.com.br/inicio" style="font-family: 'Roboto'; text-decoration: none; color: #fff; font-weight: bold;font-size:25px; line-height: 0px">UNYFLEX</a>
            <ul class="navbar-nav" id="nav_superior">
                <!-- <li class="nav-item">
                <a class="btn-sup" href="inicio">Inicio</a>
            </li> -->
                <li class="nav-item">
                    <div class="dropdown">
                        <a class="btn-sup dropdown-toggle" href="" role="button" data-toggle="dropdown">Cursos</a>
                        <div class="dropdown-menu" style="cursor:pointer">
                            <span style="margin-left: 10px;"> Selecione o departamento </span>
                            <hr>
                            <a href="https://unyflex.com.br/setores/geral" class="dropdown-item">Todos os cursos</a>
                            <a href="https://unyflex.com.br/setores/ambiente-urbanismo" class="dropdown-item" id="1">Ambiente e Urbanismo</a>
                            <a href="https://unyflex.com.br/setores/compras-publicas" class="dropdown-item" id="2">Compras Públicas</a>
                            <a href="https://unyflex.com.br/setores/contadores-municipais" class="dropdown-item" id="3">Contadores Municipais</a>
                            <a href="https://unyflex.com.br/setores/controle-interno" class="dropdown-item" id="4">Controle Interno</a>
                            <a href="https://unyflex.com.br/setores/financas-municipais" class="dropdown-item" id="5">Finanças Municipais</a>
                            <a href="https://unyflex.com.br/setores/juridico" class="dropdown-item" id="6">Jurídico</a>
                            <a href="https://unyflex.com.br/setores/legislativo" class="dropdown-item" id="8">Legislativo</a>
                            <a href="https://unyflex.com.br/setores/licitacoes-publicas" class="dropdown-item" id="11">Licitações Públicas</a>
                            <a href="https://unyflex.com.br/setores/nova-previdencia" class="dropdown-item" id="26">Nova Previdência</a>
                            <a href="https://unyflex.com.br/setores/outros" class="dropdown-item" id="99">Outros</a>
                            <a href="https://unyflex.com.br/setores/patrimonio" class="dropdown-item" id="10">Patrimônio</a>
                            <a href="https://unyflex.com.br/setores/pregoeiro" class="dropdown-item" id="100">Pregoeiros</a>
                            <a href="https://unyflex.com.br/setores/recursos-humanos" class="dropdown-item" id="7">Recursos Humanos</a>
                            <a href="https://unyflex.com.br/setores/sistamas-informacao" class="dropdown-item" id="27">Sistemas de Informação</a>
                            <a href="https://unyflex.com.br/setores/tributacao-municipal" class="dropdown-item" id="9">Tributação Municipal</a>
                            <!-- <a class="dropdown-item" id="btn_mostracursos" href="">Cursos Rápidos</a>
                        <a class="dropdown-item" href="https://unyversidade.com.br">Cursos Profissionalizantes</a> -->
                        </div>
                    </div>
                </li>

                <li class="nav-item">
                    <div class="dropdown">
                        <a class="btn-sup dropdown-toggle" href="#" role="button" id="dropdownMenuExtensao" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cursos Modulares
                        </a>
                        <div class="dropdown-menu" style="-webkit-column-count: 2; column-count: 3; column-rule-style: ridge; column-rule-width: 1px;" aria-labelledby="dropdownMenuExtensao">
                            <a class="dropdown-item" href="https://unyflex.com.br/modular/">Todos os cursos</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/32">Advocacia Pública Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/50">Arquivologia e Gestão da Informação Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/52">Assessoria Pública Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/76">Auditoria Governamental nos Municípios</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/51">Cerimonial e Gestão de Eventos no Município</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/64">Cidades Inteligentes </a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/53">Ciências Políticas</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/60">Compliance e Ética na Gestão Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/68">Compras Públicas Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/54">Consórcios Intermunicipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/33">Contabilidade Pública Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/77">CPI Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/55">Desenvolvimento Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/42">Direito Administrativo Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/56">Direito Eleitoral Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/69">Direito Tributário Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/37">Finanças Públicas Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/78">Gestão Ambiental nos Municípios</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/79">Gestão da Educação Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/35">Gestão de Carreiras na Administração Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/38">Gestão de Convênios Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/83">Gestão de Custos na Administração Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/67">Gestão de Frotas Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/34">Gestão de Pessoas no Setor Público Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/80">Gestão de Processos no Setor Público Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/81">Gestão de Riscos no Setor Público Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/65">Gestão e Fiscalização de Obras Públicas Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/61">Gestão Estratégica da Informação Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/40">Gestão Legislativa Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/63">Gestão Pública 4.0</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/41">Gestão Pública Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/57">Governança Democrática no Município</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/58">Governança em TI na Administração Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/59">Governança Estratégica no Município</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/43">Improbidade Administrativa Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/31">Licitações e Contratos Administrativos Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/87">Licitações e Contratos Federais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/82">Liderança Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/84">Mobilidade Urbana</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/85">Noções de Gestão Pública</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/48">Ouvidoria Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/36">Patrimônio Público Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/66">Planejamento Municipal e Urbanismo</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/75">Planejamento Orçamentário Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/70">Plano Diretor Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/62">Políticas Públicas Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/49">Políticas Sociais nos Municípios</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/39">Prestação de Contas Municipais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/45">Previdenciário Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/44">Processo Legislativo Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/86">Processos Gerenciais da Gestão Pública </a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/47">Redação Oficial no Município</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/71">Regularização Fundiária no Município</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/72">Saúde Pública Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/73">Segurança Pública no Município</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/46">Terceirizações na Gestão Municipal</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/cursoModular/74">Vigilância Sanitária Municipal</a>
                        </div>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="btn-sup conhecer" id="conhecer" href="#">Plataforma</a>
                </li>

                <li class="nav-item">
                    <a class="btn-sup" href="#" id="btn_pesquisar"><i class="fal fa-search" style="font-size: 20px" aria-hidden="true"></i></a>
                </li>

                <!-- <li class="nav-item">
                <a class="btn-sup" href="baixar">Baixar</a>
                </li> -->

                <li class="nav-item">
                    <a class="btn-sup" href="https://unyflex.com.br/precos">Preços</a>
                </li>

                <li class="nav-item">
                    <a class="btn-sup" href="#quem-somos">Sobre</a>
                </li>

                <li class="nav-item">
                    <div class="dropdown show">
                        <a class="btn-sup dropdown-toggle" href="#" role="button" id="dropdownMenuNota" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Notas Tecnicas
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuNota">
                            <a class="dropdown-item" href="https://unyflex.com.br/views/assets/notas-tecnicas/nt002-presencialxonline.pdf" target="_blank">NT 002- Cursos a distância X Cursos presenciais</a>
                            <a class="dropdown-item" href="https://unyflex.com.br/views/assets/notas-tecnicas/declaracao_exclusividade.pdf" target="_blank">Declaração de Exclusividade</a>
                        </div>
                    </div>
                </li>

                <li class="nav-item">
                    <div class="dropdown show">
                        <a class="btn-sup dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Minha conta
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="../area">Minha área</a>
                            <hr style="background-color: #fff; width: 45%">
                            <a class="dropdown-item" href="../sair">Sair</a>
                        </div>
                    </div>
                </li>



                <li class="nav-item">
                    <a class="btn-sup" href="../#" id="cart"><i class="fal fa-shopping-bag" style="font-size: 20px" aria-hidden="true"></i></a>
                </li>
            </ul>
        </nav>

        <nav class="navbar navbar-expand-sm navbar-light" style="background-color: #000;" id="nav_mobile">
            <a href="" style="font-family: 'Roboto'; text-decoration: none; color: #fff; font-weight: bold;font-size:25px; line-height: 0px; position: absolute; bottom:20px">UNYFLEX</a>

            <ul class="navbar-nav">
                <li class="nav-item text-center">
                    <a class="btn-sup" href="#" id="btn_mobile"><i class="fal fa-bars" style="font-size: 23px" aria-hidden="true"></i></a>
                </li>

                <div class="collapse" id="menu_mobile">
                    <div class="row">
                        <div class="col-md-12 text-center p-2">
                            <a class="btn-sup" href="../inicio">Inicio</a>
                        </div>
                        <div class="col-md-12 text-center p-2">
                            <a class="btn-sup" href="../#">Cursos</a>
                        </div>

                        <li class="nav-item">
                            <div class="dropdown show">
                                <a class="btn-sup dropdown-toggle" href="#" role="button" id="dropdownMenuExtensao" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Extensão
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuExtensao">
                                    <a class="dropdown-item" href="https://unyflex.com.br/box/13" target="_blank">Curso de Extensão em Ambiente e Urbanismo</a> <a class="dropdown-item" href="https://unyflex.com.br/box/5" target="_blank">Curso de Extensão em Contabilidade Pública</a> <a class="dropdown-item" href="https://unyflex.com.br/box/16" target="_blank">Curso de Extensão em Controle Interno</a> <a class="dropdown-item" href="https://unyflex.com.br/box/17" target="_blank">Curso de Extensão em Direito Municipal</a> <a class="dropdown-item" href="https://unyflex.com.br/box/11" target="_blank">Curso de Extensão em Legislativo Municipal </a> <a class="dropdown-item" href="https://unyflex.com.br/box/8" target="_blank">Curso de Extensão em Licitações públicas</a> <a class="dropdown-item" href="https://unyflex.com.br/box/22" target="_blank">Curso de Extensão em Patrimônio Público</a> <a class="dropdown-item" href="https://unyflex.com.br/box/18" target="_blank">Curso de Extensão em RH Municipal</a> <a class="dropdown-item" href="https://unyflex.com.br/box/9" target="_blank">Curso de Extensão em Tributação Municipal</a> <a class="dropdown-item" href="https://unyflex.com.br/box/14" target="_blank">Curso de Extensão Regras do Ano Eleitoral</a> <a class="dropdown-item" href="https://unyflex.com.br/box/15" target="_blank">Curso de Extensão Sistema de Informação Contábeis </a> <a class="dropdown-item" href="https://unyflex.com.br/box/21" target="_blank">Pacote 21 </a>
                                </div>
                            </div>
                        </li>

                        <div class="col-md-12 text-center p-2">
                            <a class="btn-sup" href="../baixar">Baixar</a>
                        </div>

                        <div class="col-md-12 text-center p-2">
                            <a class="btn-sup" href="../#quem-somos">Sobre</a>
                        </div>

                        <div class="col-md-12 text-center p-2">
                            <a class="btn-sup" href="../ava">Acesso Assinante</a>
                        </div>

                        <div class="col-md-12 text-center p-2">
                            <a class="btn-sup" href="../area">Minha area</a>
                        </div>
                    </div>
                </div>
            </ul>
        </nav>
    </header>

    @yield('content')

    <div class="fundofooter">
        <footer class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <img class="logofooter" src="{{url('/storage/images/uny_fone.png')}}">
                    <img src="{{url('/storage/images/logo-branca.png')}}" style="width: 150px; margin-top: 40px;">
                    <h2 class="titulofooter">Tenho Interesse</h2>
                    <p class="textofooter1">Fique a vontade para tirar suas dúvidas, entraremos em contato assim que possível.</p>
                    <p class="textocontato"><i style="margin: auto" class="fab fa-whatsapp"></i>&nbsp;Whats: (41) 99758-7226</p>
                    <p class="textocontato"><i style="margin: auto" class="fas fa-phone"></i>&nbsp;Tel: (41) 3405-3501</p>
                    <p class="textofooter2">A Unyflex é um instrumento que visa a promoção da eficiência e moralização nos órgãos estatais através de cursos de Gestão Pública. Preparamos os agentes públicos para os desafios que a gestão envolve, assim contribuindo para a construção do Brasil que queremos!</p>

                </div>
                <div class="col-md-5 bordaformulariofooter">
                    <div class=" row fundoformulariofooter">
                        <form action="" method="post">
                            <div class="col-md-12 ajusteform">
                                <input class="inputfooter" type="text" name="nome" placeholder="Nome" required><i class="fas fa-user iconeinput"></i>
                            </div>
                            <div class="col-md-12">
                                <input class="inputfooter" type="email" name="email" placeholder="E-mail" required><i class="fas fa-envelope iconeinput"></i>
                            </div>
                            <div class="col-md-12">
                                <input class="inputfooter" type="text" name="telefone" placeholder="Telefone" required><i class="fas fa-phone iconeinput"></i>
                            </div>
                            <div class="col-md-12">
                                <input class="inputfooter" type="text" name="duvida" placeholder="Qual a sua dúvida?" required>
                            </div>
                            <div class="col-md-12">
                                <label for="recaptcha"></label>
                                <!-- reCAPTCHA -->
                                <div style="margin-left: 25px;" class="g-recaptcha" data-sitekey="6LenkacUAAAAADU_epfR0oq6q2O1FFjwQQclj1TU"></div>
                            </div>
                            <div class="col-md-12">
                                <input class="botaofooter" type="submit" value="ENVIAR MENSAGEM">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="fundomidiasocial">
        <footer class="container">
            <div class="row justify-content-center">
                <div class="col-md-3 ajustecaixasocial">
                    <a href="https://www.facebook.com/unyflexoficial/" class="iconesocial"><i class="fab fa-facebook-f"></i></a>
                </div>
                <div class="col-md-3 ajustecaixasocial">
                    <a href="https://twitter.com/unipublica" class="iconesocial"><i class="fab fa-twitter"></i></a>
                </div>
                <div class="col-md-3 ajustecaixasocial">
                    <a href="https://www.youtube.com/channel/UCBeIS8UTD1i9tU8hXHndTrA" class="iconesocial"><i class="fab fa-youtube"></i></a>
                </div>
                <div class="col-md-3 ajustecaixasocial socialdireita">
                    <a href="https://www.instagram.com/unyflex/" class="iconesocial"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </footer>
    </div>


    <div class="fundoautorais">
        <footer class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <p class="textoautoral">CNPJ: 36.731.728/0001-30 - www.unyflex.com.br Copyright <?php echo date("Y"); ?> - Todos os direitos reservados.</p>
                </div>
            </div>
        </footer>

    </div>

    <div class="modal fade" id="uny21" tabindex="-1" role="dialog" aria-labelledby="uny21Label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="uny21Label" style="color:#123093; font-weight: bold">Pacote UNY 21</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Temas sugeridos:</b></p>
                    <div style="font-size: 12px; line-height: 8px;">
                        <p>• IPTU, ITBI, COSIP E ITR</p>
                        <p>• Compras Municipais</p>
                        <p>• Prestação de Contas Anual</p>
                        <p>• Coronavírus: Compras e Licitações</p>
                        <p>• Fiscalização Tributária</p>
                        <p>• Servidores Municipais</p>
                        <p>• SIAP e SIM AM</p>
                        <p>• Novo Pregão Eletrônico</p>
                        <p>• FUNDEB e Fundo da Saúde</p>
                        <p>• Nova Previdência</p>
                        <p>• Vedações no Ano Eleitoral</p>
                        <p>• Nova Previdência</p>
                        <p>• Tesouraria Municipal</p>
                        <p>• Sistemas de Informação</p>
                        <p>• Dispensa e inexigibilidade</p>
                        <p>• Formação de Pregoeiro e Comissão de Licitações</p>
                        <p>• eSocial</p>
                        <p>• Registro de Preços</p>
                        <p>• Desenvolvimento Urbano e Rural</p>
                        <p>• Tributação</p>
                        <p>• Normas de Atuação do Controle Interno</p>
                        <br><br>
                        <p>Os temas podem ser alterados diretamente com nossos atendentes, perante avaliação de nosso catálogo.</p>
                        <br>
                    </div>
                    <button class="btn btn-block" style="background-color:#123093; color:#fff" onclick="abreUny21">Adquirir</button>
                </div>
            </div>
        </div>
    </div>

    <script src="//code-eu1.jivosite.com/widget/5VTINsZCU7" async></script>
    <script src="//receiver.posclick.dinamize.com/forms/js/330317_3056.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="/views/public/includes/scripts-curso.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-12" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="position: absolute;bottom: 0; float:right; width:800px; right:0; margin-bottom:10px;margin-right:10px; ">
            <div class="modal-content">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>

                <div class="modal-body">
                    <p style="font-size:12px; color:#000; font-weight:bold"> Utilizamos cookies e tecnologias
                        semelhantes para melhorar a sua experiência, de acordo com a nossa Política de Privacidade e,
                        ao continuar navegando, você concorda com estas condições.
                        <button type="button" style="width:60px; height:30px;font-size:10px; margin-left:40px;margin-top:8px;margin " class="btn btn-primary" class="close" data-dismiss="modal" aria-label="Close">OK</button>
                    <p>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#exampleModal').modal('show');
        });
    </script>

    <script>
        $('.conhecer').on('click', function() {
            console.log('#', 'aqui')
            $.ajax({
                type: 'POST',
                url: "/ava/views/public/includes/functions.php",

                cache: false,
                data: {
                    "func": "testeAva",
                },
                success: function(e) {
                    if (e == 'sucesso') {
                        document.location.href = 'https://unyflex.com.br/ava/inicio/todos/ordem'
                    }
                },
            });
        })


        $('#btn_mobile').on('click', function() {
            $('#menu_mobile').collapse('toggle')
        })

        var prevScrollpos = window.pageYOffset;
        // window.onscroll = function() {
        //     var currentScrollPos = window.pageYOffset;
        //     if (currentScrollPos == 0) {
        //         $('.barra').attr('style', 'top:0px')
        //         $('#img_logo').fadeIn('slow')
        //     } else {
        //         $('.barra').attr('style', 'top:-58px')
        //         $('#img_logo').fadeOut('slow')
        //     }
        // }
    </script>
    <script>
        $(document).ready(function() {


            $('#telefone').mask('(00) 0 0000-0000');

            $('#form_cadastro').find('#txt_telefone_cad').mask('(00) 0 0000-0000');
            $('#form_cadastro').find('#txt_cpf_cad').mask('000.000.000-00', {
                reverse: true
            });

            $('#btn_termo').on('click', function() {
                busca_termo($('#txt_termo').val());
            })
            $('#btn_termo_cad').on('click', function() {
                busca_termo($('#txt_termo').val());
            })

            $('.tags').on('click', function(e) {
                busca_setor($(this).val(), $(this).text());
            })

            $('#b_check_cad').on("change", function() {
                if (this.checked) {
                    $('#termos_condicoes_cad').collapse('show');
                } else {
                    $('#termos_condicoes_cad').collapse('hide');
                }
            })


            $('#termos_m_cad').on('click', function() {
                $('#termos_condicoes_cad').collapse('show');
            });


            $('#btn_pesquisar').on('click', function(e) {


                if ($('#pesquisa').is(':visible') == false) {


                    e.preventDefault();
                    $('#resultado_pesquisa').collapse('hide')
                    $('#re_pesquisa').html('')
                    $('#pesquisa').fadeIn('slow')
                    $('#txt_pesquisa').val('')
                } else {
                    $('#pesquisa').fadeOut('slow', function() {
                        e.preventDefault();
                        $('#resultado_pesquisa').collapse('hide')
                        $('#re_pesquisa').html('')
                        $('#txt_pesquisa').val('')
                    })
                }
            })

            $('#btn_pesquisa').on('click', function(e) {
                var pesquisa = $('#txt_pesquisa').val();
                pesquisa_site(pesquisa);
            })

            $('#btn_mostracursos').on('click', function(e) {
                e.preventDefault()
                mostrar_cursos()
            })

            $('#btn_mostracursos2').on('click', function(e) {
                e.preventDefault()
                mostrar_cursos()
            })

            $('#btn_mostraboxs').on('click', function(e) {
                e.preventDefault()
                mostrar_boxs()
            })

            $('#btn_mostralives').on('click', function(e) {
                e.preventDefault()
                mostrar_lives()
            })


            $("#txt_cep_cad").blur(function() {
                busca_cep(this)
            });

            $('#btn_cancelar_cad').on('click', function() {
                $('#m_21').modal('hide')
            })

            $('#btn_concluir_cad').on('click', function() {

                cad_box21()
            })

            $('#c_box').on('click', function(e) {
                e.preventDefault()
                $('#m_21').modal('show');

            })

            $('#m_21').on('show.bs.modal', function() {
                $('#btn_concluir_cad').html('')
                $('#btn_concluir_cad').append('Enviar');
                $('#btn_concluir_cad').prop('disabled', false)
                $(this).find('.form-control').val('')
            })

            $('.tgbox100').on('click', function(e) {
                e.preventDefault()
                var id = $(this).attr('id')
                busca_box100(id)
            })

            $('#f_cursos').on('click', function() {
                $('#re_cursos').html('').then($('#listar_cursos').collapse('toggle'));
            })

            $('.tagMenu').on('click', function() {

                var idCategoria = $(this).attr('id');
                idCategoria = idCategoria - 1;
                $('.tags')[idCategoria].click();

            });

        })



        function abreCurso100() {
            $('#uny100').modal('show');
        }

        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#txt_endereco_cad").val("");
            $("#txt_bairro_cad").val("");
            $("#txt_cidade_cad").val("");
            $("#txt_uf_cad").val("");
        }

        function busca_cep(cep) {
            //Nova variável "cep" somente com dígitos.
            var cep = $(cep).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if (validacep.test(cep)) {

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#txt_endereco_cad").val(dados.logradouro);
                            $("#txt_bairro_cad").val(dados.bairro);
                            $("#txt_cidade_cad").val(dados.localidade);
                            $("#txt_uf_cad").val(dados.uf);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        }

        function mostrar_cursos() {
            if ($('#re_cursos').html().length == 0) {
                $.ajax({
                    type: 'POST',
                    url: "/views/public/includes/functions.php",
                    cache: false,
                    data: {
                        "func": "mostrar_cursos",
                    },
                    success: function(e) {
                        console.log(e)
                        for (let a = 0; a < e.length; a++) {

                            $('#re_cursos').append('<a href="curso/' + e[a] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a] + '.png" class="zoom" width="295" height="155" style="object-fit: none"></a>')

                        }

                        $('#listar_cursos').collapse('show');
                    },
                });
            } else {
                $('#re_cursos').html('').then(mostrar_cursos())
            }


        }

        function mostrar_lives() {
            if ($('#re_cursos').html().length == 0) {
                $.ajax({
                    type: 'POST',
                    url: "/views/public/includes/functions.php",
                    cache: false,
                    data: {
                        "func": "mostrar_lives",
                    },
                    success: function(e) {
                        console.log(e)
                        for (let a = 0; a < e.length; a++) {

                            $('#re_cursos').append('<a href="curso/' + e[a] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a] + '.png" class="zoom" width="295" height="155" style="object-fit: none"></a>')

                        }

                        $('#listar_cursos').collapse('show');
                    },
                });
            } else {
                $('#re_cursos').html('').then(mostrar_lives())
            }


        }

        function mostrar_boxs() {
            if ($('#re_cursos').html().length == 0) {
                $.ajax({
                    type: 'POST',
                    url: "/views/public/includes/functions.php",
                    cache: false,
                    data: {
                        "func": "mostrar_boxs",
                    },
                    success: function(e) {
                        console.log(e)
                        for (let a = 0; a < e.length; a++) {

                            $('#re_cursos').append('<a href="box/' + e[a] + '"><img src="https://unyflex.com.br/ava/views/assets/images/boxs/' + e[a] + '.png" class="zoom" width="295" height="155" style="object-fit: none"></a>')

                        }

                        $('#listar_cursos').collapse('show');
                    },
                });
            } else {
                $('#re_cursos').html('').then(mostrar_boxs())

            }


        }

        function img_error() {
            $(document).find('img').on('error', function() {
                $(this).css('display', 'none')
            })
        }

        function pesquisa_site(pesquisa) {
            $.ajax({
                type: 'POST',
                url: "/views/public/includes/functions.php",
                cache: false,
                data: {
                    "func": "pesquisa_site",
                    "pesquisa": pesquisa,
                },
                success: function(e) {

                    if (e == 'erro' || e == null) {
                        $('#texto_pesquisa').text('Não foi possível completar a pesquisa, por favor verifique a quantidade de caracteres e a palavra informada.')
                        $('#resultado_pesquisa').collapse('show')
                        $('#re_pesquisa').html('')
                    } else {
                        $('#texto_pesquisa').text('Foram encontrados ' + e.length + ' resultados em nossa plataforma.')
                        $('#resultado_pesquisa').collapse('show');
                        $('#re_pesquisa').html('')
                        for (let a = 0; a < e.length; a++) {
                            $('#re_pesquisa').append('<a href="curso/' + e[a]['id'] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a]['id'] + '.png" class="zoom"></a>')
                        }
                    }
                },
            });
        }

        function cad_box21() {
            var btn = $('#btn_concluir_cad')
            if (v_campos($(document).find('#m_21').find('.prq'))) {
                $(btn).html('')
                $(btn).append('<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>');
                $(btn).prop('disabled', true)
                var form = $('#form_cadastro').find('.form-control');
                var obj = {};
                for (let a = 0; a < form.length; a++) {
                    var key = $(form[a]).attr('id');
                    obj[key] = $(form[a]).val();
                }
                var dados = obj;

                $.ajax({
                    type: 'POST',
                    url: "/views/public/includes/functions.php",
                    cache: false,
                    data: {
                        "func": "cad_box21",
                        "dados": dados,
                    },
                    success: function(e) {
                        if (e == 'sucesso') {
                            $('#btn_concluir_cad').html('')
                            $('#btn_concluir_cad').append('Enviar');
                            $('#btn_concluir_cad').prop('disabled', false)
                            alert('Seus dados foram enviados com sucesso. Logo entraremos em contato!')
                            limpa_campos($('#form_cadastro'))
                            $('#m_21').modal('hide')
                        } else {
                            alert('Houve um erro ao cadastrar seus dados, verifique e tente novamente.')
                        }
                    },
                })
            } else {
                alert('Preencha corretamente todos os campos e tente novamente.')
            }
        }



        function busca_setor(id_cat, nome_cat) {
            $.ajax({
                type: 'POST',
                url: "/views/public/includes/functions.php",
                cache: false,
                data: {
                    "func": "busca_setor",
                    "id_cat": id_cat,
                },
                success: function(e) {
                    console.log(e)
                    $('#texto_cat').text(nome_cat.toLowerCase())
                    $('#re_setor').html('')

                    for (let a = 0; a < e.length; a++) {
                        $('#re_setor').append('<a href="curso/' + e[a]['id'] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a]['id'] + '.png" class="zoom"></a>')
                    }

                    $('#curso_setor').collapse('show');
                },
            });
        }

        function busca_box100(id_cat) {
            $.ajax({
                type: 'POST',
                url: "/views/public/includes/functions.php",
                cache: false,
                data: {
                    "func": "busca_box100",
                    "id_cat": id_cat,
                },
                success: function(e) {
                    window.location.href = 'https://unyflex.com.br/box/' + e
                },
            });
        }


        function v_campos(campo) {
            var c = campo;
            var verificado = false
            if (c.length > 0) {
                for (let index = 0; index < c.length; index++) {
                    var d = $(c[index]).val()
                    if (d.length > 0 && d != null) {
                        verificado = true;
                    } else {
                        verificado = false;
                        return false
                    }
                }
            }
            return verificado;
        }

        function limpa_campos(form) {
            $(form).find('.form-control').val('')
        }


        $.fn.validateCPF = function(a) {
            var b = this;
            var c = b.val().replace('.', '').replace('.', '').replace('-', '');
            if (c.length != 11) {
                return false;
            } else {
                var d = /1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|8{11}|9{11}|0{11}/;
                if (d.test(c)) {
                    return false;
                } else {
                    var x = 0;
                    var y = 0;
                    var e = 0;
                    var f = 0;
                    var g = 0;
                    var h = "";
                    var k = "";
                    var l = c.length;
                    x = l - 1;
                    for (var i = 0; i <= l - 3; i++) {
                        y = c.substring(i, i + 1);
                        e = e + (y * x);
                        x = x - 1;
                        h = h + y;
                    }
                    f = 11 - (e % 11);

                    if (f == 10 || f == 11) {
                        f = 0;
                    }

                    k = c.substring(0, l - 2) + f;
                    x = 11;
                    e = 0;
                    for (var j = 0; j <= (l - 2); j++) {
                        e = e + (k.substring(j, j + 1) * x);
                        x = x - 1;
                    }
                    g = 11 - (e % 11);
                    if (g == 10) {
                        g = 0;
                    }
                    if (g == 11) {
                        g = 0;
                    }
                    if ((f + "" + g) == c.substring(l, l - 2)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        };
    </script>

</body>

</html>