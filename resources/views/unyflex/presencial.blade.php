@include('unyflex.header')







    <div class="container-fluid" style="padding: 20px 0 30px 0">

        <div class="row">

            <div class="col-md-12">

                <div class="container-fluid" style="width: 90%; background-color: #0000008c; color:#c7c7c7; border-radius: 35px; padding: 30px">

                    <div class="row">

                        <div class="col-md-3">

                            <img class="img-fluid zoom" style="border-radius: 15px;" src="{{url("storage/cursos/banner/$curso->photo");}}">

                        </div>

                        <div class="col-md-9" style="color:#fff">

                            <h3>{{$curso->title}}</h3>

                            <p><i class="fa fa-calendar" style="color:#ff6600" aria-hidden="true"></i> <strong style="color:#ff6600">Data: </strong>{{$curso->start_date}}</p>

                            <p><i class="fa fa-clock-o" style="color:#ff6600" aria-hidden="true"></i> <strong style="color:#ff6600">Carga Horária:</strong> {{$curso->workload}} horas</p>

                            <br>

                            <form action="{{ route('certificado') }}" method="post" enctype="multipart/form-data" data-single="true" method="post">
                 <center>
                @csrf
                 <input type="hidden" value="{{$curso->id}}" name="idcurso">
                  <button type="submit" style="background-color:#ff6600" class="btn btn-primary w-full  mr-2 mb-2" >Emitir Certificado</button>
                  </center>
                 </form>
</div></div>
                            
                                                    <div class="col-md-12" style="margin-top:40px">

                                <div class="row">

                                    <div class="col-md-2">

                                        <h5>Paineis do Curso</h5>

                                    </div>

                                    <div class="col-md-10">

                                        <div style="position: relative; top: 10px; z-index: 1; height: 1px; background: #CCCCCC;"></div>

                                    </div>

                                </div>

                                <div class="table-responsive" style="margin-top:30px;">

                                    <table class="table">

                                        <thead style="color:#fff; text-align: left;">

                                            <tr>

                                                <th style="border:0;">Painel</th>

                                                <th style="border:0;">Data</th>

                                                

                                            </tr>

                                        </thead>

                                        <tbody style="color:#fff">

                                        @foreach ($paineis as $painel )

                                            

                                      

                                                                                            <tr>

                                                    <td style="border:0;">{{$painel->title}}</td>

                                                    <td style="border:0;"><?php $data=$painel->start_time; 
                                                                                $data=date('d/m/Y', strtotime($data) );
                                                                                echo $data; ?></td>

                                                    @endforeach

                                                       

                                                        

                                                                                    </tbody>

                                    </table>

                                </div>

                            </div>

                                                <div class="col-md-12" style="margin-top:40px">

                            <div class="row">

                                <div class="col-md-2">

                                    <h5>Materiais do Curso</h5>

                                </div>

                                <div class="col-md-10">

                                    <div style="position: relative; top: 10px; z-index: 1; height: 1px; background: #CCCCCC;"></div>

                                </div>

                            </div>


                            <div class="table-responsive" style="margin-top:30px;">

                                <table class="table">

                                    <thead style="color:#fff; text-align: left;">

                                        <tr>

                                            <th style="border:0;"></th>

                                            <th style="border:0;">Nome</th>

                                            <th style="border:0;">Tipo</th>

                                           


                                        </tr>

                                    </thead>

                                    <tbody style="color:#fff">
                                @foreach ($materiais as $materiall )
                        @if ($materiall!=null)
                                <tr>
                                                 <?php $materialarquivo = $materiall->materials->file_name?>
                                                <td style="border:0;"><a href="{{url("storage/materials/$materialarquivo");}}" download="arquivoaula" style="background-color: #ff6600; color: #fff;" class="btn btn-sm btn-default" role="button"><span class="glyphicon glyphicon-download"></span> Download</a></td>

                                                <td style="border:0;">{{$materiall->materials->name}}</td>

                                                <td style="border:0;">{{$materiall->materials->type}}</td>

                                               

                                            </tr>
                                                  @endif
                    @endforeach


                                                                            </tbody>

                                </table>

                            </div>

                        </div>

                       

                    </div>

                </div>

            </div>

        </div>

    </div><script src="https://www.googleadservices.com/pagead/conversion/429051028/?random=1647627973866&amp;cv=9&amp;fst=1647627973866&amp;num=1&amp;value=0&amp;label=rEabCOT-q4IDEJSZy8wB&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=12&amp;u_tz=-180&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wg3e0&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Funyflex.com.br%2FassistirPresencial%2F2591&amp;ref=https%3A%2F%2Funyflex.com.br%2Farea&amp;tiba=Unyflex%20-%20Flexibilidade%20no%20Ensino%20Unip%C3%BAblica&amp;auid=2140745716.1647546686&amp;hn=www.googleadservices.com&amp;bttype=purchase&amp;async=1&amp;rfmt=3&amp;fmt=4"></script><script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/429051028/?random=1647627973872&amp;cv=9&amp;fst=1647627973872&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=12&amp;u_tz=-180&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wg3e0&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Funyflex.com.br%2FassistirPresencial%2F2591&amp;ref=https%3A%2F%2Funyflex.com.br%2Farea&amp;tiba=Unyflex%20-%20Flexibilidade%20no%20Ensino%20Unip%C3%BAblica&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>



    <!-- modal erro -->

    <div class="modal fade" id="modal_erro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Reportar erro</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">

                    <form>

                        <div class="form-group">

                            <input type="text" class="form-control" id="txt_url" disabled="">

                        </div>

                        <div class="form-group">

                            <textarea class="form-control" id="txt_erro" rows="3" placeholder="Descreva o erro em detalhes para que possamos corrigí-los o mais breve possível"></textarea>

                        </div>

                        <button type="button" class="btn mb-2" style="float: right; background-color: #ff6600; color:#fff" id="enviar_erro">Enviar</button>

                    </form>



                </div>

            </div>

        </div>

    </div>





    <!-- Lista Duvidas -->

    <div class="modal fade bd-example-modal-lg" id="novaDuvida" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Nova Dúvidas</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <form action="" method="post">

                    <div class="modal-body">

                        <div class="row">

                            <div class="form-group col-md-12">

                                <label for="setor" class="col-md-2 control-label">Setor</label>

                                <div class="col-md-12 ">

                                    <select name="setor" id="setor" class="form-control">

                                        <option disabled="" selected="">Selecione</option>

                                        <option value="compras">Compras e Licitações</option>

                                        <option value="contabilidade">Contabilidade e Orçamento</option>

                                        <option value="sistemas">Sistemas</option>

                                        <option value="rh">Gestão de Pessoal (RH)</option>

                                        <option value="juridico">Jurídico / Advogados</option>

                                        <option value="patrimonio">Controle Interno e Patrimônio</option>

                                        <option value="tributacao">Gestão Fiscal e Tributação</option>

                                        <option value="processo-legislativo">Processo Legislativo</option>

                                    </select>

                                </div>

                            </div>

                            <div class="form-group col-md-12">

                                <label for="setor" class="col-md-2 control-label">Professor</label>

                                <div class="col-md-12">

                                    <select name="professor" id="professor" class="form-control">

                                        <option disabled="" selected="">Selecione</option>

                                                                                    <option value="1000005">Carlos Antonio Lesskiu</option>

                                                                                    <option value="1000034">Daniel Maurício</option>

                                                                                    <option value="1000079">Willian Batista de Oliveira</option>

                                                                                    <option value="58">Janete Probst Munhoz</option>

                                                                            </select>

                                </div>

                            </div>

                            <div class="form-group col-md-12">

                                <label for="assunto" class="col-md-2 control-label">Assunto</label>

                                <div class="col-md-12">

                                    <input required="" class="form-control" name="assunto" id="assunto" type="text" placeholder="Assunto">

                                </div>

                            </div>

                            <div class="form-group col-md-12">

                                <label for="mensagem" class="col-md-2 control-label">Mensagem</label>

                                <div class="col-md-12">

                                    <textarea required="" class="form-control" name="mensagem" rows="3" id="mensagem" placeholder="Mensagem"></textarea>

                                    <span class="help-block"></span>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                        <input type="submit" name="nova-mensagem" class="btn btn-raised bg-unipublica btn-primary" value="Enviar">

                    </div>

                </form>

            </div>

        </div>

    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>

    <script src="//vjs.zencdn.net/7.8.2/video.min.js"></script>







    <script>

        var ipCliente = '179.48.49.239'

        var urlCliente = window.location.href;

        $('#btn_prova').on('click', function() {

            $('#modal_prova').modal('show')

        })



        $('#iniciar_prova').submit(function(e) {

            e.preventDefault()

            let dados = null;

            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "carregaProva",

                    "id": window.location.pathname.split('/')[2],

                    "idB": window.location.pathname.split('/')[3] == null ? '0' : window.location.pathname.split('/')[3]

                },

                success: function(e) {

                    if (e == '1') {

                        alert('Erro ao processar prova, por favor tente novamente mais tarde. Caso o erro persista, entre em contato conosco pelo chat.');

                    } else if (e == '2') {

                        alert('Você não possui este curso disponível em sua área.')

                    } else if (e == '3') {

                        alert('Você já realizou o número de tentativas máxima. Entre em contato conosco para dúvidas!')

                    } else {

                        $('#idProva').val(e[0])

                        $('#inicio').text(e[1])

                        $('#fim').text(e[2])

                        $('#iniciar_prova').fadeOut('fast', function() {

                            $('#prova').fadeIn('fast')

                            e[3].forEach(element => {

                                var idPerg = element.idPerg

                                var perg = element.pergunta

                                var a =

                                    '    <div class="card">  ' +

                                    '             <div class="card-body">  ' +

                                    '   <div class="questao" id="' + idPerg + '">  ' +

                                    '' + perg + '' +

                                    '  </div>  ' +

                                    ' <div class="form-group col-md-12" id="q' + idPerg + '"></div>' +

                                    '             </div>  ' +

                                    '  </div>  ';

                                $('#questoes').append(a);

                                element['respostas'].forEach(element2 => {

                                    var idResp = element2.idResp

                                    var resp = element2.resposta

                                    $('#q' + idPerg).append(resp)

                                })

                            });

                        })

                    }

                },

            })



        })



        $('#prova').submit(function(e) {

            e.preventDefault()

            $('#result').text('')

            $('#result_icon').html('')

            $('#result_button').html('')



            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "pGravaProva",

                    "dados": verResp()

                },

                success: function(e) {

                    $('#prova').fadeOut('slow', function() {

                        $('#conclusao').fadeIn('slow', function() {

                            if (e == '0') {

                                $('#result').text('Aprovado')

                                $('#result_icon').append('<i class="fas fa-check-circle" style="font-size: 40px; color: green"></i>')

                                $('#result_button').append('<form action="../certificado" method="post">  ' +

                                    '   <input type="hidden" id="idaluno" name="id" value="13337">  ' +

                                    '   <input type="hidden" id="curso" name="curso" value="2591">  ' +

                                    '   <button type="submit" class="btn" style="background-color: #ff6600; color:#fff;">Gerar certificado</button>  ' +

                                    '  </form>  ');



                            } else if (e == '1') {

                                $('#result').text('Reprovado')

                                $('#result_icon').append('<i class="fas fa-exclamation-circle" style="font-size: 40px; color: red"></i>')



                            } else if (e == '2') {

                                alert('Erro ao processar resultado, por favor entre em contato conosco informado o erro.')

                                window.location.reload()

                            }

                        })

                    })

                },

            })







        })



        function verResp() {

            let a = [];

            let b = [];

            $('.questao').each(function(index, element) {

                var q = $(this).attr('id')

                $($('#q' + $(element).attr('id')).find('input').each(function(index, element2) {

                    if ($(element2).is(':checked')) {

                        var r = $(this).attr('id')

                        b.push({

                            'idQuestao': q,

                            'idResposta': r

                        })

                    }



                }))

            })

            a = JSON.stringify({

                'idProva': $('#idProva').val(),

                'dados': b

            })

            return a;

        }





        var socket = io('https://unyflexlive.com.br:21369/')



        socket.on('user', data => {

            data == 1 ? socket.emit('cadUser', [socket.id, $('#usuario').val()]) : '';

        })







        $('#chat_docente').on('show.bs.modal', function() {

            var usuario = $('#usuario').val()

            if (usuario.length > 0) {

                var curso = $('#curso').val()

                var docente = $('#docente').val()

                let obj = {

                    'usuario': usuario,

                    'curso': curso,

                    'docente': docente

                }

                socket.emit('historicoMsg', obj);

            }

        })



        $('#docente').on('change', () => {

            $('.mensagens').html('')

            var usuario = $('#usuario').val()

            var curso = $('#curso').val()

            var docente = $('#docente').val()

            let obj = {

                'usuario': usuario,

                'curso': curso,

                'docente': docente

            }

            socket.emit('historicoMsg', obj);

        })



        socket.on('hMsg', function(obj) {

            console.log(obj)

            for (mensagem of obj) {

                rMsg(mensagem);

            }

        })

        socket.on('realChat', function(obj) {

            let a = {

                'data': obj.data,

                'mensagem': obj.dados[0].dados[0].dados[0].mensagem,

                'tipo': obj.tipo

            }

            rMsg(a);

        })





        $('#chat').submit(function(e) {

            e.preventDefault()

            var n_usuario = $('#n_usuario').val()

            var usuario = $('#usuario').val()

            var mensagem = $('#mensagem').val()

            var curso = $('#curso').val()

            var docente = $('#docente').val()





            if (usuario.length && mensagem.length) {

                var obj = {

                    data: new Date(),

                    tipo: '0',

                    usuario: usuario,

                    dados: [{

                        curso: curso,

                        dados: [{

                            docente: docente,

                            dados: [{

                                mensagem: mensagem,

                            }]

                        }]

                    }]



                }

                eMsg(obj);

                socket.emit('enviaMsg', obj);

            }



            mensagem = $('#mensagem').val('');

            $('#mensagem').focus();

        });



        function eMsg(data) {

            var dt = new Date();

            var time = dt.getHours() == 0 ? '00' + ":" + dt.getMinutes() + ":" + dt.getSeconds() : dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var msg = data.dados[0].dados[0].dados[0].mensagem

            var user = data.usuario

            $('.mensagens').append('   <div class="container bmsg float-right">  ' +

                '           <div class="row">  ' +

                '               <div class="col-md-12 pt-3 pl-3 pr-3">  ' +

                '                   <span>' + msg + '</span>  ' +

                '               </div>  ' +

                '           </div>  ' +

                '           <div class="row">  ' +

                '               <div class="col-md-12">  ' +

                '                   <div class="float-right">  ' +

                '                       <small class="text-muted">' + time + '</small>  ' +

                '                       <small><i class="far fa-check"  ' +

                '                               style="color: #ff6600"></i></small>  ' +

                '                   </div>  ' +

                '               </div>  ' +

                '           </div>  ' +

                '      </div>  ');

            $('.mensagens').scrollTop($('.mensagens')[0].scrollHeight)

        }



        function rMsg(data) {

            var dt = new Date(data.data);

            var time = dt.getHours() == 0 ? '00' + ":" + dt.getMinutes() + ":" + dt.getSeconds() : dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var msg = data.mensagem

            var tipo = data.tipo

            tipo == '0' ? $('.mensagens').append('   <div class="container bmsg float-right">  ' +

                '           <div class="row">  ' +

                '               <div class="col-md-12 pt-3 pl-3 pr-3">  ' +

                '                   <span>' + msg + '</span>  ' +

                '               </div>  ' +

                '           </div>  ' +

                '           <div class="row">  ' +

                '               <div class="col-md-12">  ' +

                '                   <div class="float-right">  ' +

                '                       <small class="text-muted">' + time + '</small>  ' +

                '                       <small><i class="far fa-check"  ' +

                '                               style="color: #ff6600"></i></small>  ' +

                '                   </div>  ' +

                '               </div>  ' +

                '           </div>  ' +

                '      </div>  ') : $('.mensagens').append('   <div class="container bmsg float-left">  ' +

                '           <div class="row">  ' +

                '               <div class="col-md-12 pt-3 pl-3 pr-3">  ' +

                '                   <span>' + msg + '</span>  ' +

                '               </div>  ' +

                '           </div>  ' +

                '           <div class="row">  ' +

                '               <div class="col-md-12">  ' +

                '                   <div class="float-right">  ' +

                '                       <small class="text-muted">' + time + '</small>  ' +

                '                       <small><i class="far fa-check"  ' +

                '                               style="color: #ff6600"></i></small>  ' +

                '                   </div>  ' +

                '               </div>  ' +

                '           </div>  ' +

                '      </div>  ');

            $('.mensagens').scrollTop($('.mensagens')[0].scrollHeight);

        }



        $('.btn_aulap').on('click', function() {

            var id = $(this).attr('id')







            if (id != '') {

                $('#aula').attr('src', 'https://www.youtube.com/embed/' + id + '?controls=1')

            } else {

                alert('Este painel não está disponível. Verifique na programação do curso o horário de transmissão!')

            }

        })



        $(document).ready(function() {







            $('#btn_erro').on('click', function() {

                $('#modal_erro').modal('show')

            })



            $('#btn_chat_professor').on('click', function() {

                $('#chat_docente').modal('show')

            })



            $('#modal_erro').on('show.bs.modal', function(e) {

                $('#txt_url').val(window.location.href)

            })



            $('#enviar_erro').on('click', function() {

                envia_erro()

            })









            $('#btn_iniciarcurso').on('click', function() {

                $('#corpo').fadeOut('slow', function() {

                    $('#img_logo').animate({

                        height: 80,

                        top: -10,

                    });

                    $('#player_curso').fadeIn('slow')

                })

            })



            $('#btn_voltarcurso').on('click', function() {

                $('#player_curso').fadeOut('slow', function() {

                    $('#img_logo').animate({

                        height: 220,

                        top: -20

                    });

                    $('#aula').attr('src', '')

                    $('#chat_aovivo').attr('src', '')

                    $('#corpo').fadeIn('slow')

                })

            })



            $('.btn_aovivo').on('click', function() {

                var id = $(this).attr('id')

                if (id != '') {

                    $('#aula').attr('src', 'https://www.youtube.com/embed/' + id + '?controls=1')

                    $('#chat_aovivo').attr('src', 'https://www.youtube.com/live_chat?v=' + id + '&embed_domain=unyflex.com.br')

                } else {

                    alert('Este painel não está disponível. Verifique na programação do curso o horário de transmissão!')

                }

            })





            $('#btn_pesquisar').on('click', function(e) {

                if ($('#pesquisa').is(':visible') == false) {

                    e.preventDefault();

                    $('#resultado_pesquisa').collapse('hide')

                    $('#re_pesquisa').html('')

                    $('#pesquisa').fadeIn('slow')

                    $('#txt_pesquisa').val('')

                } else {

                    $('#pesquisa').fadeOut('slow', function() {

                        e.preventDefault();

                        $('#resultado_pesquisa').collapse('hide')

                        $('#re_pesquisa').html('')

                        $('#txt_pesquisa').val('')

                    })

                }

            })



            $('#btn_pesquisa').on('click', function(e) {

                var pesquisa = $('#txt_pesquisa').val();



                pesquisa_site(pesquisa);

            })





        });



        function envia_erro() {



            var erro = $('#txt_erro').val()



            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "envia_erro_avulso",

                    "url": window.location.href,

                    "erro": erro

                },

                success: function(e) {

                    console.log(e)

                    if (e == 'sucesso') {

                        alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')

                        $('#modal_erro').modal('hide')

                    }



                },

            })



        }



        function pesquisa_site(pesquisa) {

            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "pesquisa_site",

                    "pesquisa": pesquisa,

                },

                success: function(e) {

                    if (e == 'erro' || e == null) {

                        $('#texto_pesquisa').text('Não foi possível completar a pesquisa, por favor verifique a quantidade de caracteres e a palavra informada.')

                        $('#resultado_pesquisa').collapse('show')

                        $('#re_pesquisa').html('')

                    } else {

                        $('#texto_pesquisa').text('Foram encontrados ' + e.length + ' resultados em nossa plataforma.')

                        $('#resultado_pesquisa').collapse('show');

                        $('#re_pesquisa').html('')

                        for (let a = 0; a < e.length; a++) {

                            $('#re_pesquisa').append('<a href="https://unyflex.com.br/curso/' + e[a]['id'] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a]['id'] + '.png" class="zoom"></a>')

                        }

                    }

                },

            });

        }

    </script>









</body></html>