@include('unyflex.header')





    <div class="collapse" id="pesquisa">

        <div class="row" style="justify-content: center">

            <div class="container" style="display: flex; flex-direction: row; margin: 15px 0 0 120px;">

                <input type="text" id="txt_pesquisa" class="form-control" name="" style="border-radius: 25px; text-align: center; height: 60px" placeholder="digite o nome do curso, painel ou máteria.">

                <button type="button" class="btn btn-outline-dark zoom" style="border-radius: 25px;position: relative;bottom: 0;left: -109px;border: none;width: 120px; background-color: #ff6600" id="btn_pesquisa"><i class="fas fa-search" style="color: white" aria-hidden="true"></i></button>

            </div>

        </div>

        <div class="collapse" id="resultado_pesquisa">

            <h4 style="text-align: center; color:#fff;margin-top: 35px; font-weight: 400"><span id="texto_pesquisa"></span></h4>

                <div id="re_pesquisa" class="row" style="font-size: 25px; color:#ff6600; padding: 30px; justify-content: center">

                </div>

        </div>



    </div>







    <div class="container" style="padding: 20px 0 30px 0">

        <div class="row">

            <div class="container" style="width: 100%; background-color: #0000008c; border-radius: 35px; padding: 30px">

                <div class="col-md-12 text-center">

                    <h4 style="color: #fff"> <span style="color: #ff6600; font-weight: bold">{{$student->name}}</span>.</h4>

                                     <br><br>


                        <p></p>

                                    </div>





                <nav>
                @if($porcentagem>70)
                <h4 style="color:#fff">Parabéns Você foi Aprovado Com <span style="color: #00ff00; font-weight: bold">{{$porcentagem}}% </span>De Aproveitamento.</h4>
                <form action="{{ route('certificado') }}" method="post" enctype="multipart/form-data" data-single="true" method="post">
                 <center>
                @csrf
                 <input type="hidden" value="{{$curso->id}}" name="idcurso">
                  <button type="submit" style="background-color:#ff6600" class="btn btn-primary w-full  mr-2 mb-2" >Emitir Certificado</button>
                  </center>
                 </form>
                @else
                 <h4 style="color:#fff">Infelizmente você não foi aprovado com apenas: <span style="color: #ff0000; font-weight: bold">{{$porcentagem}}% </span> De Aproveitamento. Refaça nossa prova para atingir o resultado desejado</h4>
                 <a href="/unyflex/prova/{{$curso->slug}}">
                 <center>
                  <input type="button" style="background-color:#ff6600" class="btn btn-primary w-full  mr-2 mb-2" value="Refazer Prova">
                  </center>
                  </a>
                @endif
                </div>

            </div>

        </div>



        <div class="row" style="padding-top: 20px">

            <div class="container" style="width: 100%; background-color: #0000008c; border-radius: 35px; padding: 30px">

                <div class="col-md-12 text-center">

                    <h4 style="color: #fff">Cursos recomendados para você</h4>

                </div>

                <div class="col-md-12 text-center" style="padding-top: 40px">

                    <div class="row" style="justify-content: center">

                                                    <div class="col-md-3 text-center">

                                <a href="../../curso/311"><img class="img-fluid zoom" src="https://unyflex.com.br/ava/views/assets/images/box-curso/311.png" alt=""></a>

                            </div>

                                                    <div class="col-md-3 text-center">

                                <a href="../../curso/283"><img class="img-fluid zoom" src="https://unyflex.com.br/ava/views/assets/images/box-curso/283.png" alt=""></a>

                            </div>

                                                    <div class="col-md-3 text-center">

                                <a href="../../curso/356"><img class="img-fluid zoom" src="https://unyflex.com.br/ava/views/assets/images/box-curso/356.png" alt=""></a>

                            </div>

                                                    <div class="col-md-3 text-center">

                                <a href="../../curso/346"><img class="img-fluid zoom" src="https://unyflex.com.br/ava/views/assets/images/box-curso/346.png" alt=""></a>

                            </div>

                                            </div>

                </div>

            </div>

        </div>



        <!-- modals -->

        <!-- entrevistas -->



        <div class="modal fade" id="modal_entrevista" tabindex="-1" role="dialog">

            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <h5 class="modal-title" id="modal_ent_titulo">Modal title</h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">×</span>

                        </button>

                    </div>

                    <div class="modal-body p-0" style="min-height: 500px">

                        <iframe id="player" src="https://www.youtube.com/embed/yLQu12BSBVI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

                    </div>

                </div>

            </div>

        </div>



        <!-- podcasts -->



        <div class="modal fade" id="modal_podcast" tabindex="-1" role="dialog">

            <div class="modal-dialog modal-dialog-centered " role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <h5 class="modal-title" id="modal_pod_titulo"></h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">×</span>

                        </button>

                    </div>

                    <div class="modal-body p-0" style="min-height: 150px">

                        <div class="container" style="height: 150px">

                            <div class="row h-100" style="align-content: center">

                                <audio id="player_pod" src="" controls="" preload="auto" autoplay="" style="width: 100%"></audio>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>



    



    <script>

        var ipCliente = '179.48.49.239'

        var urlCliente = window.location.href

        $.getScript("https://unyflex.com.br/views/public/includes/watcher.js")

        $(document).ready(function() {



            $('#btn_pesquisar').on('click', function(e) {

                if ($('#pesquisa').is(':visible') == false) {



                    e.preventDefault();

                    $('#resultado_pesquisa').collapse('hide')

                    $('#re_pesquisa').html('')

                    $('#pesquisa').fadeIn('slow')

                    $('#txt_pesquisa').val('')

                } else {

                    $('#pesquisa').fadeOut('slow', function() {

                        e.preventDefault();

                        $('#resultado_pesquisa').collapse('hide')

                        $('#re_pesquisa').html('')



                        $('#txt_pesquisa').val('')

                    })

                }

            })



            $('#btn_pesquisa').on('click', function(e) {

                var pesquisa = $('#txt_pesquisa').val();



                pesquisa_site(pesquisa);

            })



            $('.carrega-uny100').on('click', function(e) {

                e.preventDefault()

                carrega_uny100($(this).attr('id'))

            })

            $('.carrega-matuny100').on('click', function(e) {

                $('#re_mat').html('')

                $('#re_ent').html('')

                $('#re_pod').html('')

                e.preventDefault()

                carrega_uny100_mat($(this).attr('id'))

            })



            $('#voltar_pacote').on('click', function(e) {

                e.preventDefault()



                $('#m_cursos').fadeOut('fast', function() {

                    $('#m_pacotes').fadeIn('fast')

                    $('#re_cursos').html('')

                })



            })

            $('#voltar_material').on('click', function(e) {

                e.preventDefault()

                $('#mat_').fadeOut('fast', function() {

                    $('#mat_pacotes').fadeIn('fast')

                    $('#re_mat').html('')

                    $('#re_ent').html('')

                    $('#re_pod').html('')

                })



            })



            $('#modal_entrevista').on('hide.bs.modal', function() {

                $('#player').attr('src', '')

            })

            $('#modal_podcast').on('hide.bs.modal', function() {

                $('#player_pod').attr('src', '')

            })



            $('#modal_entrevista').on('shown.bs.modal', function() {

                $('#player').attr('width', $('#modal_entrevista').find('.modal-body')[0].clientWidth).attr('height', $('#modal_entrevista').find('.modal-body')[0].clientHeight)

            })





        });







        function carrega_uny100(id) {

            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "carrega_uny100",

                    "id": id,

                },

                success: function(data) {

                    if (data != null || data != '') {

                        for (let index = 0; index < data.length; index++) {

                            $('#m_pacotes').fadeOut('fast', function() {

                                $('#re_cursos').append('   <div class="col-md-3 text-center">  ' +

                                    '   <a href="https://unyflex.com.br/assistir/' + data[index]['id'] + '/' + id + '" class="carrega-curso100" id=""><img class="img-fluid zoom" src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + data[index]['id'] + '.png" alt="">  ' +

                                    '  </a></div>  ');

                                $('#m_cursos').fadeIn('slow')

                            })



                        }

                    }

                },

            });

        }



        function carrega_uny100_mat(id) {

            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "carrega_uny100_mat",

                    "id": id,

                },

                success: function(data) {

                    console.log(data)

                    if (data != null) {

                        $('#mat_pacotes').fadeOut('fast', function() {

                            if (data['apostilas'] != null) {

                                for (let a = 0; a < data['apostilas'].length; a++) {

                                    $('#re_mat').append('   <div class="row" style="justify-content: center">  ' +

                                        '  <a href="https://unyflex.com.br/ava/views/assets/materiais/' + data['apostilas'][a]['arquivo'] + '" target="_blank" style="text-decoration-color: #ff6600; color: #fff">' + data['apostilas'][a]['nome'] + '</a> </div>  ');



                                }

                            }

                            if (data['entrevistas'] != null) {

                                for (let b = 0; b < data['entrevistas'].length; b++) {

                                    $('#re_ent').append('   <div class="row" style="justify-content: center">  ' +

                                        '  <a class="ent" data-src="' + data['entrevistas'][b]['link'] + '" href="#" style="text-decoration-color: #ff6600; color: #fff">' + data['entrevistas'][b]['titulo'] + '</a> </div>  ');



                                }

                            }

                            if (data['podcasts'] != null) {

                                for (let c = 0; c < data['podcasts'].length; c++) {

                                    $('#re_pod').append('   <div class="row" style="justify-content: center">  ' +

                                        '  <a href="" class="pod" data-src="https://unyflex.com.br/ava/views/assets/podcast/' + data['podcasts'][c]['arquivo'] + '" style="text-decoration-color: #ff6600; color: #fff">' + data['podcasts'][c]['titulo'] + '</a> </div>  ');



                                }

                            }

                            $('#mat_').fadeIn('slow')

                            $('.ent').on('click', function(e) {

                                e.preventDefault()



                                $('#modal_ent_titulo').text($(this).text())

                                $('#player').attr('src', $(this).data('src'))

                                $('#modal_entrevista').modal('show');

                            })

                            $('.pod').on('click', function(e) {

                                e.preventDefault()

                                $('#modal_pod_titulo').text($(this).text())

                                $('#player_pod').attr('src', $(this).data('src'))

                                $('#modal_podcast').modal('show');

                            })

                        })



                    }

                },

            });

        }





        function pesquisa_site(pesquisa) {

            $.ajax({

                type: 'POST',

                url: "/views/public/includes/functions.php",

                cache: false,

                data: {

                    "func": "pesquisa_site",

                    "pesquisa": pesquisa,

                },

                success: function(e) {

                    if (e == 'erro' || e == null) {

                        $('#texto_pesquisa').text('Não foi possível completar a pesquisa, por favor verifique a quantidade de caracteres e a palavra informada.')

                        $('#resultado_pesquisa').collapse('show')

                        $('#re_pesquisa').html('')

                    } else {

                        $('#texto_pesquisa').text('Foram encontrados ' + e.length + ' resultados em nossa plataforma.')

                        $('#resultado_pesquisa').collapse('show');

                        $('#re_pesquisa').html('')

                        for (let a = 0; a < e.length; a++) {

                            $('#re_pesquisa').append('<a href="https://unyflex.com.br/curso/' + e[a]['id'] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a]['id'] + '.png" class="zoom"></a>')

                        }

                    }

                },

            });

        }

    </script>







</body>

</html>

