@include('unyflex.header')





    <div class="container-fluid" id="player_curso" style="display: none; height: 100%; background-color: #0000007a">



        <div class="row">

            <div class="col-md-8" style="padding:0">



           <iframe id="aula" height="700" width="100%" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

                



            </div>



            <div class="col-md-4" style="padding: 0;">

                <div class="container" style="background-color: #0000007a; min-height: 700px">

                    <div class="row" style="justify-content: center">

                        <button type="button" class="btn btn-link" style="text-decoration: none; color:#ff6600" id="btn_voltarcurso">Voltar</button>

                    </div>

                    <div class="col-md-12" style="text-align: center; padding-top: 15px">

                        <h3 style="font-weight: bold; text-align: center; color: #fff">Conteúdo do Curso</h3>

                    </div>

                    <hr>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="accordion" id="paineis_aulas">

                                                                        <div class="container">

                                            <div class="row" style="justify-content: center">

                                                <h5 style="color: #ff6600">Painel - 1</h5>

                                            </div>

                                            <div class="row" style="justify-content: center">

                                                <button class="btn btn-link" style="text-decoration: none; color:#fff" type="button" data-toggle="collapse" data-target="#p_1" aria-expanded="true" aria-controls="collapseOne">

                                                    Gestão Financeira Aquisições – Retenções – Empenhamento<br><span style="color:#9a9a9a">Precauções e Providências</span>

                                                </button>

                                            </div>

                                            <div id="p_1" class="collapse" data-parent="#paineis_aulas">

                                                <div class="container">

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="mW0DVX804WY">

                                                                Parte - 1                                                            </button>

                                                        </div>

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="aBshg1cSvSM">

                                                                Parte - 2                                                            </button>

                                                        </div>

                                                                                                    </div>

                                            </div>

                                        </div>

                                        <hr>

                                                                            <div class="container">

                                            <div class="row" style="justify-content: center">

                                                <h5 style="color: #ff6600">Painel - 2</h5>

                                            </div>

                                            <div class="row" style="justify-content: center">

                                                <button class="btn btn-link" style="text-decoration: none; color:#fff" type="button" data-toggle="collapse" data-target="#p_2" aria-expanded="true" aria-controls="collapseOne">

                                                    Gestão Financeira Aquisições – Retenções – Empenhamento<br><span style="color:#9a9a9a">Retenções Obrigatórias</span>

                                                </button>

                                            </div>

                                            <div id="p_2" class="collapse" data-parent="#paineis_aulas">

                                                <div class="container">

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="3i3v_yskyV0">

                                                                Parte - 1                                                            </button>

                                                        </div>

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="dkUzijpc6Ac">

                                                                Parte - 2                                                            </button>

                                                        </div>

                                                                                                    </div>

                                            </div>

                                        </div>

                                        <hr>

                                                                            <div class="container">

                                            <div class="row" style="justify-content: center">

                                                <h5 style="color: #ff6600">Painel - 3</h5>

                                            </div>

                                            <div class="row" style="justify-content: center">

                                                <button class="btn btn-link" style="text-decoration: none; color:#fff" type="button" data-toggle="collapse" data-target="#p_3" aria-expanded="true" aria-controls="collapseOne">

                                                    Gestão Financeira Aquisições – Retenções – Empenhamento<br><span style="color:#9a9a9a">Técnicas e Procedimentos para os Empenhos</span>

                                                </button>

                                            </div>

                                            <div id="p_3" class="collapse" data-parent="#paineis_aulas">

                                                <div class="container">

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="m45LTVlWzow">

                                                                Parte - 1                                                            </button>

                                                        </div>

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="5XcoodfL_8Y">

                                                                Parte - 2                                                            </button>

                                                        </div>

                                                                                                    </div>

                                            </div>

                                        </div>

                                        <hr>

                                                                            <div class="container">

                                            <div class="row" style="justify-content: center">

                                                <h5 style="color: #ff6600">Painel - 4</h5>

                                            </div>

                                            <div class="row" style="justify-content: center">

                                                <button class="btn btn-link" style="text-decoration: none; color:#fff" type="button" data-toggle="collapse" data-target="#p_4" aria-expanded="true" aria-controls="collapseOne">

                                                    Gestão Financeira Aquisições – Retenções – Empenhamento<br><span style="color:#9a9a9a">Aquisições e Contratações Públicas</span>

                                                </button>

                                            </div>

                                            <div id="p_4" class="collapse" data-parent="#paineis_aulas">

                                                <div class="container">

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="oixKSUR3rHI">

                                                                Parte - 1                                                            </button>

                                                        </div>

                                                                                                            <div class="row">

                                                            <button class="btn btn-link btn_aulap" style="text-decoration: none; color:#ff6600" type="button" id="RPw4h7zEG18">

                                                                Parte - 2                                                            </button>

                                                        </div>

                                                                                                    </div>

                                            </div>

                                        </div>

                                        <hr>

                                    

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-12" style="padding: 0; display: none">

                <div class="row" style="justify-content: center; padding-top: 15px">

                    <h5 style="color:#fff">Chat para dúvidas</h5>

                </div>

                <iframe allowfullscreen="" frameborder="0" src="" width="100%" height="500px" id="chat_aovivo"></iframe>

            </div>

        </div>

    </div>



    <div class="container" style="padding-top: 25px" id="corpo">

        <div class="collapse" id="pesquisa" style="padding-bottom: 25px">

            <div class="row" style="justify-content: center">

                <div class="container" style="display: flex; flex-direction: row; margin: 15px 0 0 120px;">

                    <input type="text" id="txt_pesquisa" class="form-control" name="" style="border-radius: 25px; text-align: center; height: 60px" placeholder="digite o nome do curso, painel ou máteria.">

                    <button type="button" class="btn btn-outline-dark zoom" style="border-radius: 25px;position: relative;bottom: 0;left: -109px;border: none;width: 120px; background-color: #ff6600" id="btn_pesquisa"><i class="fas fa-search" style="color: white" aria-hidden="true"></i></button>

                </div>

            </div>

            <div class="collapse" id="resultado_pesquisa">

                <h4 style="text-align: center; color:#fff;margin-top: 35px; font-weight: 400"><span id="texto_pesquisa"></span></h4>

                    <div id="re_pesquisa" class="row" style="font-size: 25px; color:#ff6600; padding: 30px; justify-content: center">

                    </div>

            </div>



        </div>





        <div class="row" style="justify-content: center">

            <img class="zoom" src="{{url("storage/cursos/banner/$curso->photo");}}" style="height: 400px; border-radius: 25px" id="banner">

            <!-- aulas -->



        </div>

        <div class="row" style="padding-top: 30px">

            <div class="col-md-12 text-center">

                <h3 style="color:#fff">{{$curso->title}}</h3>

                <h5 style="color:#9a9a9a">{{$curso->subtitle}}</h5>

            </div>

        </div>

        <div class="row">

            <div class="col-md-2">



            </div>

            <div class="col-md-8 text-center" style="color: #9a9a9a">

                <span>{{$curso->end_date}}</span>

                <span> | </span>

                <span>Carga Horária: {{$curso->workload }}</span>

                <!-- <p><small style="color:red">Manutenção no servidor de avaliações, por favor tente novamente mais tarde.</small></p> -->



            </div>

            <div class="col-md-2">



            </div>

        </div>

        <div class="row" style="justify-content: center; padding-top: 20px">

            <div class="col-md-10">

                <div class="row" style="justify-content: center">

                  

                                            <div class="col-md-3 p-3">

                            <button type="button" class="btn btn-block collapsed" style="background-color: #ff6600; color: #fff" id="btn_materiais" data-toggle="collapse" data-target="#materiais" aria-expanded="false">Materiais</button>



                        </div>

                                                                            <div class="col-md-3 p-3">

                                   
                                        <a href="/unyflex/prova/{{$curso->slug}}">
                                        <button  class="btn btn-block" style="background-color: #ff6600; color: #fff">Iniciar prova</button>
                                        </a>
                                    </div>

                                                                        

                   

                </div>

            </div>

        </div>

        <div class="row" style="padding-top: 30px; justify-content: center">

            <div class="collapse" id="materiais" style="">

                <div class="row">

                    <div class="col-md-12" id="r_materiais">

    
                        @foreach ($materiais as $materiall )
                        @if ($materiall!=null)
                        
                            
                        
                        <div class="row" style="justify-content: center">
                                        <?php $materialarquivo = $materiall->materials->file_name?>
                                    <a href="{{url("storage/materials/$materialarquivo");}}" target="_blank" style="text-decoration-color: #ff6600; color: #fff">{{$materiall->materials->name}}</a>

                                </div>
                            

                     
                       
                         @endif
                    @endforeach




                                                </div>

                </div>

            </div>

        </div>

        <div class="row" style="padding-top: 50px">

            <div class="col-md-12 text-center">



                <div id="programacao">

                    <h3 style="text-align: center; color: #fff; font-weight: bold;padding-bottom: 20px; padding-top: 20px">Confira a programação do curso</h3>

                    





                        @foreach ($curso->panels as $painel )

                            

                       

                            <div class="row" style="justify-content: center; padding-bottom: 15px">

                                <h4 style="color: #fff">Painel: {{$loop->iteration}}</h4>

                            </div>

                            <div class="row" style="justify-content: center">

                                <h3 style="color: #fff; font-weight:bold">{{$painel->title}}</h3>

                            </div>

                            <div class="row" style="justify-content: center; padding-bottom: 10px">

                                <h4 style="color: #a0a0a0; font-weight:normal">{{$painel->title}}</h4>

                            </div>

                            <div class="row" style="justify-content: center;">

                                <span style="color:#ff6600; font-weight: bold">Docente: {{$painel->teachers->name}}<span style="color:#fff;font-weight: normal"></span></span>

                            </div>

                          

                                

                            @foreach ($painel->video_lesson as $video )
                                
                           

                    <iframe style="padding-bottom: 20px; padding-top: 25px" id="you" height="500" width="100%" src="{{$video->link}}" frameborder="0"  allowfullscreen=""></iframe>

 @endforeach

                <span style="color:whitesmoke">

                <?=$painel->content?>

              </span>

                     @endforeach

            </div>

        </div>

    </div>



    



    <!-- modals -->



    <!-- modal erro -->

    <div class="modal fade" id="modal_erro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Reportar erro</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">

                    <form>

                        <div class="form-group">

                            <input type="text" class="form-control" id="txt_url" disabled="">

                        </div>

                        <div class="form-group">

                            <textarea class="form-control" id="txt_erro" rows="3" placeholder="Descreva o erro em detalhes para que possamos corrigí-los o mais breve possível"></textarea>

                        </div>

                        <button type="button" class="btn mb-2" style="float: right; background-color: #ff6600; color:#fff" id="enviar_erro">Enviar</button>

                    </form>



                </div>

            </div>

        </div>

    </div>







    <!-- prova -->

    <div class="modal fade" id="modal_prova" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">

        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Avaliação de Certificação</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">

                    <form id="iniciar_prova">

                        <div class="row">

                            <div class="col-md-12 text-center">

                                <p style="font-size: 17px;"><b>Atenção:</b> Ao iniciar a avaliação você terá 20 minutos para concluir a mesma.</p>



                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12 text-center">

                                <button type="" class="btn" style="background-color: #ff6600; color: #fff" data-toggle="modal" data-target="#provaModal">Iniciar</button>

                            </div>

                        </div>

                    </form>

                    <form id="prova" style="display: none;">

                        <input type="hidden" id="idProva" value="">

                        <div class="container">

                            <div class="row">

                                <div class="col-md-6">

                                    <p><b>Hora de inicio:</b> <span id="inicio"></span></p>

                                </div>

                                <div class="col-md-6" style="text-align: right;">

                                    <p><b>Finalizar até:</b> <span id="fim"></span></p>

                                </div>

                            </div>

                            <div>

                                <div class="col-md-12" id="questoes">

                                </div>

                            </div>

                        </div>

                        <div class="row" style="justify-content: center;">

                            <button type="submit" class="btn" style="background-color: #ff6600; color: #fff">Concluir prova</button>

                        </div>

                    </form>

                    <div id="conclusao" style="display: none;">

                        <div class="container" style="padding: 30px">

                            <div class="row" style="justify-content: center;">

                                <h4>Resultado da avaliação</h4>

                            </div>

                            <div class="col-md-12">

                                <div class="row" style="justify-content: center;">

                                    <h2 id="result"></h2>

                                </div>

                                <div class="row" id="result_icon" style="justify-content: center;">



                                </div>

                                <div class="row mt-3" id="result_button" style="justify-content: center;">



                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <!-- Chat com o professor -->

    <!-- <div class="modal fade bd-example-modal-lg" id="chat_docente" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Chat com o Professor(a)</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <div class="modal-body">

                    <div class="col-10" style="margin: 0 auto">

                        <form id="chat">

                            <div class="form-group col-md-12">

                                <div class="form-row col-md-12">

                                    <input type="hidden" name="curso" id="curso" value="69">

                                    <input type="hidden" name="usuario" id="usuario" value="12129">

                                    <input type="hidden" name="n_usuario" id="n_usuario" value="Paulo Orfanelli">

                                    <div class="form-group">

                                        <label for="">Selecione o docente</label>

                                        <select class="form-control" name="docente" id="docente">

                                                                                            <option class="dropdown-item" value="1">Jonias de Oliveira</option>

                                                                                            <option class="dropdown-item" value="28">João Henrique Mildenberger</option>

                                                                                            <option class="dropdown-item" value="51">Rafael Galvão Rocha Ramalho</option>

                                                                                            <option class="dropdown-item" value="6">Clayson do Nascimento Andrade</option>

                                                                                    </select>

                                    </div>

                                </div>

                                <div class="form-row col-md-12">

                                    <div class="mensagens"></div>

                                </div>

                                <div class="form-row col-md-12">

                                    <div class="input-group mb-3">

                                        <input type="text" name="mensagem" id="mensagem" class="form-control" placeholder="Digite sua mensagem" autocomplete="off">

                                        <div class="input-group-append">

                                            <button class="btn btn-outline-secondary enviachat" type="submit" id="button-addon2"><i class="fal fa-paper-plane"></i></button>

                                        </div>

                                    </div>

                                </div>

                            </div>



                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div> -->

<!-- TESTE PROVA -->



<!-- Button trigger modal --

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">

  Launch demo modal

</button>



<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

<div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title" id="exampleModalLabel">Prova</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">×</span>

        </button>

      </div>

      <div class="modal-body">



                <p>

                    O questionário sobre o curso estará disponível assim que clicar no botão começar. Não será permitido fechar esta janela após o início da prova. Assim que o tempo expirar as respostas selecionadas serão enviadas automaticamente.

                </p>



                

                <p>

                    Tentatívas Restantes: 0                </p>



                <p>

                    Duração Máxima: 30 minutos.

                </p>



                

            </div>

            <div class="modal-footer programacao">

                <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>

            </div>

        </div>

    </div>

</div>





<!-- Modal -->

<div class="modal fade bd-example-modal-lg" id="perguntasModal" tabindex="-1" role="dialog" aria-labelledby="perguntasModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-header programacao">

                <h5 class="modal-title titulo-prova" id="perguntasModalLabel">Prova</h5>               

            </div>

            <div class="modal-body prova-interno">  

                <div class="bgbar"><div id="myBar"></div></div>



                <form id="resultadoProva" action="../views/public/resultadoProva.php" method="post">



                    <br><br>

<p class="pergunta">TESTE

                                        

                    </p>

                    <input type="hidden" name="idQuestao1" value="194">



                    <p class="pergunta">

                        1) A vedação ao nepotismo impede a contratação de::                

                    </p>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta1" value="556">a) Irmãos e primos;</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta1" value="557">b) Avôs e concunhados;</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta1" value="558">c) Cônjuges e sogros.</label>                             

                    </div>



                    <br>

<p class="pergunta">TESTE

                                        

                    </p>

                    <input type="hidden" name="idQuestao2" value="195">



                    <p class="pergunta">

                        2) Todas as diferenças das fontes de recursos deverão serem ajustadas::                

                    </p>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta2" value="559">a) Dentro do ano</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta2" value="560">b) Dentro do mês</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta2" value="561">c) Dentro do bimestre</label>                             

                    </div>



                    <br>

<p class="pergunta">TESTE

                                        

                    </p>

                    <input type="hidden" name="idQuestao3" value="196">



                    <p class="pergunta">

                        3) O procedimento administrativo tendente a verificar a  ocorrência do fato gerador da obrigação correspondente, determinar a matéria tributável, calcular o montante do  tributo devido, identificar o sujeito passivo e, sendo o caso, propor a aplicação da penalidade cabível, corresponde ao  estágio da receita pública denominado :                

                    </p>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta3" value="562">a) recolhimento</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta3" value="563">b) arrecadação</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta3" value="564">c) lançamento</label>                             

                    </div>



                    <br>

<p class="pergunta">TESTE

                                        

                    </p>

                    <input type="hidden" name="idQuestao4" value="197">



                    <p class="pergunta">

                        4) A cotação de preços pode ocorrer::                

                    </p>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta4" value="565">a) na comparação com preços pagos pelo próprio órgão</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta4" value="566">b) em busca no estabelecimento que mais vende</label>                             

                    </div>



                        



                    <div class="radio resposta">                           

                        <label><input type="radio" name="resposta4" value="567">c) perguntando ao prefeito </label>                             

                    </div>



                    <br>                    <input type="hidden" name="numeroPerguntas" value="5">

                    <input type="hidden" name="idCurso" value="69">

                    <input type="hidden" name="idUsuario" value="12129">

                    <input type="hidden" name="idProva" value="165">

                    <input type="submit" class="btn btn-success btn-block" name="enviar-prova" value="Enviar">

                </form>

            </div>

        </div>

        ...

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        <button type="button" class="btn btn-primary">Save changes</button>

      </div>

    </div>

  



    























    <!-- Lista Duvidas -->

    <div class="modal fade bd-example-modal-lg" id="listaDuvidas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Tira Dúvidas</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">

                    <div class="card-body">

                        <div class="row">

                            <div class="table-responsive" style="margin-bottom:200px;">

                                <table class="table table-striped">

                                    <thead>

                                        <tr>

                                            <th>Tópico</th>

                                            <th>Criado em</th>

                                            <th>Última Resposta</th>

                                            <th>Status</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                                                            </tbody>

                                </table>



                            </div>

                        </div>

                    </div>

                    <button class="btn btn-info" data-toggle="modal" data-target="#criarDuvida" style="font-size:12px; margin-top:40px; text-decoration:none;">Criar um novo tópico</button>

                </div>

            </div>

        </div>

    </div>



    <div class="modal fade bd-example-modal-lg" id="criarDuvida" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Tira Dúvidas</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body">

                    <div class="card-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">

                                    <select class="form-control" id="setor" name="setor">

                                        <option selected="">Selecione o setor</option>

                                        <option value="1">Ambiente e Urbanismo</option>

                                        <option value="25">Ano Eleitoral</option>

                                        <option value="2">Compras Públicas</option>

                                        <option value="3">Contadores Municipais</option>

                                        <option value="4">Controle Interno</option>

                                        <option value="5">Finanças Municipais</option>

                                        <option value="28">Gratuitos</option>

                                        <option value="6">Jurídico</option>

                                        <option value="8">Legislativo</option>

                                        <option value="11">Licitações Públicas</option>

                                        <option value="26">Nova Previdência</option>

                                        <option value="99">Outros</option>

                                        <option value="10">Patrimônio</option>

                                        <option value="100">Pregoeiros</option>

                                        <option value="7">Recursos Humanos</option>

                                        <option value="27">Sistemas de Informação</option>

                                        <option value="9">Tributação Municipal</option>



                                    </select>

                                </div>

                                <div class="form-group" id="d_motivo">



                                    <input type="text" class="form-control" id="assunto" name="assunto" placeholder="Assunto">

                                </div>

                                <div class="form-group" id="d_motivo">

                                    <textarea class="form-control" id="mensagem" name="mensagem" rows="8" placeholder="Digite sua dúvida aqui.."></textarea>

                                </div>

                            </div>

                            <div class="col-md-6" style="height: auto">

                                <img src="https://unipublicabrasil.com.br/uploads/email/uny_sf.png" style="margin: auto;    

                                                                                                                   display: block; padding-top: 35px">

                            </div>

                        </div>

                        <div class="card-footer">

                            <!--ENVIAR DÚVIDA/PONTUAR ALUNO -->

                            <input type="hidden" id="idUsuario" name="idUsuario" value="12129">

                            <input type="hidden" id="idCurso" name="idCurso" value="69">

                            <button type="button" onclick="cadastraPergunta()" class="btn btn-primary btn-block" style="background-color:#ff6600; border: 0px">Enviar!! </button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div class="modal fade bd-example-modal-lg" id="duvidaInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="exampleModalCenterTitle">Tira Dúvidas</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">×</span>

                    </button>

                </div>

                <div class="modal-body" id="respostas">





                </div>

            </div>

        </div>



        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

        <script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>

        <script src="//vjs.zencdn.net/7.8.2/video.min.js"></script>







        <script>

            var ipCliente = '179.48.49.239'

            var urlCliente = window.location.href

            $.getScript("https://unyflex.com.br/views/public/includes/watcher.js")

   

           

            $('#btn_prova').on('click', function() {

                $('#modal_prova').modal('show')

            })*/

            $('#iniciar_prova').submit(function(e) {

                

                

                // console.log('teste');

                // return false;

                

                e.preventDefault()

                let dados = null;

                $.ajax({

                    type: 'POST',

                    url: "/views/public/includes/functions.php",

                    cache: false,

                    data: {

                        "func": "carregaProva",

                        "id": window.location.pathname.split('/')[2],

                        "idB": window.location.pathname.split('/')[3] == null ? '0' : window.location.pathname.split('/')[3]

                    },

                    success: function(e) {

                        

                     

                        

                        

                        if (e == '1') {

                            alert('Erro ao processar prova, por favor tente novamente mais tarde. Caso o erro persista, entre em contato conosco pelo chat.');

                        } else if (e == '2') {

                            alert('Você não possui este curso disponível em sua área.')

                        } else if (e == '3') {

                            alert('Você já realizou o número de tentativas máxima. Entre em contato conosco para dúvidas!')

                        } else {

                            $('#idProva').val(e[0])

                            $('#inicio').text(e[1])

                            $('#fim').text(e[2])

                            $('#iniciar_prova').fadeOut('fast', function() {

                                $('#prova').fadeIn('fast')

                                e[3].forEach(element => {

                                    var idPerg = element.idPerg

                                    var perg = element.pergunta

                                    var a =

                                        '    <div class="card">  ' +

                                        '             <div class="card-body">  ' +

                                        '   <div class="questao" id="' + idPerg + '">  ' +

                                        '' + perg + '' +

                                        '  </div>  ' +

                                        ' <div class="form-group col-md-12" id="q' + idPerg + '"></div>' +

                                        '             </div>  ' +

                                        '  </div>  ';

                                    $('#questoes').append(a);

                                    element['respostas'].forEach(element2 => {

                                        var idResp = element2.idResp

                                        var resp = element2.resposta

                                        $('#q' + idPerg).append(resp)

                                    })

                                });

                            })

                        }

                    },

                })



            })



            $('#prova').submit(function(e) {

                e.preventDefault()

                $('#result').text('')

                $('#result_icon').html('')

                $('#result_button').html('')



                $.ajax({

                    type: 'POST',

                    url: "/views/public/includes/functions.php",

                    cache: false,

                    data: {

                        "func": "pGravaProva",

                        "dados": verResp()

                    },

                    success: function(e) {

                        $('#prova').fadeOut('slow', function() {

                            $('#conclusao').fadeIn('slow', function() {

                                if (e == '0') {

                                    $('#result').text('Aprovado')

                                    $('#result_icon').append('<i class="fas fa-check-circle" style="font-size: 40px; color: green"></i>')

                                    $('#result_button').append('<form action="../certificado" method="post">  ' +

                                        '   <input type="hidden" id="idaluno" name="id" value="12129">  ' +

                                        '   <input type="hidden" id="curso" name="curso" value="69">  ' +

                                        '   <button type="submit" class="btn" style="background-color: #ff6600; color:#fff;">Gerar certificado</button>  ' +

                                        '  </form>  ');



                                } else if (e == '1') {

                                    $('#result').text('Reprovado')

                                    $('#result_icon').append('<i class="fas fa-exclamation-circle" style="font-size: 40px; color: red"></i>')



                                } else if (e == '2') {

                                    alert('Erro ao processar resultado, por favor entre em contato conosco informado o erro.')

                                    window.location.reload()

                                }

                            })

                        })

                    },

                })







            })



            function verResp() {

                let a = [];

                let b = [];

                $('.questao').each(function(index, element) {

                    var q = $(this).attr('id')

                    $($('#q' + $(element).attr('id')).find('input').each(function(index, element2) {

                        if ($(element2).is(':checked')) {

                            var r = $(this).attr('id')

                            b.push({

                                'idQuestao': q,

                                'idResposta': r

                            })

                        }



                    }))

                })

                a = JSON.stringify({

                    'idProva': $('#idProva').val(),

                    'dados': b

                })

                return a;

            }





            var socket = io('https://unyflexlive.com.br:21369/')



            socket.on('user', data => {

                data == 1 ? socket.emit('cadUser', [socket.id, $('#usuario').val()]) : '';

            })







            $('#chat_docente').on('show.bs.modal', function() {

                var usuario = $('#usuario').val()

                if (usuario.length > 0) {

                    var curso = $('#curso').val()

                    var docente = $('#docente').val()

                    let obj = {

                        'usuario': usuario,

                        'curso': curso,

                        'docente': docente

                    }

                    socket.emit('historicoMsg', obj);

                }

            })



            $('#docente').on('change', () => {

                $('.mensagens').html('')

                var usuario = $('#usuario').val()

                var curso = $('#curso').val()

                var docente = $('#docente').val()

                let obj = {

                    'usuario': usuario,

                    'curso': curso,

                    'docente': docente

                }

                socket.emit('historicoMsg', obj);

            })



            socket.on('hMsg', function(obj) {

                console.log(obj)

                for (mensagem of obj) {

                    rMsg(mensagem);

                }

            })

            socket.on('realChat', function(obj) {

                let a = {

                    'data': obj.data,

                    'mensagem': obj.dados[0].dados[0].dados[0].mensagem,

                    'tipo': obj.tipo

                }

                rMsg(a);

            })





            $('#chat').submit(function(e) {

                e.preventDefault()

                var n_usuario = $('#n_usuario').val()

                var usuario = $('#usuario').val()

                var mensagem = $('#mensagem').val()

                var curso = $('#curso').val()

                var docente = $('#docente').val()





                if (usuario.length && mensagem.length) {

                    var obj = {

                        data: new Date(),

                        tipo: '0',

                        usuario: usuario,

                        dados: [{

                            curso: curso,

                            dados: [{

                                docente: docente,

                                dados: [{

                                    mensagem: mensagem,

                                }]

                            }]

                        }]



                    }

                    eMsg(obj);

                   

                }



                mensagem = $('#mensagem').val('');

                $('#mensagem').focus();

            });



            function eMsg(data) {

                var dt = new Date();

                var time = dt.getHours() == 0 ? '00' + ":" + dt.getMinutes() + ":" + dt.getSeconds() : dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                var msg = data.dados[0].dados[0].dados[0].mensagem

                var user = data.usuario

                $('.mensagens').append('   <div class="container bmsg float-right">  ' +

                    '           <div class="row">  ' +

                    '               <div class="col-md-12 pt-3 pl-3 pr-3">  ' +

                    '                   <span>' + msg + '</span>  ' +

                    '               </div>  ' +

                    '           </div>  ' +

                    '           <div class="row">  ' +

                    '               <div class="col-md-12">  ' +

                    '                   <div class="float-right">  ' +

                    '                       <small class="text-muted">' + time + '</small>  ' +

                    '                       <small><i class="far fa-check"  ' +

                    '                               style="color: #ff6600"></i></small>  ' +

                    '                   </div>  ' +

                    '               </div>  ' +

                    '           </div>  ' +

                    '      </div>  ');

                $('.mensagens').scrollTop($('.mensagens')[0].scrollHeight)

            }



            function rMsg(data) {

                var dt = new Date(data.data);

                var time = dt.getHours() == 0 ? '00' + ":" + dt.getMinutes() + ":" + dt.getSeconds() : dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                var msg = data.mensagem

                var tipo = data.tipo

                tipo == '0' ? $('.mensagens').append('   <div class="container bmsg float-right">  ' +

                    '           <div class="row">  ' +

                    '               <div class="col-md-12 pt-3 pl-3 pr-3">  ' +

                    '                   <span>' + msg + '</span>  ' +

                    '               </div>  ' +

                    '           </div>  ' +

                    '           <div class="row">  ' +

                    '               <div class="col-md-12">  ' +

                    '                   <div class="float-right">  ' +

                    '                       <small class="text-muted">' + time + '</small>  ' +

                    '                       <small><i class="far fa-check"  ' +

                    '                               style="color: #ff6600"></i></small>  ' +

                    '                   </div>  ' +

                    '               </div>  ' +

                    '           </div>  ' +

                    '      </div>  ') : $('.mensagens').append('   <div class="container bmsg float-left">  ' +

                    '           <div class="row">  ' +

                    '               <div class="col-md-12 pt-3 pl-3 pr-3">  ' +

                    '                   <span>' + msg + '</span>  ' +

                    '               </div>  ' +

                    '           </div>  ' +

                    '           <div class="row">  ' +

                    '               <div class="col-md-12">  ' +

                    '                   <div class="float-right">  ' +

                    '                       <small class="text-muted">' + time + '</small>  ' +

                    '                       <small><i class="far fa-check"  ' +

                    '                               style="color: #ff6600"></i></small>  ' +

                    '                   </div>  ' +

                    '               </div>  ' +

                    '           </div>  ' +

                    '      </div>  ');

                $('.mensagens').scrollTop($('.mensagens')[0].scrollHeight);

            }



            $('.btn_aulap').on('click', function() {

                var id = $(this).attr('id')







                if (id != '') {

                    $('#aula').attr('src', 'https://www.youtube.com/embed/' + id + '?controls=1')

                } else {

                    alert('Este painel não está disponível. Verifique na programação do curso o horário de transmissão!')

                }

            })



            $(document).ready(function() {







                $('#btn_erro').on('click', function() {

                    $('#modal_erro').modal('show')

                })



                $('#btn_chat_professor').on('click', function() {

                    $('#chat_docente').modal('show')

                })



                $('#modal_erro').on('show.bs.modal', function(e) {

                    $('#txt_url').val(window.location.href)

                })



                $('#enviar_erro').on('click', function() {

                    envia_erro()

                })









                $('#btn_iniciarcurso').on('click', function() {

                    $('#corpo').fadeOut('slow', function() {

                        $('#img_logo').animate({

                            height: 80,

                            top: -10,

                        });

                        $('#player_curso').fadeIn('slow')

                    })

                })



                $('#btn_voltarcurso').on('click', function() {

                    $('#player_curso').fadeOut('slow', function() {

                        $('#img_logo').animate({

                            height: 220,

                            top: -20

                        });

                        $('#aula').attr('src', '')

                        $('#chat_aovivo').attr('src', '')

                        $('#corpo').fadeIn('slow')

                    })

                })



                $('.btn_aovivo').on('click', function() {

                    var id = $(this).attr('id')

                    if (id != '') {

                        $('#aula').attr('src', 'https://www.youtube.com/embed/' + id + '?controls=1')

                        $('#chat_aovivo').attr('src', 'https://www.youtube.com/live_chat?v=' + id + '&embed_domain=unyflex.com.br')

                    } else {

                        alert('Este painel não está disponível. Verifique na programação do curso o horário de transmissão!')

                    }

                })





                $('#btn_pesquisar').on('click', function(e) {

                    if ($('#pesquisa').is(':visible') == false) {

                        e.preventDefault();

                        $('#resultado_pesquisa').collapse('hide')

                        $('#re_pesquisa').html('')

                        $('#pesquisa').fadeIn('slow')

                        $('#txt_pesquisa').val('')

                    } else {

                        $('#pesquisa').fadeOut('slow', function() {

                            e.preventDefault();

                            $('#resultado_pesquisa').collapse('hide')

                            $('#re_pesquisa').html('')

                            $('#txt_pesquisa').val('')

                        })

                    }

                })



                $('#btn_pesquisa').on('click', function(e) {

                    var pesquisa = $('#txt_pesquisa').val();



                    pesquisa_site(pesquisa);

                })





            });



            function envia_erro() {



                var erro = $('#txt_erro').val()



                $.ajax({

                    type: 'POST',

                    url: "/views/public/includes/functions.php",

                    cache: false,

                    data: {

                        "func": "envia_erro_avulso",

                        "url": window.location.href,

                        "erro": erro

                    },

                    success: function(e) {

                        console.log(e)

                        if (e == 'sucesso') {

                            alert('Agradecemos por reportar o erro. Em breve já estará corrigido!')

                            $('#modal_erro').modal('hide')

                        }



                    },

                })



            }



            function pesquisa_site(pesquisa) {

                $.ajax({

                    type: 'POST',

                    url: "/views/public/includes/functions.php",

                    cache: false,

                    data: {

                        "func": "pesquisa_site",

                        "pesquisa": pesquisa,

                    },

                    success: function(e) {

                        if (e == 'erro' || e == null) {

                            $('#texto_pesquisa').text('Não foi possível completar a pesquisa, por favor verifique a quantidade de caracteres e a palavra informada.')

                            $('#resultado_pesquisa').collapse('show')

                            $('#re_pesquisa').html('')

                        } else {

                            $('#texto_pesquisa').text('Foram encontrados ' + e.length + ' resultados em nossa plataforma.')

                            $('#resultado_pesquisa').collapse('show');

                            $('#re_pesquisa').html('')

                            for (let a = 0; a < e.length; a++) {

                                $('#re_pesquisa').append('<a href="https://unyflex.com.br/curso/' + e[a]['id'] + '"><img src="https://unyflex.com.br/ava/views/assets/images/box-curso/' + e[a]['id'] + '.png" class="zoom"></a>')

                            }

                        }

                    },

                });

            }



            function cadastraPergunta() {

                var setor = $('#setor').val();

                var assunto = $('#assunto').val();

                var mensagem = $('textarea#mensagem').val();

                var idUsuario = $('#idUsuario').val();

                var idCurso = $('#idCurso').val();



                $.ajax({

                    type: 'POST',

                    url: "/views/public/includes/functions.php",

                    cache: false,

                    data: {

                        "func": "cadastra_duvida",

                        "idUsuario": idUsuario,

                        "idCurso": idCurso,

                        "setor": setor,

                        "assunto": assunto,

                        "mensagem": mensagem

                    },

                    success: function(e) {

                        if (e == 'erro' || e == null) {

                            alert('Não foi possível cadastrar sua dúvida');

                        } else {

                            alert('Dúvida cadastrada com sucesso');

                            window.location.reload();

                        }

                    },

                });

            }



            function carregaPergunta(id) {



                $.ajax({

                    type: 'POST',

                    url: "/views/public/includes/functions.php",

                    cache: false,

                    data: {

                        "func": "verifica_duvida",

                        "idDuvida": id,

                        'cpfAluno': 41743114800,

                        "idAluno": 12129                    },

                    success: function(e) {

                        $('#listaDuvidas').modal('hide')

                        $('#respostas').html(e);

                        $('#duvidaInfo').modal('show');

                    },

                });

            }

        </script>



<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title" id="exampleModalLabel">Prova</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">×</span>

        </button>

      </div>

      <div class="modal-body">



                <p>

                    O questionário sobre o curso estará disponível assim que clicar no botão começar. Não será permitido fechar esta janela após o início da prova. Assim que o tempo expirar as respostas selecionadas serão enviadas automaticamente.

                </p>



                

                <p>

                    Tentatívas Restantes: 0                </p>



                <p>

                    Duração Máxima: 30 minutos.

                </p>



                

            </div>

            <div class="modal-footer programacao">

                <button type="button" class="btn btn-secondary fechar-modal" data-dismiss="modal">Fechar</button>

            </div>

        </div>

    </div>

</div>





<!-- Modal -->

<div class="modal fade bd-example-modal-lg" id="perguntasModal" tabindex="-1" role="dialog" aria-labelledby="perguntasModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-header programacao">

                <h5 class="modal-title titulo-prova" id="perguntasModalLabel">Prova</h5>               

            </div>

            <div class="modal-body prova-interno">  

                <div class="bgbar"><div id="myBar"></div></div>



                <form id="resultadoProva" action="../resultadoProva" method="post">



</p></form></div></div></div></div></div></body></html>