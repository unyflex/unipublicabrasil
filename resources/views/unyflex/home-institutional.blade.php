@extends('unyflex.header')

@section('content')
<div class="vid">
    <div class="bg-video">
        <video style="background-size:cover; width: 2500px; ;height: 800px;margin-left:-40px; " src="https://unipublicabrasil.com.br/fundo-_3.mp4" onloadedmetadata="this.muted = true" playsinline autoplay muted loop></video>
    </div>

    <div id="inicio" class="container-fluid" style="background-color: transparent; margin-top:-1px; padding-bottom: 30px ">
        <div class="row" id="apps">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 d-none d-md-block d-lg-block p-3 text-right">
                <a style="padding: 5px" href="https://play.google.com/store/apps/details?id=unf.uny.unipublica"><img class="img-fluid" src="{{asset('dist/images/app_android.png')}}" style="height: 40px"></a>
                <a style="padding: 5px" href="https://apps.apple.com/app/id1490259777"><img class="img-fluid" src="{{asset('dist/images/app_ios.png')}}" style="height: 40px"></a>
            </div>
        </div>

        <div class="row" id='logo_c_home'>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6  p-3">
                <img class="zoom img-nav" src="https://unyflex.com.br/views/assets/images/uny_fone.png" id="img_nav">
            </div>
        </div>

        <div class="collapse" id="listar_cursos">
            <div class="row" style="padding-top: 20px">
                <div class="col-md-12 text-center">
                    <a href="#" id="f_cursos" style="color: #fff; font-weight: bold;"><i class="fal fa-times"></i></a>
                </div>
            </div>
            <div id="re_cursos" class="row" style="font-size: 25px; color:#ff6600; padding: 50px; justify-content: center"></div>
        </div>

        <div class="row" style="padding-top:50px">
            <div class="col-md-12" style="padding: 10px">
                <h3 class="zoom" id="t_bhome" style="text-align: center; color: white; font-size: 50px; line-height: 75px;text-shadow: 4px 4px 8px rgba(0, 0, 0, 0.46);">
                    <span>Assinatura mensal de cursos presenciais e online
                </h3>
            </div>
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-default" style="background-color:#ff6600; color:#fff; padding-left: 20px; padding-right: 20px;">Experimente!</a>
                <a href="/lista/cursos.xlsx" class="btn btn-default" style="background-color:black; color:#fff; padding-left: 20px; padding-right: 20px;">Baixe a Lista de Cursos</a>
            </div>
        </div>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

        <body data-spy="scroll" data-target=".navbar" data-offset="50" style="overflow-x: hidden">

    </div>

</div>

<div id="planos" class="container-fluid" style="padding-bottom: 20px">
    <div class="container-fluid">
        <div class="row pb-2" style="justify-content: center; margin-bottom:40px;">
            <h2 style="text-align: center; color:#000;padding-top: 35px; font-weight: 400"><span style="color:#ff6600">.</span>próximos cursos <span style="color:red; font-weight: bold"><img width="70" src="https://unipublicabrasil.com.br/logoyoutube.png"> </span>
                <br><span style="font-size: 14px"> By YouTube </span>
            </h2>
        </div>
        <div class="row" style="margin-bottom: 60px; justify-content: center;">
            @foreach($nextCourses as $nextCourse)
            <div class="col-md-3 text-center">
                <a href="{{url('/curso/' . $nextCourse->id_cursos)}}">
                    <img src="https://unyflex.com.br/ava/views/assets/images/box-curso/{{$nextCourse->id_cursos}}.png">
                </a>
                <p><b>Data de Início:</b> {{date('d/m/Y', strtotime($nextCourse->cur_live_inicio));}}</p>
                <p><b>Valor:</b> R$ {{$nextCourse->cur_valor}}, ou <b style="color:green">GRATUITO</b><br> para Assinantes</p>
                <a href="{{url('/curso/' . $nextCourse->id_cursos)}}" class="btn" style="font-size: 13px; background-color:#ff6600; padding-left: 50px; padding-right:50px; text-decoration: none; color:#fff">Mais informações</a>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row pb-2" style="justify-content: center; margin-top:-62px;">
        <h2 style="text-align: center; color:#000; font-weight: 400"><span style="color:#ff6600">.</span>cursos <span style="color:#ff6600; font-weight: bold;">modulares</span></h2>
    </div>
    <div class="row" style="background-color:#131926 ; padding: 30px; ">
        <div class="m-auto listaCursos">
            @foreach($searchCoursesExtesions as $searchCoursesExtesion)
            <div>
                <a href="{{url('/cursoModular/' . $searchCoursesExtesion->idCursoExtensao)}}">
                    <img src="https://unyflex.com.br/views/assets/images/cursos-extensao/{{$searchCoursesExtesion->idCursoExtensao}}.png" class="img-fluid">
                </a>
            </div>
            @endforeach
            <a href="{{url('/unyflex/modular/')}}">
                <img src="{{url('/storage/box-curso/vertodos-modulares.png')}}" class="img-fluid">
            </a>
        </div>
    </div>
    <div class="row pb-2" style="justify-content: center; margin-bottom:4px;">
        <h2 style="text-align: center; color:#000;padding-top: 35px; font-weight: 400"><span style="color:#ff6600">.</span>novos cursos <span style="color:#ff6600; font-weight: bold;">disponíveis</span></h2>
    </div>
    <div class="row" style="margin-top:4px; padding-bottom: 40px; padding: 30px;  ">
        <div class="m-auto listaCursos">
            @foreach($newCourses as $newCourse)
            <a href="{{url('/curso/' . $newCourse->id_cursos)}}" style="max-width: 300px;">
                <img src="{{url('/storage/box-curso/' . $newCourse->id_cursos)}}.png" class="img-fluid">
                <span class=" btn" style="font-size: 13px;background-color:#ff6600;margin-left: 25%;text-decoration: none;color:#fff;">
                    comece já</span>
            </a>
            @endforeach
            <a href="{{url('/setores/geral')}}">
                <img src="{{url('/storage/box-curso/vertodos.png')}}" class="img-fluid">
            </a>
        </div>
    </div>
</div>
<br>
<br>
<div class="row pb-2" style="justify-content: center; margin-top:-62px;">
    <h2 style="text-align: center; color:#000; font-weight: 400"><span style="color:#ff6600">.</span>cursos <span style="color:#ff6600; font-weight: bold;">presenciais</span></h2>
</div>
<div class="row" style="background-color:#131926 ; padding: 30px; ">
    <div class="m-auto listaCursos">
        @foreach($faceToFaceCourses as $faceToFaceCourse)
        <div>
            <a href="{{url('/presencial/' . $faceToFaceCourse->id)}}">
                <img src="https://unipublicabrasil.com.br/uploads/cursos/{{$faceToFaceCourse->bannercurso}}" class="img-fluid">
            </a>
        </div>
        @endforeach
        <div>
            <a href="{{url('/presenciais/')}}">
                <img src="{{url('/storage/box-curso/vertodos-modulares.png')}}" class="img-fluid">
            </a>
        </div>
    </div>
</div>

<div id="assinatura" class="container-fluid" style="padding-bottom: 60px; padding-top: 40px;  color:#fff;background-color:#131926">
    <div class="row">
        <div class="col-md-6">
            <h2 style="text-align: center; padding-top: 35px; font-weight: 400"><span style="color:#ff6600">.</span><span style="color:#ff6600; font-weight: bold;">assinatura</span> unyflex</h2>

            <p style="padding-left:10%; padding-right: 10%; padding-top: 40px; text-align: justify; text-indent: 50px;">Na modalidade assinatura, você aprenderá de forma continuada, síncrona (ao vivo) e assíncrona (online) com quem sabe e entende do assunto. Com mais de 200 cursos completos, mais de 1000 videoaulas, mais de 300 apostilas, nesta plataforma, você vai encontrar o que é mais relevante para seu trabalho, aprendendo com segurança, seriedade e poder.</p>
            <p style="padding-left:10%; padding-right: 10%; padding-top: 40px; text-align: justify; text-indent: 50px;">Nossos cursos trazem uma visão prática, com foco na lei, apresentada por nossos professores que também atuam em órgãos públicos.</p>
            <a href="{{url('/assinatura')}}" style="color:#fff; text-decoration: none;">
                <p style="text-align: center; padding-top: 40px;">
                    Clique para saber mais &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-chevron-right"></i>
                </p>
            </a>
        </div>
        <div class="col-md-6 text-center">
            <img src="{{url('/storage/mockups.png')}}" alt="" class="img-fluid" style="max-width: 60%">
        </div>
    </div>
</div>

<div class="row">
    <div class="container-fluid m-auto">
        <section class="container" style="margin-top:40px; margin-bottom: 40px;">
            <div class="row">
                <div class="col-md-3 text-center zoom">
                    <i class="fas fa-users" style="font-size: 50px; color:#ff6600"></i>
                    <p>Mais de 35.000 <br>alunos certificados</p>
                </div>
                <div class="col-md-3 text-center zoom">
                    <i class="fas fa-map-marked-alt" style="font-size: 50px; color:#ff6600 "></i>
                    <p>500 Munícipios <br>Aperfeiçoados</p>
                </div>
                <div class="col-md-3 text-center zoom">
                    <i class="fas fa-hand-pointer" style="font-size: 50px; color:#ff6600"></i>
                    <p>Certificação Segura <br>e Homologada</p>
                </div>
                <div class="col-md-3 text-center zoom">
                    <i class="fas fa-chalkboard-teacher" style="font-size: 50px; color:#ff6600"></i>
                    <p>Professores Mestres <br>e Especialistas</p>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="quem-somos" class="container-fluid">
    <div class="row" style="justify-content: center;">
        <div class="col-md-7 zoom ml-5">
            <p style="color:white; font-size: 90px;font-weight: bold"><span style="color: black">.</span>somos</p>
            <div class="text-center">
                <span style="color: #fff; font-size: 45px;">tecnologia</span>
                <span style="color: white; font-size: 55px; font-weight: bold;padding-left:5px">inovação</span>
            </div>
            <span style="color: #fff; font-size: 55px; float:left; line-height:50px;font-weight: bold">especialistas</span>
            <span style="color: #fff; font-size: 55px;line-height:50px; padding-left:15px;">flexíveis</span>
            <div class="text-center">
                <span style="color: white; font-size: 70px; font-weight: bold;padding-left:5px; line-height: 60px;">referência</span>
                <span style="color: #fff; font-size: 45px;">ensino</span>
                <span style="color: #fff; font-size: 50px;padding-left:5px; line-height: 25px;font-weight:bold">conhecimento</span>
            </div>
        </div>

    </div>
</div>
<div id="duvidas" class="container-fluid" style="margin-top: -5px;">
    <div class="container" style="padding: 15px; ">
        <div class="row">
            <div class="col-md-12" style="margin-top: 25px">
                <h2 style="text-align: center; color:black;margin-top: 25px;"><span style="color:#ff6600">.</span>dúvidas frequentes</h2>
                <div class="col-md-10" style="margin: 0 auto">
                    <div class="card-body">
                        <div class="accordion" id="accordionExample">
                            <div class="card" style="margin-top:0px">
                                <div class="card-header np" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#c_1" aria-expanded="true" aria-controls="c_1" style="color:#ff6600; font-size: 20px">
                                            1 - Certificados
                                        </button>
                                    </h2>
                                </div>

                                <div id="c_1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        Certificados são limitados aos usuários individuais bastando ser aprovados com nota mínima em nossas provas.
                                    </div>
                                </div>

                                <div class="card-header np" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_2" aria-expanded="false" aria-controls="c_2" style="color:#ff6600; font-size: 20px">
                                            2 - Tempo de Vídeo e Carga Horária
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        O tempo de vídeo é divulgado conforme o tempo de gravação. Já a carga horária inclui o estudo de materiais e realização de provas, por isso é maior.
                                    </div>

                                </div>

                                <div class="card-header np" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_3" aria-expanded="false" aria-controls="c_3" style="color:#ff6600; font-size: 20px">
                                            3 - Dúvidas na plataforma
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_3" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        O aluno pode tirar dúvidas sobre as matérias atráves do botão "Falar com o Professor"
                                        <br><small class="text-muted"> * Conforme seu plano de assinatura</small>
                                    </div>
                                </div>

                                <div class="card-header np" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_4" aria-expanded="false" aria-controls="c_4" style="color:#ff6600; font-size: 20px">
                                            4 - Dúvidas durante a Live
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_4" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        Durante as aulas ao vivo você pode solucionar dúvidas simultaneamente a aula.
                                    </div>
                                </div>
                                <div class="card-header np" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_5" aria-expanded="false" aria-controls="c_5" style="color:#ff6600; font-size: 20px">
                                            5 - Contratação - Entidades Públicas
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_5" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        O processo de contratação é feito através do contrato de inexigibilidade com exclusividade do termo e atestado de capacidade de outras entidades.
                                    </div>
                                </div>
                                <div class="card-header np" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_6" aria-expanded="false" aria-controls="c_6" style="color:#ff6600; font-size: 20px">
                                            6 - Contratação - Plano Corporativo
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_6" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        No plano corporativo além do número de usuários, o contrato contempla 1 (um) acesso de gestão master (administrador), podendo este verificar relátorios de acessos, relátórios de provas, tempo que o usuário esteve logado na plataforma, bem como realizar alterações em cadastros dos alunos.
                                    </div>
                                </div>

                                <div class="card-header np" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_7" aria-expanded="false" aria-controls="c_7" style="color:#ff6600; font-size: 20px">
                                            7 - Contratação - Pessoa Física
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_7" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        O contrato é feito no próprio CPF, sendo intransferível até o prazo estabelecido no plano.
                                    </div>
                                </div>

                                <div class="card-header np" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c_8" aria-expanded="false" aria-controls="c_8" style="color:#ff6600; font-size: 20px">
                                            8 - Dos Valores e Pagamentos
                                        </button>
                                    </h2>
                                </div>
                                <div id="c_8" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        Nos planos corporativos o tempo de vigência do contrato é de no mínimo 12 meses recebendo o contratante o benefício de documentos e exclusividade nos atendimentos.
                                        <br><br>
                                        Nos pagamentos por <b>boleto bancário</b> o acesso só é liberado após a compensação bancária.
                                        <br><br>
                                        Para pagamentos em <b>cartão de crédito</b>, o próprio sistema realiza a liberação após a confirmação do pagamento.
                                        <br><br>
                                        <small class="text-muted"> * Lembre-se: quanto maior o plano, maior o desconto.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop