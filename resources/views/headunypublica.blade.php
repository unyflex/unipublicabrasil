<!-- SEPARAR HEAD-->
<!DOCTYPE html>
<html lang="PT-BR">

<head>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MW4GW4P');
    </script>
    <!-- End Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unypublica Unyflex - Escola de Gestão Pública</title>
    <link rel="shortcut icon" href="https://www.unipublicabrasil.com.br/assets/img/favicon_rosa.png" type="image/x-icon">
    <meta name="theme-color" content="#ffffff" />

    <meta name="description" content="Unipublica - Unyflex" />
    <meta name="author" content="Luiz Felipe Gonçalves Sanches e Agência WDK" />

    <meta name="twitter:card" content="Unyflex" />
    <meta name="twitter:site" content="https://unipublicabrasil.com.br/" />
    <meta name="twitter:image" content="https://unipublicabrasil.com.br/assets/img/favicon_rosa.png" />

    <meta property="og:image" content="https://unipublicabrasil.com.br/assets/img/favicon_rosa.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="500" />
    <meta property="og:image" content="https://unipublicabrasil.com.br/assets/img/favicon_rosa.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="500" />


    
<!--  Font Awesome    -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" />

<!-- Owl Stylesheets 
  <link rel="stylesheet" href="./assets/vendor/css/docs.theme.min.css">-->
<link rel="stylesheet" href="https://unipublicabrasil.com.br/assets/vendor/css/owl.carousel.min.css">
<link rel="stylesheet" href="https://unipublicabrasil.com.br/assets/vendor/css/owl.theme.default.min.css">

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src='https://unipublicabrasil.com.br/assets/vendor/bootstrap/js/bootstrap.min.js'></script>
<script src="https://unipublicabrasil.com.br/assets/vendor/js/owl.carousel.js"></script>

<!-- Bootstrap CSS -->
<link href="https://unipublicabrasil.com.br/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Effect Trasition -->
<link href="https://unipublicabrasil.com.br/assets/vendor/css/aos.css" rel="stylesheet">

<!-- Main CSS -->
<link href="https://unipublicabrasil.com.br/assets/css/style.css" rel="stylesheet">
 <link rel="stylesheet" href="https://unipublicabrasil.com.br/assets/css/pag.css">

 <!-- METATAGS -->
<meta name="description" content="MARCA REGISTRADA Escola de Gestão Pública reconhecida pelo MEC, especializada em treinamentos p/ servidores públicos. Mais de 8 anos com atuação séria e reputação ilibada.">
<meta name="keywords" content="escola de gestao publica, qualificação publica, cursos a distancia, servidores de camaras, advogados municipais, redacao oficial, licitação, controle interno, politicas publicas municipais, patrimonio publico ">
<!-- FAVICONS -->
<link rel="icon" href="img/favicon-32.png" sizes="32x32">
<!-- CÓDIGO ANALYTICS -->
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-60468884-1', 'auto');
    ga('send', 'pageview');
</script>

<!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
    window.__insp = window.__insp || [];
    __insp.push(['wid', 1769910607]);
    (function() {
        function ldinsp() {
            if (typeof window.__inspld != "undefined") return;
            window.__inspld = 1;
            var insp = document.createElement('script');
            insp.type = 'text/javascript';
            insp.async = true;
            insp.id = "inspsync";
            insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(insp, x);
        };
        setTimeout(ldinsp, 500);
        document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
    })();
</script>
<!-- End Inspectlet Embed Code -->

<!-- CSS do Bootstrap -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<!-- Família de Fonte Open Sans -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- Família de Ícones Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- CSS Personalizado -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Facebook Developers -->
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId: '1083332535013268',
            xfbml: true,
            version: 'v2.5'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
        document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1849643161991286'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1849643161991286&ev=PageView&noscript=1"
  /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Google Tag Manager -->
<script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MW4GW4P');
</script>
<!-- End Google Tag Manager -->

</head>

<body>