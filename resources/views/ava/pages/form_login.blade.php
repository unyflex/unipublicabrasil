<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title><?php echo $_configurate['ava_titulo']; ?></title>

        <link href='//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
        <link href='//use.fontawesome.com/releases/v5.7.2/css/all.css' rel='stylesheet'>
        <link href='<?php echo $_configurate['ava_css'] ?>/style.css' rel='stylesheet'>

        <script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>                               

    </head>

<body oncontextmenu='return false' class='snippet-body'>
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-lg-5 col-md-7 offset-lg-4 offset-md-3">
                <div class="panel border bg-white">
                    <div class="panel-heading">
                        <img src="<?php echo $_configurate['ava_image']; ?>/logo-unyflex.png">
                    </div>
                    <div class="panel-body p-3">
                         <form method="POST" action="{{ route('login') }}">
                        @csrf
                            <div class="form-group py-2">
                                <div class="input-field"> 
                                    <span class="far fa-user p-2"></span> 
                                    <input  v-model="name" id="email" type="text" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus> 
                                </div>
                            </div>
                            <div class="form-group py-1 pb-2">
                                <div class="input-field"> 
                                    <span class="fas fa-lock px-2"></span> 
                                    <input id="password" type="password" v-model="password"  class="inputs" name="password" required autocomplete="current-password"> 
                                    <button type="button" class="btn bg-white text-muted" id="eye"><span class="far fa-eye-slash"></span></button> 
                                </div>
                            </div>
                            <div class="form-inline"> 
                                <input type="checkbox" name="remember" id="remember"> 
                                <label for="remember" class="text-muted">Lembrar-me do acesso</label> 
                                <a href="#" id="forgot" class="font-weight-bold">Recuperar senha</a> 
                            </div>
                              
                            <div class="form-inline"> 
                                <button class="btn btn-primary btn-block mt-3" type="submit">Acessar Plataforma</button>
                            </div>
                            <div class="text-center pt-4 text-muted">Ainda não é nosso aluno? <a href="#">Fale com a gente!</a> </div>
                        </form>
                    </div>
                    <div class="mx-3 my-2 py-2 bordert">
                        <div class="text-center py-3">
                            <a href="https://wwww.facebook.com" target="_blank" class="px-2">
                                <img src="https://www.dpreview.com/files/p/articles/4698742202/facebook.jpeg" alt="" class="img-redes"> 
                            </a> 
                            <a href="https://www.google.com" target="_blank" class="px-2"> 
                                <img src="https://www.freepnglogos.com/uploads/google-logo-png/google-logo-png-suite-everything-you-need-know-about-google-newest-0.png" alt="" class="img-redes"> 
                            </a> 
                            <a href="https://www.github.com" target="_blank" class="px-2"> 
                                <img src="https://www.freepnglogos.com/uploads/512x512-logo-png/512x512-logo-github-icon-35.png" alt="" class="img-redes"> 
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type='text/javascript' src='//stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src='<?php echo $_configurate['ava_js']; ?>/jquery.mask.js'></script>
    <script type='text/javascript' src='<?php echo $_configurate['ava_js']; ?>/ava.js'></script>
    
    </body>
</html>