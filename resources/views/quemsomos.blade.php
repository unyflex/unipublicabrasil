@include('headunypublica')
@include('headerunypublica')



 <!--	Font Awesome	-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" />
    
    <!-- Owl Stylesheets 
        <link rel="stylesheet" href="./assets/vendor/css/docs.theme.min.css">-->
        
    <link rel="stylesheet" href="./assets/vendor/css/owl.carousel.min.css">
    <link rel="stylesheet" href="./assets/vendor/css/owl.theme.default.min.css">
    
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
    <script src='./assets/vendor/bootstrap/js/bootstrap.min.js'></script>
    <script src="./assets/vendor/js/owl.carousel.js"></script>
    
    <!-- Bootstrap CSS -->
    <link href="./assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Effect Trasition -->
    <link href="./assets/vendor/css/aos.css" rel="stylesheet">
    
    <!-- Main CSS -->
    <link href="./assets/css/style.css" rel="stylesheet">
    <link href="./assets/css/pag.css" rel="stylesheet">
</head>
<body>
    <section id="titleLine">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Conheça a UnyFlex, <br><font color='#FF6C00 '>e o porque somos líder no segmento</font></h1>
                </div>
            </div>
        </div>
    </section>
    <section id="quemSomos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-justify" style="padding: 25px;">
                    <p>A UnyFlex é uma <b>Escola de Gestão Pública</b> localizada no Sul do País, em grande expansão e com novos polos. A cada momento que se passa, pensamos mais no bem-estar de nossos alunos, formando-os com qualidade e excelência. Todos nossos professores são extremamente capacitados na área de gestão pública, o que nos torna líder no segmento. </p>
                    <p>Preparamos os agentes governamentais para a melhora dos serviços públicos, a eliminação de falhas, irregularidades, prejuízos e responsabilizações. Somos instrumento para promoção da eficiência e moralização nos órgãos estatais e contribuímos para a construção do Brasil que queremos!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <ul class="list-group">
                        <li class="list-group-item " style="background-color: #FF6C00 ;" style=" color:black;font-weight:bold"> <font color="#fff"><b> Atuamos com tecnologias de ensino mais recentes</font> </b> </li>
                        <li class="list-group-item">Cursos Rápidos Presenciais (na sede e nos polos)</li>
                        <li class="list-group-item">Cursos Rápidos Online (2h a 14h – ao vivo e videoaulas)</li>
                        <li class="list-group-item">Pós-Graduação (especialização com registro no MEC – 450h – 100% à distância)</li>
                        <li class="list-group-item">Cursos In Company (realizados no local escolhido pelo contratante)</li>
                        <li class="list-group-item">Cursos de Formação Modular (72 horas/aula – 12 módulos – com tutoria orientativa)</li>
                        <li class="list-group-item">UNYFLEX (videoteca composta por mais de 1.200 cursos)</li>
                        <li class="list-group-item">Cursos On Demand (com conteúdo indicado pelo contratante)</li>
                        <li class="list-group-item">Graduação 100% Ead (em construção)</li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="embed-responsive embed-responsive-16by9">
                        
                        <iframe width="420" height="236" src="https://www.youtube.com/embed/Og7Kp4dwVHg" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="text-justify" style="margin-top: 25px">
                        <h3>Nos dedicamos à formação contínua de Servidores Públicos.</h3>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:25px">
                <div class="col-lg-12">
                    <div class="card-deck">
                        <div class="card">
                            <img class="card-img-top" src="/images/quem-somos-curso.png" alt="Cursos UnyFlex">
                            <div class="card-body">
                                <h5 class="card-title">Cursos UnyFlex</h5>
                                <p class="card-text text-justify">Com diversos formatos de cursos Premium, Master, Mão na Massa, Clássicos, Distância (e-learning/online) e à Pós-Graduação, a escola investe na qualidade e seriedade, certificando seus alunos e garantindo. Dispomos dos formatos: <b>Premium, Master, Mão na Massa e Clássicos.</b></p>
                                <p>Em todos os formatos, os docentes são especializados e com vivência na área.</p>
                            </div>
                        </div>
                        <div class="card">
                            <img class="card-img-top" src="/images/quem-somos-curso-2.png" alt="Nosso Público Alvo">
                            <div class="card-body">
                                <h5 class="card-title">Nosso Público Alvo</h5>
                                <p class="card-text text-justify">Servidores e Equivalentes, Autoridades Públicas, Membros da Sociedade Civil Organizada, e particulares que pretendam aprender sobre esse segmento.</p>
                            </div>
                        </div>
                        <div class="card">
                            <img class="card-img-top" src="/images/quem-somos-curso-3.png" alt="Feedback">
                            <div class="card-body">
                                <h5 class="card-title">Feedback</h5>
                                <p class="card-text text-justify">Todos os cursos passam por uma avaliação criteriosa pelos próprios alunos, graças ao respeito e responsabilidade empregada neste trabalho. No anos de 2014, 2015, 2016, 2017, 2018, 2019 e 2020 por exemplo, o índice de satisfação de nossos alunos teve média anual de 93, 92.2, 93.8, 93.4, 93.3, 93.5 e 93.99 respectivamente.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top:25px">
                <div class="col-lg-12">
                    <ul class="list-group">
                           
                    <li class="list-group-item " style="background-color: #FF6C00 ;" style="color:black;font-weight:bold; font-color#fff;"><font color="#fff"> <b>Transparência</li></b>  </font>
                        <li class="list-group-item">Embora não possuindo natureza jurídica pública, a Escola aplica o princípio da transparência de seus atos, mantendo em sua página eletrônica um espaço específico para esse fim, onde disponibiliza além de fotos, depoimentos e notas de avaliação dos alunos, todas as certidões de caráter fiscal, técnica e jurídica.</li>
                        
                        <li class="list-group-item " style="background-color: #FF6C00 ;" style="color:black;font-weight:bold; font-color#fff;"><font color="#fff"><b>Missão</li></b>  </font>
                        <li class="list-group-item">Preparar os servidores municipais, repassando-lhes informações e ensinamentos gerais e específicos de suas respectivas áreas de atuação, e contribuir com: a) promoção da eficiência e eficácia dos serviços públicos.b) o combate às irregularidades técnicas, evitando prejuízos e responsabilizações, tanto para a população quanto para os agentes públicos.</li>
                        
                        <li class="list-group-item " style="background-color: #FF6C00 ;" style="color:black;font-weight:bold; font-color#fff;"><font color="#fff"><b>Visão</li></b>  </font>
                        <li class="list-group-item">Ser a marca de referência no segmento (Top of Mind), reconhecida por sua competência e seriedade, pela motivação de seus colaboradores, e pela satisfação de seus alunos.</li>
                        
                        <li class="list-group-item " style="background-color: #FF6C00 ;" style="color:black;font-weight:bold; font-color#fff;"><font color="#fff"><b>Valores</li></b>  </font>
                        <li class="list-group-item">a) Atuação idônea; b) Ética profissional; c) Ensino de qualidade; d) Respeito nos relacionamentos; e) Reconhecimento do apoio recebido</li>
                        
                        <li class="list-group-item " style="background-color: #FF6C00 ;" style="color:black;font-weight:bold; font-color#fff;"><font color="#fff"><b>   O que já realizamos</li></b> </font>
                        <li class="list-group-item">a) 11 anos de trabalhos; b) 1600 cursos; c) 36500 alunos; d) 150 docentes; d) Nenhum impedimento legal; e) Grandes amizades</li>

                    </ul>
                </div>           
            </div> 
            
            <div class="text-justify" style="padding: 25px">
                    <h3>Sistema de Leitura Biométrica</h3>
                    <hr>
                    <p>A Unipública foi a primeira escola de capacitação de agentes públicos municipais do país a implantar o sistema biométrico de presença (pela impressão digital), no ano de 2009, para substituir a lista de assinaturas coletadas manualmente. Utilizando-se da tecnologia avançada, promovendo eficiência, seriedade e segurança aos seus alunos.</p>

                     <p>Já que os seus serviços são pagos com verbas públicas, nada mais correto do que proteger o erário, exigindo que os alunos registrem as respectivas presenças (entrada e saída) em um sistema rigoroso de controle. É que, em caso de frequência menor que o exigido (75%), o sistema não libera ao aluno o certificado de conclusão do curso.</p>
                    
                     <p>Todavia, esse instrumento de controle é benéfico, pois o aluno sabe que está protegido de questionamentos e responsabilizações indevidas, assim o marcador eletrônico lhe garante comprovar que de fato esteve presente em cada módulo ou painel.</p>
            
            <h3>Depoimentos</h3>
            <hr>
            
            </div>
            
            
            
        </div>
    </section>



    <section id="testemunhos">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="carouselTestemunho" class="carousel slide" data-ride="carousel" data-interval="false">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselTestemunho" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselTestemunho" data-slide-to="1"></li>
                        <li data-target="#carouselTestemunho" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                     
                        @foreach ($depositions as $deposition)
                            
                      
                            <div class="carousel-item  <?php if ($loop->index == 0) { ?> active <?php } ?>">
                                <div class="box-testemunho">
                                    <div class="row">
                                        <div class="col-lg-4 offset-lg-1 text-center">
                                            <img src="https://unipublicabrasil.com.br/dev-paulo/storage/app/depoimentos/{{$deposition->foto}}" class="rounded-circle img-fluid mt-5">
                                        </div>
                                        <div class="col-lg-6">
                                            <h6><strong>“</strong> <?= $deposition->depoimento ?> ”</h6>
                                            <p class="mt-5 mb-0"><strong>{{$deposition->cargo}}</strong></p>
                                            <h5>{{$deposition->nome}}</h5>
                                            <p>{{$deposition->cidade}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselTestemunho" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselTestemunho" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

 <script>
        $(document).ready(function() {
            $(".owl-carousel").owlCarousel({
                loop: true,
                margin: 10,
                dots: false,
                nav: true,
                navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
                responsive: {
                    0: {
                        items: 1,
                        margin: 0,
                        loop: false
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        });
    </script>

    @include('footerunypublica')