@include('headunypublica')

@include('headerunypublica')







<!-- FAZER PROGRAMAÇÃO PARA EXIBIR BANNER -->



      <section id="">

  

            <div class="container">

                <div class="row">

                    <div class="col-lg-12">

                        <div id="carouselHome" class="carousel slide" data-ride="carousel" data-interval="false">

                            <ol class="carousel-indicators">



                                @for ($a = 0 ; $a <=$banner_count ; $a++)

                                    

                              

                                    <li data-target="#carouselHome" data-slide-to="{{$a}}" class=""></li>

                               @endfor

                            </ol>

                            <div class="carousel-inner">

                              

                                @foreach ($banners as $banner)

                                    

                               

                                    <div class="carousel-item <?php if ($loop->index == 0) { ?> active <?php } ?>">

                                        <a href="{{$banner->link}}">

                                            <img class="d-block w-100" src="{{url("storage/banners/home/$banner->imagem");}}">

                                            <div class="carousel-caption">

                                                <h1 style="color: white;"><strong></strong> </h1>

                                                <!--<p style="color: white;">Lorem ipsum dolor sit amet, consectetur adipiscing<br> elit, sed do eiusmod tempor incid.</p>-->

                                                <!-- <a href="https://unyflex.com.br/assinatura" class="btn-unyflex-solid"><i class="fas fa-check"></i> Assine Agora!</a> -->

                                            </div>

                                    </div>

                                    </a>

                                 @endforeach

                                <a class="carousel-control-prev" href="#carouselHome" role="button" data-slide="prev">

                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                                    <span class="sr-only">Previous</span>

                                </a>

                                <a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">

                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>

                                    <span class="sr-only">Next</span>

                                </a>

                            </div>

                     

                        </div>

                    </div>

                </div>

    </section>





<!-- CATEGORIAS -->

<section id="listagem">

        <div class="container">

            <div class="row mt-3">



                



                        <!-- PARTE GERAL -->

                        

                        



                                                            <div class="w-100 pl-3 pt-3">

                                        <h3><strong>Cursos Já Realizados</strong></h3>

                                        <hr>

                                    </div>

                                        @foreach ($realizados as $realizado)

                                            

                                        

                                     <div class="col-lg-4 px-0">

                                        <div class="box-cursos">

                                            <div class="box-content-top">

                                                <a href="https://unipublicabrasil.com.br/curso.php?curso=portal-ouvidoria-e-lgpd-estudo-pratico&amp;id=2527">

                                                    <img src="{{url("storage/cursos/banner/$realizado->photo")}}" class="img-fluid" alt="">

                                                </a>

                                                <p><i class="fas fa-map-marker-alt"></i>Curitiba - PR </p>

                                                <div class="box-data">

                                                    <p><strong>{{$realizado->workload}} </strong>Horas</p>

                                                </div>

                                            </div>

                                            

                                    <p class="tag-data"><i class="far fa-calendar"></i>{{date('d/m/Y', strtotime($realizado->start_date))}}</p>

                                            <hr>

                                            <ul>

                                            @foreach ($realizado->panels as $painel)

                                             <li>{{$painel->title}}</li>

                                             @endforeach

                                             </ul>



                                            <a href="https://unipublicabrasil.com.br/curso.php?curso=portal-ouvidoria-e-lgpd-estudo-pratico&amp;id=2527" class="btn-unyflex-light">Ver Programação Completa</a>

                                        </div>

                                    </div>

                                @endforeach

        

    </section>



    

    <section id="testemunhos">

    <div class="container">

        <div class="row">

            <div class="col-lg-12">

                <div id="carouselTestemunho" class="carousel slide" data-ride="carousel" data-interval="false">

                    <ol class="carousel-indicators">

                        <li data-target="#carouselTestemunho" data-slide-to="0" class="active"></li>

                        <li data-target="#carouselTestemunho" data-slide-to="1"></li>

                        <li data-target="#carouselTestemunho" data-slide-to="2"></li>

                    </ol>



                    <div class="carousel-inner">

                     

                        @foreach ($depositions as $deposition)

                            

                      

                            <div class="carousel-item  <?php if ($loop->index == 0) { ?> active <?php } ?>">

                                <div class="box-testemunho">

                                    <div class="row">

                                        <div class="col-lg-4 offset-lg-1 text-center">

                                           <img src="{{url("storage/depoimentos/$deposition->foto");}}" class="rounded-circle img-fluid mt-5">

                                        </div>

                                        <div class="col-lg-6">

                                            <h6><strong>“</strong> <?= $deposition->depoimento ?> ”</h6>

                                            <p class="mt-5 mb-0"><strong>{{$deposition->cargo}}</strong></p>

                                            <h5>{{$deposition->nome}}</h5>

                                            <p>{{$deposition->cidade}}</p>

                                        </div>

                                    </div>

                                </div>

                            </div>

                          @endforeach

                    </div>

                    <a class="carousel-control-prev" href="#carouselTestemunho" role="button" data-slide="prev">

                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                        <span class="sr-only">Previous</span>

                    </a>

                    <a class="carousel-control-next" href="#carouselTestemunho" role="button" data-slide="next">

                        <span class="carousel-control-next-icon" aria-hidden="true"></span>

                        <span class="sr-only">Next</span>

                    </a>

                </div>

            </div>

        </div>

    </div>

</section>